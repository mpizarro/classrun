let color;
$(() => {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
    });

    color = $('#color').val();

    if (tipo_id == '3' || tipo_id == '4') addCheckedToInput();
    else addCheckedToInputEje();

    $('.body-panel').each(function(i, e) {
        const height = $(e).children('div').height();
        if (height > 170) $(e).parent('.container-panel').find('[see-more]').show();
    });

    $(document).on('click', '[see-more]', function() {
        const heightDiv = $(this).parents('.container-panel').children('.body-panel').children('div').height();
        const heightBody = $(this).parents('.container-panel').children('.body-panel').height();
        const heightContainer = $(this).parents('.container-panel').height();

        if ($(this).attr('see-more') == 'more') {
            $(this).parents('.container-panel').children('.body-panel').animate({'height': `${ heightDiv }px`});
            $(this).parents('.container-panel').animate({'height': `${ heightContainer + (heightDiv - heightBody) }px`});
            $(this).attr('see-more', 'less');
            $(this).html('Ver menos ...');
        } else {
            $(this).parents('.container-panel').children('.body-panel').animate({'height': '170px'});
            $(this).parents('.container-panel').animate({'height': '285px'});
            $(this).attr('see-more', 'more');
            $(this).html('Ver más ...');
        }
    });

    $(document).on('change', '[check]', function() {
        const oa = $(this).attr('oa');

        if ($(this).prop('checked')) {
            addCheckedToInput();
        } else {
            $(this).parents('.head-panel').css({'background': '#d7d7d7'});
            $(`label[oa='${ oa }']`).remove();
        }
    });

    $(document).on('change', '[check-eje]', function() {
        const eje = $(this).attr('eje');
        console.log(eje);
        

        if ($(this).prop('checked')) {
            addCheckedToInputEje();
        } else {
            $(this).parents('.head-panel').css({'background': '#d7d7d7'});
            $(`label[oa='${ eje }']`).remove();
        }
    });

    $(document).on('click', '[btn-continuar]', function() {
        if ($('input[type="checkbox"]:checked').length > 0) $('#form-oas').submit();
        else Toast.fire({
                type: 'warning',
                title: `Debes seleccionar al menos un ${ tipo_id == '3' || tipo_id == '4' ? 'OA' : 'Eje' }`
            });
    });
});

// Metodos
const addCheckedToInput = () => {
    $('[oas-selected]').html('');

    $('input[type="checkbox"]').each((i, e) => {

        if ($(e).prop('checked')) {
            $(e).parents('.head-panel').css({'background': `${ color }`});
            $(e).attr('checked', 'checked');
            const oa = $(e).attr('oa');
    
            $('[oas-selected]').append(`<label class="name-oa col-xs-3 col-sm-3 col-md-3 col-lg-3" oa="${ oa }">${ oa }</label>`);
        }

    });
}

const addCheckedToInputEje = () => {
    let html = '';
    $('input[type="checkbox"]').each((i, e) => {
        
        if ($(e).prop('checked')) {
            $(e).parents('.head-panel').css({'background': `${ color }`});
            $(e).attr('checked', 'checked');
            const eje = $(e).attr('eje');
            
            html += `<label class="name-oa col-xs-12 " oa="${ eje }"><i class='fa fa-circle m-r-5' style='color: ${ color }'></i> ${ eje }</label>`;
        }
    });
    $('[oas-selected]').html(html);
}