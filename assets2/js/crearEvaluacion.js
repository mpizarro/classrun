let idEvaluacion, idCurso;
var nivel_id, asignatura_id, tipo_id, prueba_id, left=0, tipo='', asignatura='', nivel='';


$(document).on('input', '#inputNuevaEv', function() 
{
    if($(this).val() != '')
        $('#btnNuevaEv').removeAttr('disabled');    
    else
        $('#btnNuevaEv').attr('disabled','disabled');
        
});

$(document).on('click', '.delete-button', function() 
{
    let prueba = $(this).attr("id");
    Swal.fire({
        title: '¿Desea Eliminar la Evaluación?',
        text: "No se podrán revertir los cambios",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!'
      }).then((result) => {
        if (result.value) 
        {
            $.post(siteUrl+'/Home/deletePrueba' ,{prueba:prueba})
            .done(function(json){
                json = JSON.parse(json);
                
                if (json.success == 'success') 
                {
                     
                    Swal.fire(
                        'Eliminada!',
                        'Tu evaluación ha sido eliminada.',
                        'success'
                    )
                    $("#containerPrueba"+prueba).css("display", "none");                      
                }
                
            });
           
        }
      })
});

$(document).on('click', '.delete-button-asignacion', function() 
{
    let asignacion = $(this).attr("id");
    Swal.fire({
        title: '¿Desea Eliminar la asignación?',
        text: "Se eliminarán las hojas de respuestas de sus alumnos relacionadas con esta prueba. No se podrán revertir los cambios",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!'
      }).then((result) => {
        if (result.value) 
        {
            $.post(siteUrl+'/Asignaciones/deleteAsignacion' ,{asignacion:asignacion})
            .done(function(json){
                json = JSON.parse(json);
                
                if (json.success == 'success') 
                {
                     
                    Swal.fire(
                        'Eliminada!',
                        'Tu asignación ha sido eliminada.',
                        'success'
                    )
                    $(".asignacionHeader"+asignacion).css("display", "none");
                    $(".asignacionBody"+asignacion).css("display", "none");                      
                }
                
            });
           
        }
      })
});

$(document).on('click', '#btnNuevaEv', function() {
    console.log("asdjaosd");

});
$( "#editarEstandarizada" ).focus();
$('#editarEstandarizada').keypress(function(key) 
{
    console.log(key.charCode);
    if(key.charCode == 45 || key.charCode == 47 || key.charCode == 92) 
        return false;
});

$(document).on('click', '[btn-ver-evaluacion]', function() {
    
    let idEvaluacion = $(this).attr('id-evaluacion');
    
    if($(this).attr('origen') == 'classrun')
        $("#data-prueba").html('<object style="width: 100%;height: 100% !important;" data="'+`${ siteUrl}/administrador/verEvaluacion/${ idEvaluacion }/CLASSRUN`+'"/>');    
    else
        $("#data-prueba").html('<object style="width: 100%;height: 100% !important;" data="'+`${ siteUrl}/administrador/verEvaluacion/${ idEvaluacion }/PERSONAL/`+'"/>');    
       
    $('#para-descarga').attr('id-evaluacion', idEvaluacion);
    $('#para-descarga').attr('origen', $(this).attr('origen'));

    $('#modal-evaluacion').modal();
});

$(document).on('click', '[btn-download-evaluacion]', function() {
    const idEvaluacion = $(this).attr('id-evaluacion');
    const origen = $(this).attr('origen');
    
    window.location.href = `${ siteUrl }/administrador/verEvaluacionImprime/${ idEvaluacion }/${ origen }`;
});

    $(document).on('keyup', '.input-alt', function(event) {
        let code = event.keyCode;
        let text = code != 8 ? String.fromCharCode(code) : null;
        let idAlumno = $(this).attr('id-alumno');
        let idPregunta = $(this).attr('id-pregunta');
        const leters = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','Ñ','O','P','Q','R','S','T','U','V','W','X','Y','Z',null];

        if (code == 37) $(this).parent('td').prev().find('.input-alt').focus();
        else if (code == 39) $(this).parent('td').next().find('.input-alt').focus();
        else if (code == 38) $(this).parent('td').parent('tr').prev().find(`td > input[id-pregunta="${idPregunta}"]`).focus();
        else if (code == 40) $(this).parent('td').parent('tr').next().find(`td > input[id-pregunta="${idPregunta}"]`).focus();

        if (leters.indexOf(text) != -1) {
            let data = {
                'id_alumno': idAlumno,
                'id_pregunta': idPregunta,
                'id_asignacion': $(this).attr('id-asignacion'),
                'respuesta': text,
                'origen': $(this).attr('origen')
            }
            $.post(`${ siteUrl}/Asignaciones/setAlternativa`, data)
            .done(json => {
                json = JSON.parse(json);
                if (!json.response) {
                    let letraIncorrecta = $(document).find(`#${ idAlumno }-${ idPregunta }`).val();
                    $(document).find(`#${ idAlumno }-${ idPregunta }`).val(null)
                    swal({type:"error", title:`"${ letraIncorrecta.toUpperCase() }", no se admite como alternativa dentro de esta pregunta.`});
                } else {
                    $(document).find(`#${ idAlumno }-${ idPregunta }`).removeClass('correcta incorrecta');
                    $(document).find(`#${ idAlumno }-${ idPregunta }`).addClass(`${json.class}`);
                }
            });
            $(this).parent('td').next().children('.input-alt').focus();
        }
    });

var finalizarAsignacion = (idAsignacion) => {
    $.ajax (
    {
        url : `${siteUrl}/asignaciones/finalizarAsignacion/${idAsignacion}`,
        dataType: 'json',
        success : (json) => {
            if(json.status) {
                if( $("#btns-"+idAsignacion).attr("estado") == "1" )
                    {
                        $("#btns-"+idAsignacion).attr("estado", "0");
                        $("#btns-"+idAsignacion).attr("onclick","ejecutarAsignacion("+idAsignacion+","+$("#btns-"+idAsignacion).attr("curso_id")+")");
                    }
    
                $("#btn-"+idAsignacion).attr("disabled", true);
                $("#btns-"+idAsignacion).html('<i class="mdi mdi-play" style="font-size:25px" ></i>');
                $("#btns-"+idAsignacion).attr("disabled", false);
                $("#btns-"+idAsignacion).css("background", "#46da46");
                $("#btn-informe-"+idAsignacion).css("display", "block");

            } else {
            }
        }
    });
};
var ejecutarAsignacion = (idAsignacion, idCurso) => 
{
    $.post(`${ siteUrl }/curso/cursoHasAsignacion/${ idCurso }`)
    .done(function(json) {
        json = JSON.parse(json);
        
        $.ajax ({
            url : `${siteUrl}/asignaciones/ejecutarAsignacion/${idAsignacion}`,
            dataType: 'json',
            success : (json)=> {                
                if(json.status) {                    
                    
                    if( $("#btns-"+idAsignacion).attr("estado") == "0" )
                    {
                        $("#btns-"+idAsignacion).attr("estado", "1");
                        $("#btns-"+idAsignacion).attr("onclick","finalizarAsignacion("+idAsignacion+")");
                    }


                    $("#btns-"+idAsignacion).html('<i class="mdi mdi-pause" style="font-size:25px" ></i>');
                    //$("#btns-"+idAsignacion).attr("disabled", true);
                    $("#btns-"+idAsignacion).css("background", "#f1df49");
                    $("#btn-"+idAsignacion).attr("disabled", false);
                    
                } else {
                    
                }
            }
        });

    });
};

$('#left').on('click', function() 
{
    

    switch(left) 
    {
        case 1: $('#folder-1').css("display", "block");
                $('#folder-2').css("display", "none");
                $('#ruta').html('<i class="mdi mdi-airplay"></i>Evaluaciones/');
                left=0;
        break;
        case 2: $('#folder-2').css("display", "block");
                $('#folder-3').css("display", "none");
                $('#ruta').html('<i class="mdi mdi-airplay"></i>Evaluaciones/'+tipo);
                left=1
        break;
        case 3: $('#folder-3').css("display", "block");
                $('#folder-4').css("display", "none");
                $('#ruta').html('<i class="mdi mdi-airplay"></i>Evaluaciones/'+tipo+'/'+asignatura);
                left=2;
        break;
    }
});


$('.folder-1').on('click', function() {

    left = 1;
    tipo_id = $(this).attr("tipo");
            
    switch(tipo_id) 
    {
        case "1": tipo = 'Simce';
        break;
        case "4": tipo = 'Cobertura';
        break;
        case "3": tipo = 'Test Procesos';
        break;
    }

    $('#folder-1').css("display", "none");
    $('#folder-2').css("display", "block");
    $('#ruta').html('<i class="mdi mdi-airplay"></i>Evaluaciones/'+tipo+'/');
    
});

$('.folder-2').on('click', function() {
    left = 2;
    asignatura_id = $(this).attr("asignatura");

    switch(asignatura_id) 
    {
        case "1": asignatura = 'Lenguaje';
        break;
        case "3": asignatura = 'Matemática';
        break;
        case "4": asignatura = 'Historia';
        break;
        case "5": asignatura = 'Ciencias';
        break;
    }

    $('#folder-2').css("display", "none");
    $('#folder-3').css("display", "block");
    $('#ruta').html('<i class="mdi mdi-airplay"></i>Evaluaciones/'+tipo+'/'+asignatura+'/');
});

$('.folder-3').on('click', function() 
{
    $('#preloader').css("display", "block");
    $('#status').css("display", "block");

    left = 3;
    nivel_id = $(this).attr("nivel");
    
    switch(nivel_id) 
    {
        case "1": nivel = '1ro Básico';
        break;
        case "2": nivel = '2do Básico';
        break;
        case "3": nivel = '3ro Básico';
        break;
        case "4": nivel = '4to Básico';
        break;
        case "5": nivel = '5to Básico';
        break;
        case "6": nivel = '6to Básico';
        break;
        case "7": nivel = '7mo Básico';
        break;
        case "8": nivel = '8vo Básico';
        break;
        case "9": nivel = '1ro Medio';
        break;
        case "10": nivel = '2do Medio';
        break;
        case "11": nivel = '3ro Medio';
        break;
        case "12": nivel = '4to Medio';
        break;
    }

    $('#folder-3').css("display", "none");
    $('#folder-4').css("display", "block");
    $('#ruta').html('<i class="mdi mdi-airplay"></i>Evaluaciones/'+tipo+'/'+asignatura+'/'+nivel+'/');
    
    $.post(siteUrl+'/home/seleccionar' ,{nivel_id:nivel_id, tipo_id:tipo_id, asignatura_id:asignatura_id})
    .done(function(json){
        json = JSON.parse(json);
        
        if (json.html) 
        {
            $('#preloader').css("display", "none");
            $('#status').css("display", "none");

            $('#folder-4').html(json.html);
        }
        
    });
});

$(document).on('click', '.folder-4', function() {
    
    prueba_id = $(this).attr("prueba");
    $('#exampleModalLongTitle-2').html("Nombre para la evaluación");
    $('.modal-footer').css("display", "block");
    //$('#folder-4').css("display", "none");
    $('#nombrePrueba').css("display", "block");
    $('#exampleModalLong-2').modal('show');
});

$(document).on('click', '[btn-continuar]', function() {
    if ($('input[type="checkbox"]:checked').length > 0) $('#form-oas').submit();
    else Toast.fire({
            type: 'warning',
            title: `Debes seleccionar al menos un ${ tipo_id == '1' || tipo_id == '2' ? 'OA' : 'Eje' }`
        });
});

$(document).on('click', '.btn-asignar', function() {
    idEvaluacion    = $(this).attr('id-evaluacion');
    $('#btn-asignar-modal').attr('origen', $(this).attr('origen'));
     
    $('#select-niveles > option[value="null"]').attr('selected', true);
    $('#select-cursos').val(null);
    $('#modal-asignar-curso').modal();
});

$(document).on('click', '.btn-editar', function() {
    idEvaluacion    = $(this).attr('id-evaluacion');
    let nombre      = $(this).attr('nombre');  

    $('#Eprueba_id').val(idEvaluacion);
    $('#Enivel_id').val(nivel_id);
    $('#Easignatura_id').val(asignatura_id);
    $('#Eprueba_id').val(idEvaluacion);
    $('#Etipo_id').val(tipo_id);


    $( "#editarEstandarizada" ).val( nombre + "(copia)");
    $('#modal-mensaje-editar').modal();
});

function btnAsignacionesPrueba(prueba)
{
    if($('.btnAsignacionesPrueba'+prueba).attr('estado') == 'cerrado')
    {
        $('.btnAsignacionesPrueba'+prueba).attr('estado', 'abierto');
        $('.asignacionesPrueba'+prueba).css("display", "block");
        $('.btnAsignacionesPrueba'+prueba).html('<i class="mdi mdi-minus"></i>');
    }
    else
    {
        $('.btnAsignacionesPrueba'+prueba).attr('estado', 'cerrado');
        $('.asignacionesPrueba'+prueba).css("display", "none");
        $('.btnAsignacionesPrueba'+prueba).html('<i class="mdi mdi-plus"></i>');
    }

}

$(document).on('click', '.btn-alumnos', function() {
    const idCurso = $(this).attr('id-curso');
    const idEvaluacion = $(this).attr('id-evaluacion');
    const idAsignacion = $(this).attr('id-asignacion');
    const origen       = $(this).attr('origen');

    $.post(`${ siteUrl }/curso/getDataPlanillaAlumno`, {idCurso, idEvaluacion, idAsignacion, origen})
    .done(response => {
        response = JSON.parse(response);
        let html = '';

        if (response.curso)
            $('#modal-curso-label').html(`<i class="fa fa-file-text-o"></i> ALUMNOS - ${ response.curso.curso } ${ response.curso.letra }`.toUpperCase());

        if (response.evaluacion)
            $('#modal-curso .modal-header p').html(`${ response.evaluacion.tipo } ${ response.evaluacion.nivel } ${ response.evaluacion.asignatura }`.toUpperCase());

        if (response.alumnos.length > 0) {
            for (const alumno of response.alumnos) {
                html += `<tr>
                    <td><input type="checkbox" ${ alumno.estado != 3 ? 'checked' : '' } switch-habilitar id-alumno="${ alumno.id }" id-asignacion="${ idAsignacion }" class="js-switch" data-color="#0098ff" data-size="small"></td>
                    <td>${ alumno.rut }</td>
                    <td>${ alumno.nombre }</td>
                    <td>
                        <div class="progress progress-sm" style="width: 70%; display: inline-flex;">
                            <div class="progress-bar progress-bar-info" style="width: ${ alumno.avance }%;" role="progressbar"> <span class="sr-only"></span> </div>
                        </div>
                        ${ alumno.avance }%
                    </td>
                    <td>${ alumno.tiempo }</td>
                </tr>`;
            }
        }
        $('#table-curso tbody').html(html);

        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
            if(!$(this).prop('checked')) $(this).siblings('span.switchery').css({'background-color': 'rgb(223, 223, 223)'});
        });

        $('#modal-curso').modal();
    });
});

$(document).on('click', '#btn-actualizar', function() {
    const idAsignacion = $(this).attr('id-asignacion');
    const origen        = $(this).attr('origen');

    $.ajax ({
        url : siteUrl+'/home/importNumikApp/'+idAsignacion,
        
        success : (json)=> {                
            console.log("cargado");
            $.post(`${ siteUrl }/asignaciones/getDataTabulacion`, {idAsignacion, origen})
            .done(response => {
                response = JSON.parse(response);
                buildPlanillaTabulacion(response, idAsignacion);
                
            });            
        }
    });
});

$(document).on('click', '.btn-tabulacion', function() {
    
    const idAsignacion  = $(this).attr('id-asignacion');
    const origen        = $(this).attr('origen');
    $('#preloader').css("display", "block");
    $('#status').css("display", "block");

    qr(idAsignacion, $(this).attr('cantPreguntas'), $(this).attr('nombrePrueba'));

    $('[btn-actualizar], [btn-reiniciar]').attr('id-asignacion', idAsignacion);
    $('[btn-actualizar]').attr('origen', origen);

    $.post(`${ siteUrl }/asignaciones/getDataTabulacion`, {idAsignacion, origen})
    .done(response => 
        {
        $('#preloader').css("display", "none");
        $('#status').css("display", "none");
        response = JSON.parse(response);
        buildPlanillaTabulacion(response, idAsignacion);
        $('#modal-tabulacion').modal();
    });
});

function qr(asignacion, preguntas, nombre)
{
    $.post(siteUrl+'/home/qr' ,{asignacion:asignacion, preguntas:preguntas, nombre:nombre})
        .done(function(json){
        json = JSON.parse(json);
        
        if (json.html) 
        {
            $('#imgQr').html('<img src="'+json.html+'" />');
        }
    
});

}


const buildPlanillaTabulacion = (response, idAsignacion) => {
    $("#detalle-evaluacion").html(`${ response.data.tipo } ${ response.data.curso } "${ response.data.letra }" ${ response.data.asignatura }`.toUpperCase());
    let html = `<table class="table"><thead style="background-color:#56b2bf;color:#fff;"><tr><th style="width: 200px;text-align: left;font-weight: bold;border-bottom: 3px solid #56b2bf;color:#fff;"> Nombre Alumno</th>`;

    if (response.preguntas.length > 0)
        for (const pregunta of response.preguntas)
            html += `<th style="font-weight: bold;border-bottom: 3px solid #56b2bf;color:#fff;">P${ pregunta.orden < 10 ? '0'+pregunta.orden : pregunta.orden }</th>`;

    html += `</tr></thead><tbody>`;
    if (response.alumnos.length > 0)
        for (const alumno of response.alumnos) {
            html += `<tr>
                <td style="text-align: left;">${ alumno.nombre } ${ alumno.apellido }</td>`;
            if (response.preguntas.length > 0)
                for (const pregunta of response.preguntas)
                    html += `<td><input type="text" class="input-alt" origen="${ response.data.origen }" id="${ alumno.id }-${ pregunta.id }" id-pregunta="${ pregunta.id }" id-alumno="${ alumno.id }" id-asignacion="${ idAsignacion }" maxlength="1"></td>`;

            html += `</tr>`;
        }

    html += `</tbody></table>`;
    $('#tabla-tabulacion').html(html);

    if (response.dataRespuesta)
        for (const resp of response.dataRespuesta) {
            $(`#${ resp.alumno_id }-${ resp.pregunta_id }`).val(`${ resp.letra }`.toUpperCase());
            $(`#${ resp.alumno_id }-${ resp.pregunta_id }`).removeClass('correcta incorrecta');
            $(`#${ resp.alumno_id }-${ resp.pregunta_id }`).addClass(`${ resp.class }`);
        }
}


if(pathPrueba == 1)
{
    $('div[tipo='+pTipo+']').click();
    $('div[asignatura='+pAsignatura+']').click();
    $('div[nivel='+pNivel+']').click();

}

$(() => {
    //dt_language.oPaginate.sNext = ">";
    //dt_language.oPaginate.sPrevious = "<";
/*
    let tables = $('#table-evaluaciones').DataTable({
        "bLengthChange" : false,
        "info": false,
        "dom": "lrtp",
        "pageLength": 10,
        "order": [[ 0, "desc" ]],
        "language": dt_language
    });*/

    $('#search').on('keyup', function () {
        tables.search( this.value ).draw();
    });

    $(document).find('.dataTables_paginate').appendTo('#paginador');

    $('#select-niveles').on('change', function() {
        idNivel = $(this).val();
        
        $.post(siteUrl+`/home/getCursosByNivel`, {idNivel})
        .done(response => {
            response = JSON.parse(response);
            let html = '';
            if (response.cursos) {
                html = `<option value="null" hidden>Selecciona un curso</option>`;
                for (const curso of response.cursos) {
                    html += `<option value="${ curso.id }">${ curso.curso } ${ curso.letra }</option>`;
                }
            } else {
                html += `<option value="null" hidden>No existen Cursos</option>`;
            }
            
            $('#select-cursos').html(html);
        });
    });

    $('#select-cursos').on('change', function() {
        idCurso = $(this).val();
        $('[btn-asignar-curso]').prop("disabled", false);
    });

    $('[btn-cerrar-modal-asignar-curso]').on('click', function() {
        $('#modal-asignar-curso').modal('hide');
    });

    $('[btn-asignar-curso]').on('click', function() {
        const origen = $(this).attr('origen');
        $.ajax({
            url:siteUrl+`/Evaluaciones/ejectuarEvaluacion/${ idEvaluacion }/${ idCurso }/${ origen }`,
            success: response => {                
                response = JSON.parse(response);
                if (response.status == 'success') {
                    left = 3;                                        
                    switch(nivel_id) 
                    {
                        case "1": nivel = '1ro Básico';
                        break;
                        case "2": nivel = '2do Básico';
                        break;
                        case "3": nivel = '3ro Básico';
                        break;
                        case "4": nivel = '4to Básico';
                        break;
                        case "5": nivel = '5to Básico';
                        break;
                        case "6": nivel = '6to Básico';
                        break;
                        case "7": nivel = '7mo Básico';
                        break;
                        case "8": nivel = '8vo Básico';
                        break;
                        case "9": nivel = '1ro Medio';
                        break;
                        case "10": nivel = '2do Medio';
                        break;
                        case "11": nivel = '3ro Medio';
                        break;
                        case "12": nivel = '4to Medio';
                        break;
                    }

                    $('#folder-3').css("display", "none");
                    $('#folder-4').css("display", "block");
                    $('#ruta').html('<i class="mdi mdi-airplay"></i>Evaluaciones/'+tipo+'/'+asignatura+'/'+nivel+'/');
                    
                    $.post(siteUrl+'/home/seleccionar' ,{nivel_id:nivel_id, tipo_id:tipo_id, asignatura_id:asignatura_id})
                    .done(function(json){
                        json = JSON.parse(json);
                        
                        if (json.html) 
                        {
                            $('#folder-4').html(json.html);
                            $('#modal-asignar-curso').modal('hide');
                        }
                        
                    });
                    
                } else {
                    swal("¡Error!", "Se produjo un error al intentar asignar la evaluación", "error");
                }
            }
        });
    });

    $('[btn-open-modal-crear]').on('click', function() {
        $('#modal-crear').modal();
    });

/*
    $(document).on('click', '[btn-ver-evaluacion]', function() {
        const idEvaluacion = $(this).attr('id-evaluacion');
        window.location.href = `${ siteUrl }/Evaluaciones/descargarEvaluacionPDF/${ idEvaluacion }`;
    });
*/
    $(document).ready(function()
    {
        $("#nivel_id").change(function () {
            $("#nivel_id option:selected").each(function () {
                nivel_id = $('#nivel_id').val();
                $.post(siteUrl+`/home/get_asignatura` ,{
                    nivel_id:nivel_id
                })
                .done(function(json){
                    json = JSON.parse(json);
                    let html = '';
                    if (json.asignaturashasnivel) {
                        html = `<option value="null" hidden>Selecciona una asignatura</option>`;
                        for (const asignaturahasnivel of json.asignaturashasnivel) {                                                        
                            html += `<option value="${ asignaturahasnivel.id }">${ asignaturahasnivel.asignatura }</option>`;
                        }
                    }
                    $('#asignatura_id').html(html);


                });
            });
        });
    });


});
