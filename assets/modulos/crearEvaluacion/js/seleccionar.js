let idPrueba = null;
let idAsignatura;
let idNivel;
let idTipo;
let nombre;

$(() => {
    $(document).on('click', function(e) {
        const element = $('[tr-prueba]');
        const element2 = $('[btn-usar-evaluacion]');

        if (!element.is(e.target) && element.has(e.target).length === 0) {
            if (!element2.is(e.target)) {
                $('[tr-prueba]').removeClass('selected');
                idPrueba = null;
            }
        }
    });

    $(document).on('click', '[tr-prueba]', function() {
        $('[tr-prueba]').removeClass('selected');
        $(this).addClass('selected');

        idPrueba = $(this).attr('id-prueba');
    });

    $(document).on('click', '[btn-usar-evaluacion]', function() {
        if (idPrueba != null) {
            idAsignatura = $('#asignatura_id').val();
            idNivel = $('#nivel_id').val();
            idTipo = $('#tipo_id').val();
            nombre = $('#nombre').val();

            window.location.href = `${ siteUrl() }/home/seleccionar_tipo_prueba?asignatura_id=${ idAsignatura }&nivel_id=${ idNivel }&tipo_id=${ idTipo }&nombre=${ nombre }&prueba_aeduc=${ idPrueba }`;
        } else {
            $.toast({
                heading: '¡Atención!',
                text: 'Si deseas usar una evaluación de AEDUC antes debes seleccionarla',
                position: 'top-right',
                icon: 'warning',
                hideAfter: 4000,
                stack: 2
            });
        }
    });

    $(document).on('click', '[btn-crear-evaluacion]', function() {
        idAsignatura = $('#asignatura_id').val();
        idNivel = $('#nivel_id').val();
        idTipo = $('#tipo_id').val();
        nombre = $('#nombre').val();

        window.location.href = `${ siteUrl() }/home/seleccionar_tipo_prueba?asignatura_id=${ idAsignatura }&nivel_id=${ idNivel }&tipo_id=${ idTipo }&nombre=${ nombre }`;
    });
});


