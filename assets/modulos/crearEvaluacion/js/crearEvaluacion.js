let idEvaluacion, idCurso;

$(() => {
    dt_language.oPaginate.sNext = ">";
    dt_language.oPaginate.sPrevious = "<";

    let tables = $('#table-evaluaciones').DataTable({
        "bLengthChange" : false,
        "info": false,
        "dom": "lrtp",
        "pageLength": 10,
        "order": [[ 0, "desc" ]],
        "language": dt_language
    });

    $('#search').on('keyup', function () {
        tables.search( this.value ).draw();
    });

    $(document).find('.dataTables_paginate').appendTo('#paginador');

    $(document).on('click', '[btn-asignar]', function() {
        idEvaluacion = $(this).attr('id-evaluacion');

        $('#select-niveles > option[value="null"]').attr('selected', true);
        $('#select-cursos').val(null);
        $('#modal-asignar-curso').modal();
    });

    $('#select-niveles').on('change', function() {
        idNivel = $(this).val();
        $.post(`${ siteUrl() }/home/getCursosByNivel`, {idNivel})
        .done(response => {
            response = JSON.parse(response);
            let html = '';
            if (response.cursos) {
                html = `<option value="null" hidden>Selecciona un curso</option>`;
                for (const curso of response.cursos) {
                    html += `<option value="${ curso.id }">${ curso.curso } ${ curso.letra }</option>`;
                }
            } else {
                html += `<option value="null" hidden>No existen Cursos</option>`;
            }
            $('#select-cursos').html(html);
        });
    });

    $('#select-cursos').on('change', function() {
        idCurso = $(this).val();
        $('[btn-asignar-curso]').prop("disabled", false);
    });

    $('[btn-cerrar-modal-asignar-curso]').on('click', function() {
        $('#modal-asignar-curso').modal('hide');
    });

    $('[btn-asignar-curso]').on('click', function() {
        $.ajax({
            url: `${ siteUrl() }/Evaluaciones/ejectuarEvaluacion/${ idEvaluacion }/${ idCurso }/PERSONAL`,
            success: response => {
                console.log(response);
                response = JSON.parse(response);
                if (response.status == 'success') {
                    swal({
                        title : "Evaluación Ejecutada",
                        text : "La evaluación ha sido ejecutada y estará disponible para todos los alumnos del curso seleccionado",
                        type : "success",
                        allowEscapeKey : false
                    },() => {
                        window.location.href = `${ siteUrl() }/asignaciones/personal`;
                    });
                } else {
                    swal("¡Error!", "Se produjo un error al intentar asignar la evaluación", "error");
                }
            }
        });
    });

    $('[btn-open-modal-crear]').on('click', function() {
        $('#modal-crear').modal();
    });

    $(document).on('click', '[btn-ver-evaluacion]', function() {
        const idEvaluacion = $(this).attr('id-evaluacion');
        window.location.href = `${ siteUrl() }/Evaluaciones/descargarEvaluacionPDF/${ idEvaluacion }`;
    });

    $(document).ready(function(){
        $("#nivel_id").change(function () {
            $("#nivel_id option:selected").each(function () {
                nivel_id = $('#nivel_id').val();
                $.post(`${ siteUrl() }/home/get_asignatura` ,{
                    nivel_id:nivel_id
                })
                .done(function(json){
                    json = JSON.parse(json);
                    let html = '';
                    if (json.asignaturashasnivel) {
                        html = `<option value="null" hidden>Selecciona una asignatura</option>`;
                        for (const asignaturahasnivel of json.asignaturashasnivel) {
                            console.log(asignaturahasnivel.id);
                            
                            html += `<option value="${ asignaturahasnivel.id }">${ asignaturahasnivel.asignatura }</option>`;
                        }
                    }
                    $('#asignatura_id').html(html);


                });
            });
        });
    });

});
