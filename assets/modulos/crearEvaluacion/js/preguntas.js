$(() => {

    switch(tipo) 
    {
        case "1": tipo = 'Simce';
        break;
        case "2": tipo = 'Cobertura';
        break;
        case "3": tipo = 'Test Procesos';
        break;
    }

    switch(nivel) 
    {
        case "1": nivel = '1ro Básico';
        break;
        case "2": nivel = '2do Básico';
        break;
        case "3": nivel = '3ro Básico';
        break;
        case "4": nivel = '4to Básico';
        break;
        case "5": nivel = '5to Básico';
        break;
        case "6": nivel = '6to Básico';
        break;
        case "7": nivel = '7mo Básico';
        break;
        case "8": nivel = '8vo Básico';
        break;
        case "9": nivel = '1ro Medio';
        break;
        case "10": nivel = '2do Medio';
        break;
        case "11": nivel = '3ro Medio';
        break;
        case "12": nivel = '4to Medio';
        break;
    }

    switch(asignatura) 
    {
        case "1": asignatura = 'Lenguaje';
        break;
        case "3": asignatura = 'Matemática';
        break;
        case "4": asignatura = 'Ciencia';
        break;
        case "5": asignatura = 'Historia';
        break;
    }

    $('#ruta').html('<i class="mdi mdi-airplay"></i>Evaluaciones/'+tipo+'/'+asignatura+'/'+nivel+'/'+nombre);

    if (tipo_id == '3' || tipo_id == '4') {
        $('#container_preguntas').find('.enc-pregunta').each(function(position, element) {
            const oa = $(element).attr('data-oa');
            
            let count = Number($(`#oa-${ oa }`).attr('count'));
            count++;

            $(`#oa-${ oa }`).attr('count', count);
            $(`#oa-${ oa }`).html(`${ count == 1 ? count + ' pregunta' : count + ' preguntas' }`);
        });
    } else if (tipo_id == '1' || tipo_id == '2') {
        $('#container_preguntas').find('.enc-pregunta').each(function(position, element) {
            const eje = $(element).attr('data-eje');
            
            let count = Number($(`#eje-${ eje }`).attr('count'));
            count++;

            $(`#eje-${ eje }`).attr('count', count);
            $(`#eje-${ eje }`).html(`${ count == 1 ? count + ' pregunta' : count + ' preguntas' }`);
        });
    }

    $(document).on('change', '[input-leter]', function() {
        const color = $(this).attr('color');

        $('[input-leter]').parent('.leter-resp').css({'background': '#fff'});
        $('[input-leter]').parent('.leter-resp').children('span').css({'color': color});
        $('[input-leter]').parent('.leter-resp').siblings('.respuesta').css({'border-color': '#bfbfbf'});
        
        $(this).parent('.leter-resp').css({'background': color});
        $(this).parent('.leter-resp').children('span').css({'color': '#fff'});
        $(this).parent('.leter-resp').siblings('.respuesta').css({'border-color': color});
    });

    $(document).on('click', '[clean-enc-pregunta]', function() {
        $(this).siblings('textarea').val('');
    });

    $(document).on('click', '[clean-all]', function() {
        $('.panel-evaluacion-crear').find('select').each(function(e) {
            $(this).children(`option`).attr("selected", false);
            $(this).children(`option[value='']`).attr("selected", true);
        });

        $('.panel-evaluacion-crear').find('textarea').each(function(i, e) {
            $(e).val('');
        });

        $('.panel-evaluacion-crear').find('input[type="file"]').each(function(i, e) {
            $(e).val('');
        });

        $('.panel-evaluacion-crear').find('input[input-leter]').each(function(i, e) {
            const color = $(e).attr('color');

            $(e).parent('label').css({'border-color': color, 'background': '#fff'});
            $(e).parent('label').siblings('.respuesta').css({'border-color': '#bfbfbf'});
            $(e).siblings('span').css({'color': color});
            $(e).attr('checked', false);
        });
    });

    $(document).on('click', '[btn-pregunta-sugeridas]', function() {
        let link = $(this).attr('link');
        let countData = [];
        
        if (tipo_id == '3' || tipo_id == '4') {
            $('#oas-selected').find('.name-oa').each(function(position, element) {
                const oa = $(element).attr('oa');
                let count = Number($(element).parents('tr').find(`#oa-${ oa }`).attr('count'));
                countData.push({oa, count});
            });
        } else if (tipo_id == '1' || tipo_id == '2') {
            $('#oas-selected').find('.name-eje').each(function(position, element) {
                const eje = $(element).attr('eje');
                let count = Number($(element).parents('tr').find(`#eje-${ eje }`).attr('count'));
                countData.push({eje, count});
            });
        }
        
        let fragmentUrl_b64 = btoa(JSON.stringify(countData));
        window.location.href = `${ link }/${ fragmentUrl_b64 }`;        
    });
});