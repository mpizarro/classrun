$(document).ready(() => {
    let preguntas = data;
    if (preguntas) {
        let i = 1;
        let letra = '';
        preguntas.forEach((pregunta) => {
            let appendData = `<div class="container pregunta" style="display:block;page-break-after: auto;">
                <div class="row" style="margin-bottom:20px">
                    <div class="col-md-12 " style="border-bottom: 3px solid #56b2bf;border-radius: 5px;border-left: 3px solid #56b2bf;">
                    <div class="numero-${pregunta.id}" style="height: 45px;width: 45px;display: flex;justify-content: center;vertical-align: middle;align-items: center;border: 1px solid #56b2bf;position: absolute;background: #FFFFFF!important;color: #56b2bf !important;border-radius: 10px;left: -18px;" id="${pregunta.id}"><h2 class="text-h1" style="color: #56b2bf !important">${i}</h2></div>
                        <h5 class="text-respuesta">
                            ${pregunta.pregunta}
                        </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-group">`;

            if (tipoEvaluacion == "1") {
                var numeros = `<div class="col-md-2 numero-${pregunta.id} numeros-2"  onclick="move(${pregunta.id})">${i}</div>`;
                $('#menu-1').show();
                $('#menu-2').hide();
            } else {
                var numeros = `<div class="col-md-2 numero-${pregunta.id} numeros"  onclick="move(${pregunta.id})">${i}</div>`;
                $('#menu-2').show();
                $('#menu-1').hide();
            }

            let x = 1;
            if (pregunta.alternativas) {
                pregunta.alternativas.forEach((alternativa) => {

                    var diccionario = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'];
    				letra = diccionario[x - 1];
                    
                    appendData +=
                        `
                        <li data-pregunta="${pregunta.id}" id="alternativa-${alternativa.id}" class="list-group-item li-alternativa" style="border-color: #fff;display: flex;align-items: center;"><input type="checkbox"><div class="text-alternativas" style="border-color: #56b2bf !important;"><h3 class="text-h3" style="color:#56b2bf !important;">${letra}</h3></div><span class="no-wrap">${alternativa.respuesta}</span></li>
                    `;
                    x++;
                });
            }
            appendData += `</ul>
                    </div>
                </div>
            </div>`;
            if (tipoEvaluacion == "1")
                $('#numeros-preguntas').append(numeros);
            else
                $('#numeros-preguntas-22').append(numeros);




            $('#rowContainer').append(appendData);
            i++;
        });
    } else {
        swal
            ({
                    type: 'error',
                    title: '¡Error!',
                    text: 'Hemos detectado que esta evaluación no tiene preguntas o respuestas asignadas, por favor contáctese con soporte técnico',
                    allowEscapeKey: false,
                    confirmButtonText: '✓'
                },
                () => window.history.back());
    }
    if (respuestas !== false) {
        respuestas.forEach((respuesta) => {
            $(`[data-pregunta="${ respuesta.pregunta_id }"]#alternativa-${ respuesta.respuesta_id }`).trigger('click');
        });
    }
});
