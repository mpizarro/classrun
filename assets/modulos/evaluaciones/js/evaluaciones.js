let idAsignatura = null, numeroEvaluacion = null, nivelEvaluacion = null, idCursoTarget = null;

var niveles = [], nivelesCurso = [], idEvaluacion = 0, codigo = '';

data.forEach((curso) => {
    if(!nivelesCurso.find((nivel)=> nivel.id == curso.nivel_id)) {
        nivelesCurso.push ({
            'id' : curso.nivel_id,
            'nivel' : curso.nivel,
            'tipo' : curso.nivel_id < 9 ? 'basica' : 'media'
        });
    }
});
$(document).ready(()=> {
    var asignaturas =  [{
        "id" : '1',
        "asignatura" : 'Lenguaje y Comunicación'
    }, {
        "id" : '5',
        "asignatura" : 'Ciencias Naturales'
    }, {
        "id" : '3',
        "asignatura" : 'Matemática'
    }, {
        "id" : '4',
        "asignatura" : 'Historia, Geografía y Ciencias Sociales'
    }];
    $('.asignaturaContainer').each((j, e)=> {
        asignaturas.forEach((asignatura, index)=> {
           $(e).append (`
                <div class="col-md-12 text-center">
                    <button class="btn btn-default asignatura-btn" onclick="setAsignatura(${ asignatura.id }, this)" ${index != 0 ? 'margin-vertical' : ''}>${ asignatura.asignatura }</button>
                </div>
            `);
        });
    });

    $('.numbersContent .number, .ellipsis.numberEllipsis').click(function() {
        addParentTitleOrange(this);

        let number = Number($(this).text().replace(/\ /g, ''));

        $('.number.orange, .ellipsis.orange, li.orange').each((i, e)=> {
           $(e).removeClass('orange');
        });

        numeroEvaluacion = number;
        $(this).addClass('orange');
    });
    $('.ellipsis .dropdown-menu li').click(function() {
        addParentTitleOrange(this);
        $('.number, .ellipsis.orange, li.orange').each((i, e)=> {
            $(e).removeClass('orange');
        });

        numeroEvaluacion = Number($(this).children().text());
        $(this).parent().parent().parent().addClass('orange');
        $(this).addClass('orange');
    });
    $('.btn-continuar').on('click', '.btn-info', function() {
        htmlElement = $(this).parent();
        while(!htmlElement.hasClass('col-md-3') && !htmlElement.hasClass('shadow')) {
            htmlElement = $(htmlElement).parent();
        }
        let tipoPrueba = {};
        switch($(htmlElement).children('.title-tipo.orange').children().text().toUpperCase()) {
            case 'SIMCE':
                tipoPrueba.tipo = 'SIMCE';
                tipoPrueba.id = '1';
                break;
            case 'COBERTURA':
                tipoPrueba.tipo = 'COBERTURA';
                tipoPrueba.id = '4';
                break;
            case 'UNIDAD':
                tipoPrueba.tipo = 'UNIDAD';
                tipoPrueba.id = '3';
                break;
            case 'PME':
                tipoPrueba.tipo = 'PME';
                tipoPrueba.id = '2';
                break;
        }

        $.post(`${ siteUrl() }/Evaluaciones/getServicios`)
        .done( function ( json ) {
            json = JSON.parse(json);
            if (json.servicios.indexOf(tipoPrueba.id) >= 0) {
                if(numeroEvaluacion && idAsignatura) {
                    $('[data-step-nav="1"]').attr('passed', 'true');
                    $('[data-step-nav="1"]').removeAttr('active');
                    $('[data-step-nav="2"]').attr('active','true');

                    $('#infoEvaluacion').html('<strong>Numero Evaluación : </strong> ' + numeroEvaluacion);
                    $('#infoAsignatura').html(asignaturas.find((asignatura) => asignatura.id == idAsignatura).asignatura);
                    $('#infoTipo').html(tipoPrueba.tipo);

                    $('[data-step="1"]').hide(100);

                    $.ajax({
                        url : `${ siteUrl() }/evaluaciones/getNivelesOpciones/${idAsignatura}/${numeroEvaluacion}/${tipoPrueba.id}`,
                        dataType : 'json',
                        success : (json) => {
                            if(json.status) {
                                niveles = json.data;
                                $('#rowNiveles').html(`<div margin-vertical><strong margin-vertical>Seleccione el nivel de la prueba</strong></div>`);
                                niveles.forEach((nivel, i)=> {
                                    let asignaturaName = asignaturaByCodigo(nivel.codigo);
                                    $('#rowNiveles').append(`
                                        <div class="col-md-12 text-center" margin-vertical>
                                            <button class="btn btn-default btn-nivel" codigo="${ nivel.codigo }" data-nivel-id="${ nivel.id }"> ${ nivel.nivel } ${ asignaturaName }</button>
                                        </div>
                                    `);
                                });
                            } else {
                                $('#rowNiveles').html(`<div margin-vertical>
                                    <strong margin-vertical>No existen niveles para esta evaluación</strong>
                                </div>`);
                            }
                        }
                    });
                    $('[data-step="2"]').show(100);
                } else {
                    $.toast ({
                        heading: '¡Información Faltante!',
                        text: `Por favor seleccione ${ numeroEvaluacion ? 'asignatura' : 'el numero de la evaluación' } para continuar`,
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500,
                        stack: 1
                    });
                }
            } else {
                $("#modal-evaluacion-deshabilitada").modal();
                $("#modal-evaluacion-deshabilitada").css({'display': 'flex', 'align-items': 'center'});
            }
        });
    });
    $('#rowNiveles').on('click', '.btn-nivel', function() {
        if(!$(this).hasClass('btn-primary')) {
            nivelEvaluacion = $(this).attr('data-nivel-id');
            idEvaluacion = $(this).attr('data-nivel-id');
            codigo = $(this).attr('codigo');

            $('#continuarNivel').prop('disabled', null);
            $('#rowNiveles .btn-nivel.btn-primary').each((i, e)=> {
                $(e).removeClass('btn-primary');
                $(e).addClass('btn-default');
            });
            $(this).addClass('btn-primary');
            $(this).removeClass('btn-default');
        }
    });
});
var goSetCurso = () => {
    $('[data-step-nav="2"]').attr('passed', 'true');
    $('[data-step-nav="2"]').removeAttr('active');
    $('[data-step-nav="3"]').attr('active','true');

    $('#selectNivel').hide(100);
    $('#dataSelected').append('<div class="row"><hr margin-horizontal no-margin-bottom></div>');
    $('#dataSelected').append('<div class="row text-center" no-margin-top><h5>Nivel de la Prueba</h5></div>');
    $('#dataSelected').append(`<div class="col-md-12 text-center" style="pointer-events: none;">
        <button class="btn btn-primary" margin-vertical style="width:50%; white-space: initial"> ${ (niveles.find((nivel) => nivel.id == nivelEvaluacion)).nivel } </button>
    </div>`);
    $('#dataSelected').append(`<div class="col-md-12 text-center">
        <button class="btn btn-info" margin-vertical style="width:50%; white-space: initial" onclick="verEvaluacion()"> Ver Evaluación </button>
    </div>`);
    $('#dataSelected').append('<div class="row"><hr margin-horizontal="" no-margin-bottom=""></div>');

    nivelesCurso.forEach((nivel, i) => {
        $('#' + nivel.tipo).append(`
            <div class="col-md-4 text-center" padding margin-vertical>
                <button class="btn btn-default btn-nivelCurso" data-nivel-id="${ nivel.id }" > ${ nivel.nivel } </button>
            </div>
        `);
    });
    $('#selectCurso').show(100);
}
$('#basica, #media').on('click', '.btn-nivelCurso', function() {
    if(!$(this).hasClass('orange')) {
        $('.btn-nivelCurso.btn-primary').each((i, e)=> {
            $(e).removeClass('btn-primary');
            $(e).addClass('btn-default');
        });
        $(this).addClass('btn-primary');
        $(this).removeClass('btn-default');
    }
    let idNivel = $(this).attr('data-nivel-id');

    $('#colLetter').html('');
    data.filter((curso) => curso.nivel_id == idNivel).forEach((curso) => {
        $('#colLetter').append(`
            <div class='row m-b-10'>
                <strong margin-vertical>Seleccione el curso que evaluará</strong>
            </div>
            <div class="letter" data-curso="${ curso.id }">
                ${ curso.letra }
            </div>
        `);
    });
});
$('#colLetter').on('click', '.letter' , function() {
    $('#continuarEjecutar').prop('disabled', false);

    if(!$(this).hasClass('orange')) {
        $('.letter.orange').each((i, e)=> {
            $(e).removeClass('orange');
        });
        $(this).addClass('orange');
        idCursoTarget = $(this).attr('data-curso');
    }
});
$('#continuarEjecutar').click(() => {
    $.ajax({
        url: `${siteUrl()}/curso/getAlumnosCursoById/${idCursoTarget}/${idEvaluacion}`,
        dataType : 'json',
        success(json) {
            const infoCurso = $('.btn-nivelCurso.btn-primary').text() + ' ' + $('.letter.orange').text().toUpperCase();

            $('#title-alumnos').text(`LISTA DE ALUMNOS - ${infoCurso}`);
            $('[data-step="4"] #dataSelected').html($('[data-step="2"] #dataSelected').html());
            $('[data-step="4"] #dataSelected').append('<div class="row"><hr margin-horizontal="" no-margin-bottom=""></div>');
            $('[data-step="4"] #dataSelected').append('<div class="row text-center" no-margin-top=""><h5>Curso</h5></div>');
            $('[data-step="4"] #dataSelected').append(`<div class="col-md-12 text-center" style="pointer-events: none;"><button class="btn btn-primary" margin-vertical="" style="width:50%; white-space: initial">${infoCurso}</button></div>`);
            $('[data-step="4"] #dataSelected').append('<div class="row"><hr margin-horizontal="" no-margin-bottom=""></div>');
            $('[data-step="4"] #dataSelected').append(`<div class="col-md-12 text-center"><button class="btn btn-primary" style="background:#5AD817!important; border: 1px solid #5AD817!important" margin-vertical="" style="width:50%; white-space: initial" onclick="ejecutarPrueba()">Ejecutar</button></div>`);
            if(json.status == 'success') {
                $('[data-step="2"]').css('display','none');
                $('[data-step-nav="3"]').attr('passed', 'true');
                $('[data-step-nav="3"]').removeAttr('active');
                $('[data-step-nav="4"]').attr('active','true');

                $('[data-step="3"]').hide(100);
                $('[data-step="4"]').show(100);
                $('table').hide();

                $('thead').html(`
                <tr>
                    <th>Rut</th>
                    <th>Nombre Alumno</th>
                </tr>`);
                let tbody = '';
                for(let alumno of json.alumnos){
                    tbody += `
                    <tr id="alumno-${alumno.id}" class="alumnoTr">
                        <td>${formateaRut(alumno.username)}</td>
                        <td>${alumno.nombre} ${alumno.apellido}</td>
                    </tr>`;
                }
                $('tbody').html(tbody);
                if (json.evaluaciones) {
                    if(json.evaluaciones.length > 0) {
                        json.evaluaciones.forEach((e) => {
                            $('.alumnoTr').append(`<td class="asignacion-${e.asignacion_id} nota empty-nota">(0.0)</td>`);
                            $('thead tr').append(`<th style="text-align: center">Evaluación ${e.num}</th>`);
                        });
                    }
                }
                if (json.hojasDeRespuestasAlumno) {
                    if(Object.keys(json.hojasDeRespuestasAlumno).length > 0) {
                        Object.keys(json.hojasDeRespuestasAlumno).forEach((idAlumno) => {
                            const hojaRespuesta = json.hojasDeRespuestasAlumno[idAlumno];
                            if(Object.keys(hojaRespuesta).length > 0){
                                Object.keys(hojaRespuesta).forEach((idAsignacion)=>{
                                    let promedio = getPromedio(hojaRespuesta[idAsignacion]);
                                    if(String(promedio).length == 1){
                                        promedio = String(promedio) + '.0';
                                    }
                                    const e = $(`#alumno-${idAlumno} .asignacion-${idAsignacion}`);
                                    $(e).text(`(${promedio})`);
                                    $(e).removeClass('empty-nota');
                                    if(promedio < 4.0) {
                                        $(e).addClass('red-nota');
                                    } else {
                                        $(e).addClass('blue-nota');
                                    }
                                });
                            }
                        });
                    }
                }
            } else if (json.status == 'curso_has_asignaciones') {
                swal('¡Atención!', 'El curso seleccionado ya tiene una asignación.', 'warning');
            } else if (json.status == 'prueba_asignada_al_curso') {
                swal('¡Atención!', 'El curso ya tuvo esta prueba asignada por otro profesor', 'warning');

            } else {
                swal('¡Error!', '¡No existen alumnos cargados en el curso!', 'error');
            }
            $('table').show();
        }
    });
});
var setAsignatura = function(id, htmlElement) {
    addParentTitleOrange(htmlElement);

    if(!$(htmlElement).hasClass('btn-primary')) {
        $('.btn-primary.asignatura-btn').each((i, e)=> {
            $(e).removeClass('btn-primary');
            $(e).addClass('btn-default');
        });
        $(htmlElement).removeClass('btn-default');
        $(htmlElement).addClass('btn-primary');
        idAsignatura = id;
    }
}
var addParentTitleOrange = (htmlElement, isShadow = false)=> {
    if(!isShadow) {
        htmlElement = $(htmlElement).parent();
        while(!htmlElement.hasClass('col-md-3') && !htmlElement.hasClass('shadow')) {
            htmlElement = $(htmlElement).parent();
        }
    }
    if(!$(htmlElement).children('.title-tipo').hasClass('orange')) {
        removeAllSelected();
        $(htmlElement).children('.title-tipo').addClass('orange');
    }
    addOptionContinuar(htmlElement);
}
var addOptionContinuar = (htmlElement)=> {
    let button = $(htmlElement).children('div.row.text-center.btn-continuar').children('button');

    if(!$(button).hasClass('btn-info')) {
        $(button).addClass('btn-info');
        $(button).removeClass('btn-default');
    }
}
var removeAllSelected = ()=> {
    idAsignatura = null;
    numeroEvaluacion = null;

    $('.title-tipo.orange').each((i, e)=> {
        $(e).removeClass('orange');
    });
    $('.title-tipo.orange').each((i, e)=> {
        $(e).removeClass('orange');
    });
    $('.number, .ellipsis.orange, li.orange').each((i, e)=> {
        $(e).removeClass('orange');
    });
    $('.btn-primary.asignatura-btn').each((i, e)=> {
        $(e).removeClass('btn-primary');
        $(e).addClass('btn-default');
    });
    $('.btn-continuar .btn-info').each((i, e)=> {
        $(e).removeClass('btn-info');
        $(e).addClass('btn-default');
    });
}

var ejecutarPrueba = ()=>{
    $.ajax({
        url : `${ siteUrl() }/evaluaciones/ejectuarEvaluacion/${ idEvaluacion }/${ idCursoTarget }`,
        dataType: 'JSON',
        success : (json)=> {
            if(json.status) {
                swal({
                    title : "Evaluación Ejecutada",
                    text : "La evaluación ha sido ejecutada y estará disponible para todos los alumnos del curso seleccionado",
                    type : "success",
                    allowEscapeKey : false
                },() => {
                    window.location.href = `${siteUrl()}/asignaciones`;
                });
            } else {
                swal({
                    title : "Evaluación No Ejecutada",
                    text : "Hemos detectado un error. Por favor contáctese con soporte técnico y verifique que no exista ninguna prueba ejecutada",
                    type : "success",
                    allowEscapeKey : false
                });
                $.toast({
                    heading: '',
                    text: '',
                    position: 'top-right',
                    icon: 'error',
                    hideAfter: 3500,
                    stack: 2
                });
            }
        }
    });
}
var formateaRut = (rut) => {
    var actual = rut.replace(/^0+/, "");
    if (actual != '' && actual.length > 1) {
        var sinPuntos = actual.replace(/\./g, "");
        var actualLimpio = sinPuntos.replace(/-/g, "");
        var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
        var rutPuntos = "";
        var i = 0;
        var j = 1;
        for (i = inicio.length - 1; i >= 0; i--) {
            var letra = inicio.charAt(i);
            rutPuntos = letra + rutPuntos;
            if (j % 3 == 0 && j <= inicio.length - 1) {
                rutPuntos = "." + rutPuntos;
            }
            j++;
        }
        var dv = actualLimpio.substring(actualLimpio.length - 1);
        rutPuntos = rutPuntos + "-" + dv;
    }
    return rutPuntos;
}
var getPromedio = (hojaRespuesta) => {
    let totalRespuestas = hojaRespuesta[0].maxNotas,
        nota,
        correctas = 0;
    for (const respuesta of hojaRespuesta) {
        if(respuesta.correcta){
            correctas++;
        }
    }
    const punt = correctas * 100 / totalRespuestas;

    nota = (punt < 60) ? (5 * (punt*0.01) + 1) : (7.5 * ((punt*0.01) - 0.6) + 4);
    if(nota < 2) {
        nota = 2;
    }
    nota = Math.round(parseInt(nota*100)/10)/10;
    return nota;
}

const verEvaluacion = () => {
    $('#data-prueba-url').attr('data', `${siteUrl()}/administrador/verEvaluacion/${idEvaluacion}`);
    $('#btn-descargar').attr('href', `${ siteUrl() }/evaluaciones/downloadEvaluacion/${ codigo }`);
	$('#ver-prueba').modal('show');
}

const asignaturaByCodigo = (codigo) => {
    const alias = codigo.substr(0, 3).toUpperCase();
    let asignaturaName;
    switch (alias) {
        case 'QUI':
            asignaturaName = 'Química';
            break;
        case 'BIO':
            asignaturaName = 'Biología';
            break;
        case 'FIS':
            asignaturaName = 'Física';
            break;
        default:
            asignaturaName = '';
            break;
    }
    return asignaturaName;
}
