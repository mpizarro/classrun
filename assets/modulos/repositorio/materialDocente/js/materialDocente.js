let asignaturaSelected, tipoDocumentoSelected, nivelSelected;
$(document).ready(()=>
{
	//$('.tableContainer').hide();
	data = orderByKey(data, 'orden');
	data.forEach((e,i)=>
	{
		$('#rowContainerAsignaturas').append(`<div class="col-md-3 containerAsignaturas">
            <div class="asignatura ${e.nombre.toLowerCase()}">
                ${e.nombre.charAt(0).toUpperCase()}${e.nombre.substring(1,e.nombre.length).toLowerCase()}
            </div>
        </div>`);
	});
	$('#iframe').load(()=>
	{
		$(".preloader").fadeOut();
		$('.tableContainer').css('pointer-events','initial');
	});
	$(document).on('click', ".containerAsignaturas", function()
	{
		$('#nivelesContainer-1,#titleNiveles,#nivelesContainer-2,#nivelesContainer-3').hide();
		tipoDocumentoSelected = undefined, nivelSelected = undefined;
		let element = this;
		$('.asignaturaSelected').each((i,e)=>
		{
			if(e !== element)
			{
				$(e).removeClass('asignaturaSelected');
			}
		});
		if(!$(this).hasClass('asignaturaSelected'))
		{
			$(this).addClass('asignaturaSelected');
		}
		asignaturaSelected = data.find((dataOne) => { return dataOne.nombre == $(element).text().trim().toUpperCase()})
		let tipoDocumentos = (asignaturaSelected.tipoDocumentos.sort(function(a, b)
		{
		    return a['orden'] - b['orden'];
		}));
		$('#nivelesContainerSelect').html('<option selected disabled>Seleccionar una opción</option>');
		tipoDocumentos.forEach((tipoDocumento, i)=>
		{
			$('#nivelesContainerSelect').append
			(`
				<option>${tipoDocumento.nombre}</option>
			`);
		});
		setDateTable();
	});
	$('#nivelesContainerSelect').change(function()
	{
		$('#titleNiveles,#nivelesContainer-1,#nivelesContainer-2,#nivelesContainer-3').show();
		$('#nivelesContainer-1,#nivelesContainer-2,#nivelesContainer-3').html('');
		let textOption = $(this).children('option:selected').text();
		tipoDocumentoSelected = asignaturaSelected.tipoDocumentos.find((tipoDocumento) => { return tipoDocumento.nombre == textOption});
		for(let i = 0; i < tipoDocumentoSelected.niveles.length; i++)
		{
			if(tipoDocumentoSelected.niveles[i].nombre == "NT")
			{
				if(tipoDocumentoSelected.niveles[i].orden == "1")
				{
					tipoDocumentoSelected.niveles[i].orden = 0;
				}
				else
				{
					tipoDocumentoSelected.niveles[i].orden = -1;
				}
			}
		}
		let niveles = (tipoDocumentoSelected.niveles.sort(function(a, b)
		{
		    return a['orden'] - b['orden'];
		}));
		niveles.forEach((nivel, i)=>
		{
			if(i <= 3)
			{
				$('#nivelesContainer-1').append
				(`
					<div class="col-md-3 text-center containerNivel">
	                    <button onclick="selectNivel(${nivel.orden}, this)" class="btn btn-default btn-nivel nivelContainer nivelBtn ${i == 3 ? 'lastNivel' : ''} ${i == 0 ? 'firstNivel' : ''}"> ${nivel.nombre != "NT" ? (nivel.orden + "º " + nivel.nombre.split(' ')[1]) : nivel.nombre + (Number(nivel.orden) + 2)}</button>
	                </div>
				`);
			}
			else if(i <= 7)
			{
				$('#nivelesContainer-2').append
				(`
					<div class="col-md-3 text-center containerNivel">
	                    <button onclick="selectNivel(${nivel.orden}, this)" class="btn btn-default btn-nivel nivelContainer nivelBtn ${i == 7 ? 'lastNivel' : ''} ${i == 4 ? 'firstNivel' : ''}"> ${nivel.orden}º ${nivel.nombre.split(' ')[1]}</button>
	                </div>
				`);
			}
			else
			{
				$('#nivelesContainer-3').append
				(`
					<div class="col-md-3 text-center containerNivel">
	                    <button onclick="selectNivel(${nivel.orden}, this)" class="btn btn-default btn-nivel nivelContainer nivelBtn ${i == 7 ? 'lastNivel' : ''} ${i == 8 ? 'firstNivel' : ''}"> ${nivel.orden}º ${nivel.nombre.split(' ')[1]}</button>
	                </div>
				`);
			}
		});
		setDateTable();
	});
	if (data.length > 0 ) $('.containerAsignaturas')[0].click();
});
function selectNivel(orden, e)
{
	$('.nivelBtn.btn-primary').each((i, btn)=>
	{
		$(btn).removeClass('btn-primary');
		$(btn).addClass('btn-default');
	});
	$(e).addClass('btn-primary');
	$(e).removeClass('btn-default');

	nivelSelected = tipoDocumentoSelected.niveles.find((nivel)=> { return nivel.orden == orden});
	setDateTable();
}
function setDateTable()
{
	let filtro = "";
	$('.tableContainer').show();
	$('#bodyTable').html('');

	if(asignaturaSelected && tipoDocumentoSelected && nivelSelected)
	{
		filtro = `assets/material/MATERIAL DOCENTE/${asignaturaSelected.nombre}-${asignaturaSelected.orden}/${tipoDocumentoSelected.nombre}/${nivelSelected.nombre}-${nivelSelected.orden == "0" ? '-2' : nivelSelected.orden}`;
	}
	else if(asignaturaSelected && tipoDocumentoSelected)
	{
		filtro = `assets/material/MATERIAL DOCENTE/${asignaturaSelected.nombre}-${asignaturaSelected.orden}/${tipoDocumentoSelected.nombre}`;
		$('#titleEvaluaciones,#containerEvaluaciones').show();
	}
	else if(asignaturaSelected)
	{
		filtro = `assets/material/MATERIAL DOCENTE/${asignaturaSelected.nombre}-${asignaturaSelected.orden}`;
		$('#titleEvaluaciones,#containerEvaluaciones').hide();
	}
	const filteredFiles = files.filter((file)=> { return file.ruta.indexOf(filtro) != -1 });
	filteredFiles.forEach((e, i)=>
	{

		$('#bodyTable').append
        (
        	`<div class="rowTable" ${(i == 0) ? 'id="file-one"' : ''} onclick="setFile(event, '${e.url}', this)">
            	<div class="th first"><img src="${baseUrl()}assets/images/icono-pdf.png">
            	<p>${(e.nombre.length > 40) ? e.nombre.substring(0, 30) + "..." : e.nombre}</p></div>
            	<div class="th two"><p style="font-size: 12px;">${bytesToSize(e.size)}</p></div>
            	<div class="th three"><i style="font-size: 20px" onclick="download('${e.url}')" class="fa fa-arrow-circle-down"></i></div>
        	</div>
        `);
        if(i == 0)
        {
        	$('#file-one')[0].click();
        }
	});
}
function orderByKey(data, key, push = false)
{
	let newData = [];
	data.forEach((e, i)=>
	{
		newData[i] = data.find((oneData)=> oneData.orden-1 == i);
	});
	return newData;
}
function download(url)
{
	$('#dowloadAux').attr('href', url);
	$('#dowloadAux')[0].click();
}
function bytesToSize(bytes)
{
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Byte';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}
function setFile(event, url, element)
{
	if(event.target.tagName !== 'I')
	{
		$('.file-selected').each((i, e)=>
		{
			$(e).removeClass('file-selected');
		});
		$(element).addClass('file-selected');
		$(".preloader").show();
		$('.tableContainer').css('pointer-events','none');
		$('#iframe').attr('src', url);
	}
}