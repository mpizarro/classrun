let asignaturas, nivelSet, numerosEvaluacion;
$(document).ready(()=>
{
	data = orderByKey(data, 'orden');
	data.forEach((e,i)=>
	{
		$('#rowContainerAsignaturas').append(`<div class="col-md-3 containerAsignaturas">
            <div class="asignatura ${e.nombre.toLowerCase()}">
                ${e.nombre.charAt(0).toUpperCase()}${e.nombre.substring(1,e.nombre.length).toLowerCase()}
            </div>
        </div>`);
	});
	$('#iframe').load(()=>
	{
		$(".preloader").fadeOut();
		$('.tableContainer').css('pointer-events','initial');
	});
	$(document).on('click', ".containerAsignaturas", function()
	{
		nivelSet = undefined;
		numerosEvaluacion = undefined;
		let element = this;
		$('.asignaturaSelected').each((i,e)=>
		{
			if(e !== element)
			{
				$(e).removeClass('asignaturaSelected');
			}
		});
		if(!$(this).hasClass('asignaturaSelected'))
		{
			$(this).addClass('asignaturaSelected');
		}
		let asignatura = data.find((dataOne) => { return dataOne.nombre == $(element).text().trim().toUpperCase()})
		asignaturas = asignatura;
		let niveles = (asignatura.niveles.sort(function(a, b)
		{
		    return a['orden'] - b['orden'];
		}));
		$('#nivelesContainer-2, #nivelesContainer-1').html('');
		niveles.forEach((nivel, i)=>
		{
			if(i <= 3)
			{
				$('#nivelesContainer-1').append
				(`
					<div class="col-md-3 text-center containerNivel">
	                    <button onclick="selectNivel(${nivel.orden}, this)" class="btn btn-default btn-nivel nivelContainer nivelBtn ${i == 3 ? 'lastNivel' : ''} ${i == 0 ? 'firstNivel' : ''}"> ${nivel.orden}º ${nivel.nombre.split(' ')[1]}</button>
	                </div>
				`);
			}
			else
			{
				$('#nivelesContainer-2').append
				(`
					<div class="col-md-3 text-center containerNivel">
	                    <button onclick="selectNivel(${nivel.orden}, this)" class="btn btn-default btn-nivel nivelContainer nivelBtn ${i == 7 ? 'lastNivel' : ''} ${i == 4 ? 'firstNivel' : ''}"> ${nivel.orden}º ${nivel.nombre.split(' ')[1]}</button>
	                </div>
				`);
			}
		});
		setTableData();
	});
	if (data.length > 0) $('.containerAsignaturas')[0].click();
});
function selectNivel(orden, e)
{
	$('.nivelBtn.btn-primary').each((i, btn)=>
	{
		$(btn).removeClass('btn-primary');
		$(btn).addClass('btn-default');
	});
	$(e).addClass('btn-primary');
	$(e).removeClass('btn-default');

	let nivelEscogido = asignaturas.niveles.find((nivel)=> { return nivel.orden == orden});
	nivelSet = nivelEscogido;
	let numerosEvaluacion = (nivelEscogido.numerosEvaluacion.sort(function(a, b)
	{
	    return a['orden'] - b['orden'];
	}));

	$('#containerEvaluaciones').html('');
	numerosEvaluacion.forEach((e)=>
	{
		$('#containerEvaluaciones').append
		(`
			<div class="letter" onclick="selectLetter(${e.orden}, this)">
	            ${e.orden}
	        </div>
		`);
	});
	setTableData();
}
function selectLetter(orden, element)
{
	$('.letter.orange').each((i, letter)=>
	{
		$(letter).removeClass('orange');
	});
	$(element).addClass('orange');

	numerosEvaluacion = nivelSet.numerosEvaluacion.find((numerosEvaluacion)=> { return numerosEvaluacion.orden == orden});
	setTableData();
}
function orderByKey(data, key, push = false)
{
	let newData = [];
	data.forEach((e, i)=>
	{
		newData[i] = data.find((oneData)=> oneData.orden-1 == i);
	});
	return newData;
}
function setTableData()
{
	let filteredFiles = [];
	$('.tableContainer').show();
	$('#bodyTable').html('');

	if(asignaturas && nivelSet && numerosEvaluacion)
	{
		filteredFiles = files.filter((file)=> { return file.ruta.indexOf(`assets/material/REMEDIALES/${asignaturas.nombre}-${asignaturas.orden}/${nivelSet.nombre}-${nivelSet.orden}/${numerosEvaluacion.nombreCarpeta}`) != -1 });
	}
	else if(asignaturas && nivelSet)
	{
		filteredFiles = files.filter((file)=> { return file.ruta.indexOf(`assets/material/REMEDIALES/${asignaturas.nombre}-${asignaturas.orden}/${nivelSet.nombre}-${nivelSet.orden}`) != -1 });
		$('#titleEvaluaciones,#containerEvaluaciones').show();
	}
	else if(asignaturas)
	{
		$('#titleEvaluaciones,#containerEvaluaciones').hide();
		filteredFiles = files.filter((file)=> { return file.ruta.indexOf(`assets/material/REMEDIALES/${asignaturas.nombre}-${asignaturas.orden}`) != -1 });
	}
	filteredFiles.forEach((e, i)=>
	{		
		$('#bodyTable').append
        (
        	`<div class="rowTable" ${(i == 0) ? 'id="file-one"' : ''} onclick="setFile(event, '${e.url}', this, event)">
            	<div class="th first"><img src="${baseUrl()}assets/images/icono-pdf.png">
            	<p>${(e.nombre.length > 40) ? e.nombre.substring(0, 30) + "..." : e.nombre}</p></div>
            	<div class="th two"><p style="font-size: 12px;">${bytesToSize(e.size)}</p></div>
            	<div class="th three"><i style="font-size: 20px" onclick="download('${e.url}')" class="fa fa-arrow-circle-down"></i></div>
        	</div>
        `);
        if(i == 0)
        {
        	$('#file-one')[0].click();
        }
	});
}
function download(url)
{
	$('#dowloadAux').attr('href', url);
	$('#dowloadAux')[0].click();
}
function bytesToSize(bytes)
{
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Byte';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}
function setFile(event, url, element)
{
	if(event.target.tagName !== 'I')
	{
		$('.file-selected').each((i, e)=>
		{
			$(e).removeClass('file-selected');
		});
		$(element).addClass('file-selected');
		$(".preloader").show();
		$('.tableContainer').css('pointer-events','none');
		$('#iframe').attr('src', url);
	}
}