$(document).ready(()=>
{
	let preguntas = data;
    if(preguntas)
    {
        let i = 1;
        let letra = '';
        preguntas.forEach((pregunta)=>
        {
            let appendData = `<div class="container pregunta">
                <div class="row preguntas-style" >
                    <div class="col-md-12 preguntas-style-2">
                    <div class="respuestas-style numero-${pregunta.id}" id="${pregunta.id}"><h2 class="text-h1">${i}</h2></div>
                        <h5 class="text-respuesta">
                            ${pregunta.pregunta}
                        </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-group">`;

            if(tipoEvaluacion == "1"){
                var numeros = `<div class="col-md-2 numero-${pregunta.id} numeros-2"  onclick="move(${pregunta.id})">${i}</div>`;
                $('#menu-1').show();
                $('#menu-2').hide();
            }
            else{
                var numeros = `<div class="col-md-2 numero-${pregunta.id} numeros"  onclick="move(${pregunta.id})">${i}</div>`;
                $('#menu-2').show();
                $('#menu-1').hide();
            }

            let x = 1;
            pregunta.alternativas.forEach((alternativa)=>
            {
				var diccionario = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'];
				letra = diccionario[x - 1];

                appendData +=
                `
                    <li onclick="changeStatus(${alternativa.id},${pregunta.id}, this)" data-checked="false" data-pregunta="${pregunta.id}" id="alternativa-${alternativa.id}" class="list-group-item li-alternativa" style="border-color: #fff;display: flex;align-items: center;">
                        <input type="checkbox">
                        <div class="text-alternativas">
                            <h3 class="text-h3">${letra}</h3>
                        </div>
                        <span class="no-wrap">${alternativa.respuesta}</span>
                    </li>
                `;
                x++;
            });
            appendData += `</ul>
                    </div>
                </div>
            </div>`;
            if(tipoEvaluacion == "1")
                $('#numeros-preguntas').append(numeros);
            else
                $('#numeros-preguntas-22').append(numeros);




            $('#rowContainer').append(appendData);
            i++;
        });
    }
    else
    {
        swal
        ({
            type: 'error',
            title : '¡Error!',
            text : 'Hemos detectado que esta evaluación no tiene preguntas o respuestas asignadas, por favor contáctese con soporte técnico',
            allowEscapeKey : false,
            confirmButtonText : '✓'
        },
        () => window.history.back() );
    }
    if(respuestas !== false)
    {
        respuestas.forEach((respuesta)=>
        {
            //$(`[data-pregunta="${ respuesta.pregunta_id }"]#alternativa-${ respuesta.respuesta_id }`).trigger('click');

			$(`.numero-${respuesta.pregunta_id}`).addClass('selected');
			searchOtherInputs(respuesta.pregunta_id, respuesta.respuesta_id);

			$(`#alternativa-${respuesta.respuesta_id}`).attr('data-checked', 'true');
			$($(`#alternativa-${respuesta.respuesta_id}`).children()[0]).prop('checked', true);
        });
    }
    if(newTime !== false && !localStorage.getItem('check'))
    {
        localStorage.setItem('time', newTime);
        localStorage.setItem('check', 'false');
    }
    $('#time').html(localStorage.getItem('time'));
    let momentOrigin = moment('2013-02-08 ' + $('#time').text());

    let e = setInterval(()=>
    {
        momentOrigin = momentOrigin.subtract(1, 'seconds');
        $('#time').text(momentOrigin.format('HH:mm:ss'));
        localStorage.setItem('time', momentOrigin.format('HH:mm:ss'));
        if(momentOrigin.format('HH:mm:ss') == '00:00:00')
        {
            clearInterval(e);
            finalizarEvaluacion(true);
        }
    },1000);
});
let searchOtherInputs = (idPregunta, idAlternativa) =>
{
    $('[data-pregunta="' + idPregunta + '"]').each((i, e)=>
    {
        if($(e).attr('id') !== 'alternativa-' + idAlternativa && $(e).attr('data-checked') == "true")
        {
            $(e).attr('data-checked', 'false');
            $($(e).children()[0]).prop('checked', false);
        }
    });
};
var changeStatus = (idAlternativa, idPregunta, e)=>
{
    if($(e).attr('data-checked') == "false")
    {
        $(`.numero-${idPregunta}`).addClass('selected');
        searchOtherInputs(idPregunta, idAlternativa);
        $(e).attr('data-checked', 'true');
        $($(e).children()[0]).prop('checked', true);
        $.ajax
        ({
            url : siteUrl() + `/alumno/setAlternativa/${ idAlternativa }/${ idPregunta }/${ idEvaluacion }/${ asignacion_id }`,
            dataType : 'json',
            success : (json)=>
            {
                if(!json.status)
                {
                    $.toast
                    ({
                        heading: '¡Hubo un problema!',
                        text: 'No se pudo guardar su selección',
                        position: 'top-right',
                        icon: 'error',
                        hideAfter: 4500,
                        stack: 1
                    });
                }
				if(json.status == 'closed')
                {
					swal
			        ({
			            type: 'error',
			            title : '¡Error!',
			            text : 'La prueba que está realizado ya ha sido finalizada.',
			            allowEscapeKey : false,
			            confirmButtonText : '✓'
			        },
			        () => window.location.reload() );
                }
            }
        });
    }
    else
    {
        $(`.numero-${idPregunta}`).removeClass('selected');
        $(e).attr('data-checked', 'false');
        $($(e).children()[0]).prop('checked', false);
        $.ajax
        ({
            url : siteUrl() + `/alumno/eliminarAlternativa/${ idAlternativa }/${ idPregunta }/${ idEvaluacion }/${ asignacion_id }`,
            dataType : 'json',
            success : (json)=>
            {
                if(!json.status)
                {
                    $.toast
                    ({
                        heading: '¡Hubo un problema!',
                        text: 'No se pudo guardar su selección',
                        position: 'top-right',
                        icon: 'error',
                        hideAfter: 4500,
                        stack: 1
                    });
                }
				if(json.status == 'closed')
                {
					swal
			        ({
			            type: 'error',
			            title : '¡Error!',
			            text : 'La prueba que está realizado ya ha sido finalizada.',
			            allowEscapeKey : false,
			            confirmButtonText : '✓'
			        },
			        () => window.location.href = site_url + '/alumnologin' );
                }
            }
        });
    }
};
setInterval(()=>
{
    let idPregunta = $($('input[type="checkbox"]:checked').parent()[0]).attr('data-pregunta');

    //
    $('input[type="checkbox"]:checked').siblings('.text-alternativas').addClass('selected');


    $('input[type="checkbox"]:not(:checked)').siblings('.text-alternativas').removeClass('selected');




},0);
var finalizarEvaluacion = (direct = false)=>
{
    let confirmFunction = ()=>
    {
        localStorage.clear();
        $.ajax
        ({
            url : siteUrl() + "/alumno/finalizarEvaluacion/" + idUsuarioHasAsignacion + "/" + idEvaluacion + "/" + asignacion_id + "/" + origen,
            dataType : 'json',
            success : (json)=>
            {
                console.log(json);
                
                if(json.status)
                {
                    swal
                    ({
                        title : "Se ha finalizado correctamente la evaluación",
                        type : "success",
                        allowEscapeKey : false
                    },()=>
                    {
                        window.location.reload();

                    });
                }
                else
                {
                    swal("No es posible finalizar la evaluación", "Contácte a soporte técnico", "error");
                }
            }
        });
    }
    if(direct)
    {
        confirmFunction();
    }
    else
    {
        swal({
            title : "¿Está seguro que desea finalizar la evaluación?",
            text : "Una vez finalizada, no podrá volver a realizarla",
            type : "warning",
            showCancelButton : true,
            confirmButtonText : "Finalizar",
            cancelButtonText : "Cancelar",
            closeOnConfirm : false
        },(isConfirm) =>
        {
            if(isConfirm)
            {
                confirmFunction();
            }
            else
            {
                return false;
            }
        });
    }
};
function move(idPregunta) {
    $("html, body").animate({ scrollTop: $(`#${idPregunta}`).offset().top }, 1000);
}
