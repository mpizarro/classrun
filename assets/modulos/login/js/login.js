function checkRut(rut)
{
  // Despejar Puntos
  var valor = rut.value.replace('.','');
  // Despejar GuiÃ³n
  valor = valor.replace('-','');

  // Aislar Cuerpo y DÃ­gito Verificador
  cuerpo = valor.slice(0,-1);
  dv = valor.slice(-1).toUpperCase();

  // Formatear RUN
  rut.value = cuerpo + '-'+ dv
  if(rut.value == '-')
  {
      rut.value = '';
  }

  // Si no cumple con el mÃ­nimo ej. (n.nnn.nnn)
  if(cuerpo.length < 7) { rut.setCustomValidity("Rut Incorrecto"); return false;}

  // Calcular DÃ­gito Verificador
  suma = 0;
  multiplo = 2;

  // Para cada dÃ­gito del Cuerpo
  for(i=1;i<=cuerpo.length;i++) {

      // Obtener su Producto con el MÃºltiplo Correspondiente
      index = multiplo * valor.charAt(cuerpo.length - i);

      // Sumar al Contador General
      suma = suma + index;

      // Consolidar MÃºltiplo dentro del rango [2,7]
      if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }

  }

  // Calcular DÃ­gito Verificador en base al MÃ³dulo 11
  dvEsperado = 11 - (suma % 11);

  // Casos Especiales (0 y K)
  dv = (dv == 'K')?10:dv;
  dv = (dv == 0)?11:dv;

  // Validar que el Cuerpo coincide con su DÃ­gito Verificador
  if(dvEsperado != dv) { rut.setCustomValidity("Rut Incorrecto"); return false; }

  // Si todo sale bien, eliminar errores (decretar que es vÃ¡lido)
  rut.setCustomValidity('');
}

var onInit = ()=>
{
  $(document).ready(()=>
  {
    $('#formLogin').submit(function()
    {
        var data =
        {
            username : $("#rut").val(),
            password : $("#pass").val()
        };
      
      $.ajax
      ({
          url : siteUrl + "/login/login_check",
          data : data,
          type : 'post',
          success : function(json)
          {
            json = JSON.parse(json);
            //console.log(json);
            window.location.reload();

          }
      });
      return false;
    });
  });
}
