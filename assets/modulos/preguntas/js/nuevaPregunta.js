let urlPregunta = siteUrl() + "/preguntas/guardarPregunta";
$(document).ready(() => {
    if (idPregunta) {
        $.post(siteUrl() + "/preguntas/getPregunta", { id: idPregunta })
            .done(function(json) {
                json = JSON.parse(json);
                $("#title-page").html("Editar Pregunta");
                $("#btnGuardarPregunta").html('<i class="fa fa-check"></i> Guardar');
                $("select[name='eje'] option[value='" + json.eje + "'").attr('selected', true);
                $("select[name='aprendizaje'] option[value='" + json.aprendizaje + "'").attr('selected', true);


                $(".editor").html(json.pregunta);

            });

        urlPregunta = siteUrl() + "/preguntas/actualizarPregunta/" + idPregunta;
    }

    if ($(".editor").length > 0)
    {
        setTimeout(function()
        {
            tinymce.init({
                
                selector: "textarea.editor",
                theme: "modern",
                height: 140,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor code"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor | code",
                image_prepend_url:"/ckfinder/",
                relative_urls : false,
                remove_script_host : false,
                convert_urls : true
            });
          }, 1500);



    }
});


$(document).on("submit", "#frmNuevaPregunta", function() {

    $('#btnGuardarPregunta').attr('disabled', 'disabled');
    $('#btnGuardarPregunta').off('click');
    
    let codigo = $(this).attr('codigo');
    let idEvaluacion = $(this).attr('id-evaluacion');

    if ($(".editor").val() !== "") {
        let data = $("#frmNuevaPregunta").serializeObject();
        data.idEvaluacion = idEvaluacion;
        data.codigo = codigo;
        console.log(data);
        $.ajax({
            url: urlPregunta,
            data: { 'data': JSON.stringify(data) },
            type: 'post',
            success: function(json) {
                json = JSON.parse(json);
                if (json.idPregunta) {
                    $.toast({
                        heading: '¡Perfecto!',
                        text: 'Datos guardados corretamente.',
                        position: 'top-right',
                        icon: 'success',
                        hideAfter: 1000,
                        stack: 2
                    });
                    window.location.href = siteUrl() + "/administrador/alternativa?codigo=" + codigo + "&idEvaluacion=" + idEvaluacion + "&idPregunta=" + json.idPregunta;
                } else if (json.response) {
                    $.toast({
                        heading: '¡Perfecto!',
                        text: 'Datos guardados corretamente.',
                        position: 'top-right',
                        icon: 'success',
                        hideAfter: 1000,
                        stack: 2
                    });
                    window.location.href = siteUrl() + "/administrador/preguntas?codigo=" + codigo + "&idEvaluacion=" + idEvaluacion;
                } else {
                    $.toast({
                        heading: '¡Error!',
                        text: 'Ocurrio un error al guardar los datos.',
                        position: 'top-right',
                        icon: 'error',
                        hideAfter: 2000,
                        stack: 2
                    });
                }
            }
        });
    } else {
        $.toast({
            heading: '¡Atención!',
            text: 'El contenido de la pregunta no debe esta vacío.',
            position: 'top-right',
            icon: 'warning',
            hideAfter: 3000,
            stack: 2
        });
    }
});

$(document).on('click', '.mce-btn button[role="presentation"]', function() {
    let codigo = $("#frmNuevaPregunta").attr('codigo');

    $(document).find('.mce-container.mce-window').css({ 'width': '80%', 'top': '50px', 'left': '10%', 'height': 'auto' });
    $(document).find('.mce-container.mce-panel.mce-foot, .mce-abs-layout').css({ 'width': '100%', 'text-align': 'center' });
    $(document).find('.mce-abs-layout').css({ 'height': '370px' });
    $(document).find('.mce-container-body > .mce-btn').css({ 'left': 'unset', 'position': 'relative', 'margin-left': '5px', 'margin-right': '5px' });
    $(document).find('.mce-container.mce-form.mce-abs-layout-item').css('width', '50%');

    if ($(document).find('#mce-content-img').length == 0) {
        $.post(`${siteUrl()}/evaluaciones/getImagenes`, { codigo: codigo })
            .done(json => {
                json = JSON.parse(json);
                const html = `<div id='mce-content-img' style='width: 50%; float: right;'>${json.data}</div>`;
                $(document).find('.mce-container.mce-form.mce-abs-layout-item').after(html);
            });
    } else {
        $.post(`${siteUrl()}/evaluaciones/getImagenes`, { codigo: codigo })
            .done(json => {
                json = JSON.parse(json);
                $(document).find('#mce-content-img').html(json.data);
            });
    }
});

$(document).on('click', '[add-img]', function() {
    let url = $(this).attr('src');
    $(document).find('#mceu_45-inp').val(url);
});

$(document).on("click", "#btnAtrasPreguntas", function() {
    window.location.href = siteUrl() + "/administrador/preguntas?codigo=" + $(this).attr('codigo') + "&idEvaluacion=" + $(this).attr('id-evaluacion');

});

$('#unidad').change(function()
{
    selectOas($(this).val());

});

function selectOas(unidad)
{
    $.post(siteUrl() + "/administrador/selectOas", { unidad: unidad })
    .done(function(json) {
        json = JSON.parse(json);
        $("#oa").removeAttr('disabled');
        $("#oa").html(json.data);

    });
}

$('#oa').change(function()
{
    selectIes($(this).val());

});

function selectIes(oa)
{
    $.post(siteUrl() + "/administrador/selectIes", { oa: oa })
            .done(function(json) {
                json = JSON.parse(json);
                $("#ie").removeAttr('disabled');
                $("#ie").html(json.data);

            });

}
