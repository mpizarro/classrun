//Preguntas
$(document).ready(function() {
    var tabla = $('#table-preguntas');
    tabla.footable();

    tabla.data('page-size', 5);
    tabla.trigger('footable_initialized');

    $(document).find('#search').on('keyup', function(e) {
        e.preventDefault();
        tabla.trigger('footable_filter', { filter: $(this).val() });
    });
});

$(document).on("click", "#btnModalCrearPregunta", function() {
    idEvaluacion    = $(this).attr('id-evaluacion');
    codigo          = $(this).attr('codigo');
    nivel_id        = $(this).attr('nivel_id');
    asignatura_id   = $(this).attr('asignatura_id');

    window.location.href = siteUrl() + "/administrador/pregunta?codigo=" + codigo + "&idEvaluacion=" + idEvaluacion + "&nivel_id=" + nivel_id + "&asignatura_id=" + asignatura_id;
});
$(document).on("click", ".btnEditarPregunta", function() {
    let idPregunta = $(this).attr('id-pregunta');
    idEvaluacion = $(this).attr('id-evaluacion');
    codigo          = $(this).attr('codigo');
    nivel_id        = $(this).attr('nivel_id');
    asignatura_id   = $(this).attr('asignatura_id');

    window.location.href = siteUrl() + "/administrador/pregunta?codigo=" + codigo + "&idEvaluacion=" + idEvaluacion + "&idPregunta=" + idPregunta + "&nivel_id=" + nivel_id + "&asignatura_id=" + asignatura_id;
});
$(document).on("click", ".btnEliminarPregunta", function() {
    let idPregunta = $(this).attr('id-pregunta');
    swal({
        title: "¡Atención!",
        text: "Esta seguro que desea eliminar esta pregunta?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false
    }, function() {
        $.post(siteUrl() + "/preguntas/eliminarPregunta", { id: idPregunta })
            .done(function(json) {
                json = JSON.parse(json);
                if (json.response) {
                    swal({
                        title: "¡Perfecto!",
                        text: "Alternativa eliminada conexito!",
                        type: "success",
                        confirmButtonColor: "#00c292",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    }, function() {
                        location.reload();
                    });
                } else {
                    swal("¡Error!", "No se pudo eliminar este registro.", "error");
                }
            });
    });
});
$(document).on("click", ".btnVerPregunta", function() {
    let idPregunta = $(this).attr('id-pregunta');
    $.post(siteUrl() + "/preguntas/getPregunta", { id: idPregunta })
        .done(function(json) {
            json = JSON.parse(json);
            $("#contenido-pregunta").html(json.pregunta);
            $("#mdlVerPregunta").modal();
        });
});
$(document).on("click", ".btnNuevaAlternativa", function() {
    let idPregunta = $(this).attr('id-pregunta');
    idEvaluacion = $(this).attr('id-evaluacion');
    codigo = $(this).attr('codigo');
    window.location.href = siteUrl() + "/administrador/alternativa?codigo=" + codigo + "&idEvaluacion=" + idEvaluacion + "&idPregunta=" + idPregunta;
});

//Alternativas
$(document).on("click", ".btnEditarAlternativa", function() {
    let idPregunta = $(this).attr('id-pregunta');
    let idAlternativa = $(this).attr('id-alternativa');
    idEvaluacion = $(this).attr('id-evaluacion');
    codigo = $(this).attr('codigo');
    window.location.href = siteUrl() + "/administrador/alternativa?codigo=" + codigo + "&idEvaluacion=" + idEvaluacion + "&idPregunta=" + idPregunta + "&idAlternativa=" + idAlternativa;
});
$(document).on("click", ".btnEliminarAlternativa", function() {
    let idAlternativa = $(this).attr('id-alternativa');
    swal({
        title: "¡Atención!",
        text: "Esta seguro que desea eliminar esta alternativa?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Eliminar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false
    }, function() {
        $.post(siteUrl() + "/respuestas/eliminarAlternativa", { id: idAlternativa })
            .done(function(json) {
                json = JSON.parse(json);
                if (json.response) {
                    swal({
                        title: "¡Perfecto!",
                        text: "Pregunta eliminada conexito!",
                        type: "success",
                        confirmButtonColor: "#00c292",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    }, function() {
                        location.reload();
                    });
                } else {
                    swal("¡Error!", "No se pudo eliminar este registro.", "error");
                }
            });
    });
});
$(document).on("click", ".btnVerAlternativa", function() {
    let idAlternativa = $(this).attr('id-alternativa');
    $.post(siteUrl() + "/respuestas/getAlternativa", { id: idAlternativa })
        .done(function(json) {
            json = JSON.parse(json);
            $("#contenido-alternativa").html(json.contenido);
            $("#contenido-correcta").html(json.correcta);
            $("#mdlVerAlternativa").modal();
        });
});

$(document).on('click', 'tr.row-hover', function() {

    if ($(this).attr('active') != 'true') {
        $(this).attr('active', 'true');
        $(this).css('background-color', '#e2e2e2');
        $(this).next('tr').css('background-color', '#e2e2e2');
    } else {
        $(this).attr('active', 'false');
        $(this).css('background-color', '#fff');
        $(this).next('tr').css('background-color', '#fff');
    }
});
