let idEvaluacion;
let codigo;
let unidad = { nivel: 0, asignatura: 0 };

$(document).ready(function() {
    let table = $("#table-evaluaciones").DataTable({
        "displayLength": 6,
        "columnDefs": [{
            targets: -1,
            className: 'nowrap'
        }]
    });

    $(document).find("#table-evaluaciones_filter").remove();
    $(document).find("#table-evaluaciones_length").remove();

    $(document).on("keyup", "#search", function() {
        table.search($(this).val()).draw();
    });
});

$(document).on("click", ".btnCursos", function() {
    colegio = $(this).attr('id-colegio');

    window.location.href = siteUrl() + "/administrador/cursos/" + colegio;
});

$(document).on("click", ".btnAlumnos", function() {
    colegio = $(this).attr('id-colegio');
    curso   = $(this).attr('id-curso');

    window.location.href = siteUrl() + "/administrador/alumnos/" + colegio + "/" + curso;
});

$(document).on("click", "#btnModalCrearEvaluacion", function() {
    $("#mdlCrearEvaluacion").modal();
});
$(document).on("submit", "#frmCrearEvaluacion", function() {
    let data = $("#frmCrearEvaluacion").serialize();
    console.log(data);
    $.ajax({
        url: siteUrl() + "/evaluaciones/guardarEvaluacion",
        data: data,
        type: "post",
        success: function(json) {
            json = JSON.parse(json);
            if (json.response) {
                $.toast({
                    heading: '¡Perfecto!',
                    text: 'Datos ingresados exitosamente.',
                    position: 'top-right',
                    icon: 'success',
                    hideAfter: 2000,
                    stack: 2
                });
            } else {
                $.toast({
                    heading: '¡Error!',
                    text: 'Ocurrio un error al guardar los datos.',
                    position: 'top-right',
                    icon: 'error',
                    hideAfter: 2000,
                    stack: 2
                });
            }
            location.reload();
        }
    });
})
$(document).on("click", ".btnEditarEvaluacion", function() {
    idEvaluacion = $(this).attr('id-evaluacion');
    $.post(siteUrl() + "/evaluaciones/getEvaluacion", { id: idEvaluacion })
        .done(function(json) {
            json = JSON.parse(json);
            $("#codigo").val(json.codigo);
            $("#numero").val(json.numero);
            $("#nivel option[value='" + json.nivel + "'").attr('selected', true);
            $("#asignatura option[value='" + json.asignatura + "'").attr('selected', true);
            $("#tipo option[value='" + json.tipo + "'").attr('selected', true);

            unidad.nivel = json.nivel;
            unidad.asignatura = json.asignatura;
            $("#select-unidad-update").css('display', 'block');
            
            $("#mdlEditarEvaluacion").modal();
        });
})
$(document).on("submit", "#frmEditarEvaluacion", function() {
    let data = $("#frmEditarEvaluacion").serialize();
    $.ajax({
        url: siteUrl() + "/evaluaciones/editarEvaluacion/" + idEvaluacion,
        data: data,
        type: "post",
        success: function(json) {
            json = JSON.parse(json);
            if (json.response) {
                $.toast({
                    heading: '¡Perfecto!',
                    text: 'Datos actualizado exitosamente.',
                    position: 'top-right',
                    icon: 'success',
                    hideAfter: 2000,
                    stack: 2
                });
            } else {
                $.toast({
                    heading: '¡Error!',
                    text: 'Ocurrio un error al actualizar los datos.',
                    position: 'top-right',
                    icon: 'error',
                    hideAfter: 2000,
                    stack: 2
                });
            }
            location.reload();
        }
    });
})
$(document).on("click", "#btn-nuevo-colegio", function() {
    $('#modal-nuevo-colegio').modal();
});

$(document).on("click", ".btnPreguntas", function() {
    idEvaluacion = $(this).attr('id-evaluacion');
    codigo = $(this).attr('codigo');
    window.location.href = siteUrl() + "/administrador/preguntas?codigo=" + codigo + "&idEvaluacion=" + idEvaluacion;
});

$(document).on('change', '[name="nivel"]', function() {
    unidad.nivel = $(this).val();
    if (unidad.asignatura != 0 && unidad.nivel != 0) {
        $("#select-unidad").css('display', 'block');
        getSelectUnidad(unidad);
    }
});
$(document).on('change', '[name="asignatura"]', function() {
    unidad.asignatura = $(this).val();
    if (unidad.asignatura != 0 && unidad.nivel != 0) {
        $("#select-unidad").css('display', 'block');
        getSelectUnidad(unidad, "#select-unidad");
    }
});
$(document).on('click', '.btnVerEvaluacion', function() {
    let idEvaluacion = $(this).attr('id-evaluacion');
    let data = `${siteUrl()}/administrador/verEvaluacion/${idEvaluacion}`;
    $("#objectVistaEvaluacion").attr('data', data);
    let height = $(window).height();
    $("#objectVistaEvaluacion").css('height', `${(height - 200)}px`);
    $('#modalVerEvaluacion').modal();
});

let getSelectUnidad = (unidadJson, selector, selected = false) => {
    if (selected) unidadJson.selected = selected;
    $.post(`${siteUrl()}/evaluaciones/getSelectUnidad`, unidadJson)
        .done(json => {
            json = JSON.parse(json);
            $(selector).find('select').html(json.options);
        });
}