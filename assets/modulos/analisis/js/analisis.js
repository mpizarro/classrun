$(() => {
    let table = $("#analisis").DataTable({
        'responsive'    : true,
        'paging'        : true,
        'lengthMenu'    : [8],
        'language'       : {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
    $('#search').on('keyup', function() {
        table.search( this.value ).draw();
    });    $("#buscar").click(() => {
        let inicio = $("#fechaInicio").val();
        let termino = $("#fechaFin").val();
        let profesor = $("#profesor").val();

        window.location.href = `${ siteUrl() }/analisis/get?profesor=${ profesor }&inicio=${ inicio }&termino=${ termino }`;
    });
    $("#clear").click(() => {
        window.location.href = `${ siteUrl() }/analisis/get`;
    });
});