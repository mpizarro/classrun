let urlAlternativa = siteUrl() + "/respuestas/guardarAlternativa";
$(document).ready(() => {
    if (idAlternativa) {
        $.post(siteUrl() + "/respuestas/getAlternativa", { id: idAlternativa })
            .done(function(json) {
                json = JSON.parse(json);
                console.log(json);
                $("#title-page").html("Editar Alternativa");
                $("#btnGuardarAlternativa").html('<i class="fa fa-check"></i> Guardar');
                $(".editor").html(json.contenido);

                if (!$("#correcta").prop("checked") && json.is_correcta === "t") {
                    $("#correcta").click();
                }
            });
        urlAlternativa = siteUrl() + "/respuestas/actualizarAlternativa/" + idAlternativa + "/" + idPregunta;
    }

    tinymce.init({
        selector: "textarea.editor",
        theme: "modern",
        height: 140,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",

    });

    // Switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());
    });
});

$(document).on("submit", "#frmNuevaAlternativa", function() {
    if ($(".editor").val() !== "") {
        let data = $("#frmNuevaAlternativa").serializeObject();
        data.idPregunta = $(this).attr('id-pregunta');
        data.correcta = $("#correcta").prop('checked');
        let idEvaluacion = $(this).attr('id-evaluacion');
        let codigo = $(this).attr('codigo');
        $.ajax({
            url: urlAlternativa,
            data: { 'data': JSON.stringify(data) },
            type: 'post',
            success: function(json) {
                json = JSON.parse(json);
                if (json.response == true) {
                    $.toast({
                        heading: '¡Perfecto!',
                        text: 'Datos guardados corretamente.',
                        position: 'top-right',
                        icon: 'success',
                        hideAfter: 2000,
                        stack: 2
                    });
                    location.reload();
                } else if (json.response == "update") {
                    $.toast({
                        heading: '¡Perfecto!',
                        text: 'Datos guardados corretamente.',
                        position: 'top-right',
                        icon: 'success',
                        hideAfter: 2000,
                        stack: 2
                    });
                    window.location.href = siteUrl() + "/administrador/preguntas?codigo=" + codigo + "&idEvaluacion=" + idEvaluacion;
                } else if (json.response == "exist-correcta") {
                    $.toast({
                        heading: '¡Atención!',
                        text: 'Ya existe una alternativa correcta para la pregunta en contexto.',
                        position: 'top-right',
                        icon: 'warning',
                        hideAfter: 3000,
                        stack: 2
                    });
                } else {
                    $.toast({
                        heading: '¡Error!',
                        text: 'Ocurrio un error al guardar los datos.',
                        position: 'top-right',
                        icon: 'error',
                        hideAfter: 2000,
                        stack: 2
                    });
                }
            }
        });
    } else {
        $.toast({
            heading: '¡Atención!',
            text: 'El contenido de la alternativa no debe esta vacío.',
            position: 'top-right',
            icon: 'warning',
            hideAfter: 3000,
            stack: 2
        });
    }
});
$(document).on("click", "#btnAtrasPreguntas", function() {
    let idEvaluacion = $(this).attr("id-evaluacion");
    let codigo = $(this).attr("codigo");
    window.location.href = siteUrl() + "/administrador/preguntas?codigo=" + codigo + "&idEvaluacion=" + idEvaluacion;
});


$(document).on('click', '.mce-btn button[role="presentation"]', function() {
    let codigo = $("#frmNuevaAlternativa").attr('codigo');

    $(document).find('.mce-container.mce-window').css({ 'width': '80%', 'top': '50px', 'left': '10%', 'height': 'auto' });
    $(document).find('.mce-container.mce-panel.mce-foot, .mce-abs-layout').css({ 'width': '100%', 'text-align': 'center' });
    $(document).find('.mce-abs-layout').css({ 'height': '370px' });
    $(document).find('.mce-container-body > .mce-btn').css({ 'left': 'unset', 'position': 'relative', 'margin-left': '5px', 'margin-right': '5px' });
    $(document).find('.mce-container.mce-form.mce-abs-layout-item').css('width', '50%');

    if ($(document).find('#mce-content-img').length == 0) {
        $.post(`${siteUrl()}/evaluaciones/getImagenes`, { codigo: codigo })
            .done(json => {
                json = JSON.parse(json);
                const html = `<div id='mce-content-img' style='width: 50%; float: right;'>${json.data}</div>`;
                $(document).find('.mce-container.mce-form.mce-abs-layout-item').after(html);
            });
    } else {
        $.post(`${siteUrl()}/evaluaciones/getImagenes`, { codigo: codigo })
            .done(json => {
                json = JSON.parse(json);
                $(document).find('#mce-content-img').html(json.data);
            });
    }
});

$(document).on('click', '[add-img]', function() {
    let url = $(this).attr('src');
    $(document).find('.mce-textbox').val(url);
});