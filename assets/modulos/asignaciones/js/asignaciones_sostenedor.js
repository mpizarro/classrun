$(() => {
    var table = $('#table_asignaciones').DataTable({
        "bLengthChange" : false,
        "info": false,
        "dom": "lrtp",
        "pageLength": 10,
        "language": dt_language
    });

    $('#search').on('keyup', function () {
        table.search( this.value ).draw();
    });

    $('#table_asignaciones').removeClass('dataTable');
});