$(() => {
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Asignaciones por colegio'
        },
        xAxis: {
            categories: data.colegios,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'N° Asignaciones'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            color: '#FFAA39',
            name: 'En ejecución',
            data: data.ejecucion
    
        }, {
            color: '#52DD48',
            name: 'Finalizadas',
            data: data.finalizadas
    
        }]
    });
});