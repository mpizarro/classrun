let idAsignatura = '';
let idNivel = '';
let idTipo = '';

$(() => {
    let tables = $('#table-finalizadas, #table-ejecutadas').DataTable({
        "bLengthChange" : false,
        "info": false,
        "dom": "lrtp",
        "pageLength": 5,
        "language": dt_language
    });

    $('#search').on('keyup', function () {
        tables.search( this.value ).draw();
    });

    $(document).on('click', '[btn-alumnos]', function() {
        const idCurso = $(this).attr('id-curso');
        const idEvaluacion = $(this).attr('id-evaluacion');
        const idAsignacion = $(this).attr('id-asignacion');

        $.post(`${ siteUrl() }/curso/getDataPlanillaAlumno`, {idCurso, idEvaluacion, idAsignacion})
        .done(response => {
            response = JSON.parse(response);
            let html = '';

            if (response.curso)
                $('#modal-curso-label').html(`<i class="fa fa-file-text-o"></i> ALUMNOS - ${ response.curso.curso } ${ response.curso.letra }`.toUpperCase());

            if (response.evaluacion)
                $('#modal-curso .modal-header p').html(`${ response.evaluacion.tipo } ${ response.evaluacion.nivel } ${ response.evaluacion.asignatura }`.toUpperCase());

            if (response.alumnos.length > 0) {
                for (const alumno of response.alumnos) {
                    html += `<tr>
                        <td><input type="checkbox" ${ alumno.estado != 3 ? 'checked' : '' } switch-habilitar id-alumno="${ alumno.id }" id-asignacion="${ idAsignacion }" class="js-switch" data-color="#0098ff" data-size="small"></td>
                        <td>${ alumno.rut }</td>
                        <td>${ alumno.nombre }</td>
                        <td>
                            <div class="progress progress-sm" style="width: 70%; display: inline-flex;">
                                <div class="progress-bar progress-bar-info" style="width: ${ alumno.avance }%;" role="progressbar"> <span class="sr-only"></span> </div>
                            </div>
                            ${ alumno.avance }%
                        </td>
                        <td>${ alumno.tiempo }</td>
                    </tr>`;
                }
            }
            $('#table-curso tbody').html(html);

            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
                if(!$(this).prop('checked')) $(this).siblings('span.switchery').css({'background-color': 'rgb(223, 223, 223)'});
            });

            $('#modal-curso').modal();
        });
    });

    $(document).on('click', '[btn-ver-evaluacion]', function() {
        const idEvaluacion = $(this).attr('id-evaluacion');
        const idAsignacion = $(this).attr('id-asignacion');
        $('#modal-evaluacion object').attr('data', `${ siteUrl() }/administrador/verEvaluacion/${ idEvaluacion }/PERSONAL/${idAsignacion}`);
        $('#modal-evaluacion').modal();
    });

    $(document).on('click', '[btn-actualizar]', function() {
        const idAsignacion = $(this).attr('id-asignacion');

        $.post(`${ siteUrl() }/asignaciones/getDataTabulacion`, {idAsignacion})
        .done(response => {
            response = JSON.parse(response);
            buildPlanillaTabulacion(response, idAsignacion);
            $.toast({
                heading: '¡Bien!',
                text: 'Planilla actualizada',
                position: 'top-right',
                icon: 'success',
                hideAfter: 2500,
                stack: 2
            });
        });
    });

    $(document).on('click', '[btn-tabulacion]', function() {
        const idAsignacion = $(this).attr('id-asignacion');
        $('[btn-actualizar], [btn-reiniciar]').attr('id-asignacion', idAsignacion);

        $.post(`${ siteUrl() }/asignaciones/getDataTabulacion`, {idAsignacion})
        .done(response => {
            response = JSON.parse(response);
            buildPlanillaTabulacion(response, idAsignacion);
            $('#modal-tabulacion').modal();
        });
    });

    $(document).on('click', '[btn-reiniciar]', function() {
        const idAsignacion = $(this).attr('id-asignacion');
        reiniciarHojaRespuesta(idAsignacion);
    });

    $(document).on('keyup', '.input-alt', function(event) {
        let code = event.keyCode;
        let text = code != 8 ? String.fromCharCode(code) : null;
        let idAlumno = $(this).attr('id-alumno');
        let idPregunta = $(this).attr('id-pregunta');
        const leters = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','Ñ','O','P','Q','R','S','T','U','V','W','X','Y','Z',null];

        if (code == 37) $(this).parent('td').prev().find('.input-alt').focus();
        else if (code == 39) $(this).parent('td').next().find('.input-alt').focus();
        else if (code == 38) $(this).parent('td').parent('tr').prev().find(`td > input[id-pregunta="${idPregunta}"]`).focus();
        else if (code == 40) $(this).parent('td').parent('tr').next().find(`td > input[id-pregunta="${idPregunta}"]`).focus();

        if (leters.indexOf(text) != -1) {
            let data = {
                'id_alumno': idAlumno,
                'id_pregunta': idPregunta,
                'id_asignacion': $(this).attr('id-asignacion'),
                'respuesta': text,
                'origen': 'PERSONAL'
            }
            $.post(`${ siteUrl() }/Asignaciones/setAlternativa`, data)
            .done(json => {
                json = JSON.parse(json);
                if (!json.response) {
                    let letraIncorrecta = $(document).find(`#${ idAlumno }-${ idPregunta }`).val();
                    $(document).find(`#${ idAlumno }-${ idPregunta }`).val(null)
                    swal({type:"error", title:`"${ letraIncorrecta.toUpperCase() }", no se admite como alternativa dentro de esta pregunta.`});
                } else {
                    $(document).find(`#${ idAlumno }-${ idPregunta }`).removeClass('correcta incorrecta');
                    $(document).find(`#${ idAlumno }-${ idPregunta }`).addClass(`${json.class}`);
                }
            });
            $(this).parent('td').next().children('.input-alt').focus();
        }
    });

    $(document).on('click', '[btn-finalizar]', function() {
        const idAsignacion = $(this).attr('id-asignacion');
        swal({
            title : "¿Está seguro que desea finalizar la evaluación?",
            type : "warning",
            showCancelButton : true,
            confirmButtonText : "Finalizar",
            cancelButtonText : "Cancelar",
            closeOnConfirm : false
        },(isConfirm) => {
            if(isConfirm) {
                $.ajax ({
                    url : `${siteUrl()}/asignaciones/finalizarAsignacion/${idAsignacion}`,
                    dataType: 'json',
                    success : (json) => {
                        if(json.status) {
                            swal ({
                                title : "Se ha finalizado la evaluación",
                                type : "success",
                                allowEscapeKey : false
                            },()=> window.location.reload());
                        } else {
                            swal("No se ha finalizado la evaluación", "Contácte a soporte técnico", "error");
                        }
                    }
                });
            } else {
                return false;
            }
        });
    });

    $(document).on('click', '[btn-ejecutar]', function() {
        const idCurso = $(this).attr('id-curso');
        const idAsignacion = $(this).attr('id-asignacion');

        $.post(`${ siteUrl() }/curso/cursoHasAsignacion/${ idCurso }`)
        .done(function(json) {
            json = JSON.parse(json);
            // if (!json.has_asignacion) {
                swal({
                    title : "¿Está seguro que desea ejecutar la evaluación?",
                    text : "Si existe otra en ejecución se finalizará",
                    type : "warning",
                    showCancelButton : true,
                    confirmButtonText : "Ejecutar",
                    cancelButtonText : "Cancelar",
                    closeOnConfirm : false
                },(isConfirm) => {
                    if(isConfirm) {
                        $.ajax ({
                            url : `${siteUrl()}/asignaciones/ejecutarAsignacion/${idAsignacion}`,
                            dataType: 'json',
                            success : (json)=> {
                                if(json.status) {
                                    swal ({
                                        title : "Se ha ejecutado la prueba",
                                        type : "success",
                                        allowEscapeKey : false
                                    },()=> {
                                        window.location.reload();
                                    });
                                } else {
                                    swal("No se ha ejecutado la prueba", "Contácte a soporte técnico", "error");
                                }
                            }
                        });
                    } else {
                        return false;
                    }
                });
            // } else {
            //     swal("¡No puede ejecutar prueba!", "Usted tiene una prueba en ejecución y por lo mismo no puede ejecutar una nueva", "warning");
            // }
        });
    });

    $(document).on('change', '[switch-habilitar]', function() {
        let data = {
            'id_asignacion': $(this).attr('id-asignacion'),
            'id_alumno': $(this).attr('id-alumno')
        }

        if ($(this).prop('checked')) {
            $.ajax({
                url: `${ siteUrl() }/asignaciones/renovarAsignacionesUsuario`,
                data: data,
                type: 'post',
                success(json) {
                    json = JSON.parse(json);
                    if (json.response) swal('Perfecto!','El alumno ha sido habilitado.', 'success');
                    else swal('Perfecto!','ocurrió un error al habilitar al alumno.', 'error');
                }
            });
        } else {
            $.ajax({
                url: `${ siteUrl() }/asignaciones/deshabilitarAsignacionesUsuario`,
                data: data,
                type: 'post',
                success(json) {
                    json = JSON.parse(json);
                    if (json.response) swal('Perfecto!','El alumno ha sido deshabilitado.', 'success');
                    else swal('Perfecto!','ocurrió un error al deshabilitar al alumno.', 'error');
                }
            });
            $(this).siblings('span.switchery').css({'background-color': 'rgb(223, 223, 223)'});
        }
    });

    $(document).on('change', '#filtro-asignatura', function() {
        idAsignatura = $(this).val();
    });

    $(document).on('change', '#filtro-nivel', function() {
        idNivel = $(this).val();
    });

    $(document).on('change', '#filtro-tipo', function() {
        idTipo = $(this).val();
    });

    $(document).on('click', '#filter', function() {
        window.location.href = `${ siteUrl() }/asignaciones/personal?asignatura=${ idAsignatura }&nivel=${ idNivel }&tipo=${ idTipo }`;
    });

    $(document).on('click', '#clean-filter', function() {
        window.location.href = `${ siteUrl() }/asignaciones/personal`;
    });
});

const buildPlanillaTabulacion = (response, idAsignacion) => {
    $("#detalle-evaluacion").html(`${ response.data.tipo } ${ response.data.curso } "${ response.data.letra }" ${ response.data.asignatura }`.toUpperCase());
    let html = `<table class="table"><thead style="background-color:#56b2bf;color:#fff;"><tr><th style="width: 200px;text-align: left;font-weight: bold;border-bottom: 3px solid #56b2bf;color:#fff;"> Nombre Alumno</th>`;

    if (response.preguntas.length > 0)
        for (const pregunta of response.preguntas)
            html += `<th style="font-weight: bold;border-bottom: 3px solid #56b2bf;color:#fff;">P${ pregunta.orden < 10 ? '0'+pregunta.orden : pregunta.orden }</th>`;

    html += `</tr></thead><tbody>`;
    if (response.alumnos.length > 0)
        for (const alumno of response.alumnos) {
            html += `<tr>
                <td style="text-align: left;">${ alumno.nombre } ${ alumno.apellido }</td>`;
            if (response.preguntas.length > 0)
                for (const pregunta of response.preguntas)
                    html += `<td><input type="text" class="input-alt" id="${ alumno.id }-${ pregunta.id }" id-pregunta="${ pregunta.id }" id-alumno="${ alumno.id }" id-asignacion="${ idAsignacion }" maxlength="1"></td>`;

            html += `</tr>`;
        }

    html += `</tbody></table>`;
    $('#tabla-tabulacion').html(html);

    if (response.dataRespuesta)
        for (const resp of response.dataRespuesta) {
            $(`#${ resp.alumno_id }-${ resp.pregunta_id }`).val(`${ resp.letra }`.toUpperCase());
            $(`#${ resp.alumno_id }-${ resp.pregunta_id }`).removeClass('correcta incorrecta');
            $(`#${ resp.alumno_id }-${ resp.pregunta_id }`).addClass(`${ resp.class }`);
        }
}

const reiniciarHojaRespuesta = idAsignacion => {
	swal({
		title: "¿Está seguro que desea reiniciar la evaluación?",
		text: "Si reinicia la evaluación se eliminaran todas las respuestas de los alumnos en esta evaluación.",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#db2a2a",
		confirmButtonText: "Reiniciar",
		cancelButtonText: "Cancelar",
		closeOnConfirm: false
	}, function(){
        console.log(idAsignacion);

		$.ajax({
			url: `${ siteUrl() }/Asignaciones/eliminarHojaRespuesta`,
			data: {idAsignacion},
			type: 'post',
			success(json) {
				json = JSON.parse(json);
				if (json.response) swal("Perfecto!", "Evaluación reiniciada!", "success");
				else swal("Error!", "No se logro reiniciar la evaluación", "error");

                $.post(`${ siteUrl() }/asignaciones/getDataTabulacion`, {idAsignacion})
                .done(response => {
                    response = JSON.parse(response);
                    buildPlanillaTabulacion(response, idAsignacion);
                    $('#modal-tabulacion').modal();
                });
			}
		});
	});
}
