let activeNumberPaginador = 0, dataEvaluacion, arrLetras = [];
const asignacionesFinalizadas = data.asignacionesFinalizadas, asignacionesEjecutadas = data.asignacionEjecutada;

$(document).ready(()=> {
	getLetrasDisponibles(data.asignacionesFinalizadas);
	getLetrasDisponibles(data.asignacionEjecutada);
	buildFilterLetras();

	if(data.asignacionEjecutada) {
		renderizarEjecutadas(data.asignacionEjecutada);
	} else {
		$('.ejecucion').hide();
		$('#title-ejecucion').hide();
	}
	if(data.asignacionesFinalizadas) {
		data.asignacionesFinalizadas = dividirEnArrays(data.asignacionesFinalizadas);
		renderizarFinalizadas(0);
		renderizarPaginador();
	}
	$(document).keyup((event)=> {
		if(event.keyCode == 39) {
			forwardPaginador();
		} else if(event.keyCode == 37) {
			backPaginador();
		}
	});
	$('.letrasFilter').click(function() {
		if(checkIfExisteFiltro('.letrasFilter')){
			cleanFilter('.letrasFilter');
		}
		addClassFiltro(this);
		let newAsignaciones = asignacionesFinalizadas;
		let newAsignacionesEjec = asignacionesEjecutadas;
		if($(this).children('input').prop('checked')) {
			newAsignaciones = getFiltroLetra(newAsignaciones);
			newAsignacionesEjec = getFiltroLetra(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.tipoFilter')) {
			newAsignaciones = getFiltroTipo(newAsignaciones);
			newAsignacionesEjec = getFiltroTipo(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.numeroFilter')) {
			newAsignaciones = getFiltroNumero(newAsignaciones);
			newAsignacionesEjec = getFiltroNumero(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.asignaturaFilter')) {
			newAsignaciones = getFiltroAsignatura(newAsignaciones);
			newAsignacionesEjec = getFiltroAsignatura(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.nivelFilter')) {
			newAsignaciones = getFiltroNivel(newAsignaciones);
			newAsignacionesEjec = getFiltroNivel(newAsignacionesEjec);
		}
		data.asignacionesFinalizadas = dividirEnArrays(newAsignaciones);
		data.asignacionEjecutada = dividirEnArrays(newAsignacionesEjec);
		renderizarPaginador();
		renderizarFinalizadas(0);
		renderizarEjecutadas(data.asignacionEjecutada.length > 0 ? data.asignacionEjecutada[0] : false);
	});
	$('.tipoFilter').click(function() {
		if(checkIfExisteFiltro('.tipoFilter')){
			cleanFilter('.tipoFilter');
		}
		addClassFiltro(this);
		let newAsignaciones = asignacionesFinalizadas;
		let newAsignacionesEjec = asignacionesEjecutadas;
		if($(this).children('input').prop('checked')) {
			newAsignaciones = getFiltroTipo(newAsignaciones);
			newAsignacionesEjec = getFiltroTipo(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.numeroFilter')) {
			newAsignaciones = getFiltroNumero(newAsignaciones);
			newAsignacionesEjec = getFiltroNumero(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.asignaturaFilter')) {
			newAsignaciones = getFiltroAsignatura(newAsignaciones);
			newAsignacionesEjec = getFiltroAsignatura(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.nivelFilter')) {
			newAsignaciones = getFiltroNivel(newAsignaciones);
			newAsignacionesEjec = getFiltroNivel(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.letrasFilter')) {
			newAsignaciones = getFiltroLetra(newAsignaciones);
			newAsignacionesEjec = getFiltroLetra(newAsignacionesEjec);
		}
		data.asignacionesFinalizadas = dividirEnArrays(newAsignaciones);
		data.asignacionEjecutada = dividirEnArrays(newAsignacionesEjec);

		renderizarPaginador();
		renderizarFinalizadas(0);
		renderizarEjecutadas(data.asignacionEjecutada.length > 0 ? data.asignacionEjecutada[0] : false);
	});
	$('.numeroFilter').click(function() {
		if(checkIfExisteFiltro('.numeroFilter')){
			cleanFilter('.numeroFilter');
		}
		addClassFiltro(this);
		let newAsignaciones = asignacionesFinalizadas;
		let newAsignacionesEjec = asignacionesEjecutadas;
		if($(this).children('input').prop('checked')) {
			newAsignaciones = getFiltroNumero(newAsignaciones);
			newAsignacionesEjec = getFiltroNumero(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.tipoFilter')) {
			newAsignaciones = getFiltroTipo(newAsignaciones);
			newAsignacionesEjec = getFiltroTipo(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.asignaturaFilter')) {
			newAsignaciones = getFiltroAsignatura(newAsignaciones);
			newAsignacionesEjec = getFiltroAsignatura(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.nivelFilter')) {
			newAsignaciones = getFiltroNivel(newAsignaciones);
			newAsignacionesEjec = getFiltroNivel(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.letrasFilter')) {
			newAsignaciones = getFiltroLetra(newAsignaciones);
			newAsignacionesEjec = getFiltroLetra(newAsignacionesEjec);
		}
		data.asignacionesFinalizadas = dividirEnArrays(newAsignaciones);
		data.asignacionEjecutada = dividirEnArrays(newAsignacionesEjec);
		renderizarPaginador();
		renderizarFinalizadas(0);
		renderizarEjecutadas(data.asignacionEjecutada.length > 0 ? data.asignacionEjecutada[0] : false);
	});
	$(".asignaturaFilter").click(function() {
		if(checkIfExisteFiltro('.asignaturaFilter')){
			cleanFilter('.asignaturaFilter');
		}
		addClassFiltro(this);
		let newAsignaciones = asignacionesFinalizadas;
		let newAsignacionesEjec = asignacionesEjecutadas;
		if($(this).children('input').prop('checked')) {
			newAsignaciones = getFiltroAsignatura(newAsignaciones);
			newAsignacionesEjec = getFiltroAsignatura(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.tipoFilter')) {
			newAsignaciones = getFiltroTipo(newAsignaciones);
			newAsignacionesEjec = getFiltroTipo(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.numeroFilter')) {
			newAsignaciones = getFiltroNumero(newAsignaciones);
			newAsignacionesEjec = getFiltroNumero(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.nivelFilter')) {
			newAsignaciones = getFiltroNivel(newAsignaciones);
			newAsignacionesEjec = getFiltroNivel(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.letrasFilter')) {
			newAsignaciones = getFiltroLetra(newAsignaciones);
			newAsignacionesEjec = getFiltroLetra(newAsignacionesEjec);
		}
		data.asignacionesFinalizadas = dividirEnArrays(newAsignaciones);
		data.asignacionEjecutada = dividirEnArrays(newAsignacionesEjec);
		renderizarPaginador();
		renderizarFinalizadas(0);
		renderizarEjecutadas(data.asignacionEjecutada.length > 0 ? data.asignacionEjecutada[0] : false);
	});
	$(".nivelFilter").click(function() {
		if(checkIfExisteFiltro('.nivelFilter')){
			cleanFilter('.nivelFilter');
		}
		addClassFiltro(this);
		let newAsignaciones = asignacionesFinalizadas;
		let newAsignacionesEjec = asignacionesEjecutadas;
		if($(this).children('input').prop('checked')) {
			newAsignaciones = getFiltroNivel(newAsignaciones);
			newAsignacionesEjec = getFiltroNivel(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.tipoFilter')) {
			newAsignaciones = getFiltroTipo(newAsignaciones);
			newAsignacionesEjec = getFiltroTipo(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.numeroFilter')) {
			newAsignaciones = getFiltroNumero(newAsignaciones);
			newAsignacionesEjec = getFiltroNumero(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.asignaturaFilter')) {
			newAsignaciones = getFiltroAsignatura(newAsignaciones);
			newAsignacionesEjec = getFiltroAsignatura(newAsignacionesEjec);
		}
		if(checkIfExisteFiltro('.letrasFilter')) {
			newAsignaciones = getFiltroLetra(newAsignaciones);
			newAsignacionesEjec = getFiltroLetra(newAsignacionesEjec);
		}
		data.asignacionesFinalizadas = dividirEnArrays(newAsignaciones);
		data.asignacionEjecutada = dividirEnArrays(newAsignacionesEjec);
		renderizarPaginador();
		renderizarFinalizadas(0);
		renderizarEjecutadas(data.asignacionEjecutada.length > 0 ? data.asignacionEjecutada[0] : false);
	});
	$('#btn-limpiarFiltros').click(()=> {
		data.asignacionesFinalizadas = dividirEnArrays(asignacionesFinalizadas);
		data.asignacionEjecutada = dividirEnArrays(asignacionesEjecutadas);

		$('.activeFiltro').each((i, e) => {
			$(e).removeClass('activeFiltro');
			$(e).children('input').prop('checked', false);
		});

		renderizarPaginador();
		renderizarFinalizadas(0);
		renderizarEjecutadas(data.asignacionEjecutada[0]);
	});
	$(document).on('change', '[select-profesores]', function() {
		let idAsignacion = $(this).attr('id-asignacion');
		let idProfesor = $(this).val();

		$.post(`${ siteUrl() }/Asignaciones/setAsignacionProfesor`, {idProfesor, idAsignacion})
		.done(function(json) {
			json = JSON.parse(json);
			if (json.response) swal('Perfecto!', 'Asignación actualizada.', 'success');
		});
	});
});
$(document).on('change', '[switch-habilitar]', (event) => {
	let data = {
		'id_asignacion': $(event.target).attr('id-asignacion'),
		'id_alumno': $(event.target).attr('id-alumno')
	}
	if ($(event.target).prop('checked')) {
		$.ajax({
			url: `${ siteUrl() }/asignaciones/renovarAsignacionesUsuario`,
			data: data,
			type: 'post',
			success(json) {
				json = JSON.parse(json);
				if (json.response) swal('Perfecto!','El alumno ha sido habilitado.', 'success');
				else swal('Perfecto!','ocurrió un error al habilitar al alumno.', 'error');
			}
		});
	} else {
		$.ajax({
			url: `${ siteUrl() }/asignaciones/deshabilitarAsignacionesUsuario`,
			data: data,
			type: 'post',
			success(json) {
				json = JSON.parse(json);
				if (json.response) swal('Perfecto!','El alumno ha sido deshabilitado.', 'success');
				else swal('Perfecto!','ocurrió un error al deshabilitar al alumno.', 'error');
			}
		});
		$(event.target).siblings('span.switchery').css({'background-color': 'rgb(223, 223, 223)'});
	}
});
$(document).on('click', '#btn-actualizar-planilla', (event) => {
	let idAsignacion = $(event.target).attr('id-asignacion');
	setPlanillaTabulacion(idAsignacion);
});
$(document).on('click', '#btn-reiniciar', (event) => {
	let idAsignacion = $(event.target).attr('id-asignacion');
	reiniciarHojaRespuesta(idAsignacion);
});
$(document).on('keyup', '.input-alt', (event) => {
	let code = event.keyCode;
	let text = code != 8 ? String.fromCharCode(code) : null;
	let idPregunta = $(event.target).attr('id-pregunta');
	const leters = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','Ñ','O','P','Q','R','S','T','U','V','W','X','Y','Z',null];

	if (code == 37) $(event.target).parent('td').prev().find('.input-alt').focus();
	else if (code == 39) $(event.target).parent('td').next().find('.input-alt').focus();
	else if (code == 38) $(event.target).parent('td').parent('tr').prev().find(`td > input[id-pregunta="${idPregunta}"]`).focus();
	else if (code == 40) $(event.target).parent('td').parent('tr').next().find(`td > input[id-pregunta="${idPregunta}"]`).focus();

	if (leters.indexOf(text) != -1) {
		let data = {
			'id_alumno': $(event.target).attr('id-alumno'),
			'id_pregunta': idPregunta,
			'id_asignacion': $(event.target).attr('id-asignacion'),
			'respuesta': text,
			'origen': 'AEDUC'
		}
		$.post(`${ siteUrl() }/Asignaciones/setAlternativa`, data)
		.done(json => {
			json = JSON.parse(json);
			if (!json.response) {
				swal({type:"error", title:"No se logró guardar la respuesta!"});
			} else {
				$(document).find(`#${data.id_asignacion}-${data.id_alumno}-${data.id_pregunta}`).removeClass('omitida correcta incorrecta');
				$(document).find(`#${data.id_asignacion}-${data.id_alumno}-${data.id_pregunta}`).addClass(`${json.class}`);
			}
		});
		$(event.target).parent('td').next().children('.input-alt').focus();
	}
});
const reiniciarHojaRespuesta = idAsignacion => {
	swal({
		title: "¿Está seguro que desea reiniciar la evaluación?",
		text: "Si reinicia la evaluación se eliminaran todas las respuestas de los alumnos en esta evaluación.",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#db2a2a",
		confirmButtonText: "Reiniciar",
		cancelButtonText: "Cancelar",
		closeOnConfirm: false
	}, function(){
		$.ajax({
			url: `${ siteUrl() }/Asignaciones/eliminarHojaRespuesta`,
			data: {idAsignacion},
			type: 'post',
			success(json) {
				json = JSON.parse(json);
				if (json.response) swal("Perfecto!", "Evaluación reiniciada!", "success");
				else swal("Error!", "No se logro reiniciar la evaluación", "error");

				setPlanillaTabulacion(idAsignacion);
			}
		});
	});
}
var addClassFiltro = (e) => {
	if($(e).hasClass('activeFiltro')) {
		$(e).removeClass('activeFiltro');
		$(e).children('input').prop('checked', false);
	} else {
		$(e).addClass('activeFiltro');
		$(e).children('input').prop('checked', true);
	}
}
var getFiltroAsignatura = (asignaciones) => {
	const newAsignaciones = new Array();
	const asignatura = $(".asignaturaFilter.activeFiltro").attr('data-asignatura');
	if (asignaciones){
		asignaciones.forEach((asignacion) => {
			if(asignacion.alias == asignatura) {
				newAsignaciones.push(asignacion);
			}
		});
	}

	return newAsignaciones;
}
var getFiltroLetra = (asignaciones) => {
	const searcher = $(".letrasFilter.activeFiltro").attr('letra');
	const newAsignaciones = new Array();
	if (asignaciones) {
		asignaciones.forEach((asignacion) => {
			if(asignacion.letra == searcher) {
				newAsignaciones.push(asignacion);
			}
		});
	}

	return newAsignaciones;
};
var getFiltroNumero = (asignaciones) => {
	const searcher = $(".numeroFilter.activeFiltro").attr('num');
	const newAsignaciones = new Array();
	if (asignaciones) {
		asignaciones.forEach((asignacion) => {
			if(asignacion.num == searcher) {
				newAsignaciones.push(asignacion);
			}
		});
	}

	return newAsignaciones;
};
var getFiltroTipo = (asignaciones) => {
	const tipo = $(".tipoFilter.activeFiltro").children('h6').text().toUpperCase();
	let searcher = "";
	switch(tipo) {
		case 'SIMCE':
			searcher = 'SIMCE';
			break;
		case 'COBERTURA':
			searcher = 'COBERTURA';
			break;
		case 'PME':
			searcher = 'PME';
			break;
		case 'PSU':
			searcher = 'PSU';
			break;
		case 'UNIDAD':
			searcher = 'UNIDAD';
			break;
		default:break;
	}
	const newAsignaciones = new Array();

	if (asignaciones)
	{
		asignaciones.forEach((asignacion) => {
			if(asignacion.tipo.toUpperCase() == searcher) {
				newAsignaciones.push(asignacion);
			}
		});
	}

	return newAsignaciones;
};
var getFiltroNivel = (asignaciones) => {
	const newAsignaciones = new Array();
	const nivel = $(".nivelFilter.activeFiltro").attr('data-nivel');
	if (asignaciones)
	{
		asignaciones.forEach((asignacion) => {
			if(asignacion.nivelAlias == nivel) {
				newAsignaciones.push(asignacion);
			}
		});
	}

	return newAsignaciones;
};
var cleanFilter = (classFilter) => {
	const e = $(classFilter);
	$(e).removeClass('activeFiltro');
	$(e).children('input').prop('checked', false);
};
var checkIfExisteFiltro = (filtro, element) => {
	let b = false;
	$(filtro).each((index, e)=> {
		if($(e).children('input').prop('checked')) {
			b = true;
		}
	});
	return b;
};
var finalizarAsignacion = (idAsignacion) => {
	swal({
	    title : "¿Está seguro que desea finalizar la evaluación?",
	    type : "warning",
	    showCancelButton : true,
	    confirmButtonText : "Finalizar",
	    cancelButtonText : "Cancelar",
	    closeOnConfirm : false
	},(isConfirm) => {
		if(isConfirm) {
			$.ajax ({
				url : `${siteUrl()}/asignaciones/finalizarAsignacion/${idAsignacion}`,
				dataType: 'json',
				success : (json) => {
					if(json.status) {
					    swal ({
					        title : "Se ha finalizado la evaluación",
					        type : "success",
					        allowEscapeKey : false
					    },()=> window.location.reload());
					} else {
						swal("No se ha finalizado la evaluación", "Contácte a soporte técnico", "error");
					}
				}
			});
		} else {
			return false;
		}
	});
};
var ejecutarAsignacion = (idAsignacion, idCurso) => {
	$.post(`${ siteUrl() }/curso/cursoHasAsignacion/${ idCurso }`)
	.done(function(json) {
		json = JSON.parse(json);
		// if (!json.has_asignacion) {
			swal({
				title : "Ejecutar evaluación finalizada",
				text : "¿Está seguro que desea ejecutar la evaluación?",
				type : "warning",
				showCancelButton : true,
				confirmButtonText : "Ejecutar",
				cancelButtonText : "Cancelar",
				closeOnConfirm : false
			},(isConfirm) => {
				if(isConfirm) {
					$.ajax ({
						url : `${siteUrl()}/asignaciones/ejecutarAsignacion/${idAsignacion}`,
						dataType: 'json',
						success : (json)=> {
							if(json.status) {
								swal ({
									title : "Se ha ejecutado la prueba",
									type : "success",
									allowEscapeKey : false
								},()=> {
									window.location.reload();
								});
							} else {
								swal("No se ha ejecutado la prueba", "Contácte a soporte técnico", "error");
							}
						}
					});
				} else {
					return false;
				}
			});
		// } else {
		// 	swal("¡No puede ejecutar prueba!", "Usted tiene una prueba en ejecución y por lo mismo no puede ejecutar una nueva", "warning");
		// }
	});
};
var getNumerosContainer = (num) => {
	let returnHtml = "";
	for (let i = 1; i <= 6; i++) {
		returnHtml += `<div class="numero ${num <= i ? 'noActiveBefore' : ''} ${num < i ? 'noActiveNumber' : ''} ${num == i ? 'activeNumber' : ''} ${i == 6 ? 'lastNumber' : ''}">
                	<span>${num > i ? '✔' : i}</span>
		        </div>`;
	}
	return returnHtml;
};
var renderizarFinalizadas = (index, element = false) => {
	$('#asignacionContainer').html('');
	let count=0;
	let codAsignatura='';
	if(data.asignacionesFinalizadas.length > 0)
	{
		data.asignacionesFinalizadas[index].forEach((asignacion) => {
			console.log(asignacionesFinalizadas[count].codigo);
			codAsignatura=asignacionesFinalizadas[count].codigo;
			codAsignatura=codAsignatura.substr(0,3);
			console.log(codAsignatura);
			count=count+1;

			const {tipo, nivel, asignatura, curso, letra, idCurso, idPrueba, id, codigo} = asignacion;
			const asignaturaName = asignaturaByCodigo(codigo);
			const textVerEvaluacion = `${tipo} ${nivel} ${asignatura}`;
			const textCurso = `${curso.toUpperCase()} ${letra.toUpperCase()}`;

			let url = '';
			switch (asignacion.tipo.toUpperCase()) {
				case 'SIMCE': url = `${ siteUrl() }/Reporte/simce/${ id }`; break;
				case 'PME': url = `${ siteUrl() }/Reporte/pme/${ id }`; break;
				case 'COBERTURA': url = `${ siteUrl() }/Reporte/cobertura/${ id }`; break;
				case 'UNIDAD': url = `${ siteUrl() }/Reporte/unidad/${ id }`; break;
				default: url = 'javascript:void(0)'; break;
			}

			let html = `
			    <div class="asignacionBackground finalizada ${asignacion.alias} ">

			        <div class="asignacionContainer row">
			            <div class="col-md-2" >
					        <h5 no-margin-bottom><strong>${asignacion.tipo.toUpperCase()} </strong></h5>
					        <div>
					            <div class="p-dinamic">
					                <p no-margin-bottom>${asignacion.nivel}</p>
					                <hr class="hr-home">
					                <p no-margin-bottom class="pCurso">${asignacion.curso} ${asignacion.letra}</p>
					            </div>
					        </div>
			            </div>
			            <div class="col-md-4  ">
			                <div class="title-asignatura">
			                    <h5>${asignaturaName}</h5>
			                </div>
			                <div margin-top>
			                    <div class="numerosContainer">
			                    	${ getNumerosContainer(asignacion.num) }
			                    </div>
			                </div>
						</div>`;
					if (perfil == 7) {
						html += `<div class="col-md-2" style="display: flex; justify-content: center;">
							<p style="margin-bottom: 0;">Profesor Asignado</p>
							${ buildSelectProfesores(asignacion.usuario_id, asignacion.id) }
						</div>`;
					}
					html += `<div class="col-md-${ perfil != 7 ? 4 : 2 } icons-number">
							<div>
								<a class="icon-number" title="Descargar Informe Estadístico" href='${ url }' target='_blank'>
									<i class="fa fa-cloud-download"></i>
								</a>
								<div class="icon-number" title="Listado de alumnos" onclick="
									activatePlanillaUsuarios('finalizada', '${textVerEvaluacion}', \`${textCurso}\`, ${idCurso}, ${idPrueba}, ${id})
								">
			                        <i class="fa fa-users"></i>
			                    </div>
			                    <div class="icon-number" title="Ver prueba" onclick="activateViewPruebaModal('finalizada' ,${asignacion.idPrueba}, '${textVerEvaluacion}', '${asignacion.codigo}', '${asignacion.id}')">
			                        <i class="ti-eye"></i>
			                    </div>
			                </div>
			            </div>
			            <div class="col-md-2 icons-number">
			                <button class="btn btn-ejecutar" onclick="ejecutarAsignacion(${asignacion.id}, ${ idCurso })">
			                    Ejecutar
			                </button>
			            </div>
			        </div>
			    </div>`;
			$('#asignacionContainer').append(html);
		});
		if(element) {
			activeNumberPaginador = $(element).attr('data-index');
			$('#paginador li.active').each((i, e)=> {
				$(e).removeClass('active');
			});
			$(element).addClass('active');
		}
	} else {
		$('#asignacionContainer').html('<div class="alert alert-warning"> <code>Oops!</code> ¡No existen asignaciones de este tipo finalizadas!</div>');
	}
};
const renderizarEjecutadas = (asignaciones) => {
	let html = '';
	if (asignaciones) {
		for (const asignacion of asignaciones) {
			const {tipo, nivel, curso, letra, asignatura, idCurso, idPrueba, codigo} = asignacion;
			const asignaturaName = asignaturaByCodigo(codigo);
			const textVerEvaluacion = `${tipo} ${nivel} ${asignatura}`;
			const textCurso = `${curso.toUpperCase()} ${letra.toUpperCase()}`;
			html += `<div class="asignacionBackground m-t-5 ejecucion ${ asignacion.tipo }">
				<div class="asignacionContainer row">
					<div class="col-md-2">
						<h5 no-margin-bottom><strong>${asignacion.tipo.toUpperCase()}</strong></h5>
						<div>
							<div class="p-dinamic">
								<p no-margin-bottom>${asignacion.nivel}</p>
								<hr class="hr-home">
								<p no-margin-bottom class="pCurso">${asignacion.curso} ${asignacion.letra}</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="title-asignatura">
							<h5>${asignaturaName}</h5>
						</div>
						<div margin-top>
							<div class="numerosContainer">
								${ getNumerosContainer(asignacion.num) }
							</div>
						</div>
					</div>`;
				if (perfil == 7) {
					html += `<div class="col-md-2" style="display: flex; justify-content: center;">
						<p style="margin-bottom: 0;">Profesor Asignado</p>
						${ buildSelectProfesores(asignacion.usuario_id, asignacion.id) }
					</div>`;
				}
				html += `<div class="col-md-${ perfil != 7 ? 4 : 2 } icons-number">
						<div>
							<div class="icon-number" title="Listado de alumnos" onclick="
								activatePlanillaUsuarios('ejecutada', '${textVerEvaluacion}', \`${textCurso}\`, ${idCurso}, ${idPrueba}, ${asignacion.id})
							">
								<i class="fa fa-users"></i>
							</div>
							<div class="icon-number" title="Planilla de tabulación" onclick="activatePlanillaTabulacion('ejecutada', ${asignacion.id}, '${textVerEvaluacion}', '${asignacion.codigo}')">
								<i class="fa fa-file-text-o"></i>
							</div>
							<div class="icon-number">
								<i class="ti-eye" title="Ver prueba" onclick="activateViewPruebaModal('ejecutada', ${asignacion.idPrueba}, '${textVerEvaluacion}', '${asignacion.codigo}', '${asignacion.id}')"></i>
							</div>
						</div>
					</div>
					<div class="col-md-2 icons-number">
						<button class="btn btn-finalizar" onclick="finalizarAsignacion(${asignacion.id}, ${ idCurso })">
							Finalizar
						</button>
					</div>
				</div>
			</div>`;
		}
		$('.en-ejecucion').html(html);
	} else {
		$('.en-ejecucion').html('<div class="alert alert-warning"> <code>Oops!</code> ¡No existen asignaciones de este tipo en ejecución!</div>');
	}
}
var backPaginador = () => {
	if(activeNumberPaginador > 0) {
		$('[data-index="' + (activeNumberPaginador-1) + '"]')[0].click();
	}
};
var forwardPaginador = () => {
	if(activeNumberPaginador < data.asignacionesFinalizadas.length-1) {
		$('[data-index="' + (Number(activeNumberPaginador)+1) + '"]')[0].click();
	}
};
var renderizarPaginador = () => {
	let html = `<li onclick="backPaginador()">
        <a href="javascript:void(0)">
            <i class="fa fa-angle-left"></i>
        </a>
    </li>`;
    for (let i = 0; i < data.asignacionesFinalizadas.length; i++) {
    	html += `<li data-index="${i}" class="${i==0 ? 'active' : ''}" onclick="renderizarFinalizadas(${i}, this)"><a href="javascript:void(0)"> ${i+1} </a></li>`;
    }
    html += `<li onclick="forwardPaginador()">
        <a href="javascript:void(0)">
            <i class="fa fa-angle-right"></i>
        </a>
    </li>`;
    $('#paginador').html(html);
};
var activateViewPruebaModal = (tipo, idPrueba, textoEvaluacion, codigoPrueba, idAsignacion) => {
	$("#btn-descargar").attr('href', siteUrl()+'/evaluaciones/downloadEvaluacion/'+codigoPrueba);
	$('#data-prueba-url').attr('data', `${siteUrl()}/administrador/verEvaluacion/${idPrueba}/AEDUC/${idAsignacion}`);

	$('.modal-title.info-prueba').text(textoEvaluacion);
	$('#ver-prueba .modal-body').addClass(tipo);
	$('#ver-prueba').modal('show');
}
var activatePlanillaTabulacion = (tipo, idAsignacion, textoEvaluacion, codigoPrueba) => {
	$.ajax ({
		url : `${siteUrl()}/asignaciones/getPlanillaTabulacion/${idAsignacion}`,
		dataType: 'json',
		success : json => {
			if(json.status) {
				$("#btn-actualizar-planilla, #btn-reiniciar").attr("id-asignacion", json.idAsignacion);
				let headHtml = `
				<tr>
					<th style="min-width: 200px;"><i class="fa fa-user"></i> Nombre Alumno</th>`
				for(const pregunta of json.dataEvaluacion) {
					headHtml += '<th style="width: 20px;" data-pregunta="' + pregunta.id + '">P'+(pregunta.orden < 10 ? '0'+pregunta.orden : pregunta.orden)+'</th>';
				}
				$('#head-planilla-tabulacion').html(headHtml += '</tr>');
				let htmlBody = '';
				for(const alumno of json.alumnos) {
					htmlBody += `
						<tr>
							<td style="padding: 10px 8px 0 8px!important;"><i class="fa fa-user"></i> ${alumno.nombre} ${alumno.apellido}</td>`;
					for(const pregunta of json.dataEvaluacion) {
						if(alumno.respuestas == false) {
							htmlBody += `
							<td>
								<input tipe="text" maxlength="1" id="${json.idAsignacion}-${alumno.id}-${pregunta.id}" id-alumno="${ alumno.id }" id-pregunta="${ pregunta.id }" id-asignacion="${ json.idAsignacion }" class="input-alt omitida" value="">
							</td>`;
						} else {
							let letra = '';
							let respuestaAlumno = alumno.respuestas.find((respuesta) => {
								return respuesta.pregunta_id == pregunta.id;
							});
							let respuestaFinal = undefined;
							if (respuestaAlumno !== undefined) {
								respuestaFinal = pregunta.alternativas.find(alternativa => {
									return alternativa.id == respuestaAlumno.respuesta_id
								});
								if (respuestaFinal !== undefined) letra = getLetraAlernativa(pregunta.alternativas, respuestaFinal);
							}

							let clase = '';
							if (respuestaFinal !== undefined) clase = respuestaFinal.correcta == 't' ? 'correcta' : 'incorrecta';
							else clase = 'omitida';

							htmlBody += `<td title='${pregunta.orden}'>
								<input tipe="text" maxlength="1" id="${json.idAsignacion}-${alumno.id}-${pregunta.id}" id-alumno="${ alumno.id }" id-pregunta="${ pregunta.id }" id-asignacion="${ json.idAsignacion }" class="input-alt ${ clase }" value="${ letra }">
							</td>`;
						}
					}
					htmlBody += `</tr>`;
					$('#planilla-tabulacion .modal-body').addClass(tipo);
				}
				$('#body-planilla-tabulacion').html(htmlBody);
				$('#planilla-tabulacion').modal('show');
				$('.modal-title.info-prueba').text(textoEvaluacion)
			} else {
				swal('No se ha encontrado información', 'Contácte a soporte técnico', 'error');
				$('#planilla-tabulacion').modal('hide');
			}
		}
	})
}
const setPlanillaTabulacion = idAsignacion => {
	$.ajax ({
		url : `${siteUrl()}/asignaciones/getPlanillaTabulacion/${idAsignacion}`,
		dataType: 'json',
		success : json => {
			if(json.status) {
				$("#btn-actualizar-planilla, #btn-reiniciar").attr("id-asignacion", json.idAsignacion);
				let headHtml = `
				<tr>
					<th style="min-width: 200px;"><i class="fa fa-user"></i> Nombre Alumno</th>`
				for(const pregunta of json.dataEvaluacion) {
					headHtml += '<th style="width: 20px;" data-pregunta="' + pregunta.id + '">P'+(pregunta.orden < 10 ? '0'+pregunta.orden : pregunta.orden)+'</th>';
				}
				$('#head-planilla-tabulacion').html(headHtml += '</tr>');
				let htmlBody = '';
				for(const alumno of json.alumnos) {
					htmlBody += `
						<tr>
							<td style="padding: 10px 8px 0 8px!important;"><i class="fa fa-user"></i> ${alumno.nombre} ${alumno.apellido}</td>`;
					for(const pregunta of json.dataEvaluacion) {
						if(alumno.respuestas == false) {
							htmlBody += `
							<td>
								<input tipe="text" maxlength="1" id-alumno="${ alumno.id }" id-pregunta="${ pregunta.id }" id-asignacion="${ json.idAsignacion }" class="input-alt omitida" value="">
							</td>`;
						} else {
							let letra = '';
							let respuestaAlumno = alumno.respuestas.find((respuesta) => {
								return respuesta.pregunta_id == pregunta.id;
							});
							let respuestaFinal = undefined;
							if (respuestaAlumno !== undefined) {
								respuestaFinal = pregunta.alternativas.find(alternativa => {
									return alternativa.id == respuestaAlumno.respuesta_id
								});
								if (respuestaFinal !== undefined) letra = getLetraAlernativa(pregunta.alternativas, respuestaFinal);
							}

							let clase = '';
							if (respuestaFinal !== undefined) clase = respuestaFinal.correcta == 't' ? 'correcta' : 'incorrecta';
							else clase = 'omitida';

							htmlBody += `<td title='${pregunta.orden}'>
								<input tipe="text" maxlength="1" id-alumno="${ alumno.id }" id-pregunta="${ pregunta.id }" id-asignacion="${ json.idAsignacion }" class="input-alt ${ clase }" value="${ letra }">
							</td>`;
						}
					}
					htmlBody += `</tr>`;
				}
				$('#body-planilla-tabulacion').html(htmlBody);
			} else {
				swal('No se ha encontrado información', 'Contácte a soporte técnico', 'error');
			}
		}
	})
}
const setCeldaPlanillaTabulacion = (idAsignacion, idPregunta, letra, idAlumno) => {
	$.ajax ({
		url : `${siteUrl()}/asignaciones/getCeldaPlanillaTabulacion`,
		data: {idAsignacion, idPregunta, letra},
		type: 'post',
		success : json => {
			json = JSON.parse(json);

			$(document).find(`#${idAsignacion}-${idAlumno}-${idPregunta}`).removeClass('omitida correcta incorrecta');
			$(document).find(`#${idAsignacion}-${idAlumno}-${idPregunta}`).addClass(`${json.class}`);
		}
	})
}
var activatePlanillaUsuarios = (tipo, textVerEvaluacion, textCurso, idCurso, idPrueba, idAsignacion) => {
	$('#planilla-usuarios .modal-body').addClass(tipo);
	$('.modal-title.planilla-usuarios').html('<i class="fa fa-users"></i> ALUMNOS - '  + textCurso);
	$('#planilla-usuarios .modal-title.info-prueba').text(textVerEvaluacion);
    $.ajax({
        url: `${siteUrl()}/curso/getCursoById/${idCurso}/${idPrueba}/0/${idAsignacion}`,
        dataType : 'json',
        success(json) {
			if(json.status) {
				$('#body-usuarios, #head-usuarios').html('');
				const { alumnos, hojasDeRespuestasAlumno, evaluaciones } = json;
				let htmlHeader = `<tr>
					<th style="width: 90px">Habilitar</th>
					<th>Estado</th>
					<th style="width: 17%;">Rut</th>
					<th>Nombre Alumno</th>
					<th>Progreso</th>`;
				if (evaluaciones) {
					for (const evaluacion of evaluaciones) {
						htmlHeader += `<th style="text-align : center">Evaluacion ${evaluacion.num}</th>`;
					}
				}
				htmlHeader +=  `<th style="width: 100px;">Tiempo</th></tr>`;
				$('#head-usuarios').html(htmlHeader);
				for(const alumno of alumnos) {
					let checked = '';
					let estado = '<i class="fa fa-circle m-r-5" style="color: #008efa;"></i> Sin iniciar';
					if (typeof json.dataAlumnos != "undefined" && typeof json.dataAlumnos[alumno.id] != "undefined") {
						if (json.dataAlumnos[alumno.id].estado == "1") checked = 'checked';

						switch (json.dataAlumnos[alumno.id].estado) {
							case "1": estado = `<i class="fa fa-circle m-r-5" style="color: #008efa;"></i> Sin iniciar`; break;
							case "2": estado = `<i class="fa fa-circle m-r-5" style="color: #FFAA39;"></i> En curso`; break;
							case "3": estado = `<i class="fa fa-circle m-r-5" style="color: #52DD48;"></i> Finalizada`; break;
						}
					}

					let htmAlumno =
					`<tr>
						<td><input type="checkbox" ${ checked } switch-habilitar id-asignacion="${ idAsignacion }" id-alumno="${ alumno.id }" class="js-switch" data-color="#0098ff" data-size="small" /></td>
						<td>${ estado }</td>
						<td>${ formateaRut(alumno.username) }</td>
						<td>${ alumno.nombre } ${ alumno.apellido }</td>`;
					if (hojasDeRespuestasAlumno) {
						if(Object.keys(hojasDeRespuestasAlumno[alumno.id]).find(asignacionHojaRespuesta => asignacionHojaRespuesta == idAsignacion)) {
							const respuestas = hojasDeRespuestasAlumno[alumno.id][idAsignacion];
							const porcentajeAvance = parseInt(respuestas.length / respuestas[0].maxNotas * 100);

							let colorBar = '';
							try{
								if (json.dataAlumnos[alumno.id].estado == 2) colorBar = '#93e260';
								else if (json.dataAlumnos[alumno.id].estado == 3 && porcentajeAvance >= 100) colorBar = '#93e260';
								else colorBar = '#545354';
							} catch (e) {

							}

							htmAlumno +=
							`<td>
								<div class="progress progress-xs">
									<div class="progress-bar" style="width: ${porcentajeAvance}%; background-color: ${colorBar}"></div>
								</div> ${porcentajeAvance}%
							</td>`
						} else {
							htmAlumno +=
							`<td>
								<div class="progress progress-xs">
									<div class="progress-bar" style="width: 0%"></div>
								</div> 0%
							</td>`;
						}
					} else {
						htmAlumno +=
						`<td>
							<div class="progress progress-xs">
								<div class="progress-bar" style="width: 0%"></div>
							</div> 0%
						</td>`;
					}
					if (evaluaciones) {
						for(const evaluacion of evaluaciones) {
							if(Object.keys(hojasDeRespuestasAlumno[alumno.id]).find(hojaRespuesta => hojaRespuesta == evaluacion.asignacion_id)) {
								const respuestas = hojasDeRespuestasAlumno[alumno.id][evaluacion.asignacion_id];
								const promedio = getPromedio(respuestas);
								htmAlumno += `<td class="nota ${promedio >= 4 ? 'blue-nota' : 'red-nota'}">(${String(promedio).length == 1 ? promedio + '.0' : promedio })</td>`;
							} else {
								htmAlumno += `<td class="nota empty-nota">(0.0)</td>`;
							}
						}
					}

					if (json.dataAlumnos) {
						htmAlumno +=  `<td>${json.dataAlumnos[alumno.id] ? json.dataAlumnos[alumno.id].tiempo : '00:00:00' }</td>
						</tr>`;
					} else {
						htmAlumno +=  `<td>00:00:00</td>
						</tr>`;
					}

					$('#body-usuarios').append(htmAlumno);
				}
				$('#planilla-usuarios').modal('show');
			} else {
				swal('No se ha encontrado información', 'Contácte a soporte técnico', 'error')
			}
			$('.js-switch').each(function() {
				new Switchery($(this)[0], $(this).data());
				if(!$(this).prop('checked')) $(this).siblings('span.switchery').css({'background-color': 'rgb(223, 223, 223)'});
			});
		}
	});
}
var dividirEnArrays = (asignaciones) => {
	const arrayReturn = new Array();
	let i = 0, j = 0;
	asignaciones.forEach((asignacion, i) => {
		if(!(arrayReturn[j] instanceof Array)) {
			arrayReturn[j] = new Array();
		}
		arrayReturn[j].push(asignacion);
		if(arrayReturn[j].length == 4) {
			j++;
		}
	});
	return arrayReturn;
};
var getPromedio = (hojaRespuesta) => {
    let totalRespuestas = hojaRespuesta[0].maxNotas,
        nota,
        correctas = 0;
    for (const respuesta of hojaRespuesta) {
        if(respuesta.correcta){
            correctas++;
        }
    }
    const punt = correctas * 100 / totalRespuestas;

    nota = (punt < 60) ? (5 * (punt*0.01) + 1) : (7.5 * ((punt*0.01) - 0.6) + 4);
    if(nota < 2) {
        nota = 2;
    }
    nota = Math.round(parseInt(nota*100)/10)/10;
    return nota;
}
var formateaRut = (rut) => {
    var actual = rut.replace(/^0+/, "");
    if (actual != '' && actual.length > 1) {
        var sinPuntos = actual.replace(/\./g, "");
        var actualLimpio = sinPuntos.replace(/-/g, "");
        var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
        var rutPuntos = "";
        var i = 0;
        var j = 1;
        for (i = inicio.length - 1; i >= 0; i--) {
            var letra = inicio.charAt(i);
            rutPuntos = letra + rutPuntos;
            if (j % 3 == 0 && j <= inicio.length - 1) {
                rutPuntos = "." + rutPuntos;
            }
            j++;
        }
        var dv = actualLimpio.substring(actualLimpio.length - 1);
        rutPuntos = rutPuntos + "-" + dv;
    }
    return rutPuntos;
}
var getLetraAlernativa = (alternativas, alternativaFinal) => {
	let i = 0;
	for (const alternativa of alternativas) {
		if(alternativa.id == alternativaFinal.id) {
			break;
		}
		i++;
	}
	return ['A','B','C','D','E','F','G'][i];
}
const buildSelectProfesores = (idProfesorAsignado, idAsignacion) => {
	let html = `<select class="form-control" select-profesores id-asignacion="${ idAsignacion }">
					<option value="null" hidden>Profesor no asignado</option>`;
	if (data['profesores']) {
		for (const profesor of data['profesores']) {
			html += `<option value='${ profesor.id }' ${ idProfesorAsignado == profesor.id ? 'selected' : '' }>${ profesor.nombre } ${ profesor.apellido }</option>`;
		}
	}
	html += '</select>';
	return html;
}
const asignaturaByCodigo = (codigo) => {
    const alias = codigo.substr(0, 3).toUpperCase();
    let asignaturaName;
    switch (alias) {
        case 'LEN': asignaturaName = 'Lenguaje y Comunicación'; break;
				case 'COM': asignaturaName = 'Lenguaje y Comunicación'; break;
        case 'MAT': asignaturaName = 'Matemática'; break;
        case 'HIS': asignaturaName = 'Historia, Geografía y Ciencias Sociales'; break;
        case 'CIE': asignaturaName = 'Ciencias Naturales'; break;
        case 'QUI': asignaturaName = 'Química'; break;
        case 'BIO': asignaturaName = 'Biología'; break;
        case 'FIS': asignaturaName = 'Física'; break;
        default: asignaturaName = ''; break;
    }
    return asignaturaName.toUpperCase();
}
const getLetrasDisponibles = (asignaciones) => {
	if (asignaciones) {
		for (const asignacion of asignaciones) {
			if (arrLetras.indexOf(asignacion.letra) == -1) arrLetras.push(asignacion.letra)
		}
	}
}
const buildFilterLetras = () => {
	let html = `<div class="row" padding-left-1>
		<div class="col-md-12">
			<h5 id="title-data"><strong>Letra Curso</strong></h5>
		</div>
	</div>
	<div class="row" padding-left-1>`;

	for (let i = 0; i < arrLetras.length; i++) {
		html += `<div class="col-md-12">
			<div class="checkbox-inline-custom letrasFilter" letra="${ arrLetras[i] }" margin-bottom>
				<h6>${ arrLetras[i] }</h6>
				<input type="checkbox" value=""/>
			</div>
		</div>`
	}
	html += `</div>`;
	if (arrLetras.length > 0) $("#letrasFilter").html(html);
}
