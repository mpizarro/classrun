
/**
 * Constructor de la clase
 *
 * @author Miguel López <miguel.lopez[at]aeduc.cl>
 * @version 2019-05-16
 */
function Form() {
    'use strict';
    return;
}

var respuestas = 1;

/**
 * Método principal que hace los chequeos:
 *   - Campo obligatorio (en caso de aplicar)
 *   - Tipo de dato del campo
 * @param id ID del formulario o nada si se desean revisar todos los campos
 * @return =true pasó la validación ok
 */
Form.check = function (id) {
    'use strict';
    var fields, i, j, checks, status, label;
    // seleccionar campos que se deben chequear
    if (id !== undefined) {
        try {
            fields = document.getElementById(id).getElementsByClassName("check");
        } catch (error) {
            fields = [];
        }
    } else {
        fields = document.getElementsByClassName("check");
    }
    // chequear campos
    for (i = 0; i < fields.length; i += 1) {
        fields[i].parentNode.parentNode.className = fields[i].parentNode.parentNode.className.replace(/(?:^|\s)has-error(?!\S)/g, '');
        if (fields[i].getAttribute("disabled")=="disabled") {
            continue;
        }
        try {
            fields[i].value = fields[i].value.trim();
        } catch (error) {}
        checks = fields[i].getAttribute("class").replace("check ", "").split(" ");
        if (checks.indexOf("notempty") === -1 && __.empty(fields[i].value)) {
            continue;
        }
        for (j = 0; j < checks.length; j += 1) {
            if (checks[j] === "" || Form["check_" + checks[j]] === undefined) {
                continue;
            }
            try {
                status = Form["check_" + checks[j]](fields[i]);
                if (status !== true) {
                    if (fields[i].parentNode.parentNode.getElementsByTagName("label").length)
                        label = fields[i].parentNode.parentNode.getElementsByTagName("label")[0].textContent.replace("* ", "");
                    else if (!__.empty(fields[i].placeholder))
                        label = fields[i].placeholder;
                    else
                        label = fields[i].name;
                    fields[i].parentNode.parentNode.className += " has-error";
                    alert(status.replace("%s", label));
                    try {
                        fields[i].select();
                    } catch (error) {
                    }
                    fields[i].focus();
                    return false;
                }
            } catch (error) {
                console.log("Error al ejecutar el método %s: ".replace("%s", "Form.check_" + checks[j]) + error);
            }
        }
    }
    // retornar estado final
    return true;
};

/**
 * Método que revisa que el campo sea un RUT válido
 * @param Campo que se quiere validar
 * @return =true pasó la validación ok
 */
Form.check_rut = function (field) {
    'use strict';
    var dv = field.value.charAt(field.value.length - 1).toUpperCase(),
        rut = field.value.replace(/\./g, "").replace("-", "");
    if (dv !== "K")
        dv = parseInt(dv);
    rut = rut.substr(0, rut.length - 1);
    if (dv !== __.rutDV(rut)) {
        return "¡%s es incorrecto!";
    }
    // field.value = __.num(rut) + "-" + dv;
    return true;
};

/**
 * Método que permite verificar si realmente se desea enviar el formulario
 * @param msg Mensaje que se debe mostrar al usuario para confirmar el formulario
 */
Form.checkSend = function (msg) {
    'use strict';
    msg = msg || "¿Está seguro que desea enviar el formulario?"
    if (confirm(msg)) {
        return true;
    } else {
        return false;
    }
}

/**
 * Método que revisa que el campo sea un entero
 * @param Campo que se quiere validar
 * @return =true pasó la validación ok
 */
Form.check_integer = function (field) {
    'use strict';
    if (!__.isInt(field.value)) {
        return "¡%s debe ser un número entero!";
    }
    field.value = parseInt(field.value);
    return true;
};

/**
 * Método que revisa que el campo sea un número real (entero o decimal)
 * @param Campo que se quiere validar
 * @return =true pasó la validación ok
 */
Form.check_real = function (field) {
    'use strict';
    if (Form.check_integer(field)===true) {
        return true;
    }
    field.value = field.value.replace(',', '.');
    if (!__.isFloat(field.value)) {
        return "¡%s debe ser un número entero o decimal con punto!";
    }
    field.value = parseFloat(field.value);
    return true;
};

/**
 * Método para agregar una fila a una tabla en un formulario
 * @param id ID de la tabla donde se deben agregar los campos
 */
Form.addJS = function (id, alternativas, callback) 
{
    'use strict';
    var fields, i;
    respuestas++;
    
    if(respuestas <= alternativas)
    {
        document.getElementById(id).insertAdjacentHTML('beforeend', window["inputsJS_" + id]);
    
        if (callback != undefined) 
        {
            callback($(document).find(`#${ id }`).children('.container-resp:last-child'));
        }

        $('input[name="correcta[]"]').each(function (i, e) 
        {
            $('input[name="correcta[]"]').get(i).value = i;
            $(e).siblings('span').html(getLetraByIndice(i));
        });
    }

}

/**
 * Método para eliminar una fila de una tabla en un formulario
 * @param link Elemento link (<a>) que es parte de la fila que se desea remover
 */
Form.delJS = function (link) {
    'use strict';
    link.parentNode.parentNode.remove();
    setLetraAlternativa(link);
};

Form.delPreguntaJS = function (link) 
{
    Swal.fire({
        title: '¿Está segur@ que desea eliminar esta pregunta?',
        text: "La acción no podrá revertirse",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminémosla!',
        confirmButtonColor: '#56b2bf'
      }).then((result) => {
        if (result.value) 
        {
            'use strict';
            var pregunta = $(link).attr('data-pregunta');
            var prueba = $(link).attr('data-prueba');

            $.ajax({
                url: siteUrl() + '/home/eliminar_pregunta',
                data: {
                    pregunta : pregunta,
                    prueba : prueba
                },
                type: 'POST',
                success(e) 
                {
                    Swal.fire(
                        'Borrado!',
                        'Tu pregunta ha sido eliminada satisfactoriamente',
                        'success'
                      );
                    link.parentNode.parentNode.remove();
                    window.location.reload();
                }
            });

            
        
        }
      })
};

Form.editPreguntaJS = function (link) {
    'use strict';
    var pregunta = $(link).attr('data-pregunta');

    var asignatura_id = $('#form1').find('#asignatura_id').val();
    var nivel_id = $('#form1').find('#nivel_id').val();
    var tipo_id = $('#form1').find('#tipo_id').val();
    var prueba_id = $('#form1').find('#prueba_id').val();
    var oas = $('#form1').find('#oas').val();
    var ejes = $('#form1').find('#ejes').val();
    var prueba_aeduc = $('#form1').find('#prueba_aeduc').val();
    var nombre = $('#form1').find('#nombre').val();

    $.ajax({
        url: siteUrl() + '/home/redirecciona_para_editar',
        data: {
            pregunta : pregunta,
            asignatura_id : asignatura_id,
            nivel_id : nivel_id,
            tipo_id : tipo_id,
            prueba_id : prueba_id,
            oas : oas,
            ejes : ejes,
            prueba_aeduc : prueba_aeduc,
            nombre: nombre
        },
        type: 'POST',
        success(e) {
            location.href=e;
        }
    });

};

Form.moveToUpPreguntaJS = function(link) {
    'use strict';
    var pregunta = $(link).attr('data-pregunta');
    var prueba = $(link).attr('data-prueba');

    $.ajax({
        url: siteUrl() + '/home/mover_arriba_pregunta',
        data: {
            pregunta : pregunta,
            prueba : prueba
        },
        type: 'POST',
        success(e) {
            location.reload();
        }
    });
}

Form.moveToDownPreguntaJS = function(link) {
    'use strict';
    var pregunta = $(link).attr('data-pregunta');
    var prueba = $(link).attr('data-prueba');
    
    $.ajax({
        url: siteUrl() + '/home/mover_abajo_pregunta',
        data: {
            pregunta : pregunta,
            prueba : prueba
        },
        type: 'POST',
        success(e) {
            location.reload();
        }
    });
}

/**
 * Función para eliminar o editar una fila
 *
 * @author Miguel López <miguel.lopez[at]aeduc.cl>
 * @version 2019-05-16
 * @param object objecto que está invocando la llamada
 * @param action acción que ejecutará
 */
Form.setJS = function (object, action) {
    'use strict';

    var mensaje = {
        del : 'eliminar',
        editar : 'editar'
    };

    var btn_class = {
        del : 'btn btn-danger',
        editar : 'btn btn-info'
    };

    var id = $(object).data('id');
    $('#message').html('¿Está seguro que desea <b>' + mensaje[action] + '</b> el registro con id ' + id + '?');
    $('#modal-title').html(object.title);
    var link = base_url + controller + '/' + action + '/' + id;
    $('#confirm').attr('href', link);
    $('#confirm').attr('class', btn_class[action]);
    $('#confirm').text(ucWords(mensaje[action]));
};

/**
 * Función creada para el metodo ucWords
 *
 * @author Miguel López <miguel.lopez[at]aeduc.cl>
 * @version 2019-05-16
 */
function ucFirst(string) {
   return string.substr(0, 1).toUpperCase() + string.substr(1, string.length).toLowerCase();
}

/**
 * Método que deja la primera letra de una palabra en mayúscula
 *
 * @author Miguel López <miguel.lopez[at]aeduc.cl>
 * @version 2019-05-16
 */
function ucWords(string) {
    var arrayWords;
    var returnString = "";
    var len;
    arrayWords = string.split(" ");
    len = arrayWords.length;
    for ( i = 0; i < len; i++) {
        if (i != (len - 1)) {
            returnString = returnString + ucFirst(arrayWords[i]) + " ";
        } else {
            returnString = returnString + ucFirst(arrayWords[i]);
        }
    }
    return returnString;
}

/**
 * Método que calcula la edad según la fecha de nacimiento
 *
 * @author Miguel López <miguel.lopez[at]aeduc.cl>
 * @param nacimientoField indica el input en donde se obtiene la fecha de nacimiento
 * @param edadField indica el input en se mostrará la edad
 * @return void
 * @version 2019-05-16
 */
function calcula_edad(nacimientoField, edadField) {
    var fecha = $(nacimientoField).val();
    var fechaActual = new Date()
    var diaActual = fechaActual.getDate();
    var mmActual = fechaActual.getMonth() + 1;
    var yyyyActual = fechaActual.getFullYear();
    FechaNac = fecha.split("-");
    var diaCumple = FechaNac[0];
    var mmCumple = FechaNac[1];
    var yyyyCumple = FechaNac[2];
    if (mmCumple.substr(0, 1) == 0) {
        mmCumple = mmCumple.substring(1, 2);
    }
    if (diaCumple.substr(0, 1) == 0) {
        diaCumple = diaCumple.substring(1, 2);
    }
    var edad = yyyyActual - yyyyCumple;
    if ((mmActual < mmCumple) || (mmActual == mmCumple && diaActual < diaCumple)) {
        edad--;
    }
    $(edadField).val(edad);
}

Number.prototype.formatMoney = function(c, d, t){
var n = this,
    c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

const getLetraByIndice = indice => {
    const abc = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","Ñ","O","P","Q","R","S","U","V","W","X","Y","Z"];
    return abc[indice];
}

const setLetraAlternativa = () => {
    $('#container_alternativas').children('.container-resp').each(function(i, e) {
        $(e).find('span').html(getLetraByIndice(i));
    });
}