<?php
class Colegio_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function save($data)
    {
        $return = false;
        
        $this->db->insert('colegio', $data);
        
        if($this->db->affected_rows() > 0)
            $return = true;
        
        return $return;
    }
    
    public function getColegiosBySostenedor($usuario_id, $rbd=false)
    {
        $this->db->select('c.*');
        $this->db->join('colegio c', 'c.id = uc.colegio_id');
        if ($rbd)
            $this->db->where('c.rbd', $rbd);
        $this->db->where('uc.usuario_id', $usuario_id);
        $response = $this->db->get('usuario_has_colegio uc');

        return $response->result();
    }

    public function getAll()
    {
        $this->db->select('c.id, c.nombre, co.comuna, c.rbd');
        //$this->db->where('c.id<', 10);
        $this->db->join('comuna co', 'co.id = c.comuna_id');
        $response = $this->db->get('colegio c');

        return $response->result();
    }

    public function getCursos($rbd)
    {
        $response = $this->db->get('"'.$rbd.'".curso c');

        return $response->result();
    }

    public function getAlumnos($colegio, $curso)
    {
        $this->db->where('colegio_id', $colegio);
        $this->db->where('curso_id', $curso);

        $response = $this->db->get('usuario');

        return $response->result();
    }

    public function getCantidadAlumnos($rbd)
    {
        $return = false;

        $result = $this->db->query("SELECT curso_id, COUNT(*) FROM usuario WHERE perfil_id = 3 AND colegio_id=(SELECT id FROM colegio WHERE rbd='$rbd') GROUP BY curso_id");

        if($result->num_rows() > 0)
        {
            foreach ($result->result() as $value) 
            {
                $array[$value->curso_id] = $value->count;
            }
            $return = $array;
        }

        return $return;
    }

    public function getCantidadDocentes()
    {
        $return = false;

        $result = $this->db->query("SELECT colegio_id, COUNT(*) FROM usuario WHERE perfil_id =2 GROUP BY colegio_id");

        if($result->num_rows() > 0)
        {
            foreach ($result->result() as $value) 
            {
                $array[$value->colegio_id] = $value->count;
            }
            $return = $array;
        }

        return $return;

    }

    public function createSchema($rbd)
    {
        $return = false;
       
        if ($this->db->simple_query('CREATE SCHEMA "'.$rbd.'"'))
            $return = true;
        
        return $return;
    }

    public function nuevoColegio($data)
    {
        $return = false;
        $rbd    = $data['rbd'];

        if($this->save($data))
            if($this->createSchema($rbd))
            {
                if ($this->db->simple_query($this->schema($rbd)))
                    $return = true;
            }
    
        return $return;
    }

    public function schema($rbd)
    {
        $sql = '
        CREATE TABLE "'.$rbd.'".curso
        (
          id serial NOT NULL,
          curso character varying(20) NOT NULL,
          letra character varying(2) NOT NULL,
          colegio_id integer NOT NULL,
          nivel_id integer NOT NULL,
          usuario_id integer,
          CONSTRAINT curso_pk PRIMARY KEY (id),
          CONSTRAINT curso_nivel FOREIGN KEY (nivel_id)
              REFERENCES public.nivel (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION,
          CONSTRAINT curso_usuario FOREIGN KEY (usuario_id)
              REFERENCES public.usuario (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION
        )
        WITH (
          OIDS=FALSE
        );
        ALTER TABLE "'.$rbd.'".curso
          OWNER TO postgres;
        
        CREATE TABLE "'.$rbd.'".asignacion
        (
          id serial NOT NULL,
          estado integer NOT NULL DEFAULT 1,
          informe_url character varying(200) NOT NULL,
          modificado timestamp without time zone NOT NULL,
          curso_id integer NOT NULL,
          usuario_id integer NOT NULL,
          prueba_id integer NOT NULL,
          origen character varying(101),
          CONSTRAINT asignacion_pk PRIMARY KEY (id),
          CONSTRAINT asignacion_curso FOREIGN KEY (curso_id)
              REFERENCES "'.$rbd.'".curso (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION,
          CONSTRAINT asignacion_usuario FOREIGN KEY (usuario_id)
              REFERENCES public.usuario (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION
        )
        WITH (
          OIDS=FALSE
        );
        ALTER TABLE "'.$rbd.'".asignacion
          OWNER TO postgres;
        
        
        CREATE TABLE "'.$rbd.'".hojarespuesta
        (
          id serial NOT NULL,
          puntaje integer,
          usuario_id integer NOT NULL,
          respuesta_id integer NOT NULL,
          pregunta_id integer NOT NULL,
          asignacion_id integer NOT NULL,
          alternativa_seleccionada integer DEFAULT 0,
          finalizo integer DEFAULT 1,
          CONSTRAINT hojarespuesta_pk PRIMARY KEY (id),
          CONSTRAINT hojarespuesta_asignacion FOREIGN KEY (asignacion_id)
              REFERENCES "'.$rbd.'".asignacion (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION,
          CONSTRAINT hojarespuesta_pregunta FOREIGN KEY (pregunta_id)
              REFERENCES public.pregunta (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION,
          CONSTRAINT hojarespuesta_respuesta FOREIGN KEY (respuesta_id)
              REFERENCES public.respuesta (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION,
          CONSTRAINT hojarespuesta_usuario FOREIGN KEY (usuario_id)
              REFERENCES public.usuario (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION
        )
        WITH (
          OIDS=FALSE
        );
        ALTER TABLE "'.$rbd.'".hojarespuesta
          OWNER TO postgres;
        
        CREATE TABLE "'.$rbd.'".pregunta
        (
          id serial NOT NULL,
          pregunta text NOT NULL,
          ejetematico_id integer,
          taxonomia_id integer,
          habilidadcurricular_id integer,
          texto_id integer,
          ie_id integer,
          codigo character varying(100) NOT NULL,
          padre integer,
          puntaje integer,
          imagen character varying(50),
          has_imagen boolean,
          estado_id integer DEFAULT 1,
          unidad_id integer,
          oa_id integer,
          titulo text,
          contenido text,
          limite_imagen integer,
          imagen_link character varying(255),
          CONSTRAINT pregunta_pk PRIMARY KEY (id),
          CONSTRAINT pregunta_ejetematico FOREIGN KEY (ejetematico_id)
              REFERENCES public.ejetematico (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION,
          CONSTRAINT pregunta_estado FOREIGN KEY (estado_id)
              REFERENCES public.estado (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION,
          CONSTRAINT pregunta_habilidadcurricular FOREIGN KEY (habilidadcurricular_id)
              REFERENCES public.habilidadcurricular (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION,
          CONSTRAINT pregunta_ie FOREIGN KEY (ie_id)
              REFERENCES public.ie (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION,
          CONSTRAINT pregunta_taxonomia FOREIGN KEY (taxonomia_id)
              REFERENCES public.taxonomia (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION
        )
        WITH (
          OIDS=FALSE
        );
        ALTER TABLE "'.$rbd.'".pregunta
          OWNER TO postgres;
        
        CREATE TABLE "'.$rbd.'".prueba
        (
          id serial NOT NULL,
          codigo character varying(50) NOT NULL,
          usuario_id integer,
          nivel_id integer NOT NULL,
          asignatura_id integer NOT NULL,
          anyo integer NOT NULL,
          num integer NOT NULL,
          tipo_id integer NOT NULL,
          unidad_id integer,
          version integer,
          nivel_inicio integer,
          nivel_fin integer,
          forma_prueba integer,
          nombre character varying(255),
          creada boolean,
          prueba_original integer,
          modificado timestamp without time zone,
          CONSTRAINT prueba_pk PRIMARY KEY (id),
          CONSTRAINT prueba_asignatura FOREIGN KEY (asignatura_id)
              REFERENCES public.asignatura (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION,
          CONSTRAINT prueba_nivel FOREIGN KEY (nivel_id)
              REFERENCES public.nivel (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION,
          CONSTRAINT prueba_tipo FOREIGN KEY (tipo_id)
              REFERENCES public.tipo (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION,
          CONSTRAINT prueba_unidad FOREIGN KEY (unidad_id)
              REFERENCES public.unidad (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION,
          CONSTRAINT prueba_usuario_id FOREIGN KEY (usuario_id)
              REFERENCES public.usuario (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION
        )
        WITH (
          OIDS=FALSE
        );
        ALTER TABLE "'.$rbd.'".prueba
          OWNER TO postgres;
        
        CREATE TABLE "'.$rbd.'".prueba_has_pregunta
        (
          id serial NOT NULL,
          prueba_id integer NOT NULL,
          pregunta_id integer NOT NULL,
          orden integer NOT NULL,
          CONSTRAINT prueba_has_pregunta_pk PRIMARY KEY (id),
          CONSTRAINT prueba_has_pregunta_prueba FOREIGN KEY (prueba_id)
              REFERENCES "'.$rbd.'".prueba (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION
        )
        WITH (
          OIDS=FALSE
        );
        ALTER TABLE "'.$rbd.'".prueba_has_pregunta
          OWNER TO postgres;
        
        CREATE TABLE "'.$rbd.'".respuesta
        (
          id serial NOT NULL,
          respuesta text NOT NULL,
          pregunta_id integer NOT NULL,
          correcta boolean NOT NULL,
          puntaje integer,
          imagen character varying(200),
          estado_id integer DEFAULT 1,
          categoria integer,
          letra character varying(1),
          imagen_link character varying(255),
          CONSTRAINT respuesta_pk PRIMARY KEY (id),
          CONSTRAINT respuesta_estado FOREIGN KEY (estado_id)
              REFERENCES public.estado (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION,
          CONSTRAINT respuesta_pregunta FOREIGN KEY (pregunta_id)
              REFERENCES "'.$rbd.'".pregunta (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION
        )
        WITH (
          OIDS=FALSE
        );
        ALTER TABLE "'.$rbd.'".respuesta
          OWNER TO postgres;
        
        CREATE TABLE "'.$rbd.'".usuario_has_asignacion
        (
          id serial NOT NULL,
          usuario_id integer NOT NULL,
          asignacion_id integer NOT NULL,
          estado integer NOT NULL,
          inicio integer,
          fin integer,
          CONSTRAINT usuario_has_asignacion_pk PRIMARY KEY (id),
          CONSTRAINT usuario_has_asignacion_asignacion FOREIGN KEY (asignacion_id)
              REFERENCES "'.$rbd.'".asignacion (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION,
          CONSTRAINT usuario_has_asignacion_usuario FOREIGN KEY (usuario_id)
              REFERENCES public.usuario (id) MATCH SIMPLE
              ON UPDATE NO ACTION ON DELETE NO ACTION
        )
        WITH (
          OIDS=FALSE
        );
        ALTER TABLE "'.$rbd.'".usuario_has_asignacion
          OWNER TO postgres;
        
        ';

        return $sql;
    }

    
}
