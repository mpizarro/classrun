<?php
class Alumno_model extends CI_Model
{
    private $rbd;

    public function __construct()
    {
        parent::__construct();
        $this->rbd = $this->session->rbd;
    }
    public function getAlumnoByCurso($idCurso)
    {
        $result= $this->db->get_where('usuario', ['curso_id' => $idCurso, 'colegio_id' => $this->session->colegio_id ]);
        return $result->num_rows() > 0 ? $result->result() : false;
    }
    public function getEvaluacionesAlumno($origen)
    {

        $this->db->join($this->rbd . '.asignacion s', 's.id = uha.asignacion_id');
        $this->db->join('usuario u', 'u.id = uha.usuario_id');
        if ($origen == 'CLASSRUN')
        {
            $this->db->join('prueba p', 'p.id = s.prueba_id');
            $nombre = 'p.nombre nombrePrueba, ';
        }
        else
        {
            $this->db->join($this->rbd .'.prueba p', 'p.id = s.prueba_id');
            $nombre = '';
        }
        
        $this->db->select('p.num, p.nivel_id, a.asignatura, t.tipo, s.origen, uha.id as usuario_has_asignacion_id, s.id, a.color, a.id as id_asignatura, r.nombre, r.apellido, a.alias');
        $this->db->join('asignatura a', 'a.id = p.asignatura_id');
        $this->db->join('tipo t', 't.id = p.tipo_id');
        $this->db->join('usuario r', 'r.id = s.usuario_id');
        $this->db->where('uha.usuario_id', $this->session->idUser);
        $this->db->where('u.curso_id', $this->session->userdata['curso_id']);
        $this->db->where('s.origen', $origen);
        $this->db->where('p.tipo_id IN (1, 2, 3, 4)');
        $this->db->where('uha.estado IN (1, 2)');
        $this->db->order_by('s.id', 'DESC');
        $query = $this->db->get($this->rbd . '.usuario_has_asignacion uha');

        return $query->num_rows() > 0 ? $query->result() : [];
        
    }
    public function ejectuarEvaluacion($idUsuarioAsignacion, $origen='CLASSRUN', $id)
    {
        $return = false;

        $this->db->where('uha.id', $idUsuarioAsignacion);
        if ($this->db->update($this->rbd.'".usuario_has_asignacion uha', array('estado' => 2))) {
            $this->db->flush_cache();

            $this->db->select('p.id');
            $this->db->from($this->rbd.'".usuario_has_asignacion uha');
            $this->db->where('a.id', $id);
            $this->db->join($this->rbd.'.asignacion a', 'uha.asignacion_id = a.id');
            if ($origen == 'CLASSRUN')
                $this->db->join('prueba p', 'a.prueba_id = p.id');
            else
                $this->db->join($this->rbd.'.prueba p', 'a.prueba_id = p.id');

            $this->db->where('uha.id', $idUsuarioAsignacion);

            $query = $this->db->get();
            $return = $query->row()->id;
        }
        return $return;
    }

    public function asignarEvaluacion($idAsignacion)
    {
        $this->db->where('asignacion_id', $idAsignacion)
            ->update($this->rbd.'.usuario_has_asignacion uha', ['estado' => 1]);

        return $this->db->affected_rows() > 0 ? true : false;
    }
    public function checkearSiEsRealizable($idEvaluacion, $returnIdAsignacion = false, $checkEstado = true, $asignacion_id)
    {
        $this->db->select('uha.id, uha.asignacion_id');
        $this->db->from($this->rbd.'.usuario_has_asignacion uha');

        $this->db->join($this->rbd.'.asignacion a', 'uha.asignacion_id = a.id');

        if ($checkEstado) {
            $this->db->where('uha.estado', 2);
        }

        $this->db->where('a.id', $asignacion_id);
        $this->db->where('uha.usuario_id', $this->session->userdata['idUser']);
        $this->db->where('a.prueba_id', $idEvaluacion);

        $query = $this->db->get();
        return $returnIdAsignacion ? $query->row()->asignacion_id : $query->num_rows() > 0;
    }
    public function finalizarAsignacion($idAsignacion, $usuario=false)
    {
        $data = array('estado' => 3);
        $this->db->where('asignacion_id', $idAsignacion);

        if ($usuario) {
            $this->db->where('usuario_id', $usuario);
        }

        return $this->db->update($this->rbd.'.usuario_has_asignacion', $data);
    }
    public function setAlternativa($idAlternativa, $idPregunta, $idEvaluacion, $id_asignacion)
    {
        $idAsignacion = $this->checkearSiEsRealizable($idEvaluacion, true, false, $id_asignacion);

        $this->db->flush_cache();
        $query = $this->db->query('
            SELECT * FROM "'.$this->rbd.'".hojarespuesta
            WHERE asignacion_id = '.$id_asignacion.'
            AND pregunta_id = '.$idPregunta.'
            AND usuario_id = '.$this->session->idUser);
        $data = array(
            'asignacion_id'  => $id_asignacion,
            'pregunta_id' => $idPregunta,
            'respuesta_id' => $idAlternativa,
            'usuario_id' => $this->session->userdata['idUser']
        );
        if ($query->num_rows() > 0) {
            $idHojaDeRespuesta = $query->row()->id;

            $this->db->flush_cache();
            $this->db->query(
                'UPDATE "'.$this->rbd.'".hojarespuesta SET respuesta_id = '.$idAlternativa.'  WHERE id = '.$idHojaDeRespuesta
            );
        } else {
            $this->db->flush_cache();

            $this->db->insert('"'.$this->rbd.'".hojarespuesta', $data, false);
        }
        return $this->db->affected_rows() > 0;
    }
    public function eliminarAlternativa($idAlternativa, $idPregunta, $idEvaluacion, $idAlumno, $id_asignacion)
    {
        $idAsignacion = $this->checkearSiEsRealizable($idEvaluacion, true, false, $id_asignacion);

        $this->db->query(
            'DELETE FROM "'.$this->rbd.'".hojarespuesta
            WHERE asignacion_id = '.$id_asignacion.'
            AND pregunta_id = '.$idPregunta.'
            AND usuario_id = '.$idAlumno
        );
        return $this->db->affected_rows();
    }
    public function getRespuestas($idAsignacion, $idAlumno)
    {
        $query = $this->db->query(
            'SELECT pregunta_id, respuesta_id
            FROM "'.$this->rbd.'".hojarespuesta
            WHERE asignacion_id = '.$idAsignacion.'
            AND usuario_id = '.$idAlumno
        );

        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function verificaAlumnoRespuestas($rutAlumno, $asignacion)
    {
        $alumno = "$rutAlumno";
        
        if(substr($alumno, 0,1) == '0')
            $alumno = substr($alumno, 1);
            
        $rut = substr($alumno, 0, -1)."-".$alumno[strlen($alumno)-1];
        
        $query = $this->db->query(
            'SELECT id, (SELECT COUNT(*) FROM "'.$this->rbd.'".hojarespuesta WHERE usuario_id = (SELECT id FROM usuario WHERE username=\''.$rut.'\') AND asignacion_id='.$asignacion.') respuestas FROM usuario WHERE username=\''.$rut.'\''
    );

        return $query->num_rows() > 0 ? $query->row() : false;
        
    }

    public function deleteHojarespuestaAlumno($asignacion, $alumno)
    {
        $return = false;

        $this->db->where('usuario_id', $alumno);
        $this->db->where('asignacion_id', $asignacion);
        
        if($this->db->delete($this->rbd.'.hojarespuesta'))
            $return = true;
    
        return $return;
    }
}
