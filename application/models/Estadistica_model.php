<?php
class Estadistica_model extends CI_Model 
{
    private $rbd;

    public function __construct() 
    {
        parent::__construct();
        $this->rbd = $this->session->rbd;
    }

    public function setRbd($rbd)
    {
        $this->rbd = $rbd;
    }
    
    public function getAsignaturas() 
    {
        $result = $this->db->get('asignatura');
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    function getRespuestas($asignacion, $origen)
    {
        $return = false;

        $sql = 'SELECT hr.usuario_id, SUM( CASE WHEN r.correcta =TRUE THEN 1 ELSE 0 END)  AS correctas, 
        SUM( CASE WHEN r.correcta =FALSE THEN 1 ELSE 0 END)  AS incorrectas 
        FROM "'.$this->rbd.'".hojarespuesta hr ';
        if ($origen == 'CLASSRUN')
            $sql .= 'JOIN respuesta r ON r.id = hr.respuesta_id ';
        else
            $sql .= 'JOIN "'.$this->rbd.'".respuesta r ON r.id = hr.respuesta_id ';

        $sql .= 'WHERE hr.asignacion_id = '.$asignacion.'
        GROUP BY hr.usuario_id';

        $query = $this->db->query($sql);

        if($query->num_rows() > 0)
            $return = $query->result();
        
        return $return;        
    }

    public function gethabilidadesByAsignacion($idAsignacion, $origen) {
        $sql = 'SELECT p.habilidadcurricular_id, hc.habilidadcurricular, hr.usuario_id,
        SUM(CASE WHEN r.correcta = TRUE THEN 1 ELSE 0 END) AS correctas,
        SUM(CASE WHEN r.correcta = FALSE THEN 1 ELSE 0 END) AS incorrectas
            FROM "'.$this->rbd.'".hojarespuesta hr ';
        if ($origen == 'CLASSRUN') {
            $sql .= 'JOIN pregunta p ON p.id = hr.pregunta_id
            JOIN respuesta r ON r.id = hr.respuesta_id ';
        } else {
            $sql .= 'JOIN "'.$this->rbd.'".pregunta p ON p.id = hr.pregunta_id
            JOIN "'.$this->rbd.'".respuesta r ON r.id = hr.respuesta_id ';
        }
        $sql .= 'JOIN habilidadcurricular hc ON hc.id = p.habilidadcurricular_id
	        WHERE hr.asignacion_id = '.$idAsignacion.'
            GROUP BY p.habilidadcurricular_id, hc.habilidadcurricular, hr.usuario_id';
        $result = $this->db->query($sql);
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function getOrdenPreguntas($idPrueba, $origen) {
        $sql = 'SELECT pp.pregunta_id, pp.orden, rp.id AS respuesta_id, rp.correcta ';
        if ($origen == 'CLASSRUN') {
            $sql .= 'FROM prueba_has_pregunta pp
            JOIN pregunta pt ON pt.id = pp.pregunta_id
            JOIN respuesta rp ON rp.pregunta_id = pt.id ';
        } else {
            $sql .= 'FROM "'.$this->rbd.'".prueba_has_pregunta pp
            JOIN "'.$this->rbd.'".pregunta pt ON pt.id = pp.pregunta_id
            JOIN "'.$this->rbd.'".respuesta rp ON rp.pregunta_id = pt.id ';
        }
        $sql .= 'WHERE pp.prueba_id = '.$idPrueba.'
        AND rp.estado_id = 1
        ORDER BY pp.orden, rp.id';

        $result = $this->db->query($sql);
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }
    
    public function getPreguntasRespuesta($idAsignacion) {
        $sql = 'SELECT hr.usuario_id, hr.pregunta_id, hr.respuesta_id, rp.correcta
        FROM "'.$this->rbd.'".hojarespuesta hr
        JOIN respuesta rp ON rp.id = hr.respuesta_id
	    WHERE hr.asignacion_id = '.$idAsignacion.'
        ORDER BY hr.usuario_id, hr.pregunta_id, hr.respuesta_id';
        
        $result = $this->db->query($sql);
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }
    
    public function getHabilidadesByPrueba($idPrueba, $origen) {
        $sql = 'SELECT hc.id, hc.habilidadcurricular,
        SUM(CASE WHEN hc.id = p.habilidadcurricular_id THEN 1 ELSE 0 END) AS count
        FROM habilidadcurricular hc ';
        if ($origen == 'CLASSRUN') {
            $sql .= 'JOIN pregunta p ON p.habilidadcurricular_id = hc.id
            JOIN prueba_has_pregunta pp ON pp.pregunta_id = p.id ';
        } else {
            $sql .= 'JOIN "'.$this->rbd.'".pregunta p ON p.habilidadcurricular_id = hc.id
            JOIN "'.$this->rbd.'".prueba_has_pregunta pp ON pp.pregunta_id = p.id ';
        }
        $sql .= 'WHERE pp.prueba_id = '.$idPrueba.'
        GROUP BY hc.id, hc.habilidadcurricular';
        
        $result = $this->db->query($sql);
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function getOasByPrueba($idPrueba) {
        $result = $this->db->distinct()
            ->select('oa.codigo, oa.id, oa.oa, SUM(CASE WHEN oa.id = ie.oa_id THEN 1 ELSE 0 END) AS n_oas', FALSE)
            ->join('pregunta pt', 'pt.id = pp.pregunta_id')
            ->join('ie', 'ie.id = pt.ie_id')
            ->join('oa', 'oa.id = ie.oa_id')
            ->group_by('oa.codigo, oa.id, oa.oa')
            ->where(['pp.prueba_id' => $idPrueba])
            ->get('prueba_has_pregunta pp');
            
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function getIesByPrueba($idPrueba) {
        $result = $this->db->distinct()
            ->select('ie.oa_id, pt.ie_id, SUM(CASE WHEN ie.id = pt.ie_id THEN 1 ELSE 0 END) AS n_ies', FALSE)
            ->join('pregunta pt', 'pt.id = pp.pregunta_id')
            ->join('ie', 'ie.id = pt.ie_id')
            ->where('pp.prueba_id', $idPrueba)
            ->group_by('ie.oa_id, pt.ie_id')
            ->get('prueba_has_pregunta pp');
            
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function getOasByAsignacion($idAsignacion) {
        $sql = 'SELECT ua.usuario_id, oa.codigo, ie.oa_id,
        SUM(CASE WHEN rp.correcta = TRUE THEN 1 ELSE 0 END) AS correctas
        FROM "'.$this->rbd.'".hojarespuesta hr
        JOIN pregunta pt ON pt.id = hr.pregunta_id
        JOIN respuesta rp ON rp.id = hr.respuesta_id
        JOIN ie ON ie.id = pt.ie_id
        JOIN oa ON oa.id = ie.oa_id
        JOIN "'.$this->rbd.'".usuario_has_asignacion ua ON ua.id = hr.usuario_has_asignacion_id
        WHERE ua.asignacion_id = '.$idAsignacion.'
        GROUP BY ua.usuario_id, oa.codigo, ie.oa_id';
        $result = $this->db->query($sql);
        
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function getIesByAsignacion($idAsignacion) {
        $sql = 'SELECT ua.usuario_id, ie.oa_id, pt.ie_id,
        SUM(CASE WHEN rp.correcta = TRUE THEN 1 ELSE 0 END) AS correctas
        FROM "'.$this->rbd.'".hojarespuesta hr
        JOIN pregunta pt ON pt.id = hr.pregunta_id
        JOIN respuesta rp ON rp.id = hr.respuesta_id
        JOIN ie ON ie.id = pt.ie_id
        JOIN "'.$this->rbd.'".usuario_has_asignacion ua ON ua.id = hr.usuario_has_asignacion_id
        WHERE ua.asignacion_id = '.$idAsignacion.'
        GROUP BY ua.usuario_id, ie.oa_id, pt.ie_id';
        $result = $this->db->query($sql);
        
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function getPreguntaHabilidadOaByPrueba($idPrueba, $origen) {
        $sql = 'SELECT pg.id, pp.orden, hc.id AS habilidad_id, hc.habilidadcurricular, ie.oa_id, oa.oa, oa.codigo ';
        if ($origen == 'CLASSRUN') {
            $sql .= 'FROM prueba_has_pregunta pp
            JOIN pregunta pg ON pg.id = pp.pregunta_id ';
        } else {
            $sql .= 'FROM "'.$this->rbd.'".prueba_has_pregunta pp
            JOIN "'.$this->rbd.'".pregunta pg ON pg.id = pp.pregunta_id ';
        }
        $sql .= 'JOIN habilidadcurricular hc ON hc.id = pg.habilidadcurricular_id
        JOIN ie ON ie.id = pg.ie_id
        JOIN oa ON oa.id = ie.oa_id
        WHERE pp.prueba_id = '.$idPrueba.'
        ORDER BY pp.orden';
        $result = $this->db->query($sql);
        
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function getPreguntaOaIeByPrueba($idPrueba) {
        $sql = 'SELECT pg.id, pp.orden, ie.oa_id, oa.codigo, oa.oa, pg.ie_id, ie.ie
        FROM prueba_has_pregunta pp
        JOIN pregunta pg ON pg.id = pp.pregunta_id
        JOIN ie ON ie.id = pg.ie_id
        JOIN oa ON oa.id = ie.oa_id
        WHERE pp.prueba_id = '.$idPrueba.'
        ORDER BY pp.orden';

        $result = $this->db->query($sql);
        
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function getOasRespIeEjeByAsignacion($idAsignacion, $origen) {
        $sql = 'SELECT hr.usuario_id, hr.pregunta_id, hr.respuesta_id, rp.correcta, ie.oa_id, oa.codigo, pg.ejetematico_id, et.ejetematico, pg.ie_id, ie.ie
        FROM "'.$this->rbd.'".hojarespuesta hr ';
        if ($origen == 'CLASSRUN') {
            $sql .= 'JOIN pregunta pg ON pg.id = hr.pregunta_id
            JOIN respuesta rp ON rp.id = hr.respuesta_id ';
        } else {
            $sql .= 'JOIN "'.$this->rbd.'".pregunta pg ON pg.id = hr.pregunta_id
            JOIN "'.$this->rbd.'".respuesta rp ON rp.id = hr.respuesta_id ';
        }
        $sql .= 'LEFT JOIN ie ON ie.id = pg.ie_id
        LEFT JOIN oa ON oa.id = ie.oa_id
        LEFT JOIN ejetematico et ON et.id = pg.ejetematico_id
        JOIN usuario u ON u.id = hr.usuario_id
        WHERE hr.asignacion_id = '.$idAsignacion.'
        ORDER BY hr.usuario_id, hr.pregunta_id, hr.respuesta_id, ie.oa_id';

        $result = $this->db->query($sql);
        
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function getOasRespIeByAsignacion($idAsignacion, $origen) {
        $sql = 'SELECT hr.usuario_id, hr.pregunta_id, hr.respuesta_id, rp.correcta, ie.oa_id, oa.codigo, pg.ie_id, ie.ie
        FROM "'.$this->rbd.'".hojarespuesta hr ';
        if ($origen == 'CLASSRUN') {
            $sql .= 'JOIN pregunta pg ON pg.id = hr.pregunta_id
            JOIN respuesta rp ON rp.id = hr.respuesta_id ';
        } else {
            $sql .= 'JOIN "'.$this->rbd.'".pregunta pg ON pg.id = hr.pregunta_id
            JOIN  "'.$this->rbd.'".respuesta rp ON rp.id = hr.respuesta_id ';
        }
        $sql .= 'JOIN ie ON ie.id = pg.ie_id
        JOIN oa ON oa.id = ie.oa_id
        JOIN usuario u ON u.id = hr.usuario_id
        WHERE hr.asignacion_id = '.$idAsignacion.'
        ORDER BY hr.usuario_id, hr.pregunta_id, hr.respuesta_id, ie.oa_id';

        $result = $this->db->query($sql);
        
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }


    public function getEjePreguntasByPrueba($idPrueba) {
        $sql = 'SELECT pp.orden, pp.pregunta_id, pg.ejetematico_id
        FROM prueba_has_pregunta pp
        JOIN pregunta pg ON pg.id = pp.pregunta_id
        JOIN ejetematico et ON et.id = pg.ejetematico_id
        WHERE pp.prueba_id = '.$idPrueba.'
        ORDER BY pp.orden';
        $result = $this->db->query($sql);
        
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function getAlumnosByAsignacion($idAsignacion) {
        $sql = 'SELECT u.id, u.nombre, u.apellido, ua.inicio, ua.fin
        FROM "'.$this->rbd.'".usuario_has_asignacion ua
        JOIN usuario u ON u.id = ua.usuario_id
        WHERE ua.asignacion_id = '.$idAsignacion.'
        AND u.perfil_id = 3';
        $result = $this->db->query($sql);
        
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }
     



















    function getPSA($id_asignacionprueba)//Puntaje Simce por Alumno
    {
        $crciap = $this->getCRCIAP($id_asignacionprueba);//rppa
        $cpp    = $this->getCPP($id_asignacionprueba); //cpp
        $psa = array();

        if($crciap)
        {
            foreach ($crciap as $key => $value)
            {
                $psa[$key]['id_usuario']        = $value->usuario_id_usuario;
                $psa[$key]['fullname']          = $value->nombre_alumno;
                $psa[$key]['correctas']         = $value->correctas;
                $psa[$key]['incorrectas']       = $value->incorrectas;
                $psa[$key]['porc_correctas']    = round(($value->correctas*100)/$cpp, 1);
                $psa[$key]['porc_incorrectas']  = round(($value->incorrectas*100)/$cpp, 1);
                $psa[$key]['omitidas']          = $cpp - $value->correctas - $value->incorrectas;
                $psa[$key]['porc_omitidas']     = round((($cpp - $value->correctas - $value->incorrectas)*100)/$cpp, 1);
                $psa[$key]['psa']               = round(((($value->correctas - $cpp / 2)) / 3 * (450 / $cpp) + 265), 0);

                $logro = $psa[$key]['porc_correctas'];
                
                if($logro < 60)
                    $nota = 5 * ($logro*0.01) + 1;
                else
                    $nota = 7.5 * (($logro*0.01) - 0.6) + 4;

                if($nota < 2)
                    $nota = 2;
                
                $psa[$key]['nota'] = round($nota,1);

                if ($psa[$key]['psa'] < 240)
                  $psa[$key]['nivel'] = 'INICIAL';
                else 
                    if ($psa[$key]['psa'] >= 240 && $psa[$key]['psa'] < 280)
                  $psa[$key]['nivel'] = 'BASICO';//INTERMEDIO
                else
                  $psa[$key]['nivel'] = 'AVANZADO';
                //0-60 => INICIAL(4.0)
                //61-80 => (4.1 - 5.5) INTERMEDIO
                //81-100 => (5.6 -7.0) AVANZADO
            }
        }
        else
        {
            $psa = false;
        }
        return $psa;
    }

}