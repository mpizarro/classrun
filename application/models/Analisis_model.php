<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Analisis_model extends CI_Model
{
    public function __construct()
    {
		parent::__construct();
    }

    public function set()
    {
        $data = [
            'usuario_id' =>  $this->session->idUser,
            'broker_id' => 2,
            'controller' => $this->router->fetch_class(). '/' . $this->router->fetch_method(),
            'ifecha' => date('YmdHis'),
            'fecha' => date('Y-m-d H:i:s')
        ];
        $this->db->insert('usuario_analisis', $data);
    }

    public function getAnalisis($idColegio, $idProfesor, $inicio, $termino) {
        $sql = "SELECT u.nombre, u.apellido, count(*) as cantidad, LEFT(CAST(ua.ifecha as varchar(8)), 8) as fecha
        FROM usuario_analisis ua
        JOIN usuario u ON u.id = ua.usuario_id
        JOIN colegio c ON c.id = u.colegio_id
        WHERE u.colegio_id = $idColegio
        AND u.perfil_id = 2
        AND ua.broker_id = 2";

        $sql .= $idProfesor ? " AND ua.usuario_id = $idProfesor" : "";
        $sql .= $inicio ? " AND ua.fecha >= '$inicio 00:00'" : "";
        $sql .= $termino ? " AND ua.fecha <= '$termino 23:59'" : "";

        $sql .= "GROUP BY u.nombre, u.apellido, LEFT(CAST(ua.ifecha as varchar(8)), 8)
        ORDER BY 1, 4 DESC";
        $result = $this->db->query($sql);
        
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }
}
