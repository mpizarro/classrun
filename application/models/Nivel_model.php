<?php
class Nivel_model extends CI_Model
{
    private $rbd;

    public function __construct()
    {
        parent::__construct();
        $this->rbd = $this->session->rbd;
    }
    public function getNiveles()
    {
        $result = $this->db->order_by('id')->get('nivel');
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }
    public function getMaxNivelByColegio($idColegio) {
        $result = $this->db->distinct()
            ->select('max(c.nivel_id), min(c.nivel_id)', FALSE)
            ->join($this->rbd.'.curso c', 'c.nivel_id = n.id')
            ->get_where('nivel n', ['c.colegio_id' => $idColegio]);
        return $result->num_rows() > 0 ? $result->row() : FALSE;
    }
    public function getNivelesByCursos($cursos) {
        $result = $this->db->distinct()
        ->select('n.id, n.nivel')
        ->join('nivel n', 'n.id = c.nivel_id')
        ->where_in('c.id', $cursos)
        ->get($this->rbd.'.curso c');
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }
}