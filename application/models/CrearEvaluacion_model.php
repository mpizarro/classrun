<?php

use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Core\Exception\GoogleException;

class CrearEvaluacion_model extends CI_Model {

    private $rbd;

    /**
     * Método constructor
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->rbd = $this->session->rbd;
    }

    /**
     * Retorna los tipos de prueba para evalúa
     *
     * @return array
     */
    public function getTipoPruebas($idColegio)
    {
        return $this->db->select('t.id, t.tipo')
            ->join('servicio s', 's.id = t.id', 'left')
            ->join('colegio_has_servicio cs', 'cs.servicio_id = s.id', 'left')
            ->where('cs.colegio_id', $idColegio)
            ->where_in('t.id', [1, 2, 3 ,4])
            ->get('tipo t')
            ->result();
    }

    /**
     * Retorna asignaturas
     *
     * @return array
     */
    public function getAsignaturas()
    {
        return $this->db->select('id, asignatura')
            ->order_by('asignatura', 'asc')
            ->get_where('asignatura', ['visible' => true])
            ->result();
    }
    /**
     * Retorna asignaturashasnivel
     *
     * @return array
     */
    public function getAsignaturashasnivel($idNivel)
    {
        $rbd=6773;
        $result =  $this->db->select('a.id,a.asignatura')
        ->join('asignatura a', 'a.id = nha.asignatura_id')
        ->where('nha.nivel_id', $idNivel)
        ->get($rbd.'.nivel_has_asignatura nha')
        ->result();
        return $result;
    }
    

    /**
     * Retorna niveles
     *
     * @return array
     */
    public function getNiveles()
    {
        return $this->db->select('id, nivel')
            ->order_by('id', 'asc')
            ->get('nivel')
            ->result();
    }

    /**
     * Retorna una asignatura
     *
     * @return object
     */
    public function getAsignatura($asignatura_id)
    {
        return $this->db->select('id, asignatura')
            ->where('id', $asignatura_id)
            ->order_by('asignatura', 'asc')
            ->get('asignatura')
            ->row();
    }

    /**
     * Retorna un nivel
     *
     * @return object
     */
    public function getNivel($nivel_id)
    {
        return $this->db->select('id, nivel')
            ->where('id', $nivel_id)
            ->order_by('id', 'asc')
            ->get('nivel')
            ->row();
    }

    /**
     * Retorna un tipo de prueba
     *
     * @return object
     */
    public function getTipoPrueba($tipo_id)
    {
        return $this->db->select('id, tipo')
            ->where('id', $tipo_id)
            ->where_in('id', [1, 2, 3 ,4])
            ->get('tipo')
            ->row();
    }

    /**
     * Retorna niveles
     *
     * @return array
     */
    public function getEjes($asignatura_id, $nivel_id)
    {
        $data = [];
        $ejes = $this->db->distinct()
            ->select('r.ejetematico_id')
            ->join('prueba_has_pregunta php', 'php.pregunta_id = r.id')
            ->join('prueba p', 'p.id = php.prueba_id')
            ->where('p.asignatura_id', $asignatura_id)
            ->where('p.nivel_id', $nivel_id)
            ->get('pregunta r')
            ->result();
        foreach($ejes as $eje)
        {
            $data[] = $eje->ejetematico_id;
        }

        return $this->db->select('id, ejetematico')
            ->where_in('id', count($data) > 0 ? $data : [0])
            ->order_by('ejetematico', 'asc')
            ->get('ejetematico')
            ->result();
    }

    /**
     * Retorna niveles
     *
     * @return array
     */
    public function getHabilidades($asignatura_id, $nivel_id)
    {
        $data = [];
        $habilidades = $this->db->distinct()
            ->select('r.habilidadcurricular_id')
            ->join('prueba_has_pregunta php', 'php.pregunta_id = r.id')
            ->join('prueba p', 'p.id = php.prueba_id')
            ->where('p.asignatura_id', $asignatura_id)
            ->where('p.nivel_id', $nivel_id)
            ->get('pregunta r')
            ->result();
        foreach($habilidades as $habilidad)
        {
            $data[] = $habilidad->habilidadcurricular_id;
        }

        return $this->db->select('id, habilidadcurricular')
            ->where_in('id', count($data) > 0 ? $data : [0] )
            ->order_by('habilidadcurricular', 'asc')
            ->get('habilidadcurricular')
            ->result();
    }

    /**
     * Retorna oas
     *
     * @return array
     */
    public function getOAs($params)
    {
        $data = [];

        $oas = $this->db->select('id, oa, CAST(REPLACE(codigo, \'OA\', \'\') AS integer) as codigo')
            ->where('nivel_id', $params['nivel_id'])
            ->where('asignatura_id', $params['asignatura_id'])
            ->order_by('CAST(REPLACE(codigo, \'OA\', \'\') AS integer) ASC')
            ->get('oa')
            ->result();

        foreach ($oas as $key => $oa) {
            if (!in_multiarray($oa->codigo, $data)){
                $data[] = [
                    'id' => $oa->id,
                    'oa' => $oa->oa,
                    'codigo_real' => $oa->codigo,
                    'codigo' => 'OA' . $oa->codigo
                ];
            }
        }

        return arrayToObject($data);
    }

    /**
     * Retorna un grupo de oas
     *
     * @return array
     */
    public function getGrupoOAs($nivel_id, $asignatura_id)
    {
        return $this->db->select('id, oa, codigo')
            ->where('nivel_id', $nivel_id)
            ->where('asignatura_id', $asignatura_id)
            ->order_by('CAST(substr(codigo, 3,3) AS integer) ASC')
            ->get('oa')
            ->result();
    }

    /**
     * Retorna un grupo de ejes
     *
     * @return array
     */
    public function getGrupoEjes($ejes)
    {
        return $this->db->select('id, ejetematico')
            ->where_in('id', $ejes)
            ->get('ejetematico')
            ->result();
    }

    /**
     * Retorna preguntas según el OA seleccionado
     *
     * @return array
     */
    public function getPreguntasFromOA($tipo_id, $asignatura_id, $nivel_id)
    {
        
        $query = $this->db->query(
            "SELECT * FROM prueba_has_pregunta
            JOIN pregunta ON pregunta.id =  prueba_has_pregunta.pregunta_id
            JOIN oa ON pregunta.oa_id = oa.id
            WHERE prueba_id IN (SELECT id FROM prueba WHERE nivel_id=$nivel_id AND asignatura_id=$asignatura_id AND tipo_id=$tipo_id)"
        );

        return $query->num_rows() > 0 ? $query->result() : false;
    }

    /**
     * Retorna preguntas según el Eje seleccionado
     *
     * @return array
     */
    public function getPreguntasFromEje($ejes, $prueba_id, $asignatura_id, $nivel_id)
    {
        $preguntas = 0;
        $ejetematico = "";

        if ($prueba_id)
        {
            $preguntas = [];
            $query = $this->db->query("SELECT pregunta_id FROM prueba_has_pregunta WHERE prueba_id = $prueba_id");

            foreach($query->result() as $pregunta)
            {
                $preguntas[] = $pregunta->pregunta_id;
            }
        }

        foreach ($ejes as $eje) 
            $ejetematico.= $eje.",";
                
        $query = $this->db->query(
            "SELECT p.id, p.pregunta, p.ejetematico_id, p.habilidadcurricular_id, e.ejetematico 
        FROM prueba_has_pregunta pp
        JOIN pregunta p ON p.id =  pp.pregunta_id
        JOIN ejetematico e ON e.id = p.ejetematico_id 
        WHERE prueba_id IN (SELECT id FROM prueba WHERE tipo_id = 1 AND nivel_id = $nivel_id AND asignatura_id=$asignatura_id) 
        AND ejetematico_id IN (".trim($ejetematico, ',').")"
        );

        return $query->num_rows() > 0 ? $query->result() : false;
        
    }

    /**
     * Retorna respuestas según la pregunta
     *
     * @return array
     */
    public function getRespuestasFromPregunta($pregunta_id)
    {
        return $this->db->select('id, respuesta, correcta, letra')
            ->where('pregunta_id', $pregunta_id)
            ->where('estado_id', 1)
            ->order_by('id', 'asc')
            ->get('respuesta')
            ->result();
    }

    /**
     * Crea una prueba
     *
     * @return int
     */
    public function setPrueba($data)
    {
        $this->db->insert($this->rbd . '.prueba', $data);
        if ($this->db->affected_rows())
        {
            return $this->db->insert_id();
        }
        return 0;
    }

    /**
     * Crea una pregunta y sus respuestas
     *
     * @return int
     */
    public function setPregunta($data)
    {

        $this->db->trans_begin();

        $pregunta_data = [
            'pregunta' => '@',
            'ejetematico_id' => $data['ejetematico_id'] ? $data['ejetematico_id'] : null,
            'habilidadcurricular_id' => $data['habilidadcurricular_id'] ? $data['habilidadcurricular_id'] :  null,
            'ie_id' => $data['ie_id'],
            /*'oa_id' => $data['oa_id'],*/
            'codigo' => ''
        ];

        $this->db->insert($this->rbd . '.pregunta', $pregunta_data);
        $id_pregunta = $this->db->insert_id();

        if (!$data['pregunta_editar_orden'])
        {
            $query = $this->db->select('COALESCE(MAX(orden), 0) + 1 as orden')
                    ->where('prueba_id', $data['prueba_id'])
                    ->get($this->rbd . '.prueba_has_pregunta');

            $orden = $query->row()->orden;
        } else {
            $orden = $data['pregunta_editar_orden'];
        }

        $this->db->insert($this->rbd . '.prueba_has_pregunta', ['prueba_id' => $data['prueba_id'], 'pregunta_id' => $id_pregunta, 'orden' => $orden]);

        $diccionario = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

        foreach ($data['respuesta'] as $i => $respuesta) {
            $correcta = FALSE;
            if ($data['correcta'][0] == $i)
            {
                $correcta = TRUE;
            }

            $this->db->insert($this->rbd . '.respuesta', [
                'respuesta' => $respuesta,
                'pregunta_id' => $id_pregunta,
                'correcta' => $correcta,
                'estado_id' => 1,
                'letra' => $diccionario[$i]
            ]);

            $id_respuesta = $this->db->insert_id();

        }

        $this->db->where('id', $id_pregunta)
            ->update($this->rbd . '.pregunta', ['pregunta' => $data['pregunta']]);

        $this->db->query('DELETE FROM "'.$this->rbd.'".respuesta WHERE pregunta_id in (SELECT id FROM "'.$this->rbd.'".pregunta WHERE pregunta = \'@\')');
        $this->db->query('DELETE FROM "'.$this->rbd.'".prueba_has_pregunta WHERE pregunta_id in (SELECT id FROM "'.$this->rbd.'".pregunta WHERE pregunta = \'@\')');
        $this->db->query('DELETE FROM "'.$this->rbd.'".pregunta WHERE pregunta = \'@\'');

         $this->db->trans_commit();

        return true;
    }

    /**
     * Retorna preguntas creadas según prueba
     *
     * @return array
     */
    public function getPreguntasCreadas($prueba_id)
    {
        return $this->db->select('p.id, p.pregunta, p.ejetematico_id, e.ejetematico, p.habilidadcurricular_id, php.orden, (SELECT codigo FROM oa WHERE oa.id IN (SELECT ie.oa_id FROM ie WHERE ie.id = p.ie_id) LIMIT 1) as oa')
            ->join($this->rbd . '.prueba_has_pregunta php', 'php.pregunta_id = p.id')
            ->join('ejetematico e', 'e.id = p.ejetematico_id', 'left')
            ->where('php.prueba_id = ' . $prueba_id)
            ->order_by('php.orden', 'ASC')
            ->get($this->rbd . '.pregunta p')
            ->result();
    }

    /**
     * Retorna respuestas según la pregunta creada
     *
     * @return array
     */
    public function getRespuestasFromPreguntaCreada($pregunta_id)
    {
        return $this->db->select('id, respuesta, correcta, letra')
            ->where('pregunta_id', $pregunta_id)
            ->where('estado_id', 1)
            ->order_by('id', 'asc')
            ->get($this->rbd . '.respuesta')
            ->result();
    }

    /**
     * Elimina una pregunta creada
     *
     * @return void
     */
    public function destroyPreguntaCreada($pregunta, $prueba)
    {
        $this->db->delete($this->rbd . '.respuesta', ['pregunta_id' => $pregunta]);
        $this->db->delete($this->rbd . '.prueba_has_pregunta', ['prueba_id' => $prueba, 'pregunta_id' => $pregunta]);
        $this->db->delete($this->rbd . '.pregunta', ['id' => $pregunta]);

        // reordena todas las preguntas
        $query = $this->db->select('pregunta_id')
            ->where('prueba_id', $prueba)
            ->order_by('orden', 'ASC')
            ->get($this->rbd . '.prueba_has_pregunta');

        $preguntas = $query->result();

        foreach ($preguntas as $key => $php) {
            $this->db->where(['prueba_id' => $prueba, 'pregunta_id' => $php->pregunta_id])
                ->update($this->rbd . '.prueba_has_pregunta', ['orden' => $key + 1]);
        }
    }

    /**
     * Mueve una posición más arriba la pregunta
     *
     * @return void
     */
    public function moveToUpPreguntaCreada($pregunta, $prueba)
    {
        $query = $this->db->select('orden')
            ->where(['prueba_id' => $prueba, 'pregunta_id' => $pregunta])
            ->get($this->rbd . '.prueba_has_pregunta');

        $orden = $query->row()->orden;

        if ($orden > 1)
        {
            $this->db->where(['prueba_id' => $prueba, 'orden' => $orden - 1])
                ->update($this->rbd . '.prueba_has_pregunta', ['orden' => $orden]);

            $this->db->where(['prueba_id' => $prueba, 'pregunta_id' => $pregunta])
                ->update($this->rbd . '.prueba_has_pregunta', ['orden' => $orden - 1]);
        }

    }

    /**
     * Mueve una posición más abajo la pregunta
     *
     * @return void
     */
    public function moveToDownPreguntaCreada($pregunta, $prueba)
    {
        $query = $this->db->select('orden')
            ->where(['prueba_id' => $prueba, 'pregunta_id' => $pregunta])
            ->get($this->rbd . '.prueba_has_pregunta');

        $orden = $query->row()->orden;

        $query = $this->db->select('MAX(orden) as orden_maximo')
            ->where(['prueba_id' => $prueba])
            ->get($this->rbd . '.prueba_has_pregunta');

        $orden_maximo = $query->row()->orden_maximo;

        if ($orden < $orden_maximo)
        {
            $this->db->where(['prueba_id' => $prueba, 'orden' => $orden + 1])
                ->update($this->rbd . '.prueba_has_pregunta', ['orden' => $orden]);

            $this->db->where(['prueba_id' => $prueba, 'pregunta_id' => $pregunta])
                ->update($this->rbd . '.prueba_has_pregunta', ['orden' => $orden + 1]);
        }
    }

    /**
     * Copia las preguntas de Aeduc al banco de preguntas del profesor
     *
     * @return void
     */
    public function copyPreguntasToCreadas($prueba_id, $preguntas)
    {
        foreach ($preguntas as $key => $pregunta) {

            $query = $this->db->select('p.pregunta, p.ejetematico_id, p.habilidadcurricular_id, p.ie_id, p.codigo, (SELECT id FROM oa WHERE oa.id IN (SELECT ie.oa_id FROM ie WHERE ie.id = p.ie_id) LIMIT 1) as oa_id')
                ->where('p.id', $pregunta)
                ->get('pregunta p');

            $pregunta_data = $query->row_array();

            $this->db->insert($this->rbd . '.pregunta', $pregunta_data);
            $id_pregunta = $this->db->insert_id();

            $query = $this->db->select('COALESCE(MAX(orden), 0) + 1 as orden')
                    ->where('prueba_id', $prueba_id)
                    ->get($this->rbd . '.prueba_has_pregunta');

            $orden = $query->row()->orden;

            $this->db->insert($this->rbd . '.prueba_has_pregunta', ['prueba_id' => $prueba_id, 'pregunta_id' => $id_pregunta, 'orden' => $orden]);

            $query = $this->db->select('respuesta, ' . $id_pregunta . ' as pregunta_id, correcta, estado_id, letra')
                    ->where('pregunta_id', $pregunta)
                    ->get('respuesta');

            $respuestas = $query->result_array();

            $this->db->insert_batch($this->rbd . '.respuesta', $respuestas);
        }
    }

    /**
     * Retorna las pruebas de aeduc
     *
     * @return array
     */
    

    public function getPruebasAeduc($nivel_id, $asignatura_id, $tipo_id)
    {
        return $this->db->select('COUNT(php.pregunta_id) AS count_pregunta, p.id,p.codigo, p.codigo, p.num, a.asignatura, n.nivel, t.tipo, p.nivel_id, p.asignatura_id, p.tipo_id')
            ->where(['p.nivel_id' => $nivel_id, 'p.asignatura_id' => $asignatura_id, 'p.tipo_id' => $tipo_id])
            ->join('asignatura a', 'a.id = p.asignatura_id')
            ->join('prueba_has_pregunta php', 'p.id = php.prueba_id', 'left')
            ->join('nivel n', 'n.id = p.nivel_id')
            ->join('tipo t', 't.id = p.tipo_id')
            ->group_by(array("p.id", "n.nivel", "a.asignatura", "t.tipo", "p.nivel_id", "p.asignatura_id", "p.tipo_id"))
            ->order_by('p.codigo', 'asc')
            ->get('prueba p')
            ->result();
    }

    /**
     * Copia una prueba de Aeduc al banco de pruebas del profesor asociado a una prueba especifica
     *
     * @return void
     */
    public function copyPruebaToCreada($prueba_id, $prueba_aeduc)
    {

        $query = $this->db->select('pregunta_id as id')
            ->where('prueba_id', $prueba_aeduc)
            ->order_by('orden', 'ASC')
            ->get('prueba_has_pregunta');

        $preguntas = $query->result();

        foreach ($preguntas as $key => $pregunta) {

            $query = $this->db->select('pregunta, ejetematico_id, habilidadcurricular_id, ie_id, codigo')
                ->where('id', $pregunta->id)
                ->get('pregunta');

            $pregunta_data = $query->row_array();

            $this->db->insert($this->rbd . '.pregunta', $pregunta_data);
            $id_pregunta = $this->db->insert_id();

            $query = $this->db->select('COALESCE(MAX(orden), 0) + 1 as orden')
                    ->where('prueba_id', $prueba_id)
                    ->get($this->rbd . '.prueba_has_pregunta');

            $orden = $query->row()->orden;

            $this->db->insert($this->rbd . '.prueba_has_pregunta', ['prueba_id' => $prueba_id, 'pregunta_id' => $id_pregunta, 'orden' => $orden]);

            $query = $this->db->select('respuesta, ' . $id_pregunta . ' as pregunta_id, correcta, estado_id, letra')
                    ->where('pregunta_id', $pregunta->id)
                    ->get('respuesta');

            $respuestas = $query->result_array();

            $this->db->insert_batch($this->rbd . '.respuesta', $respuestas);
        }
    }

    /**
     * Retorna oas de la prueba copiada desde Aeduc
     *
     * @return array
     */
    public function getOAsFromPruebaCopiada($prueba_id)
    {
        $data = [];

        $query = $this->db->distinct()
            ->select('oa.id')
            ->join($this->rbd . '.prueba_has_pregunta php', 'php.pregunta_id = p.id')
            ->join('ie', 'ie.id = p.ie_id')
            ->join('oa', 'oa.id = ie.oa_id')
            ->where('php.prueba_id', $prueba_id)
            ->get($this->rbd . '.pregunta p')
            ->result();

        foreach ($query as $oa) {
            $data[] = $oa->id;
        }

        return $data;
    }

    /**
     * Retorna ejes de la prueba copiada desde Aeduc
     *
     * @return array
     */
    public function getEjesFromPruebaCopiada($prueba_id)
    {
        $data = [];

        $query = $this->db->distinct()
            ->select('e.id')
            ->join($this->rbd . '.prueba_has_pregunta php', 'php.pregunta_id = p.id')
            ->join('ejetematico e', 'e.id = p.ejetematico_id')
            ->where('php.prueba_id', $prueba_id)
            ->get($this->rbd . '.pregunta p')
            ->result();

        foreach ($query as $eje) {
            $data[] = $eje->id;
        }

        return $data;
    }

    /**
     * Da por cerrada una prueba creada
     *
     * @param int $prueba_id ID de la prueba que se está buscando
     * @return void
     */
    public function setCerrarPrueba($prueba_id)
    {
        $this->db->where('id', $prueba_id)
            ->update($this->rbd . '.prueba', ['creada' => TRUE, 'modificado' => date('Y-m-d H:i:s')]);
    }

    /**
     * Retorna el listado de pruebas creadas por el usuario
     *
     * @return array
     */
    public function getPruebasCreadas($idAsignatura, $idNivel, $idTipo)
    {
        $usuario_id = $this->session->idUser;

        $this->db->select('COUNT(php.pregunta_id) AS count_pregunta, p.id, n.nivel, a.asignatura, t.tipo, p.nombre, p.creada,
        p.prueba_original, p.nivel_id, p.asignatura_id, p.tipo_id, p.modificado as fecha', FALSE);
        $this->db->join($this->rbd.'.prueba_has_pregunta php', 'php.prueba_id = p.id', 'left');
        $this->db->join('asignatura a', 'a.id = p.asignatura_id');
        $this->db->join('nivel n', 'n.id = p.nivel_id');
        $this->db->join('tipo t', 't.id = p.tipo_id');
        $this->db->where('p.usuario_id', $usuario_id);
        if ($idAsignatura)
            $this->db->where('a.id', $idAsignatura);
        if ($idNivel)
            $this->db->where('n.id', $idNivel);
        if ($idTipo)
            $this->db->where('t.id', $idTipo);
        $this->db->group_by('p.id, n.nivel, a.asignatura, t.tipo, p.nombre, p.creada, p.prueba_original, p.nivel_id, p.asignatura_id, p.tipo_id');
        $this->db->order_by('p.id', 'ASC');
        $response = $this->db->get($this->rbd . '.prueba p');

        return $response->result();
    }

    /**
     * Retorna el listado de IE asociados al OA seleccionado
     *
     * @return array
     */
    public function getIEsFromOas($oa_id)
    {
        $data = [];

        $ies = $this->db->distinct()
            ->select('id, ie')
            ->where('oa_id', $oa_id)
            ->get('ie')
            ->result();

        foreach ($ies as $key => $ie) {
            if (!in_multiarray($ie->ie, $data)){
                $data[] = [
                    'id' => $ie->id,
                    'ie' => $ie->ie
                ];
            }
        }

        return arrayToObject($data);
    }

    /**
     * Retorna la información de la pregunta
     *
     * @return array
     */
    public function getPregunta($prueba_id, $pregunta_id)
    {
        return $this->db->select('p.id, p.pregunta, p.ejetematico_id, p.habilidadcurricular_id, p.ie_id, p.oa_id, (SELECT orden FROM "' . $this->rbd . '".prueba_has_pregunta WHERE pregunta_id = p.id AND prueba_id = ' . $prueba_id .') as orden, (SELECT codigo FROM oa WHERE oa.id IN (SELECT ie.oa_id FROM ie WHERE ie.id = p.ie_id) LIMIT 1) as oa_codigo, (SELECT ie FROM ie WHERE ie.id = p.ie_id) ie_ie')
            ->where('p.id', $pregunta_id)
            ->get($this->rbd . '.pregunta p')
            ->row();
    }

    /**
     * Retorna la información de las respuestas
     *
     * @return array
     */
    public function getRespuestas($prueba_id, $pregunta_id)
    {
        return $this->db->select('id, respuesta, correcta, letra')
            ->where('pregunta_id', $pregunta_id)
            ->where('estado_id', 1)
            ->get($this->rbd . '.respuesta')
            ->result();
    }

    /**
     * Elimina una pregunta con sus respuestas
     *
     * @return void
     */
    public function removePregunta($prueba_id, $pregunta_id)
    {
        $this->db->query('DELETE FROM "'.$this->rbd.'".respuesta WHERE pregunta_id = ' . $pregunta_id . '');
        $this->db->query('DELETE FROM "'.$this->rbd.'".prueba_has_pregunta WHERE pregunta_id = ' . $pregunta_id . ' AND prueba_id = ' . $prueba_id . '');
        $this->db->query('DELETE FROM "'.$this->rbd.'".pregunta WHERE id = ' . $pregunta_id . '');
    }

    public function getAsignacionesPersonal($estado, $idUsuario, $idAsignatura, $idNivel, $idTipo)
    {
        $this->db->select('a.id, p.codigo, p.asignatura_id, ag.asignatura, p.nivel_id, n.nivel, p.tipo_id, t.tipo, a.prueba_id,
        p.nombre, p.usuario_id, u.nombre AS nombre_creador, u.apellido AS apellido_creador, a.curso_id, c.curso, c.letra');
        $this->db->join($this->rbd.'.prueba p', 'p.id = a.prueba_id');
        $this->db->join('asignatura ag', 'ag.id = p.asignatura_id');
        $this->db->join('nivel n', 'n.id = p.nivel_id');
        $this->db->join('tipo t', 't.id = p.tipo_id');
        $this->db->join($this->rbd.'.curso c', 'c.id = a.curso_id');
        $this->db->join('usuario u', 'u.id = p.usuario_id');
        $this->db->where(['a.estado' => $estado, 'origen' => 'PERSONAL']);

        if ($idUsuario)
            $this->db->where('a.usuario_id', $idUsuario);
        if ($idAsignatura)
            $this->db->where('ag.id', $idAsignatura);
        if ($idNivel)
            $this->db->where('n.id', $idNivel);
        if ($idTipo)
            $this->db->where('t.id', $idTipo);

        $result = $this->db->get($this->rbd.'.asignacion a');

        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function getAsignaciones($prueba_id)
    {
        $this->db->select('a.id, a.estado, a.curso_id, a.prueba_id, c.curso, c.letra');
        $this->db->join($this->rbd.'.curso c', 'a.curso_id=c.id');
        $this->db->where('prueba_id', $prueba_id);
        //$this->db->where('origen', 'PERSONAL');

        $result = $this->db->get($this->rbd.'.asignacion a');

        return $result->num_rows() > 0 ? $result->result() : FALSE;
    
    }

    public function datosPrueba($prueba_id)
    {
        $this->db->where('id', $prueba_id);
        $query = $this->db->get($this->rbd.'.prueba');

        return $query->row();
    }

    public function getDataRespuesta($asignacion, $letra, $numero, $origen)
    {
        if($origen == 'CLASSRUN')
            $prefijo = '';
        else
            $prefijo = '"'.$this->rbd.'".';

        $query = $this->db->query(
            'SELECT r.id, r.pregunta_id FROM '.$prefijo.'respuesta r
        JOIN '.$prefijo.'prueba_has_pregunta pp ON r.pregunta_id = pp.pregunta_id
        WHERE pp.orden = '.$numero.' AND pp.prueba_id = (SELECT prueba_id FROM "'.$this->rbd.'".asignacion WHERE id = '.$asignacion.')
        AND r.letra = \''.$letra.'\''
        );

        return $query->num_rows() > 0 ? $query->row() : false;

        
    }

    public function setRespuesta($alumno, $respuesta, $pregunta, $asignacion)
    {
        //$rut = substr($alumno, 0, -1)."-".$alumno[strlen($alumno)-1];
        $data = [
            'usuario_id' =>  $alumno,
            'respuesta_id' => $respuesta,
            'pregunta_id' => $pregunta,
            'asignacion_id' => $asignacion
            
        ];

        $this->db->insert($this->rbd.'.hojarespuesta', $data);
    }

    public function getOrigen($asignacion)
    {
        $query = $this->db->query(
            'SELECT origen FROM "'.$this->rbd.'".asignacion WHERE id='.$asignacion.''
        );

        return $query->num_rows() > 0 ? $query->row() : false;
    }



    public function deletePrueba($prueba)
    {
        $return = false;
        
        $this->db->trans_begin();
        
        
        $this->db->query('DELETE FROM "'.$this->rbd.'".respuesta WHERE pregunta_id IN (SELECT id FROM pregunta WHERE id IN (SELECT pregunta_id FROM "'.$this->rbd.'".prueba_has_pregunta WHERE prueba_id='.$prueba.'))');
        $this->db->query('DELETE FROM "'.$this->rbd.'".pregunta WHERE id IN (SELECT pregunta_id FROM "'.$this->rbd.'".prueba_has_pregunta WHERE prueba_id='.$prueba.')');
        $this->db->query('DELETE FROM "'.$this->rbd.'".prueba_has_pregunta WHERE prueba_id='.$prueba);
        $this->db->query('DELETE FROM "'.$this->rbd.'".prueba WHERE id='.$prueba);

        if ($this->db->trans_status() === FALSE)
        {
                $this->db->trans_rollback();
        }
        else
        {
                $this->db->trans_commit();
                $return = true;
        }

        return $return;
    }

}
