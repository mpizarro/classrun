<?php
class Evaluacion_model extends CI_Model
{
    private $rbd;
    private $id;

    public function __construct()
    {
        parent::__construct();
        $this->rbd = $this->session->rbd;
    }
    public function getEvaluaciones()
    {
        $this->db->select('p.*, n.nivel, a.asignatura, t.tipo');
        $this->db->from('prueba p');

        $this->db->join('nivel n', 'p.nivel_id = n.id');
        $this->db->join('asignatura a', 'p.asignatura_id = a.id');
        $this->db->join('tipo t', 'p.tipo_id = t.id');

        $this->db->where_in('p.tipo_id', [1, 2, 3, 4]);
        $query = $this->db->get();

        return $query->num_rows() > 0 ? $query->result() : false;
    }
    public function getNivelesEvaluacion($idAsignatura, $numeroEvaluacion, $tipoEvaluacion, $maxNivel, $minNivel)
    {
        $this->db->select('p.id, p.nivel_id, n.nivel, p.codigo');
        $this->db->from('prueba p');

        $this->db->join('nivel n', 'n.id = p.nivel_id');

        $this->db->where_in('p.tipo_id', [1, 2, 3, 4]);
        $this->db->where('asignatura_id', $idAsignatura);
        $this->db->where('tipo_id', $tipoEvaluacion);
        $this->db->where('num', $numeroEvaluacion);
        $this->db->where('p.nivel_id <=', $maxNivel);
        $this->db->where('p.nivel_id >=', $minNivel);
        $this->db->order_by('n.id', 'ASC');

        $query = $this->db->get();

        return $query->num_rows() > 0 ? $query->result() : false;
    }
    public function getStatusEvaluacionAndUser($idUsuario, $idEvaluacion)
    {
        $where = array(
            'estado' => 1,
            'usuario_id' => $idUsuario,
            'prueba_id' => $idEvaluacion
        );
        $this->db->where($where);

        $query = $this->db->get($this->rbd.'.asignacion');

        return $query->num_rows() > 0;
    }
    public function getActivasUser($idUsuario)
    {
        $where = array(
            'estado' => 1,
            'usuario_id' => $idUsuario
        );
        $this->db->where($where);

        $query = $this->db->get($this->rbd.'.asignacion');

        return $query->num_rows() > 0 ? $query->result() : false;
    }
    public function getAlternativasPregunta($idPregunta, $origen='CLASSRUN')
    {
        $this->db->select('*');
        if ($origen == 'CLASSRUN')
            $this->db->from('respuesta');
        else
            $this->db->from($this->rbd.'.respuesta');
        $this->db->where(array('pregunta_id' => $idPregunta, 'estado_id' => 1));
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();

        return $query->num_rows() > 0 ? $query->result() : false;
    }
    public function getCursos($idColegio)
    {
        $this->db->where('c.colegio_id', $idColegio);

        $query = $this->db->get("'$this->rbd'.curso c");
        return ($query->num_rows() > 0) ? $query->result() : false;
    }
    public function getAsignacionByPruebaCursoUsuario($idPrueba, $idCurso, $idUsuario, $origen = 'AEDUC')
    {
        $dataWhere = array(
            'curso_id' => $idCurso,
            'usuario_id' => $idUsuario,
            'prueba_id' => $idPrueba,
            'origen' => $origen
        );
        $this->db->where($dataWhere);

        $query = $this->db->get($this->rbd.'.asignacion');
        return $query->num_rows() == 1 ? $query->row() : false;
    }
    public function ejecutarAsignacion($idAsignacion)
    {
        $this->db->where('a.id', $idAsignacion);

        return $this->db->update($this->rbd.'.asignacion a', array('estado' => 1));
    }
    public function crearAsignacion($idEvaluacion, $idCurso, $idUsuario, $origen)
    {
        $data = array(
            'prueba_id' => $idEvaluacion,
            'curso_id' => $idCurso,
            'usuario_id' => $idUsuario,
            'informe_url' => '',
            'modificado' => 'now()',
            'origen' => $origen
        );
        $id = false;
        if ($this->db->insert($this->rbd.'.asignacion', $data)) {
            $id = $this->db->insert_id();
        }
        return $id;
    }
    public function asignarUsuarioAsignacion($data)
    {
        $this->db->insert_batch($this->rbd.'.usuario_has_asignacion', $data);
        return $this->db->affected_rows() > 0 ? true : false;
    }
    public function getStatusAsignacion($idUsuario)
    {
        $this->db->where('usuario_id', $idUsuario);
        $this->db->where('prueba_id', $this->id);

        $query = $this->db->get($this->rbd.'.asignacion');

        return $query->num_rows() == 1 ? $query->row()->estado : 0;
    }
    public function finalizarEvaluacion($idUsuario)
    {
        $this->db->where('usuario_id', $idUsuario);
        $this->db->where('prueba_id', $this->id);

        return $this->db->update($this->rbd.'.asignacion', array('estado' => 0));
    }
    public function setId($idNew)
    {
        $this->id = $idNew;
    }
    public function getTiposEvaluacion()
    {
        $this->db->select('DISTINCT t.*', false);
        $this->db->from('prueba p');
        $this->db->join('tipo t', 't.id = p.tipo_id');
        $this->db->where_in('p.tipo_id', [1, 2, 3, 4]);

        $query = $this->db->get();

        return $query->num_rows() > 0 ? $query->result() : false;
    }
    public function getTipo()
    {
        $result = $this->db->get('tipo');
        return $result->num_rows() > 0 ? $result->result() : false;
    }
    public function guardarEvaluacion($data)
    {
        return $this->db->insert('prueba', $data);
    }
    public function getEvaluacion($id, $origen="CLASSRUN", $asignacion_id = null)
    {
        $this->db->select('p.*, n.nivel, p.nivel_id, a.asignatura, p.asignatura_id, t.tipo, a.color, p.num');
        if ($origen == 'CLASSRUN')
            $this->db->from('prueba p');
        else
            $this->db->from($this->rbd.'.prueba p');
        $this->db->join('nivel n', 'p.nivel_id = n.id');
        $this->db->join('asignatura a', 'p.asignatura_id = a.id');
        $this->db->join('tipo t', 'p.tipo_id = t.id', 'left');
        if ($asignacion_id)
        {
            $this->db->join($this->rbd .'.asignacion s', 's.prueba_id = p.id');
            $this->db->where('s.id', $asignacion_id);
        }
        $this->db->where_in('p.tipo_id', [1, 2, 3, 4]);
        $this->db->where('p.id', $id);

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row() : false;
    }

    public function getEvaluacionWithoutAsignacion($id, $origen="classrun")
    {
        $this->db->select('p.*, n.nivel, p.nivel_id, a.asignatura, p.asignatura_id, t.tipo, a.color, p.num');
        if ($origen == 'classrun')
            $this->db->from('prueba p');
        else
            $this->db->from($this->rbd.'.prueba p');
        $this->db->join('nivel n', 'p.nivel_id = n.id');
        $this->db->join('asignatura a', 'p.asignatura_id = a.id');
        $this->db->join('tipo t', 'p.tipo_id = t.id', 'left');
        $this->db->where_in('p.tipo_id', [1, 2, 3, 4]);
        $this->db->where('p.id', $id);

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row() : false;
    }
    public function editarEvaluacion($data, $id)
    {
        return $this->db->where('id', $id)
                        ->update('prueba', $data);
    }
    public function getTaxonomias()
    {
        $result = $this->db->get('taxonomia');
        return $result->num_rows() > 0 ? $result->result() : false;
    }
    public function getEjes()
    {
        $result = $this->db->get('ejetematico');
        return $result->num_rows() > 0 ? $result->result() : false;
    }
    public function getHabilidadCurricular()
    {
        $result = $this->db->get('habilidadcurricular');
        return $result->num_rows() > 0 ? $result->result() : false;
    }
    public function getPruebaHasPregunta($idEvaluacion)
    {
        $result = $this->db->get_where('prueba_has_pregunta', array('prueba_id'=>$idEvaluacion));
        return $result->num_rows() > 0 ? $result->result() : false;
    }
    public function getUnidadesByNivelAsignatura($nivel, $asignatura)
    {

        $this->db->from('unidad');
        $this->db->where('nivel_id', $nivel);
        $this->db->where('asignatura_id', $asignatura);

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result() : FALSE;

    }

    public function getOasByUnidad($unidad)
    {

        $this->db->select('oa.id, oa.oa, oa.codigo');
        $this->db->from('unidad_has_oa');
        $this->db->join('oa', 'oa.id = unidad_has_oa.oa_id');
        $this->db->where('unidad_id', $unidad);

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result() : FALSE;

    }

    public function getIesByOa($oa)
    {

        $this->db->select('ie.id, ie.ie');
        $this->db->from('ie');
        $this->db->where('oa_id', $oa);

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result() : FALSE;

    }


    public function getOasByEvaluacion($evaluacion)
    {
        $result = $this->db->select('o.*')
            ->join('unidad u', 'u.id = p.unidad_id')
            ->join('unidad_has_oa uo', 'uo.unidad_id = u.id')
            ->join('oa o', 'o.id = uo.oa_id')
            ->where('p.id', $evaluacion)
            ->get('prueba p');
        return $result->num_rows() > 0 ? $result->result() : false;
    }

    public function getUsuario($idUser)
    {
        $this->db->from('usuario');
        $this->db->where('id', $idUser);
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result() : false;
    }
    public function getPreguntasEvaluacion($idEvaluacion, $origen='CLASSRUN')
    {
        $this->db->select('p.*, php.orden');
        if ($origen == 'CLASSRUN') {
            $this->db->from('pregunta p');
            $this->db->join('prueba_has_pregunta php', 'php.pregunta_id = p.id');
        } else {
            $this->db->from($this->rbd.'.pregunta p');
            $this->db->join($this->rbd.'.prueba_has_pregunta php', 'php.pregunta_id = p.id');
        }
        $this->db->where('php.prueba_id', $idEvaluacion);
        $this->db->order_by('php.orden');
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result() : false;
    }
    public function checkPrevNotasAlumno($idAlumno, $evaluacion, $checkStatus = true)
    {

        $b = $idAlumno == 20;
        $return = array();
        for ($i=$evaluacion->num; $i > 0; $i--) {
            $newCode = substr($evaluacion->codigo, 0, -1) . $i;

            $sql = 'SELECT hr.*, r.correcta
            FROM "'.$this->rbd.'".hojarespuesta hr
            JOIN "'.$this->rbd.'".asignacion ON asignacion.id = hr.asignacion_id
            JOIN prueba ON prueba.id = asignacion.prueba_id
            JOIN respuesta r ON r.id = hr.respuesta_id
            WHERE hr.usuario_id= '.$idAlumno." AND prueba.codigo = '$newCode'";

            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $data = $query->result();
                $return[$query->result()[0]->asignacion_id] = array();
                if ($data) {
                    $maxNotas = $this->maxPreguntas($newCode);
                    foreach ($data as $hojaRespuesta) {
                        $return[$query->result()[0]->asignacion_id][] = array(
                            'id' => $hojaRespuesta->id,
                            'pregunta_id' => $hojaRespuesta->pregunta_id,
                            'respuesta_id' => $hojaRespuesta->respuesta_id,
                            'correcta' => $hojaRespuesta->correcta == 't',
                            'maxNotas' => $maxNotas
                        );
                    }
                }
            }
        }
        return $return;
    }
    public function maxPreguntas($codePueba)
    {
        $this->db->select('DISTINCT php.pregunta_id', false);
        $this->db->from('prueba_has_pregunta php');
        $this->db->join('prueba p', 'php.prueba_id = p.id');
        $this->db->where('p.codigo', $codePueba);

        $query = $this->db->get();
        return $query->num_rows();
    }
    public function getEvaluacionByAsignacion($idAsignacion)
    {
        $this->db->join('prueba p', 'p.id = a.prueba_id');
        if (gettype($idAsignacion) == 'array') {
            $this->db->select('a.id AS asignacion_id, p.*');
            $this->db->where_in('a.id', $idAsignacion);
        } else {
            $this->db->where('a.id', $idAsignacion);
        }

        $this->db->order_by('p.num', 'asc');
        $query = $this->db->get($this->rbd.'.asignacion a');
        return $query->num_rows() > 0 ? $query->result() : false;
    }

    public function getServicios($idColegio) {
        $result = $this->db->select('s.id, s.servicio')
        ->join('servicio s', 's.id = cs.servicio_id')
        ->get_where('colegio_has_servicio cs', ['cs.colegio_id' => $idColegio]);
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }
}
