<?php
class Prueba_model extends CI_Model {

    private $rbd;

    public function __construct() 
    {
        parent::__construct();

        $this->rbd = $this->session->rbd;
    }

    public function setRbd($rbd)
    {
        $this->rbd = $rbd;
    }
    
    public function get($idPrueba, $origen)
    {
        $sql = 'SELECT p.id, p.codigo, p.asignatura_id, p.nivel_id, p.num, p.tipo_id, COUNT(pp.id) preguntas ';
        if ($origen == 'CLASSRUN') {
            $sql .= 'FROM prueba p
            JOIN prueba_has_pregunta pp ON pp.prueba_id = p.id
            JOIN pregunta pr ON pr.id = pp.pregunta_id ';
        } else {
            $sql .= 'FROM "'.$this->rbd.'".prueba p
            JOIN "'.$this->rbd.'".prueba_has_pregunta pp ON pp.prueba_id = p.id
            JOIN "'.$this->rbd.'".pregunta pr ON pr.id = pp.pregunta_id ';
        }
        $sql .= 'WHERE p.id = '.$idPrueba.'
        GROUP BY p.id, p.codigo, p.asignatura_id, p.nivel_id, p.num, p.tipo_id';

        $query = $this->db->query($sql);

        return $query->num_rows() > 0 ? $query->row() : FALSE;
        
    }

    public function getCountPreguntasPruebaByAsignacion($idAsignacion, $origen)
    {
        $prefijo = $this->rbd.'.'; 
        if($origen == 'CLASSRUN')
            $prefijo = '';
        
        $result = $this->db->select('COUNT(pp.pregunta_id) AS n_preguntas', FALSE)
        ->join($prefijo.'prueba p', 'p.id = a.prueba_id')
        ->join($prefijo.'prueba_has_pregunta pp', 'pp.prueba_id = p.id')
        ->where(['a.id' => $idAsignacion])
        ->get($this->rbd.'.asignacion a');

        return $result->num_rows() > 0 ? $result->row()->n_preguntas : FALSE;
    }

    public function getPruebaPreguntaByAsignacion($idAsignacion, $origen)
    {
        $prefijo = $this->rbd.'.'; 
        if($origen == 'CLASSRUN')
            $prefijo = '';

        $result = $this->db->select('pg.*, pp.orden')
        ->join($prefijo.'prueba p', 'p.id = a.prueba_id')
        ->join($prefijo.'prueba_has_pregunta pp', 'pp.prueba_id = p.id')
        ->join($prefijo.'pregunta pg', 'pg.id = pp.pregunta_id')
        ->where(['a.id' => $idAsignacion])
        ->order_by('pp.orden')
        ->get($this->rbd.'.asignacion a');

        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function getPruebaByAsignacion($idAsignacion, $origen="CLASSRUN") {
        $this->db->select('p.*');
        if ($origen == "CLASSRUN")
            $this->db->join('prueba p', 'p.id = a.prueba_id');
        else 
            $this->db->join($this->rbd.'.prueba p', 'p.id = a.prueba_id');
        $this->db->where(['a.id' => $idAsignacion]);
        $result = $this->db->get($this->rbd.'.asignacion a');
        return $result->num_rows() > 0 ? $result->row() : FALSE;
    }
}