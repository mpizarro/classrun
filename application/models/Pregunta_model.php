<?php
class Pregunta_model extends CI_Model
{
    private $id;

    public function __construct()
    {
        parent::__construct();
    }
    public function getPreguntasEvaluacion($idEvaluacion)
    {
        $this->db->select('p.*, php.orden');
        $this->db->from('prueba_has_pregunta php');
        $this->db->join('pregunta p', 'php.pregunta_id = p.id');

        $this->db->where(array('php.prueba_id'=>$idEvaluacion));

        $this->db->order_by('php.orden');

        $query = $this->db->get();

        return $query->num_rows() > 0 ? $query->result() : FALSE;
    }
    public function getPreguntaId($idPregunta)
    {
        $query = $this->db->select('p.pregunta, p.taxonomia_id, p.habilidadcurricular_id, p.ejetematico_id, p.codigo,
                            h.habilidadcurricular, e.ejetematico, p.ie_id, p.unidad_id, p.oa_id')
                        ->join('habilidadcurricular h', 'h.id = p.habilidadcurricular_id', 'left')
                        ->join('ejetematico e', 'e.id = p.ejetematico_id', 'left')
                        ->where(array('p.id' => $idPregunta))
                        ->get('pregunta p');

        return $query->num_rows() > 0 ? $query->row() : FALSE;
    }
    public function eliminarPregunta($idPregunta)
    {
        $this->db->delete('respuesta', ['pregunta_id' => $idPregunta]);
        $this->db->delete('prueba_has_pregunta', ['pregunta_id' => $idPregunta]);
        $resp = $this->db->delete('pregunta', ['id' => $idPregunta]);

        return $resp;

        // if($this->db->where('id', $idPregunta)->update('pregunta', array('estado_id' => 2)))
        //     $resp = $this->db->where('pregunta_id', $idPregunta)->update('respuesta', array('estado_id' => 2));
        // return $resp;
    }

    public function guardarPregunta($dataPregunta, $dataPHP)
    {
        $response['idPregunta'] = FALSE;
        $response['response'] = FALSE;

        if($this->db->insert('pregunta', $dataPregunta))
        {
            $response['idPregunta'] = $this->db->insert_id();
            $this->db->flush_cache();
            $dataPHP['pregunta_id'] = $response['idPregunta'];
            $response['response'] = $this->db->insert('prueba_has_pregunta', $dataPHP);
        }
        return $response;
    }
    public function actualizarPregunta($dataUpdate, $idPregunta)
    {
        return $this->db->where('id', $idPregunta)
                        ->update('pregunta', $dataUpdate);
    }
}