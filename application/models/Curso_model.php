<?php
class Curso_model extends CI_Model
{
    private $rbd;

    public function __construct()
    {
        parent::__construct();
        $this->rbd = $this->session->rbd;
    }

    public function setRbd($rbd)
    {
        $this->rbd = $rbd;
    }
    
    public function getAlumnosCursoById($idCurso)
    {
        $this->db->where('u.curso_id', $idCurso);
        $this->db->where('u.colegio_id', $this->session->colegio_id);
        $query = $this->db->get('usuario u');
        return $query->num_rows() > 0 ? $query->result() : false;
    }
    public function getCursosPosiblesUsuario($idUsuario, $rango=false)
    {
        $this->db->select('c.*, n.nivel');
        $this->db->from($this->rbd.'.curso c');
        $this->db->join('nivel n', 'n.id = c.nivel_id');
        $this->db->where_in('c.colegio_id', 'SELECT colegio_id FROM usuario WHERE id = ' . $idUsuario, false);
        $this->db->order_by('c.nivel_id, c.letra');

        $query = $this->db->get();

        return $query->num_rows() > 0 ? $query->result() : false;
    }
    public function getCursoByAsignacion($idAsignacion)
    {
        $result = $this->db->select('c.*, n.nivel')
            ->join($this->rbd.'.asignacion a', 'a.curso_id = c.id')
            ->join('nivel n', 'n.id = c.nivel_id')
            ->where('a.id', $idAsignacion)
            ->get($this->rbd.'.curso c');
            
        return $result->num_rows() > 0 ? $result->row() : false;
    }
    public function getCursosByNivel($idNivel)
    {
        $result = $this->db->select('id, letra, curso')
        ->get_where($this->rbd.'.curso', ['nivel_id' => $idNivel]);
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }
    public function getCursosConAlumnosByColegio($idColegio)
    {
        $result = $this->db->distinct()
        ->select('curso_id')
        ->get_where('usuario', ['colegio_id' => $idColegio, 'curso_id >' => 0]);
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }
    public function getCursoById($id)
    {
        $result = $this->db->get_where($this->rbd.'.curso', ['id' => $id]);
        return $result->num_rows() == 1 ? $result->row() : FALSE;
    }
}
