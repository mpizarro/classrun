<?php
class Respuesta_model extends CI_Model
{
    private $rbd;
    private $id;

    public function __construct()
    {
        parent::__construct();
        $this->rbd = $this->session->rbd;
    }

    public function setRbd($rbd)
    {
        $this->rbd = $rbd;
    }
    
    public function getAlternativasPregunta($idPregunta, $origen='CLASSRUN')
    {
        $this->db->select('*');
        if ($origen == 'CLASSRUN')
            $this->db->from('respuesta');
        else 
            $this->db->from($this->rbd.'.respuesta');

        $this->db->where(array('pregunta_id' => $idPregunta, 'estado_id' => 1));
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();

        return $query->num_rows() > 0 ? $query->result() : FALSE;
    }
    public function getAlternativaId($idAlternativa)
    {
        $query = $this->db->get_where('respuesta', array('id' => $idAlternativa));
        return $query->num_rows() > 0 ? $query->row() : FALSE;
    }
    public function getAlternativasEvaluacion($idEvaluacion)
    {
        $result = $this->db->select("r.*")
                    ->join('pregunta pr', 'pr.id = r.pregunta_id')
                    ->join('prueba_has_pregunta php', 'php.pregunta_id = pr.id')
                    ->where(array('php.prueba_id' => $idEvaluacion, 'r.estado_id' => 1))
                    ->order_by('r.id', 'ASC')
                    ->get('respuesta r');
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }
    public function guardarAlternativa($data)
    {
        return $this->db->insert('respuesta', $data);
    }
    public function actualizarAlternativa($dataUpdate, $idAlternativa)
    {
        return $this->db->where('id', $idAlternativa)
                        ->update('respuesta', $dataUpdate);
    }
    public function eliminarAlternativa($idAlternativa)
    {
        return $this->db->where('id', $idAlternativa)
                        ->update('respuesta', array('estado_id' => 2));
    }
    public function getHojaRespuestaByUsuarioAsignacion($idUsuarioAsignacion, $idPregunta, $idAsignacion) {
        $sql = 'SELECT * FROM "'.$this->rbd.'".hojarespuesta
        WHERE usuario_id = '.$idUsuarioAsignacion.' 
        AND pregunta_id = '.$idPregunta.'
        AND asignacion_id = '.$idAsignacion;
        $result = $this->db->query($sql);
        return $result->num_rows() > 0 ? TRUE : FALSE;
    }
    public function setRespuesta($idAlumno, $idPregunta, $idRespuesta, $idAsignacion) {
        $sql = 'UPDATE "'.$this->rbd.'".hojarespuesta
            SET respuesta_id = '.$idRespuesta.'
            WHERE usuario_id = '.$idAlumno.'
            AND pregunta_id = '.$idPregunta.'
            AND asignacion_id = '.$idAsignacion;
        $this->db->query($sql);
        return $this->db->affected_rows() > 0 ? TRUE : FALSE;
    }
    public function saveRespuesta($idAlumno, $idPregunta, $idRespuesta, $idAsignacion) {
        $sql = 'INSERT INTO "'.$this->rbd.'".hojarespuesta (usuario_id, pregunta_id, respuesta_id, asignacion_id)
            VALUES ('.$idAlumno.', '.$idPregunta.', '.$idRespuesta.', '.$idAsignacion.')';
        return $this->db->query($sql);
    }
    
    public function deleteResouesta($idAlumno, $idPregunta, $idAsignacion) {
        return $this->db->where(['usuario_id' => $idAlumno, 'pregunta_id' => $idPregunta, 'asignacion_id' => $idAsignacion])
            ->delete($this->rbd.'.hojarespuesta');
    }
}