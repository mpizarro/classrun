<?php
use Jasny\SSO\NotAttachedException;
use Jasny\SSO\Exception as SsoException;

require_once __DIR__ . '/../../vendor/autoload.php';

class Usuario_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

    }

    public function login($username, $password)
    {
        $this->db->from('usuario');
        $this->db->where('username', $username);
        $this->db->where('password', $password);

        $query = $this->db->get();
            
        return ($query->num_rows() > 0) ? $query->row() : false;            
        
    }

    public function getRbd($user)
    {
        $this->db->from('usuario');
        $this->db->join('colegio', 'colegio.id = usuario.colegio_id');
        $this->db->where('usuario.id', $user);
        
        $query = $this->db->get();

        return ($query->num_rows() > 0) ? $query->row() : false;

    }

    public function is_logged_in()
    {
        $temp = $this->session->userdata('perfil_id');
        if($temp)
            return true;
        else
            return false;

    }

    public function getPassword($idUser)
    {
        $this->db->select('contrasena');
        $this->db->from('usuario');
        $this->db->where('id', $idUser);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result() : false;
    }
    public function ChangePassword($passnew, $idUser)
    {
        $this->db->set('contrasena', $passnew);
        $this->db->where('id', $idUser);
        return $this->db->update('usuario');
    }
    public function getColegioUsuario($idUsuario)
    {
        $return = false;

        $this->db->from('usuario');
        $this->db->where('id', $idUsuario);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $return = $query->row()->colegio_id;
        }

        return $return;
    }

    public function getCredencialesAlumno($username)
    {
        // verifica si el usuario tiene una asignación en curso
        $row = $this->db->select('c.rbd, u.*')
            ->join('colegio c', 'c.id = u.colegio_id')
            ->where(['username' => $username])
            ->get('usuario u')->row();

        if (isset($row)) {

            $result = $this->db->get_where($row->rbd.'.usuario_has_asignacion', ['estado' => 1, 'usuario_id' => $row->id]);

            if ($result->num_rows() > 0) {
                // si lo tiene, retorna valores del usuario
                return $this->db->query("SELECT id, username, nombre, apellido, perfil_id, colegio_id, curso_id, $row->rbd as rbd
                    FROM usuario WHERE username = ?", $username)->row_array();
            }
        }
        return false;
    }

    public function getProfesoresByColegio($idColegio) {
        $result = $this->db->select('id, nombre, apellido')
            ->get_where('usuario', ['colegio_id' => $idColegio, 'perfil_id' => 2]);
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function insertMasivo($data)
    {
        $this->db->insert_batch('usuario', $data); 

        if ($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        return FALSE;
    }


}
