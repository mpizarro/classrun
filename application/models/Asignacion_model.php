<?php
class Asignacion_model extends CI_Model
{
    private $rbd;
    private $id;

    public function __construct()
    {
        parent::__construct();
        $this->rbd = $this->session->rbd;
    }

    public function setRbd($rbd)
    {
        $this->rbd = $rbd;
    }

    public function get($asignacion)
    {
        $this->db->where('id', $asignacion);

        $query = $this->db->get($this->rbd.'.asignacion');
        return $query->num_rows() > 0 ? $query->row() : false;
    }

    public function getActivaUsuario($idUsuario)
    {
        $this->db->select('p.id as idPrueba, c.id as idCurso, p.codigo, a.id, t.tipo, c.curso, c.letra, n.nivel,
        p.num, asign.asignatura, asign.alias, a.usuario_id, p.nivel_id');

        $this->db->join('prueba p', 'p.id = a.prueba_id');
        $this->db->join('tipo t', 't.id = p.tipo_id');
        $this->db->join('nivel n', 'n.id = p.nivel_id');
        $this->db->join($this->rbd . '.curso c', 'c.id = a.curso_id');
        $this->db->join('asignatura asign', 'asign.id = p.asignatura_id');

        $this->db->where_in('p.tipo_id', [1, 2, 3, 4]);
        if ($idUsuario)
            $this->db->where('a.usuario_id', $idUsuario);
        $this->db->where('a.estado', 1);
        $this->db->where('origen', 'AEDUC');

        $query = $this->db->get($this->rbd . '.asignacion a');

        return $query->num_rows() > 0 ? $query->result() : false;
    }
    public function getFinalizadasUsuario($idUsuario)
    {
        $this->db->select('p.id as idPrueba, c.id as idCurso, p.codigo, a.id, t.tipo, c.curso, c.letra, n.nivel,
        p.num, asign.asignatura, asign.alias, nivelCurso.alias as nivelAlias, a.usuario_id, p.nivel_id');

        $this->db->join('prueba p', 'p.id = a.prueba_id');
        $this->db->join('tipo t', 't.id = p.tipo_id');
        $this->db->join('nivel n', 'n.id = p.nivel_id');
        $this->db->join($this->rbd.'.curso c', 'c.id = a.curso_id');
        $this->db->join('nivel nivelCurso', 'nivelCurso.id = c.nivel_id');
        $this->db->join('asignatura asign', 'asign.id = p.asignatura_id');

        $this->db->where_in('p.tipo_id', [1, 2, 3, 4]);
        if ($idUsuario)
            $this->db->where('a.usuario_id', $idUsuario);
        $this->db->where('a.estado !=', 1);
        $this->db->where('origen', 'AEDUC');

        $this->db->order_by('a.id', 'desc');

        $query = $this->db->get($this->rbd.'.asignacion a');
        return $query->num_rows() > 0 ? $query->result() : false;
    }
    public function finalizarAsignacion($idAsignacion)
    {
        $this->db->where('id', $idAsignacion);
        return $this->db->update($this->rbd.'.asignacion', array('estado' => 3));
    }
    public function ejecutarAsignacion($idAsignacion)
    {
        $this->db->where('id', $this->id);
        return $this->db->update($this->rbd.'.asignacion', array('estado' => 1));
    }
    public function setId($id)
    {
        $this->id = $id;
    }
    public function getAlumnosAsignacion($idAsignacion)
    {
        $this->db->select('u.*');
        $this->db->from($this->rbd.'.asignacion a');
        $this->db->join('usuario u', 'a.curso_id = u.curso_id');
        $this->db->where('u.colegio_id', $this->session->colegio_id);
        $this->db->where('a.id', $idAsignacion);

        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->result() : false;
    }
    public function renovarAsignacionesUsuario($idAlumno, $idAsignacion)
    {
        $this->db->flush_cache();
        $this->db->where('uha.usuario_id', $idAlumno);
        $this->db->where('uha.asignacion_id', $idAsignacion);

        $query = $this->db->get($this->rbd.'".usuario_has_asignacion uha');
        if ($query->num_rows() > 0) {
            $idUsuarioAsignacion = $query->row()->id;

            $this->db->flush_cache();
            $this->db->where('id', $idUsuarioAsignacion);
            $this->db->update($this->rbd.'".usuario_has_asignacion', array('estado' => 1));
        } else {
            $data = array(
                'estado' => 1,
                'asignacion_id' => $idAsignacion,
                'usuario_id' => $idAlumno
            );
            $this->db->insert($this->rbd.'.usuario_has_asignacion', $data);
        }
        return $this->db->affected_rows() > 0 ? true : false;
    }
    public function finalizarAsignacionesUsuario($idAsignacion)
    {
        return $this->db->where(['asignacion_id' => $idAsignacion])
        ->update($this->rbd.'".usuario_has_asignacion', ['estado' => 3]);
    }
    public function deshabilitarAsignacionesUsuario($idUsuario, $idAsignacion)
    {
        $sql = 'UPDATE "'.$this->rbd.'".usuario_has_asignacion
            SET estado = 3
            WHERE id IN (SELECT uha.id FROM "'.$this->rbd.'".asignacion a
                JOIN usuario u ON u.curso_id = a.curso_id
                JOIN "'.$this->rbd.'".usuario_has_asignacion uha ON uha.asignacion_id = a.id AND uha.usuario_id = u.id
                WHERE a.id = '.$idAsignacion.' AND u.id = '.$idUsuario.'
            )';

        return $this->db->query($sql);
    }
    public function getRespuestasAlumnoAsignacion($idAsignacion, $idAlumno, $origen="CLASSRUN")
    {
        $this->db->select('hr.pregunta_id, hr.respuesta_id, r.correcta, hr.usuario_id, r.letra');
        if ($origen == "CLASSRUN")
            $this->db->join('respuesta r', 'r.id = hr.respuesta_id');
        else
            $this->db->join($this->rbd.'.respuesta r', 'r.id = hr.respuesta_id');
        $this->db->where(['hr.asignacion_id' => $idAsignacion, 'hr.usuario_id' => $idAlumno]);
        $query = $this->db->get($this->rbd.'.hojarespuesta hr');

        return $query->num_rows() > 0 ? $query->result() : false;
    }
    public function getUsuarioHasAsignacionByEvaluacionByUsuario($idEvaluacion, $idUsuario, $origen = 'AEDUC', $asignacion_id)
    {
        $result = $this->db->select('ua.*')
            ->join($this->rbd.'.asignacion a', 'a.id = ua.asignacion_id')
            ->where(['a.prueba_id' => $idEvaluacion, 'ua.usuario_id' => $idUsuario, 'a.origen' => $origen])
            ->where('a.estado != ', 3)
            ->where('a.id', $asignacion_id)
            ->get($this->rbd.'.usuario_has_asignacion ua');
        return $result->num_rows() == 1 ? $result->row() : false;
    }
    public function getUsuarioHasAsignacionAllByEvaluacionByUsuario($idEvaluacion, $idUsuario, $origen = 'AEDUC', $asignacion_id)
    {
        $result = $this->db->select('ua.*')
            ->join($this->rbd.'.asignacion a', 'a.id = ua.asignacion_id')
            ->where(['a.prueba_id' => $idEvaluacion, 'ua.usuario_id' => $idUsuario, 'a.origen' => $origen])
            ->where('a.id', $asignacion_id)
            ->get($this->rbd.'.usuario_has_asignacion ua');
        return $result->num_rows() == 1 ? $result->row() : false;
    }
    public function getUsuarioHasAsignacionByEvaluacionByUsuarioWithoutAsignacion($idEvaluacion, $idUsuario, $origen = 'AEDUC')
    {
        $result = $this->db->select('ua.*')
            ->join($this->rbd.'.asignacion a', 'a.id = ua.asignacion_id')
            ->where(['a.prueba_id' => $idEvaluacion, 'ua.usuario_id' => $idUsuario, 'a.origen' => $origen])
            ->where('a.estado != ', 3)
            ->get($this->rbd.'.usuario_has_asignacion ua');
        return $result->num_rows() == 1 ? $result->row() : false;
    }
    public function setTime($usuario_has_asignacion_id, $date)
    {
        $this->db->where(['id' => $usuario_has_asignacion_id])
            ->update($this->rbd.'.usuario_has_asignacion', $date);

        return $this->db->affected_rows() == 1 ? true : false;
    }
    public function getUsuarioAsignacionByUsuarioByAsignacion($idAsignacion, $idUsuario)
    {
        $result = $this->db->select('ua.*')
            ->where(['ua.asignacion_id' => $idAsignacion, 'ua.usuario_id' => $idUsuario])
            ->get($this->rbd.'.usuario_has_asignacion ua');
        return $result->num_rows() == 1 ? $result->row() : false;
    }
    public function getUsuariosAsignacionesByAsignacion($idAsignacion)
    {
        $result = $this->db->get_where($this->rbd.'.usuario_has_asignacion', ['asignacion_id' => $idAsignacion]);
        return $result->num_rows() > 0 ? $result->result() : false;
    }
    public function eliminarHojaRespuesta($idAsignacion)
    {
        $sql = 'DELETE FROM "'.$this->rbd.'".hojarespuesta WHERE asignacion_id = '.$idAsignacion;
        return $this->db->query($sql);
    }

    public function getCorrectaIncorrectaByPreguntaByAsignacion($idAsignacion, $idPregunta, $origen) {
        $this->db->select('r.correcta, pp.pregunta_id, a.id AS asignacion_id, r.id AS respuesta_id');
        if ($origen == 'CLASSRUN') {
            $this->db->join('prueba p', 'p.id = a.prueba_id');
            $this->db->join('prueba_has_pregunta pp', 'pp.prueba_id = p.id');
            $this->db->join('pregunta pg', 'pg.id = pp.pregunta_id');
            $this->db->join('respuesta r', 'r.pregunta_id = pg.id');
        } else {
            $this->db->join($this->rbd.'.prueba p', 'p.id = a.prueba_id');
            $this->db->join($this->rbd.'.prueba_has_pregunta pp', 'pp.prueba_id = p.id');
            $this->db->join($this->rbd.'.pregunta pg', 'pg.id = pp.pregunta_id');
            $this->db->join($this->rbd.'.respuesta r', 'r.pregunta_id = pg.id');
        }
        $this->db->where_in('p.tipo_id', [1, 2, 3, 4]);
        $this->db->where(['a.id' => $idAsignacion, 'pg.id' => $idPregunta]);
        $this->db->order_by('r.id');
        $result = $this->db->get($this->rbd.'.asignacion a');

        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function setAsignacionProfesor($idProfesor, $idAsignacion) {
        $this->db->where('id', $idAsignacion)
            ->update($this->rbd . '.asignacion', ['usuario_id' => $idProfesor]);
        return $this->db->affected_rows() > 0 ? TRUE : FALSE;
    }

    public function cursoHasAsignacion($idCurso) {
        $result = $this->db->join('prueba p', 'p.id = a.prueba_id')
            ->where(['a.curso_id' => $idCurso, 'a.estado' => 1])
            ->where_in('p.tipo_id', [1, 2, 3, 4])
            ->get($this->rbd.'.asignacion a');
        return $result->num_rows() > 0 ? TRUE : FALSE;
    }

    public function verificaPruebaAsignadaAnterior($idEvaluacion, $idCurso) {

        $result = $this->db->join('prueba p', 'p.id = a.prueba_id')
            ->where(['a.curso_id' => $idCurso, 'a.prueba_id' => $idEvaluacion])
            ->where('a.usuario_id != ' . $this->session->idUser)
            ->where_in('p.tipo_id', [1, 2, 3, 4])
            ->get($this->rbd.'.asignacion a');

        return $result->num_rows() > 0 ? TRUE : FALSE;
    }

    public function getAsignacionesPersonal($estado, $idUsuario, $idAsignatura, $idNivel, $idTipo)
    {
        $this->db->select('a.id, p.codigo, p.asignatura_id, ag.asignatura, p.nivel_id, n.nivel, p.tipo_id, t.tipo, a.prueba_id,
        p.nombre, p.usuario_id, u.nombre AS nombre_creador, u.apellido AS apellido_creador, a.curso_id, c.curso, c.letra');
        $this->db->join($this->rbd.'.prueba p', 'p.id = a.prueba_id');
        $this->db->join('asignatura ag', 'ag.id = p.asignatura_id');
        $this->db->join('nivel n', 'n.id = p.nivel_id');
        $this->db->join('tipo t', 't.id = p.tipo_id');
        $this->db->join($this->rbd.'.curso c', 'c.id = a.curso_id');
        $this->db->join('usuario u', 'u.id = p.usuario_id');
        $this->db->where(['a.estado' => $estado, 'origen' => 'PERSONAL']);

        if ($idUsuario)
            $this->db->where('a.usuario_id', $idUsuario);
        if ($idAsignatura)
            $this->db->where('ag.id', $idAsignatura);
        if ($idNivel)
            $this->db->where('n.id', $idNivel);
        if ($idTipo)
            $this->db->where('t.id', $idTipo);

        $result = $this->db->get($this->rbd.'.asignacion a');

        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    public function getCountRespondidaByAlumnoByAsignacion($idAsignacion, $idAlumno)
    {
        $result = $this->db->query("SELECT COUNT(hr.id) AS avance, ua.inicio, ua.fin, ua.estado, ua.usuario_id
            FROM \"$this->rbd\".usuario_has_asignacion ua
            LEFT JOIN \"$this->rbd\".hojarespuesta hr ON hr.asignacion_id = ua.asignacion_id AND hr.usuario_id = ua.usuario_id
            WHERE ua.usuario_id = '" . $idAlumno . "'
            AND ua.asignacion_id = '" . $idAsignacion . "'
            GROUP BY ua.inicio, ua.fin, ua.estado, ua.usuario_id");

        return $result->num_rows() > 0 ? $result->row() : FALSE;
    }

    public function setTiempoFinAll($idAsignacion, $date)
    {
        return $this->db->where(['asignacion_id' => $idAsignacion, 'inicio >' => 0])
        ->where("fin is NULL")
        ->update($this->rbd.'.usuario_has_asignacion', ['fin' => $date]);
    }

    public function getAllByColrgio($rbd, $filter)
    {
        $this->db->select('p.id as idPrueba, c.id as idCurso, p.codigo, a.id, t.tipo, c.curso, c.letra, n.nivel,
        p.num, asign.asignatura, asign.alias, a.usuario_id, p.nivel_id, a.origen, asign.color, a.estado');
        $this->db->join('prueba p', 'p.id = a.prueba_id');
        $this->db->join('tipo t', 't.id = p.tipo_id');
        $this->db->join('nivel n', 'n.id = p.nivel_id');
        $this->db->join($rbd . '.curso c', 'c.id = a.curso_id');
        $this->db->join('asignatura asign', 'asign.id = p.asignatura_id');

        if ($filter->asignatura_id)
            $this->db->where('p.asignatura_id', $filter->asignatura_id);
        if ($filter->tipo_id)
            $this->db->where('p.tipo_id', $filter->tipo_id);
        if ($filter->estado_id)
            $this->db->where('a.estado', $filter->estado_id);

        $this->db->where_in('p.tipo_id', [1, 2, 3, 4]);
        $response = $this->db->get($rbd . '.asignacion a');
        return $response->result();
    }

    public function countAsignacioneByColegio($rbd, $estado=false)
    {
        if ($estado)
            $this->db->where('estado', $estado);
        return $this->db->count_all_results($rbd.'.asignacion');
    }

    public function delete($asignacion)
    {
        $return = false;
        
        $this->db->trans_begin();
        
        $this->db->query('DELETE FROM "'.$this->rbd.'".hojarespuesta WHERE asignacion_id ='.$asignacion);
        $this->db->query('DELETE FROM "'.$this->rbd.'".usuario_has_asignacion WHERE asignacion_id ='.$asignacion);
        $this->db->query('DELETE FROM "'.$this->rbd.'".asignacion WHERE id ='.$asignacion);
        

        if ($this->db->trans_status() === FALSE)
        {
                $this->db->trans_rollback();
        }
        else
        {
                $this->db->trans_commit();
                $return = true;
        }

        return $return;
    }
}
