<?php
class Asignatura_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    public function getAsignaturas() {
        $result = $this->db->get('asignatura');
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }
}