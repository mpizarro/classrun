<?php
class Secure_area extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('usuario_model');

       
        if (!$this->usuario_model->is_logged_in()) {
            redirect(base_url());
        }

        date_default_timezone_set('America/Santiago');
    }
}
