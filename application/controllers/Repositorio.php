<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once ("secure_area.php");

class Repositorio extends Secure_area
{
    public $files = array();
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Analisis_model', 'analisis');
    }
    public function index() {
        $this->load->view('modulos/repositorio/not_found');
    }
    public function remediales()
    {
        $this->analisis->set();

    	$css = '<link href="' . base_url() . 'assets/modulos/repositorio/remediales/css/remediales.css" rel="stylesheet" type="text/css" />';

    	$this->load->view('layout/header', array('css' => $css));
        $this->load->view('layout/menu');

    	$this->load->view('modulos/repositorio/remediales/remediales_vista');
    	$this->load->view('modulos/repositorio/remediales/remediales_footer', array('data' => $this->getRemediales(), 'files' => $this->files));
    }
    public function materialDocente()
    {
        $this->analisis->set();
        $css = '<link href="' . base_url() . 'assets/modulos/repositorio/materialDocente/css/materialDocente.css" rel="stylesheet" type="text/css" />';

        $this->load->view('layout/header', array('css' => $css));
        $this->load->view('layout/menu');

        $this->load->view('modulos/repositorio/materialDocente/materialDocente_vista');
        $this->load->view('modulos/repositorio/materialDocente/materialDocente_footer', array('data' => $this->getMaterialDocente(), 'files' => $this->files));
        $this->getMaterialDocente();
    }
    public function getMaterialDocente()
    {
        $asignaturas = $this->getAsignaturas("MATERIAL DOCENTE");
        for ($i=0; $i < count($asignaturas); $i++)
        {
            $asignaturas[$i]['tipoDocumentos'] = $this->getTipoDocumentos($asignaturas[$i]);
            for ($j = 0; $j < count($asignaturas[$i]['tipoDocumentos']); $j++)
            {
                $asignaturas[$i]['tipoDocumentos'][$j]['niveles'] = $this->getNiveles($asignaturas[$i]['tipoDocumentos'][$j],$asignaturas[$i]);
                for ($k=0; $k < count($asignaturas[$i]['tipoDocumentos'][$j]['niveles']); $k++)
                {
                    if($asignaturas[$i]['tipoDocumentos'][$j]['nombre'] == "CUADERNILLOS")
                    {
                        $this->getFilesMaterialDocente($asignaturas[$i], $asignaturas[$i]['tipoDocumentos'][$j], $asignaturas[$i]['tipoDocumentos'][$j]['niveles'][$k], TRUE);
                    }
                    else
                    {
                        $this->getFilesMaterialDocente($asignaturas[$i], $asignaturas[$i]['tipoDocumentos'][$j], $asignaturas[$i]['tipoDocumentos'][$j]['niveles'][$k]);
                    }
                }
            }
        }
        return $asignaturas;
    }
    public function getFilesMaterialDocente($asignatura, $tipoDocumento, $nivel, $cuadernillos = FALSE)
    {
        if($cuadernillos)
        {
            $basePath = FCPATH . 'assets/material/MATERIAL DOCENTE/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $tipoDocumento['nombre'] . "/" . $nivel['nombre'] . "-" . $nivel['orden'];
            $files = array_diff(scandir($basePath), array('.', '..', '.DS_Store'));
            foreach ($files as $file)
            {
                $this->files[] = array
                (
                    'ruta' => 'assets/material/MATERIAL DOCENTE/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $tipoDocumento['nombre'] . "/" . $nivel['nombre'] . "-" . $nivel['orden'],
                    'url' => base_url() . 'assets/material/MATERIAL DOCENTE/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $tipoDocumento['nombre'] . "/" . $nivel['nombre'] . "-" . $nivel['orden'] . "/" . $file,
                    'nivel' => $nivel['nombre'] . "-" . $nivel['orden'],
                    'size' => filesize(FCPATH . 'assets/material/MATERIAL DOCENTE/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $tipoDocumento['nombre'] . "/" . $nivel['nombre'] . "-" . $nivel['orden'] . "/" . $file),
                    'nombre' => $file,
                    'asignatura' => $asignatura['nombre'] . "-" . $asignatura['orden']
                );
            }
        }
        else
        {
            $basePath = FCPATH . 'assets/material/MATERIAL DOCENTE/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $tipoDocumento['nombre'] . "/" . $nivel['nombre'] . ($nivel['nombre'] == 'NT' ? '--' : '-') . $nivel['orden'];
            $folderGuia = array_diff(scandir($basePath), array('.', '..', '.DS_Store'));
            foreach($folderGuia as $folder)
            {
                if($this->getTotalSizeFolder($basePath . "/" . $folder) > 20000)
                {
                    $files = array_diff(scandir($basePath . "/" . $folder), array('.', '..', '.DS_Store'));
                    foreach($files as $file)
                    {
                        $this->files[] = array
                        (
                            'ruta' => 'assets/material/MATERIAL DOCENTE/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $tipoDocumento['nombre'] . "/" . $nivel['nombre'] . ($nivel['nombre'] == 'NT' ? '--' : '-') . $nivel['orden'] . "/" . $folder,
                            'url' => base_url() . 'assets/material/MATERIAL DOCENTE/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $tipoDocumento['nombre'] . "/" . $nivel['nombre'] . ($nivel['nombre'] == 'NT' ? '--' : '-') . $nivel['orden'] . "/" . $folder . "/" . $file,
                            'nivel' => $nivel['nombre'] . "-" . $nivel['orden'],
                            'size' => filesize(FCPATH . 'assets/material/MATERIAL DOCENTE/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $tipoDocumento['nombre'] . "/" . $nivel['nombre'] . ($nivel['nombre'] == 'NT' ? '--' : '-') . $nivel['orden'] . "/" . $folder . "/" . $file),
                            'nombre' => $file,
                            'asignatura' => $asignatura['nombre'] . "-" . $asignatura['orden']
                        );
                    }
                }
            }
        }
    }
    public function getNiveles($tipoDocumento, $asignatura)
    {
        $niveles = array();
        $basePath = FCPATH . 'assets/material/MATERIAL DOCENTE/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $tipoDocumento['nombre'] . "/";
        $files = array_diff(scandir($basePath), array('.', '..', '.DS_Store'));
        foreach ($files as $file)
        {
            if($this->getTotalSizeFolder($basePath . $file) > 20000)
            {
                $nivelArray = explode('-', $file);
                if($nivelArray[0] == "NT")
                {
                    $nivelArray[1] = $nivelArray[2];
                }
                $niveles[] = array('orden' => $nivelArray[1], 'nombre' => $nivelArray[0]);
            }
        }
        return $niveles;
    }
    public function getTipoDocumentos($asignatura)
    {
        $tipoDocumentos = array();
        $basePath = FCPATH . 'assets/material/MATERIAL DOCENTE/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/";
        $files = array_diff(scandir($basePath), array('.', '..', '.DS_Store'));
        foreach ($files as $file)
        {
            if($this->getTotalSizeFolder($basePath . $file) > 20000)
            {
                $tipoDocumentos[] = array('nombre' => $file);
            }
        }
        return $tipoDocumentos;
    }
    public function getRemediales()
    {
        $asignaturas = $this->getAsignaturas();
        for ($i=0; $i < count($asignaturas); $i++)
        {
            $asignaturas[$i]['niveles'] = $this->getNivelesAsignatura($asignaturas[$i]['nombre'], $asignaturas[$i]['orden']);
            for ($j=0; $j < count($asignaturas[$i]['niveles']); $j++)
            {
                $asignaturas[$i]['niveles'][$j]['numerosEvaluacion'] = $this->getNumerosEvaluacion($asignaturas[$i]['niveles'][$j]['nombre'], $asignaturas[$i]['niveles'][$j]['orden'], $asignaturas[$i]['nombre'], $asignaturas[$i]['orden']);
                foreach($asignaturas[$i]['niveles'][$j]['numerosEvaluacion'] as $numeroEvaluacion)
                {
                    $this->getFilesRemediales($asignaturas[$i]['niveles'][$j], $asignaturas[$i], $numeroEvaluacion);
                    if($asignaturas[$i]['nombre'] == "LENGUAJE")
                    {
                        $this->getFilesRemediales($asignaturas[$i]['niveles'][$j], $asignaturas[$i], $numeroEvaluacion, TRUE);
                    }
                }
            }
        }
        return $asignaturas;
    }
    public function getFilesRemediales($nivel, $asignatura, $numeroEvaluacion, $searchLenguaje = FALSE)
    {
        $basePath = FCPATH . 'assets/material/REMEDIALES/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $nivel['nombre'] . "-" . $nivel['orden'] . "/" . $numeroEvaluacion['nombreCarpeta'];
        $files = array_diff(scandir($basePath), array('.', '..', '.DS_Store'));
        if($searchLenguaje)
        {
            foreach($files as $folder)
            {
                $basePath = FCPATH . 'assets/material/REMEDIALES/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $nivel['nombre'] . "-" . $nivel['orden'] . "/" . $numeroEvaluacion['nombreCarpeta'] . "/" . $folder;
                $filesChildren = array_diff(scandir($basePath), array('.', '..', '.DS_Store'));
                foreach($filesChildren as $file)
                {
                    if(strpos($file, 'pdf') !== FALSE)
                    {
                        $this->files[] = array
                        (
                            'ruta' => '/assets/material/REMEDIALES/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $nivel['nombre'] . "-" . $nivel['orden'] . "/" . $numeroEvaluacion['nombreCarpeta'],
                            'url' => base_url() . 'assets/material/REMEDIALES/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $nivel['nombre'] . "-" . $nivel['orden'] . "/" . $numeroEvaluacion['nombreCarpeta'] . "/" . $folder . "/" . $file,
                            'nivel' => $nivel['nombre'] . "-" . $nivel['orden'],
                            'size' => filesize('assets/material/REMEDIALES/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $nivel['nombre'] . "-" . $nivel['orden'] . "/" . $numeroEvaluacion['nombreCarpeta'] . "/" . $folder . "/" . $file),
                            'nombre' => $file,
                            'asignatura' => $asignatura['nombre'] . "-" . $asignatura['orden'],
                            'numeroEvaluacion' => $numeroEvaluacion['nombreCarpeta']
                        );
                    }
                }
            }
        }
        else
        {
            foreach($files as $file)
            {
                if(strpos($file, 'pdf') !== FALSE)
                {
                    $this->files[] = array
                    (
                        'ruta' => '/assets/material/REMEDIALES/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $nivel['nombre'] . "-" . $nivel['orden'] . "/" . $numeroEvaluacion['nombreCarpeta'],
                        'url' => base_url() . 'assets/material/REMEDIALES/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $nivel['nombre'] . "-" . $nivel['orden'] . "/" . $numeroEvaluacion['nombreCarpeta'] . "/" . $file,
                        'nivel' => $nivel['nombre'] . "-" . $nivel['orden'],
                        'size' => filesize('assets/material/REMEDIALES/' . $asignatura['nombre'] . "-" . $asignatura['orden'] . "/" . $nivel['nombre'] . "-" . $nivel['orden'] . "/" . $numeroEvaluacion['nombreCarpeta'] . "/" . $file),
                        'nombre' => $file,
                        'asignatura' => $asignatura['nombre'] . "-" . $asignatura['orden'],
                        'numeroEvaluacion' => $numeroEvaluacion['nombreCarpeta']
                    );
                }
            }
        }
    }
    public function getNumerosEvaluacion($nivel, $keyNiveles, $asignatura, $keyAsignatura)
    {
        $numerosEvaluacion = array();
        $basePath = FCPATH . 'assets/material/REMEDIALES/' . $asignatura . "-" . $keyAsignatura . "/" . $nivel . "-" . $keyNiveles;

        $files = array_diff(scandir($basePath), array('.', '..', '.DS_Store'));

        foreach ($files as $file)
        {
            $numeroEvaluacionArray = explode(' N', $file);
            if(count($numeroEvaluacionArray) > 1)
            {
                $numerosEvaluacion[] = array('orden' => $numeroEvaluacionArray[1], 'nombreCarpeta' => $file , 'nombre' => $numeroEvaluacionArray[0]);
            }
            else
            {
                if(strpos($file, 'pdf') !== FALSE)
                {
                    $this->files[] = array
                    (
                        'ruta' => '/assets/material/REMEDIALES/' . $asignatura . "-" . $keyAsignatura . "/" . $nivel . "-" . $keyNiveles,
                        'nombre' => $file,
                        'url' => base_url() . '/assets/material/REMEDIALES/' . $asignatura . "-" . $keyAsignatura . "/" . $nivel . "-" . $keyNiveles . "/" .$file,
                        'size' => filesize(FCPATH . '/assets/material/REMEDIALES/' . $asignatura . "-" . $keyAsignatura . "/" . $nivel . "-" . $keyNiveles . "/" .$file),
                        'nivel' => $nivel . "-" . $keyNiveles,
                        'asignatura' => $asignatura . "-" . $keyAsignatura
                    );
                }
            }
        }
        return $numerosEvaluacion;
    }
    public function getNivelesAsignatura($asignatura, $keyAsignatura)
    {
        $niveles = array();
        $basePath = FCPATH . 'assets/material/REMEDIALES/' . $asignatura . "-" . $keyAsignatura;
        $files = array_diff(scandir($basePath), array('.', '..', '.DS_Store'));

        foreach ($files as $file)
        {
            $pathNivelFolder = $basePath . "/" . $file;
            if($this->getTotalSizeFolder($pathNivelFolder) > 20000)
            {
                $nivelArray = explode('-', $file);
                $niveles[] = array('orden' => $nivelArray[1], 'nombre' => $nivelArray[0]);
            }
        }
        return $niveles;
    }
    public function getAsignaturas($tipoDocumento = "REMEDIALES")
    {
        $basePath = FCPATH . 'assets/material/' . $tipoDocumento . '/';
        $asignaturas = array();
        $return = array();
        $files = array_diff(scandir($basePath), array('.', '..', '.DS_Store'));

        foreach ($files as $file)
        {
            if($this->getTotalSizeFolder($basePath . $file) > 20000)
            {
                $asignaturaArray = explode('-', $file);
                $asignaturas[] = array('orden' => $asignaturaArray[1], 'nombre' => $asignaturaArray[0]);
            }
        }
        return $asignaturas;
    }
    private function getTotalSizeFolder($path)
    {
        $bytestotal = 0;
        $path = realpath($path);
        if($path!==false && $path!='' && file_exists($path))
        {
            foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS)) as $object)
            {
                $bytestotal += $object->getSize();
            }
        }
        return $bytestotal;
    }
}
