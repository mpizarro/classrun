<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once ("secure_area.php");

class Grafico_sostenedor extends Secure_area
{
	public function __construct()
	{
        parent::__construct();

        $this->load->model([
            'colegio_model',
            'asignacion_model'
        ]);
    }

    public function index()
    {
        $data['js'] = '<script src="'.base_url().'assets/plugins/bower_components/highcharts/highcharts.js"></script>
                    <script src="'.base_url().'assets/plugins/bower_components/highcharts/modules/exporting.js"></script>
                    <script src="'.base_url().'assets/plugins/bower_components/highcharts/modules/export-data.js"></script>
                    <script src="'.base_url().'assets/modulos/asignaciones/js/grafico_asignaciones.js"></script>';

        $data['data']['colegios'] = [];
        $data['data']['ejecucion'] = [];
        $data['data']['finalizadas'] = [];
        if ($colegios = $this->colegio_model->getColegiosBySostenedor($this->session->idUser))
            foreach ($colegios as $col) {
                $data['data']['colegios'][] = $col->rbd . ' - ' . $col->nombre;
                $data['data']['ejecucion'][] = $this->asignacion_model->countAsignacioneByColegio($col->rbd, 1);
                $data['data']['finalizadas'][] = $this->asignacion_model->countAsignacioneByColegio($col->rbd, 3);
            }

        $this->load->view('layout/header', $data);
        $this->load->view('layout/menu');
        $this->load->view('modulos/sostenedor/grafico_asignaciones');
        $this->load->view('layout/footer');
    }
}