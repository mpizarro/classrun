<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once ("secure_area.php");

class Reporte extends Secure_area
{
	public function __construct()
	{
        parent::__construct();

        //$this->load->helper('graph');
        $this->load->helper('utilidades');

        $this->load->model('Asignacion_model');
        $this->load->model('Estadistica_model');
        $this->load->model('Prueba_model');
        $this->load->model('Respuesta_model');
        $this->load->model('Curso_model');

        $this->asignacion   = new Asignacion_model();
        $this->estadistica  = new Estadistica_model();
        $this->prueba       = new Prueba_model();
        $this->respuesta    = new Respuesta_model();
        $this->curso        = new Curso_model();
    }

    public function setRbd($rbd)
    {
        $this->asignacion->setRbd($rbd);
        $this->estadistica->setRbd($rbd);
        $this->prueba->setRbd($rbd);
        $this->respuesta->setRbd($rbd);
        $this->curso->setRbd($rbd);
    }

    public function home($idAsignacion, $origen) 
    {
        $this->load->library('pdfgenerator');

        $simceData          = $this->simceData($idAsignacion, $origen);
        $habilidadesGrafico = '';
        $habilidadesValor   = '';

        foreach ($simceData['habilidades'] as $key => $value) 
        {                
            $habilidadesGrafico .= " ' ".$value['habilidad']."',";
            $habilidadesValor   .= $value['porc'].",";
        }
        
        $partial = null;

        if ($origen == 'CLASSRUN')
            $partial = getPartialsByCodigo($simceData['codigo'], $simceData['tipo_id']);
        else
            $partial = getPartialsByAsignatura($simceData['asignatura_id'], $simceData['tipo_id']);

        $simceData['pdfName']               = 'REPORTE_SIMCE';
        $simceData['habilidadesGrafico']    = $habilidadesGrafico;
        $simceData['habilidadesValor']      = $habilidadesValor;
        $simceData['encabezado']            = $partial['enc'];
        $simceData['portada']               = $partial['port'];
        $simceData['pie']                   = $partial['pie'];

        $html = $this->load->view('modulos/reportes/simce', $simceData, TRUE);
        $graphName = 'report_'.time();

        echo $html;
    }

    public function testSimce($idAsignacion, $origen)
    {
        $estadisticas           = $this->estadistica->getRespuestas($idAsignacion, $origen);
        $asignacion             = $this->asignacion->get($idAsignacion);
        $alumnos                = $this->estadistica->getAlumnosByAsignacion($idAsignacion);
        $prueba                 = $this->prueba->get($asignacion->prueba_id, $origen);
        $habilidades            = $this->estadistica->getHabilidadesByPrueba($asignacion->prueba_id, $origen);
        $logroAlumnos           = $this->logroAlumnos($idAsignacion, $origen);
        
        $simceData['html'] = $this->estandaresSimce($estadisticas, $prueba);
        $simceData['html'].= $this->habilidadesSimce();
        $simceData['js']   = $this->habilidadesSimceJs($habilidades, $alumnos);
        $simceData['html'].= $this->porcentajeLogroSimce($logroAlumnos);
        $simceData['html'].= $this->ordenRepeticion();

        $preguntasRespuesta = $this->estadistica->getPreguntasRespuesta($idAsignacion, $origen);
        $ordenPreguntas     = $this->estadistica->getOrdenPreguntas($prueba->id, $origen);

        /*
        foreach ($preguntasRespuesta as $preguntas) 
        {
                
        }
        */

        print_r($preguntasRespuesta);
        print_r($ordenPreguntas);
        //echo $this->load->view('modulos/reportes/simce', $simceData, TRUE);
        
    }

    public function estandaresSimce($estadisticas, $prueba)
    {
        $data['nivel']['inicial']       = 0;
        $data['nivel']['intermedio']    = 0;
        $data['nivel']['adecuado']      = 0;
        $data['cantAlumnos']            = 0;

        foreach ($estadisticas as $estadistica) 
        {
            $puntajeSimce           = round(((($estadistica->correctas - $prueba->preguntas / 2)) / 3 * (450 / $prueba->preguntas) + 265), 0);
            
            if ($puntajeSimce < 245) 
            {
                $nivel = 'INSUFICIENTE';
                $data['nivel']['inicial']++;
            } 
            elseif ($puntajeSimce >= 245 && $puntajeSimce <= 295) 
            {
                $nivel = 'ELEMENTAL';
                $data['nivel']['intermedio']++;
            } 
            else 
            {
                $nivel = 'ADECUADO ';
                $data['nivel']['adecuado']++;
            }
            
            $data['cantAlumnos']++;
        }
                
        $data['nivel']['inicial'] = round(($data['nivel']['inicial'] * 100) / $data['cantAlumnos'], 1);
        $data['nivel']['intermedio'] = round(($data['nivel']['intermedio'] * 100) / $data['cantAlumnos'], 1);
        $data['nivel']['adecuado'] = round(($data['nivel']['adecuado'] * 100) / $data['cantAlumnos'], 1);

        $html = '
        <div class="wrapper">
            <div style="margin-bottom: 50px;margin-top: 50px;margin-left: 50px;margin-right: 50px;">
            <div class="container-fluid" style="border: 5px solid #61b7c2;padding-left: 0px;padding-right: 0px;padding-bottom: 10px;">
                <div  style="background:#61b7c3;color:#fff;font-size: 32px;text-align: center;">
                    ESTÁNDARES SIMCE
                </div>
                        <br><div id="grafico1"></div>
            
                <div class="row" style="padding-left: 20%;">        
                        
                    <div class="col-lg-3" style="text-align: center; border: 2px solid #61b7c3;border-radius: 6px;font-weight: bold;font-size: 25px;margin-left: 10px;margin-right: 10px;">
                        '.$data['nivel']['inicial'].'%<br>
                        <div>INSUFICIENTE</div>
                    </div>        
                    <div class="col-lg-3" style="text-align: center; border: 2px solid #61b7c3;border-radius: 6px;font-weight: bold;font-size: 25px;margin-left: 10px;margin-right: 10px;">
                        '.$data['nivel']['intermedio'].'%<br>
                        <div>ELEMENTAL</div>
                    </div>
                    <div class="col-lg-3" style="text-align: center; border: 2px solid #61b7c3;border-radius: 6px;font-weight: bold;font-size: 25px;margin-left: 10px;margin-right: 10px;">
                        '.$data['nivel']['adecuado'].'%<br>
                        <div>ADECUADO</div>
                    </div>
                    
                </div>
        </div>';

        return $html;
    }

    public function habilidadesSimce()
    {
        $html = '
            <div style="margin-bottom: 50px;margin-top: 50px;margin-left: 0px;margin-right: 0px;">
                <div class="container-fluid" style="border: 5px solid #61b7c2;padding-left: 0px;padding-right: 0px;padding-bottom: 10px;">
                    <div  style="background:#61b7c3;color:#fff;font-size: 32px;text-align: center;">
                        HABILIDADES SIMCE
                    </div>
                    <br>
                    <div id="graficoHabilidades" style="text-align: -webkit-center;"></div>
                </div>
            </div>';
        
        return $html;
    }

    public function habilidadesSimceJs($habilidades, $alumnos)
    {
        $data['cantAlumnos'] = count($alumnos);
        $habilidadesGrafico = '';
        $habilidadesValor = '';

        if ($habilidades) {
            foreach ($habilidades as $h) {
                $data['habilidades'][] = array(
                    'id'        => $h->id,
                    'habilidad' => $h->habilidadcurricular,
                    'count'     => $h->count,
                    'porc'      => 0
                );
            }
        }

        for ($i = 0; $i < count($data['habilidades']); $i++) {
            $data['habilidades'][$i]['porc'] = ($data['habilidades'][$i]['porc'] > 0 && $data['cantAlumnos'] > 0)?round($data['habilidades'][$i]['porc'] / $data['cantAlumnos'], 1) : 0;
        }

        foreach ($data['habilidades'] as $key => $value) 
        {                
            $habilidadesGrafico .= " ' ".$value['habilidad']."',";
            $habilidadesValor   .= $value['porc'].",";
        }

        
/*
        $js='<script type="text/javascript">

        // Create the chart
    Highcharts.chart(\'grafico1\', {
        chart: {
            type: \'column\'
        },
        title: {
            text: \'\'
        },
        xAxis: {
            type: \'category\'
        },
        yAxis: {
            title: {
                text: \'\'
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: \'{point.y:f}%\'
                }
            }
        },
        tooltip: {
            headerFormat: \'<span style="font-size:11px">{series.name}</span><br>\',
            pointFormat: \'<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}%</b> total<br/>\'
        },
        series: [
            {
                name: "estándares",
                colorByPoint: true,
                data: [
                    {
                        name: "Inicial",
                        y:'.$nivel['inicial'].'                    
                    },
                    {
                        name: "Intermedio",
                        y:'.$nivel['intermedio'].'                    
                    },
                    {
                        name: "Adecuado",
                        y: '.$nivel['adecuado'].'                    
                    }
                ]
            }
        ]
    });
    '
    */

    $js='<script type="text/javascript">

    Highcharts.chart(\'graficoHabilidades\', {
        chart: {
            type: \'bar\'
        },
        title: {
            text: \' \'
        },
        xAxis: {
            categories: ['.$habilidadesGrafico.']
        },
        yAxis: {
            min: 0,
            title: {
                text: \'\'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: \'normal\'
            }
        },
        series: [{
            name: \'habilidad\',
            data: ['.$habilidadesValor.']
        }]
    });
    </script>';

    return $js;
    }

    public function porcentajeLogroSimce($alumnos)
    {
        
        $html = '<div style="margin-bottom: 50px;margin-top: 50px;margin-left: 0px;margin-right: 0px;">
        <div class="container-fluid" style="border: 5px solid #61b7c2;padding-left: 0px;padding-right: 0px;padding-bottom: 10px;">
            <div  style="background:#61b7c3;color:#fff;font-size: 32px;text-align: center;">
                % LOGRO SIMCE

                <button onclick="exportTableToExcel(\'tblLogros\', \'Logros\')" type="button" style="background: #61b7c2;border-color: #61b7c2;left: 32%;" class="btn btn-primary waves-effect waves-light"><i class="mdi mdi-file-excel" style="font-size: 38px;"></i></button>                    
            </div>
            <table id="tblLogros" style="border-collapse: separate; /*border: 1px solid #61b7c3;*/ width: 100%; margin-top: 24px; padding: 5px;">
                <tr>
                    <td style="background: #61b7c3; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: left;">ALUMNO</td>
                    <td style="background: #61b7c3; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">NIVEL</td>
                    <td style="background: #61b7c3; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">LOGRO</td>
                    <td style="background: #61b7c3; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">NOTA</td>            
                </tr>';
        $num = 2;

        
        foreach ($alumnos as $alumno) 
        {

            if(isset($alumno['correctas']))
            {
                $bg = $num % 2 == 0 ? '#F4F6F8' : '#FFF';
                $num++;

                $html.='
                    <tr style="background:'.$bg.';border: 1px solid #fff;">
                        <td style="background: #61b7c3;color:#fff; border-left: 1px solid #61b7c3; padding: 3px; font-size: 12px; text-align: left;">'.ucwords($alumno['nombre']." ".$alumno['apellido']).'</td>
                        <td style="border-left: 1px solid #61b7c3; padding: 3px; font-size: 12px; text-align: center;">'.$alumno['logro'].'</td>
                        <td style="border-left: 1px solid #61b7c3; padding: 3px; font-size: 12px; text-align: center;">'.$alumno['logro'].' % </td>
                        <td style="border-left: 1px solid #61b7c3; padding: 3px; font-size: 12px; text-align: center;">'.$alumno['nota'].'</td>
                    </tr>';
            }
        }

        $html.='</table>
        </div>
    </div>';

    return $html;

    }

    public function logroAlumnos($idAsignacion, $origen)
    {
        $asignacion             = $this->asignacion->get($idAsignacion);
        $alumnos                = $this->estadistica->getAlumnosByAsignacion($idAsignacion);
        $prueba                 = $this->prueba->get($asignacion->prueba_id, $origen);

        $data                   = array();
        $data['cantAlumnos']    = 0;
        $data['preguntas']      = $prueba->preguntas;
        
        foreach ($alumnos as $alumno) 
        {
            
            if(!isset($data[$alumno->id]))
            {
                $data[$alumno->id]['nombre'] = $alumno->nombre;
                $data[$alumno->id]['apellido'] = $alumno->apellido;
            }
        
            $data['cantAlumnos']++;
        }

        $estadisticas           = $this->estadistica->getRespuestas($idAsignacion, $origen);
        
        foreach ($estadisticas as $estadistica) 
        {
            $porcentajeCorrectas = round(($estadistica->correctas*100)/$data['preguntas'],0);
                
            if($porcentajeCorrectas < 60)
                $nota = round(5 * ($porcentajeCorrectas*0.01) + 1, 1);
            else
                $nota = round(7.5 * (($porcentajeCorrectas*0.01) - 0.6) + 4, 1);

            if($nota < 2)
                $nota = 2;

            $data[$estadistica->usuario_id]['correctas']    = $estadistica->correctas;
            $data[$estadistica->usuario_id]['incorrectas']  = $estadistica->incorrectas;
            $data[$estadistica->usuario_id]['omitidas']     = $data['preguntas'] - $estadistica->correctas - $estadistica->incorrectas;
            $data[$estadistica->usuario_id]['logro']        = $porcentajeCorrectas;
            $data[$estadistica->usuario_id]['nota']         = $nota;
        }

        return $data;
    }

    public function ordenRepeticion()
    {
        
        
        $html = '
        <div style="margin-bottom: 50px;margin-top: 50px;margin-left: 50px;margin-right: 50px;">
            <div class="container-fluid" style="border: 5px solid #61b7c2;padding-left: 0px;padding-right: 0px;padding-bottom: 10px;">
                <div style="background-color:#61b7c2;height:54px;width:100%">
                    <div  style="background:#61b7c3;color:#fff;font-size: 32px;text-align: center;float:left;width:80%">
                        DETALLE DE PREGUNTAS POR ORDEN DE REPETICIÓN
                    </div>
                    <button onclick="exportTableToExcel(\'tblHabilidades\', \'Habilidades\')" type="button" 
                        style="left:34px;float:left;background: #61b7c2;border-color: #61b7c2;" class="btn btn-primary waves-effect waves-light">
                    <i class="mdi mdi-file-excel" style="font-size: 38px;"></i></button>    
                </div>
    
                <table style="border-collapse: collapse; border: 1px solid #61b7c3; width: 100%; margin-top: 24px;">
                    
                    <tr>
                        <td colspan="2" style="background: #61b7c3; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center; border-right: 1px solid #fff; border-bottom: 1px solid #fff;">CORRECTA</td>
                        <td colspan="2" style="background: #61b7c3; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center; border-bottom: 1px solid #fff;">INCORRECTA</td>
                        <td colspan="2" style="background: #61b7c3; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center; border-left: 1px solid #fff; border-bottom: 1px solid #fff;">OMITIDAS</td>
                    </tr>
                    <tr>
                        <td style="background: #61b7c3; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">N° PREGUNTA</td>
                        <td style="background: #61b7c3; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">REPETICIONES</td>
                        <td style="background: #61b7c3; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">N° PREGUNTA</td>
                        <td style="background: #61b7c3; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">REPETICIONES</td>
                        <td style="background: #61b7c3; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">N° PREGUNTA</td>
                        <td style="background: #61b7c3; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">REPETICIONES</td>
                    </tr>';
    /*
                    $preguntasAux = [];
                    $pCorr = [];
                    $pInc = [];
                    $pOmit = [];
                    for ($x = 0; $x < count($preguntas); $x++) 
                    {
                        $pCorr[$preguntas[$x]['orden']] = $preguntas[$x]['correctas'];
                        $pInc[$preguntas[$x]['orden']] = $preguntas[$x]['incorrectas'];
                        $pOmit[$preguntas[$x]['orden']] = $preguntas[$x]['omitidas'];
                    }
    
                    arsort($pCorr);
                    arsort($pInc);
                    arsort($pOmit);
    
                    $pCorrAux = [];
                    foreach ($pCorr as $key => $val)
                        $pCorrAux[] = ['orden' => $key, 'ocurrencia' => $val];
                    $pIncAux = [];
                    foreach ($pInc as $key => $val)
                        $pIncAux[] = ['orden' => $key, 'ocurrencia' => $val];
                    $pOmitAux = [];
                    foreach ($pOmit as $key => $val)
                        $pOmitAux[] = ['orden' => $key, 'ocurrencia' => $val];
    
                    $preguntasAux[0] = $pCorrAux;
                    $preguntasAux[1] = $pIncAux;
                    $preguntasAux[2] = $pOmitAux;
    
                    $num = 2;
                    
                    for ($i = 0; $i < count($preguntasAux[0]); $i++)
                    {
                        $bg = $num % 2 == 0 ? '#F4F6F8' : '#FFF';
                        $num++;
    
                        $html.='<tr style="background:'.$bg.';">
                            <td style="border-left: 1px solid #61b7c3; padding: 3px; font-size: 12px; text-align: center;">'.$preguntasAux[0][$i]['orden'].'</td>
                            <td style="border-left: 1px solid #61b7c3; padding: 3px; font-size: 12px; text-align: center;">'.$preguntasAux[0][$i]['ocurrencia'].'</td>
                            <td style="border-left: 1px solid #61b7c3; padding: 3px; font-size: 12px; text-align: center;">'.$preguntasAux[1][$i]['orden'].'</td>
                            <td style="border-left: 1px solid #61b7c3; padding: 3px; font-size: 12px; text-align: center;">'.$preguntasAux[1][$i]['ocurrencia'].'</td>
                            <td style="border-left: 1px solid #61b7c3; padding: 3px; font-size: 12px; text-align: center;">'.$preguntasAux[2][$i]['orden'].'</td>
                            <td style="border-left: 1px solid #61b7c3; padding: 3px; font-size: 12px; text-align: center;">'.$preguntasAux[2][$i]['ocurrencia'].'</td>
                        </tr>';
                    }
*/
                $html.='    
                </table>
            </div>
        </div>';

        return $html;
    }
    




    public function simceData($idAsignacion, $origen) {
        $asignacion         = $this->asignacion->get($idAsignacion);
        $curso              = $this->curso->getCursoByAsignacion($idAsignacion);
        $estadisticas       = $this->estadistica->getRespuestas($idAsignacion, $origen);
        $alumnos            = $this->estadistica->getAlumnosByAsignacion($idAsignacion);
        $habilidadesAlumno  = $this->estadistica->gethabilidadesByAsignacion($idAsignacion, $origen);
        $preguntasRespuesta = $this->estadistica->getPreguntasRespuesta($idAsignacion, $origen);
        $habilidades        = $this->estadistica->getHabilidadesByPrueba($asignacion->prueba_id, $origen);
        $prueba             = $this->prueba->get($asignacion->prueba_id, $origen);
        $ordenPreguntas     = $this->estadistica->getOrdenPreguntas($prueba->id, $origen);
        $oasRespIesEje      = $this->estadistica->getOasRespIeEjeByAsignacion($idAsignacion, $origen); //----ok oasRespIeEje
		$asignatura = $prueba->codigo;
		$asignatura=substr($asignatura, 0,3);
        $color = $color2 = $color3 = $color4= '';


        switch ($prueba->asignatura_id) {
            case 1: $color = '#F47921'; $color2 = '#EBCFBB'; $color3 = '#FAC49D'; $color4 = '#FDF4ED'; break; //LENGUAJE
            case 3: $color = '#ED028A'; $color2 = '#F7CFE7'; $color3 = '#FFD5EC'; $color4 = '#FEF7FB'; break; //MATEMATICAS
            case 4: $color = '#00B4E0'; $color2 = '#6BD1EB'; $color3 = '#A6E5F4'; $color4 =	'#F1FAFD'; break; //Historia, Geografía y Ciencias Sociales
            case 5:
			 	if ($asignatura=='BIO'){
					$color = '#52d942'; $color2 = '#8bff74'; $color3 = '#00a600'; $color4 = '#D9EBD3';
				}//Ciencias Naturales

				elseif ($asignatura=='FIS'){
					$color = '#009d89'; $color2 = '#53cfb9'; $color3 = '#006e5c'; $color4 = '#D3E0DD';
				}

				elseif ($asignatura=='QUI'){
					$color = '#8d171b'; $color2 = '#c34b42'; $color3 = '#5a0000'; $color4 = '#DACBCA';
				}
				 break;
            default: $color = '#56B947'; $color2 = '#9DD597'; $color3 = '#B3DFAC'; $color4 = '#B3DFAC'; break;
        }

        $data = [
            'color'             => $color,
            'color2'            => $color2,
            'color3'            => $color3,
			'color4'			=> $color4,
			'codigo'			=> $prueba->codigo,
            'asignatura_id'     => $prueba->asignatura_id,
            'tipo_id'           => $prueba->tipo_id,
            'num_evaluacion'    => $prueba->num,
            'curso'             => $curso->nivel.' "'.$curso->letra.'"',
            'correctas'         => 0,
            'incorrectas'       => 0,
            'omitidas'          => 0,
            'porc_correctas'    => 0,
            'porc_incorrectas'  => 0,
            'porc_aprobados'    => 0,
            'porc_reprobados'   => 0,
            'porc_omitidas'     => 0,
            'preguntas'         => [],
            'alternativas'      => [],
            'cantAlumnos'       => 0,
            'puntajeSimce'      => 0,
            'nota'              => 0,
            'alumnos'           => [],
            'habilidades'       => [],
            'ejes'              => [],
            'respAltPreg'       => [
                'correcta'      => [],
                'omitidas'      => []
            ],
            'nivel'             => [
                'inicial'       => 0,
                'intermedio'    => 0,
                'adecuado'      => 0
            ]
        ];

        $alumnosOden = [];
        foreach ($alumnos as $alum) {
            $alumnosOden[$alum->id] = [
                'nombre' => $alum->nombre,
                'apellido' => $alum->apellido,
                'inicio' => $alum->inicio,
                'fin' => $alum->fin
            ];
        }

        if ($habilidades) {
            foreach ($habilidades as $h) {
                $data['habilidades'][] = array(
                    'id'        => $h->id,
                    'habilidad' => $h->habilidadcurricular,
                    'count'     => $h->count,
                    'porc'      => 0
                );
            }
        }

        $preguntasAlumno = array();

        if($preguntasRespuesta)
            foreach ($preguntasRespuesta as $val)
                $preguntasAlumno[$val->usuario_id][$val->pregunta_id] = [$val->respuesta_id, $val->correcta];

        if ($estadisticas) {
            
            foreach ($estadisticas as $estadistica) {
                $omitidas = $prueba->preguntas - $estadistica->correctas - $estadistica->incorrectas;

                $data['correctas']      = $data['correctas'] + $estadistica->correctas;
                $data['incorrectas']    = $data['incorrectas'] + $estadistica->incorrectas;
                $data['omitidas']       = $data['omitidas'] + $omitidas;
                $data['cantAlumnos']++;

                if ($prueba->preguntas > 0) {
                    $porcCorrectas          = round(($estadistica->correctas * 100)/$prueba->preguntas, 1);
                    $porcIncorrectas        = round(($estadistica->incorrectas * 100)/$prueba->preguntas, 1);
                    $porcOmitidas           = round(($omitidas * 100)/$prueba->preguntas, 1);

                    $puntajeSimce           = round(((($estadistica->correctas - $prueba->preguntas / 2)) / 3 * (450 / $prueba->preguntas) + 265), 0);
                } else {
                    $porcCorrectas = $porcIncorrectas = $porcOmitidas = $puntajeSimce = 0;
                }

                $data['puntajeSimce']  += $puntajeSimce;

                $data['porc_correctas']     += $porcCorrectas;
                $data['porc_incorrectas']   += $porcIncorrectas;
                $data['porc_omitidas']      += $porcOmitidas;

                $nivel = '';

                if ($puntajeSimce < 245) {
                    $nivel = 'INSUFICIENTE';
                    $data['nivel']['inicial']++;
                } elseif ($puntajeSimce >= 245 && $puntajeSimce <= 295) {
                    $nivel = 'ELEMENTAL';
                    $data['nivel']['intermedio']++;
                } else {
                    $nivel = 'ADECUADO ';
                    $data['nivel']['adecuado']++;
                }

                $nota = 0;

                if($porcCorrectas < 60)
                    $nota = round(5 * ($porcCorrectas*0.01) + 1, 1);
                else
                    $nota = round(7.5 * (($porcCorrectas*0.01) - 0.6) + 4, 1);

                if($nota < 2)
                    $nota = 2;

                $data['nota'] += $nota;
                $fechaInicio = new DateTime(date('Y-m-d h:i:s', ( isset($alumnosOden[$estadistica->usuario_id]['inicio'])?$alumnosOden[$estadistica->usuario_id]['inicio']: "1" ) ) );
                $fechaFin = new DateTime(date('Y-m-d h:i:s', ( isset($alumnosOden[$estadistica->usuario_id]['fin'])?$alumnosOden[$estadistica->usuario_id]['fin']: "1" )));
                $diff = $fechaInicio->diff($fechaFin);
                $tiempo = ($diff->h < 10 ? "0$diff->h" : $diff->h).':'.($diff->i < 10 ? "0$diff->i" : $diff->i);

                $data['alumnos'][] = [
                    'id'                => $estadistica->usuario_id,
                    'nombre'            => (isset($alumnosOden[$estadistica->usuario_id]['nombre'])?$alumnosOden[$estadistica->usuario_id]['nombre']:"Sin Nombre:id=".$estadistica->usuario_id),
                    'apellido'          => (isset($alumnosOden[$estadistica->usuario_id]['apellido'])?$alumnosOden[$estadistica->usuario_id]['apellido']:"Sin Apellido:id=".$estadistica->usuario_id),
                    'tiempo'            => $tiempo,
                    'correctas'         => $estadistica->correctas,
                    'incorrectas'       => $estadistica->incorrectas,
                    'omitidas'          => $omitidas,
                    'porc_correctas'    => $porcCorrectas,
                    'puntajeSimce'      => $puntajeSimce,
                    'nivel'             => $nivel,
                    'nota'              => $nota,
                    'ejes'              => [],
                    'habilidades'       => $data['habilidades'],
                    'agrupacion'        => [
                        'correctas'     => [],
                        'incorrectas'   => [],
                        'omitidas'      => []
                    ]
                ];
            }
        }

        $data['puntajeSimce'] = ($data['puntajeSimce'] > 0 && $data['cantAlumnos'] > 0)?(round($data['puntajeSimce'] / $data['cantAlumnos'], 0)):0;
        $data['nota'] = ($data['nota'] > 0 && $data['cantAlumnos'] > 0)?round($data['nota'] / $data['cantAlumnos'], 1):0;

        if ( $data['cantAlumnos'] > 0) {
            $data['porc_correctas'] = round($data['porc_correctas'] / $data['cantAlumnos'], 1);
            $data['porc_incorrectas'] = round($data['porc_incorrectas'] / $data['cantAlumnos'], 1);
            $data['porc_omitidas'] = round($data['porc_omitidas'] / $data['cantAlumnos'], 1);

            $data['nivel']['inicial'] = round(($data['nivel']['inicial'] * 100) / $data['cantAlumnos'], 1);
            $data['nivel']['intermedio'] = round(($data['nivel']['intermedio'] * 100) / $data['cantAlumnos'], 1);
            $data['nivel']['adecuado'] = round(($data['nivel']['adecuado'] * 100) / $data['cantAlumnos'], 1);
        } else {
            $data['nota'] = $data['porc_correctas'] = $data['porc_incorrectas'] = $data['porc_omitidas'] =
            $data['nivel']['inicial'] = $data['nivel']['intermedio'] = $data['nivel']['adecuado'] = 0;
        }

        if ($habilidadesAlumno) {
            foreach ($habilidadesAlumno as $ha) {
                $porc = 0;
                for ($i = 0; $i < count($data['habilidades']); $i++)
                    if ($data['habilidades'][$i]['id'] == $ha->habilidadcurricular_id) {
                        $porc = $data['habilidades'][$i]['count'] > 0 ? round(($ha->correctas * 100) / $data['habilidades'][$i]['count'], 1) : 0;
                        $data['habilidades'][$i]['porc'] += $porc;
                    }

                for ($x = 0; $x < count($data['alumnos']); $x++) {
                    if ($data['alumnos'][$x]['id'] == $ha->usuario_id) {
                        for ($z = 0; $z < count($data['alumnos'][$x]['habilidades']); $z++) {
                            if ($data['alumnos'][$x]['habilidades'][$z]['id'] == $ha->habilidadcurricular_id) {
                                $data['alumnos'][$x]['habilidades'][$z] = [
                                    'id'        => $ha->habilidadcurricular_id,
                                    'habilidad' => $ha->habilidadcurricular,
                                    'porc'      => $porc
                                ];
                            }
                        }
                    }
                }
            }
        }

        if ($oasRespIesEje) {
            foreach ($oasRespIesEje as $val) {
                $flag = FALSE;
                for ($x = 0; $x < count($data['ejes']); $x++)
                    if ($data['ejes'][$x]['id'] == $val->ejetematico_id)
                        $flag = TRUE;

                $ejesSum[$val->ejetematico_id] = isset($ejesSum[$val->ejetematico_id]) ? $ejesSum[$val->ejetematico_id] + 1: 1;
                if ($flag == FALSE) {
                    $data['ejes'][] = ['id' => $val->ejetematico_id, 'eje' => $val->ejetematico, 'logro' => 0];
                }
            }
        }

        //agregar ejes a alumnos
        for ($i = 0; $i < count($data['ejes']); $i++) {
            for ($x = 0; $x < count($data['alumnos']); $x++) {
                if (!in_array($data['ejes'][$i], $data['alumnos'][$x]['ejes'])) {
                    $data['alumnos'][$x]['ejes'][] = $data['ejes'][$i];
                }
            }
        }

        if($oasRespIesEje)
            foreach ($oasRespIesEje as $or) {
                for ($i = 0; $i < count($data['alumnos']); $i++) {
                    if ($data['alumnos'][$i]['id'] == $or->usuario_id) {
                        for ($x = 0; $x < count($data['alumnos'][$i]['ejes']); $x++) {
                            if ($data['alumnos'][$i]['ejes'][$x]['id'] == $or->ejetematico_id) {
                                if ($or->correcta == 't') {
                                    $data['alumnos'][$i]['ejes'][$x]['logro']++;
                                }
                            }
                        }
                    }
                }
            }

        $numClave = 0;
        $aux = FALSE;
        $preguntas = [];
        $preguntaClave = [];
        for ($i = 0; $i < count($ordenPreguntas); $i++) {
            if ($ordenPreguntas[$i]->pregunta_id == $aux) {
                $numClave++;
            } else {
                $aux = $ordenPreguntas[$i]->pregunta_id;
                $numClave = 1;
            }

            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['pregunta_id'] = $ordenPreguntas[$i]->pregunta_id;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['orden'] = $ordenPreguntas[$i]->orden;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['correctas'] = 0;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['incorrectas'] = 0;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['omitidas'] = 0;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['alternativas'][$ordenPreguntas[$i]->respuesta_id] = [
                'letra' => $this->getLetraByNumber($numClave),
                'ocurrencia' => 0
            ];

            if ($ordenPreguntas[$i]->correcta == 't') {
                $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['clave'] = $this->getLetraByNumber($numClave);

                $preguntas[] = $preguntaClave[$ordenPreguntas[$i]->pregunta_id];
            }

            if (!in_array($this->getLetraByNumber($numClave), $data['alternativas']))
                $data['alternativas'][] = $this->getLetraByNumber($numClave);

            foreach ($preguntasAlumno as $index => $pa) {
                if (array_key_exists($ordenPreguntas[$i]->pregunta_id, $preguntasAlumno[$index])) {
                    if ($pa[$ordenPreguntas[$i]->pregunta_id][1] == 't') {
                        for ($x = 0; $x < count($data['alumnos']); $x++)
                            if ($data['alumnos'][$x]['id'] == $index)
                                if (!in_array($ordenPreguntas[$i]->orden, $data['alumnos'][$x]['agrupacion']['correctas']))
                                    $data['alumnos'][$x]['agrupacion']['correctas'][] = $ordenPreguntas[$i]->orden;
                    } else {
                        for ($x = 0; $x < count($data['alumnos']); $x++)
                            if ($data['alumnos'][$x]['id'] == $index)
                                if (!in_array($ordenPreguntas[$i]->orden, $data['alumnos'][$x]['agrupacion']['incorrectas']))
                                    $data['alumnos'][$x]['agrupacion']['incorrectas'][] = $ordenPreguntas[$i]->orden;
                    }
                } else {
                    for ($x = 0; $x < count($data['alumnos']); $x++)
                        if ($data['alumnos'][$x]['id'] == $index)
                            if (!in_array($ordenPreguntas[$i]->orden, $data['alumnos'][$x]['agrupacion']['omitidas']))
                                $data['alumnos'][$x]['agrupacion']['omitidas'][] = $ordenPreguntas[$i]->orden;
                }
            }
        }

        foreach ($preguntasAlumno as $pa) {
            foreach ($pa as $key => $val) {
                $preguntaClave[$key]['alternativas'][$pa[$key][0]]['ocurrencia']++;
            }
        }

        for ($i = 0; $i < count($preguntas); $i++) {
            $data['preguntas'][] = $preguntaClave[$preguntas[$i]['pregunta_id']];
        }

        for ($i = 0; $i < count($data['alumnos']); $i++) {
            for ($x = 0; $x < count($data['alumnos'][$i]['agrupacion']['correctas']); $x++) {
                for ($z = 0; $z < count($data['preguntas']); $z++) {
                    if ($data['preguntas'][$z]['orden'] == $data['alumnos'][$i]['agrupacion']['correctas'][$x]) {
                        $data['preguntas'][$z]['correctas']++;
                    }
                }
            }
            for ($x = 0; $x < count($data['alumnos'][$i]['agrupacion']['incorrectas']); $x++) {
                for ($z = 0; $z < count($data['preguntas']); $z++) {
                    if ($data['preguntas'][$z]['orden'] == $data['alumnos'][$i]['agrupacion']['incorrectas'][$x]) {
                        $data['preguntas'][$z]['incorrectas']++;
                    }
                }
            }
            for ($x = 0; $x < count($data['alumnos'][$i]['agrupacion']['omitidas']); $x++) {
                for ($z = 0; $z < count($data['preguntas']); $z++) {
                    if ($data['preguntas'][$z]['orden'] == $data['alumnos'][$i]['agrupacion']['omitidas'][$x]) {
                        $data['preguntas'][$z]['omitidas']++;
                    }
                }
            }
            for ($x = 0; $x < count($data['alumnos'][$i]['ejes']); $x++) {
                $data['alumnos'][$i]['ejes'][$x]['logro'] = $ejesSum[$data['alumnos'][$i]['ejes'][$x]['id']] > 0 ? round(($data['alumnos'][$i]['ejes'][$x]['logro'] * 100) / $ejesSum[$data['alumnos'][$i]['ejes'][$x]['id']], 1) : 0;
                for ($z = 0; $z < count($data['ejes']); $z++) {
                    if ($data['ejes'][$z]['id'] == $data['alumnos'][$i]['ejes'][$x]['id'])
                        $data['ejes'][$z]['logro'] += $data['alumnos'][$i]['ejes'][$x]['logro'];
                }
            }
        }

        for ($i = 0; $i < count($data['habilidades']); $i++) {
            $data['habilidades'][$i]['porc'] = ($data['habilidades'][$i]['porc'] > 0 && $data['cantAlumnos'] > 0)?round($data['habilidades'][$i]['porc'] / $data['cantAlumnos'], 1) : 0;
        }
        for ($z = 0; $z < count($data['ejes']); $z++) {
            $data['ejes'][$z]['logro'] =  $data['cantAlumnos'] > 0 ? round($data['ejes'][$z]['logro'] / $data['cantAlumnos'], 1) : 0;
        }

        graphPie([
            'graphName' => 'chart_corr.jpg',
            'color' => ['#3491e0', '#a7a7a7'],
            'data' => [$data['porc_correctas'], (100-$data['porc_correctas'])]
        ]);
        graphPie([
            'graphName' => 'chart_incorr.jpg',
            'color' => ['#fb3131','#a7a7a7'],
            'data' => [$data['porc_incorrectas'], (100-$data['porc_incorrectas'])]
        ]);
        graphPie([
            'graphName' => 'chart_omit.jpg',
            'color' => ['#deb41c','#a7a7a7'],
            'data' => [$data['porc_omitidas'], (100-$data['porc_omitidas'])]
        ]);
        graphPie([
            'graphName' => 'chart_ini.jpg',
            'color' => ['#3491e0','#a7a7a7'],
            'data' => [$data['nivel']['inicial'], (100-$data['nivel']['inicial'])]
        ]);
        graphPie([
            'graphName' => 'chart_int.jpg',
            'color' => ['#fb3131','#a7a7a7'],
            'data' => [$data['nivel']['intermedio'], (100-$data['nivel']['intermedio'])]
        ]);
        graphPie([
            'graphName' => 'chart_ade.jpg',
            'color' => ['#deb41c','#a7a7a7'],
            'data' => [$data['nivel']['adecuado'], (100-$data['nivel']['adecuado'])]
        ]);

        $dataPorcH = ['values' => [], 'labels' => []];
        for ($i=0; $i < count($data['habilidades']); $i++) {
            $dataPorcH['values'][] = $data['habilidades'][$i]['porc'];
            $dataPorcH['labels'][] = $data['habilidades'][$i]['habilidad'];
        }
        graphBar([
            'graphName' => 'chart_rendHabilidad.jpg',
            'label' => $dataPorcH['labels'],
            'data' => $dataPorcH['values'],
            'color' => [$color],
            'size' => ['width' => 700, 'height' => 350],
            'angle' => 20
        ]);
        $puntSimceAlum = ['values' => [], 'labels' => []];
        for ($i=0; $i < count($data['alumnos']); $i++)
            $puntSimceAlum['values'][] = $data['alumnos'][$i]['puntajeSimce'];
        graphBar([
            'graphName' => 'chart_puntSimceAlum.jpg',
            'label' => [],
            'data' => count($puntSimceAlum['values']) > 0 ? $puntSimceAlum['values'] : [0],
            'color' => [$color],
            'size' => ['width' => 700, 'height' => 200]
        ]);
        $nivelAprendizaje = [
            $data['nivel']['inicial'],
            $data['nivel']['intermedio'],
            $data['nivel']['adecuado']
        ];
        graphBar([
            'graphName' => 'chart_nivel_aprendizaje.jpg',
            'label' => ['INSUFICIENTE', 'ELEMENTAL', 'ADECUADO'],
            'data' => $nivelAprendizaje,
            'color' => [$color],
            'size' => ['width' => 500, 'height' => 200]
        ]);

        return $data;
    }

    public function cobertura($idAsignacion, $origen='AEDUC', $rbd=false) {
        $this->load->library('pdfgenerator');

        if ($rbd)
            $this->setRbd($rbd);

        $conberturaData = $this->coberturaData($idAsignacion, $origen);

        $partial = null;
        if ($origen == 'AEDUC')
            $partial = getPartialsByCodigo($conberturaData['codigo'], $conberturaData['tipo_id']);
        else
            $partial = getPartialsByAsignatura($conberturaData['asignatura_id'], $conberturaData['tipo_id']);

        $conberturaData['pdfName'] = 'REPORTE_COBERTURA';
        $conberturaData['encabezado'] = $partial['enc'];
        $conberturaData['portada'] = $partial['port'];
        $conberturaData['pie'] = $partial['pie'];

        $html = $this->load->view('modulos/reportes/cobertura', $conberturaData, TRUE);
        $graphName = 'report_'.time();
        $this->pdfgenerator->generate($html, $graphName, TRUE, 'A4', 'portrait');
    }

    public function coberturaData($idAsignacion, $origen) {

        $asignacion         = $this->asignacion->get($idAsignacion);
        $curso              = $this->curso->getCursoByAsignacion($idAsignacion);
        $alumnos            = $this->estadistica->getAlumnosByAsignacion($idAsignacion);
        $estadisticas       = $this->estadistica->getRespuestas($idAsignacion, $origen);
        $habilidadesAlumno  = $this->estadistica->gethabilidadesByAsignacion($idAsignacion, $origen);
        $habilidades        = $this->estadistica->getHabilidadesByPrueba($asignacion->prueba_id, $origen);
        $prueba             = $this->prueba->get($asignacion->prueba_id, $origen);
        $ordenPreguntas     = $this->estadistica->getOrdenPreguntas($prueba->id, $origen);
        $pregHabiOa         = $this->estadistica->getPreguntaHabilidadOaByPrueba($prueba->id, $origen);
        $oasRespIesEje      = $this->estadistica->getOasRespIeEjeByAsignacion($idAsignacion, $origen); //----ok oasRespIeEje

        $color = $color2 = $color3 = '';
        switch ($prueba->asignatura_id) {
            case 1: $color = '#F47921'; $color2 = '#EBCFBB'; $color3 = '#FAC49D'; break; //LENGUAJE
            case 3: $color = '#ED028A'; $color2 = '#F7CFE7'; $color3 = '#FFD5EC'; break; //MATEMATICAS
            case 4: $color = '#00B4E0'; $color2 = '#6BD1EB'; $color3 = '#A6E5F4'; break; //Historia, Geografía y Ciencias Sociales
            case 5: $color = '#56B947'; $color2 = '#9DD597'; $color3 = '#B3DFAC'; break; //Ciencias Naturales
            default: $color = '#56B947'; $color2 = '#9DD597'; $color3 = '#B3DFAC'; break;
        }

        $data = [
            'color'             => $color,
            'color2'            => $color2,
            'color3'            => $color3,
			'codigo'			=> $prueba->codigo,
            'asignatura_id'     => $prueba->asignatura_id,
            'tipo_id'           => $prueba->tipo_id,
            'num_evaluacion'    => $prueba->num,
            'curso'             => $curso->nivel.' "'.$curso->letra.'"',
            'cantPreguntas'     => (int)$prueba->preguntas,
            'porc_correctas'    => 0,
            'porc_incorrectas'  => 0,
            'porc_omitidas'     => 0,
            'porc_aprobados'    => 0,
            'porc_reprobados'   => 0,
            'preguntas'         => [],
            'alternativas'      => [],
            'nota'              => 0,
            'cantAlumnos'       => 0,
            'alumnos'           => [],
            'habilidades'       => [],
            'oas'               => [],
            'ejes'              => [],
            'respAltPreg'       => [
                'correcta'      => [],
                'omitidas'      => []
            ]
        ];

        $alumnosOden = [];
        if ($alumnos)
            foreach ($alumnos as $alum)
                $alumnosOden[$alum->id] = [
                    'nombre' => $alum->nombre,
                    'apellido' => $alum->apellido,
                    'inicio' => $alum->inicio,
                    'fin' => $alum->fin
                ];

        $oasSum = [];
        if ($pregHabiOa)
            foreach ($pregHabiOa as $pho)
                $oasSum[$pho->codigo] = isset($oasSum[$pho->codigo]) ? $oasSum[$pho->codigo] + 1: 1;


        if ($habilidades) {
            foreach ($habilidades as $h) {
                $data['habilidades'][] = array(
                    'id'        => $h->id,
                    'habilidad' => $h->habilidadcurricular,
                    'count'     => $h->count,
                    'porc'      => 0,
                    'preguntas' => [],
                    'oas'       => []
                );
            }
        }

        if ($estadisticas) {
            foreach ($estadisticas as $est) {
                $omitidas = $prueba->preguntas - ($est->correctas + $est->incorrectas);
                $data['cantAlumnos']++;

                if ($prueba->preguntas > 0) {
                    $porcCorrectas = round(($est->correctas * 100) / $prueba->preguntas, 1);
                    $porcIncorrectas = round(($est->incorrectas * 100) / $prueba->preguntas, 1);
                    $porcOmitidas = round(($omitidas * 100) / $prueba->preguntas, 1);
                } else {
                    $porcCorrectas = $porcIncorrectas = $porcOmitidas = 0;
                }

                $nota = 0;
                if($porcCorrectas < 60)
                    $nota = round(5 * ($porcCorrectas*0.01) + 1, 1);
                else
                    $nota = round(7.5 * (($porcCorrectas*0.01) - 0.6) + 4, 1);

                if($nota < 2)
                    $nota = 2;

                $data['porc_correctas']     += $porcCorrectas;
                $data['porc_incorrectas']   += $porcIncorrectas;
                $data['porc_omitidas']      += $porcOmitidas;

                $fechaInicio = new DateTime(date('Y-m-d h:i:s', $alumnosOden[$est->usuario_id]['inicio']));
                $fechaFin = new DateTime(date('Y-m-d h:i:s', $alumnosOden[$est->usuario_id]['fin']));
                $diff = $fechaInicio->diff($fechaFin);
                $tiempo = ($diff->h < 10 ? "0$diff->h" : $diff->h).':'.($diff->i < 10 ? "0$diff->i" : $diff->i);

                $data['alumnos'][] = [
                    'id'                => $est->usuario_id,
                    'nombre'            =>  $alumnosOden[$est->usuario_id]['nombre'],
                    'apellido'          =>  $alumnosOden[$est->usuario_id]['apellido'],
                    'tiempo'            =>  $tiempo,
                    'correctas'         => (int)$est->correctas,
                    'incorrectas'       => (int)$est->incorrectas,
                    'omitidas'          => (int)$omitidas,
                    'porc_correctas'    => $porcCorrectas,
                    'porc_incorrectas'  => $porcIncorrectas,
                    'porc_omitidas'     => $porcOmitidas,
                    'nota'              => $nota,
                    'oas'               => [],
                    'ejes'              => [],
                    'habilidades'       => $data['habilidades'],
                    'agrupacion'        => [
                        'correctas'     => [],
                        'incorrectas'   => [],
                        'omitidas'      => []
                    ]
                ];
            }
        }
        if ($data['cantAlumnos'] > 0) {
            $data['porc_correctas'] = round($data['porc_correctas'] / $data['cantAlumnos'], 1);
            $data['porc_incorrectas'] = round($data['porc_incorrectas'] / $data['cantAlumnos'], 1);
            $data['porc_omitidas'] = round($data['porc_omitidas'] / $data['cantAlumnos'], 1);
        } else {
            $data['porc_correctas'] = $data['porc_incorrectas'] = $data['porc_omitidas'] = 0;
        }

        if ($habilidadesAlumno) {
            foreach ($habilidadesAlumno as $ha) {
                $porc = 0;
                for ($i = 0; $i < count($data['habilidades']); $i++) {
                    if ($data['habilidades'][$i]['id'] == $ha->habilidadcurricular_id) {
                        $porc = $data['habilidades'][$i]['count'] > 0 ? round(($ha->correctas * 100) / $data['habilidades'][$i]['count'], 1) : 0;
                        $data['habilidades'][$i]['porc'] += $porc;
                    }
                }
                for ($x = 0; $x < count($data['alumnos']); $x++) {
                    if ($data['alumnos'][$x]['id'] == $ha->usuario_id) {
                        for ($z = 0; $z < count($data['alumnos'][$x]['habilidades']); $z++) {
                            if ($data['alumnos'][$x]['habilidades'][$z]['id'] == $ha->habilidadcurricular_id) {
                                $data['alumnos'][$x]['habilidades'][$z] = [
                                    'id'        => $ha->habilidadcurricular_id,
                                    'habilidad' => $ha->habilidadcurricular,
                                    'porc'      => $porc
                                ];
                            }
                        }
                    }
                }
            }
        }

        $preguntasAlumno = [];
        if ($oasRespIesEje) {
            foreach ($oasRespIesEje as $val) {
                $preguntasAlumno[$val->usuario_id][$val->pregunta_id] = [$val->respuesta_id, $val->correcta];

                $flag = FALSE;
                for ($x = 0; $x < count($data['ejes']); $x++)
                    if ($data['ejes'][$x]['id'] == $val->ejetematico_id)
                        $flag = TRUE;

                $ejesSum[$val->ejetematico_id] = isset($ejesSum[$val->ejetematico_id]) ? $ejesSum[$val->ejetematico_id] + 1: 1;
                if ($flag == FALSE) {
                    $data['ejes'][] = ['id' => $val->ejetematico_id, 'eje' => $val->ejetematico, 'logro' => 0];
                }
            }
        }

        $numClave = 0;
        $aux = FALSE;
        $preguntas = [];
        $preguntaClave = [];
        for ($i = 0; $i < count($ordenPreguntas); $i++) {
            if ($ordenPreguntas[$i]->pregunta_id == $aux) {
                $numClave++;
            } else {
                $aux = $ordenPreguntas[$i]->pregunta_id;
                $numClave = 1;
            }

            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['pregunta_id'] = $ordenPreguntas[$i]->pregunta_id;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['orden'] = $ordenPreguntas[$i]->orden;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['correctas'] = 0;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['incorrectas'] = 0;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['omitidas'] = 0;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['alternativas'][$ordenPreguntas[$i]->respuesta_id] = [
                'letra' => $this->getLetraByNumber($numClave),
                'ocurrencia' => 0
            ];

            if ($ordenPreguntas[$i]->correcta == 't') {
                $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['clave'] = $this->getLetraByNumber($numClave);

                $preguntas[] = $preguntaClave[$ordenPreguntas[$i]->pregunta_id];
            }

            if (!in_array($this->getLetraByNumber($numClave), $data['alternativas']))
                $data['alternativas'][] = $this->getLetraByNumber($numClave);

            //Se recorre por alumnos
            foreach ($preguntasAlumno as $index => $pa) {
                if (array_key_exists($ordenPreguntas[$i]->pregunta_id, $preguntasAlumno[$index])) {
                    if ($pa[$ordenPreguntas[$i]->pregunta_id][1] == 't') {
                        for ($x = 0; $x < count($data['alumnos']); $x++) {
                            if ($data['alumnos'][$x]['id'] == $index) {
                                if (!in_array($ordenPreguntas[$i]->orden, $data['alumnos'][$x]['agrupacion']['correctas'])) {
                                    $data['alumnos'][$x]['agrupacion']['correctas'][] = $ordenPreguntas[$i]->orden;
                                }
                            }
                        }
                    } else {
                        for ($x = 0; $x < count($data['alumnos']); $x++) {
                            if ($data['alumnos'][$x]['id'] == $index) {
                                if (!in_array($ordenPreguntas[$i]->orden, $data['alumnos'][$x]['agrupacion']['incorrectas'])) {
                                    $data['alumnos'][$x]['agrupacion']['incorrectas'][] = $ordenPreguntas[$i]->orden;
                                }
                            }
                        }
                    }
                } else {
                    for ($x = 0; $x < count($data['alumnos']); $x++) {
                        if ($data['alumnos'][$x]['id'] == $index) {
                            if (!in_array($ordenPreguntas[$i]->orden, $data['alumnos'][$x]['agrupacion']['omitidas'])) {
                                $data['alumnos'][$x]['agrupacion']['omitidas'][] = $ordenPreguntas[$i]->orden;
                            }
                        }
                    }
                }
            }
        }

        foreach ($preguntasAlumno as $pa) {
            foreach ($pa as $key => $val) {
                $preguntaClave[$key]['alternativas'][$pa[$key][0]]['ocurrencia']++;
            }
        }

        for ($i = 0; $i < count($preguntas); $i++) {
            $data['preguntas'][] = $preguntaClave[$preguntas[$i]['pregunta_id']];
        }

        for ($i = 0; $i < count($data['habilidades']); $i++) {
            $data['habilidades'][$i]['porc'] = $data['cantAlumnos'] > 0 ? round($data['habilidades'][$i]['porc'] / $data['cantAlumnos'], 1) : 0;
            if ($pregHabiOa) {
                foreach ($pregHabiOa as $pho) {
                    if ($data['habilidades'][$i]['id'] == $pho->habilidad_id) {
                        $data['habilidades'][$i]['preguntas'][] = $pho->orden;
                        if (!in_array($pho->codigo, $data['habilidades'][$i]['oas']))
                            $data['habilidades'][$i]['oas'][] = $pho->codigo;
                    }
                    $flag = FALSE;
                    for ($x = 0; $x < count($data['oas']); $x++)
                        if ($data['oas'][$x]['oa'] == $pho->codigo)
                            $flag = TRUE;

                    if ($flag == FALSE) {
                        $data['oas'][] = ['oa' => $pho->codigo, 'logro' => 0, 'desc' => $pho->oa];
                    }
                }
            }
        }

        for ($i = 0; $i < count($data['oas']); $i++) {
            for ($x = 0; $x < count($data['alumnos']); $x++) {
                if (!in_array($data['oas'][$i], $data['alumnos'][$x]['oas'])) {
                    $data['alumnos'][$x]['oas'][] = $data['oas'][$i];
                }
            }
        }
        //agregar ejes a alumnos
        for ($i = 0; $i < count($data['ejes']); $i++) {
            for ($x = 0; $x < count($data['alumnos']); $x++) {
                if (!in_array($data['ejes'][$i], $data['alumnos'][$x]['ejes'])) {
                    $data['alumnos'][$x]['ejes'][] = $data['ejes'][$i];
                }
            }
        }

        if($oasRespIesEje)
            foreach ($oasRespIesEje as $or) {
                for ($i = 0; $i < count($data['alumnos']); $i++) {
                    if ($data['alumnos'][$i]['id'] == $or->usuario_id) {
                        for ($x = 0; $x < count($data['alumnos'][$i]['oas']); $x++) {
                            if ($data['alumnos'][$i]['oas'][$x]['oa'] == $or->codigo) {
                                if ($or->correcta == 't') {
                                    $data['alumnos'][$i]['oas'][$x]['logro']++;
                                }
                            }
                        }
                        for ($x = 0; $x < count($data['alumnos'][$i]['ejes']); $x++) {
                            if ($data['alumnos'][$i]['ejes'][$x]['id'] == $or->ejetematico_id) {
                                if ($or->correcta == 't') {
                                    $data['alumnos'][$i]['ejes'][$x]['logro']++;
                                }
                            }
                        }
                    }
                }
            }


        for ($i = 0; $i < count($data['alumnos']); $i++) {
            for ($x = 0; $x < count($data['alumnos'][$i]['agrupacion']['correctas']); $x++) {
                for ($z = 0; $z < count($data['preguntas']); $z++) {
                    if ($data['preguntas'][$z]['orden'] == $data['alumnos'][$i]['agrupacion']['correctas'][$x]) {
                        $data['preguntas'][$z]['correctas']++;
                    }
                }
            }
            for ($x = 0; $x < count($data['alumnos'][$i]['agrupacion']['incorrectas']); $x++) {
                for ($z = 0; $z < count($data['preguntas']); $z++) {
                    if ($data['preguntas'][$z]['orden'] == $data['alumnos'][$i]['agrupacion']['incorrectas'][$x]) {
                        $data['preguntas'][$z]['incorrectas']++;
                    }
                }
            }
            for ($x = 0; $x < count($data['alumnos'][$i]['agrupacion']['omitidas']); $x++) {
                for ($z = 0; $z < count($data['preguntas']); $z++) {
                    if ($data['preguntas'][$z]['orden'] == $data['alumnos'][$i]['agrupacion']['omitidas'][$x]) {
                        $data['preguntas'][$z]['omitidas']++;
                    }
                }
            }

            $data['nota'] += $data['alumnos'][$i]['nota'];
            if ($data['alumnos'][$i]['nota'] >= 4)
                $data['porc_aprobados']++;
            else
                $data['porc_reprobados']++;

            for ($x = 0; $x < count($data['alumnos'][$i]['oas']); $x++) {
                $data['alumnos'][$i]['oas'][$x]['logro'] = $oasSum[$data['alumnos'][$i]['oas'][$x]['oa']] > 0 ? round(($data['alumnos'][$i]['oas'][$x]['logro'] * 100) / $oasSum[$data['alumnos'][$i]['oas'][$x]['oa']], 1) : 0;
                for ($z = 0; $z < count($data['oas']); $z++) {
                    if ($data['oas'][$z]['oa'] == $data['alumnos'][$i]['oas'][$x]['oa'])
                        $data['oas'][$z]['logro'] += $data['alumnos'][$i]['oas'][$x]['logro'];
                }
            }
            for ($x = 0; $x < count($data['alumnos'][$i]['ejes']); $x++) {
                $data['alumnos'][$i]['ejes'][$x]['logro'] = $ejesSum[$data['alumnos'][$i]['ejes'][$x]['id']] > 0 ? round(($data['alumnos'][$i]['ejes'][$x]['logro'] * 100) / $ejesSum[$data['alumnos'][$i]['ejes'][$x]['id']], 1) : 0;
                for ($z = 0; $z < count($data['ejes']); $z++) {
                    if ($data['ejes'][$z]['id'] == $data['alumnos'][$i]['ejes'][$x]['id'])
                        $data['ejes'][$z]['logro'] += $data['alumnos'][$i]['ejes'][$x]['logro'];
                }
            }
        }

        $data['nota'] = $data['nota'] > 0 ? round($data['nota'] / $data['cantAlumnos'], 1) : 0;
        $data['porc_aprobados'] =  $data['cantAlumnos'] > 0 ? round(($data['porc_aprobados'] * 100) / $data['cantAlumnos'], 1) : 0;
        $data['porc_reprobados'] = $data['cantAlumnos'] > 0 ? round(($data['porc_reprobados'] * 100) / $data['cantAlumnos'], 1) : 0;

        for ($z = 0; $z < count($data['oas']); $z++)
            $data['oas'][$z]['logro'] = $data['cantAlumnos'] > 0 ? round($data['oas'][$z]['logro'] / $data['cantAlumnos'], 1) : 0;
        for ($z = 0; $z < count($data['ejes']); $z++)
            $data['ejes'][$z]['logro'] =  $data['cantAlumnos'] > 0 ? round($data['ejes'][$z]['logro'] / $data['cantAlumnos'], 1) : 0;

        $ordenOas = [];
        foreach ($data['oas'] as $oa)
            $ordenOas[preg_replace("/[^0-9]/", "", $oa['oa'])] = $oa;
        ksort($ordenOas);
        $newOas = [];
        foreach ($ordenOas as $oa)
            $newOas[] = $oa;
        $data['oas'] = $newOas;

        graphPie([
            'graphName' => 'chart_corr.jpg',
            'color' => ['#3491e0','#a7a7a7'],
            'data' => [$data['porc_correctas'], (100-$data['porc_correctas'])]
        ]);
        graphPie([
            'graphName' => 'chart_incorr.jpg',
            'color' => ['#fb3131','#a7a7a7'],
            'data' => [$data['porc_incorrectas'], (100-$data['porc_incorrectas'])]
        ]);
        graphPie([
            'graphName' => 'chart_omit.jpg',
            'color' => ['#deb41c','#a7a7a7'],
            'data' => [$data['porc_omitidas'], (100-$data['porc_omitidas'])]
        ]);

        $dataPorcH = ['values' => [], 'labels' => []];
        for ($i=0; $i < count($data['habilidades']); $i++) {
            $dataPorcH['values'][] = $data['habilidades'][$i]['porc'];
            $dataPorcH['labels'][] = $data['habilidades'][$i]['habilidad'];
        }
        graphBar([
            'graphName' => 'chart_rendHabilidad.jpg',
            'label' => count($dataPorcH['labels']) > 0 ? $dataPorcH['labels'] : ['Habilidades'],
            'data' => count($dataPorcH['values']) > 0 ? $dataPorcH['values'] : [0],
            'color' => [$color],
            'size' => ['width' => 700, 'height' => 350],
            'angle' => 20
        ]);
        $dataPorcOa = ['values' => [], 'labels' => []];
        for ($i = 0; $i < count($data['oas']); $i++) {
            $dataPorcOa['values'][] = $data['oas'][$i]['logro'];
            $dataPorcOa['labels'][] = $data['oas'][$i]['oa'];
        }
        graphBar([
            'graphName' => 'chart_promOaCurso.jpg',
            'label' => count($dataPorcOa['labels']) > 0 ? $dataPorcOa['labels'] : ['OA'],
            'data' => count($dataPorcOa['values']) > 0 ? $dataPorcOa['values'] : [0],
            'color' => [$color],
            'size' => ['width' => 700, 'height' => 200]
        ]);
        graphBar([
            'graphName' => 'chart_aproRepro.jpg',
            'label' => ['APROBADOS', 'REPROBADOS'],
            'data' => [$data['porc_aprobados'], $data['porc_reprobados']],
            'color' => [$color],
            'size' => ['width' => 500, 'height' => 200]
        ]);

        return $data;
    }

    public function unidad($idAsignacion, $origen='AEDUC', $rbd=false) {
        $this->load->library('pdfgenerator');

        if ($rbd)
            $this->setRbd($rbd);

        $unidadData = $this->unidadData($idAsignacion, $origen);

        $partial = null;
        if ($origen == 'AEDUC')
            $partial = getPartialsByCodigo($unidadData['codigo'], $unidadData['tipo_id']);
        else
            $partial = getPartialsByAsignatura($unidadData['asignatura_id'], $unidadData['tipo_id']);

        $unidadData['pdfName'] = 'REPORTE_UNIDAD';
        $unidadData['encabezado'] = $partial['enc'];
        $unidadData['portada'] = $partial['port'];
        $unidadData['pie'] = $partial['pie'];

        $html = $this->load->view('modulos/reportes/unidad', $unidadData, TRUE);
        $graphName = 'report_'.time();
        $this->pdfgenerator->generate($html, $graphName, TRUE, 'A4', 'portrait');
    }

    public function unidadData($idAsignacion, $origen) {
        $asignacion         = $this->asignacion->get($idAsignacion);
        $curso              = $this->curso->getCursoByAsignacion($idAsignacion);
        $alumnos            = $this->estadistica->getAlumnosByAsignacion($idAsignacion);
        $estadisticas       = $this->estadistica->getRespuestas($idAsignacion, $origen);
        $prueba             = $this->prueba->get($asignacion->prueba_id, $origen);
        $ordenPreguntas     = $this->estadistica->getOrdenPreguntas($prueba->id, $origen);
        $pregIeOa           = $this->estadistica->getPreguntaOaIeByPrueba($prueba->id, $origen);
        $oasRespIes         = $this->estadistica->getOasRespIeByAsignacion($idAsignacion, $origen); //----ok oasRespIeEje

		$asignatura = $prueba->codigo;
		$asignatura=substr($asignatura, 0,3);
		$color = $color2 = $color3 = $color4= '';


		switch ($prueba->asignatura_id) {
			case 1: $color = '#F47921'; $color2 = '#EBCFBB'; $color3 = '#FAC49D'; $color4 = '#FDF4ED';	break; //LENGUAJE
			case 3: $color = '#ED028A'; $color2 = '#F7CFE7'; $color3 = '#FFD5EC'; $color4 = '#FEF7FB';	break; //MATEMATICAS
			case 4: $color = '#00B4E0'; $color2 = '#6BD1EB'; $color3 = '#A6E5F4'; $color4 =	'#F1FAFD';	break; //Historia, Geografía y Ciencias Sociales
			case 5:
				if ($asignatura=='BIO'){
					$color = '#52d942'; $color2 = '#8bff74'; $color3 = '#00a600'; $color4 = '#D9EBD3';
				}//Ciencias Naturales

				elseif ($asignatura=='FIS'){
					$color = '#009d89'; $color2 = '#53cfb9'; $color3 = '#006e5c'; $color4 = '#D3E0DD';
				}

				elseif ($asignatura=='QUI'){
					$color = '#8d171b'; $color2 = '#c34b42'; $color3 = '#5a0000'; $color4 = '#DACBCA';
				}
				 break;

			default: $color = '#56B947'; $color2 = '#9DD597'; $color3 = '#B3DFAC'; $color4 = '#B3DFAC'; break;
		}


        $data = [
            'color'             => $color,
            'color2'            => $color2,
            'color3'            => $color3,
			'color4'			=> $color4,
			'codigo'			=> $prueba->codigo,
            'asignatura_id'     => $prueba->asignatura_id,
            'tipo_id'           => $prueba->tipo_id,
            'num_evaluacion'    => $prueba->num,
            'curso'             => $curso->nivel.' "'.$curso->letra.'"',
            'cantPreguntas'     => (int)$prueba->preguntas,
            'porc_correctas'    => 0,
            'porc_incorrectas'  => 0,
            'porc_omitidas'     => 0,
            'porc_aprobados'    => 0,
            'porc_reprobados'   => 0,
            'preguntas'         => [],
            'alternativas'      => [],
            'nota'              => 0,
            'cantAlumnos'       => 0,
            'alumnos'           => [],
            'oas'               => [],
            'respAltPreg'       => [
                'correcta'      => [],
                'omitidas'      => []
            ]
        ];

        $alumnosOden = [];
        if ($alumnos)
            foreach ($alumnos as $alum)
                $alumnosOden[$alum->id] = [
                    'nombre' => $alum->nombre,
                    'apellido' => $alum->apellido,
                    'inicio' => $alum->inicio,
                    'fin' => $alum->fin
                ];

        $oasSum = [];
        if ($pregIeOa)
            foreach ($pregIeOa as $pio)
                $oasSum[$pio->codigo] = isset($oasSum[$pio->codigo]) ? $oasSum[$pio->codigo] + 1: 1;

        if ($estadisticas) {
            foreach ($estadisticas as $est) {
                $omitidas = $prueba->preguntas - ($est->correctas + $est->incorrectas);
                $data['cantAlumnos']++;

                if ($prueba->preguntas > 0) {
                    $porcCorrectas = round(($est->correctas * 100) / $prueba->preguntas, 1);
                    $porcIncorrectas = round(($est->incorrectas * 100) / $prueba->preguntas, 1);
                    $porcOmitidas = round(($omitidas * 100) / $prueba->preguntas, 1);
                } else {
                    $porcCorrectas = $porcIncorrectas = $porcOmitidas = 0;
                }

                $nota = 0;
                if($porcCorrectas < 60)
                    $nota = round(5 * ($porcCorrectas*0.01) + 1, 1);
                else
                    $nota = round(7.5 * (($porcCorrectas*0.01) - 0.6) + 4, 1);

                if($nota < 2)
                    $nota = 2;

                $data['porc_correctas']     += $porcCorrectas;
                $data['porc_incorrectas']   += $porcIncorrectas;
                $data['porc_omitidas']      += $porcOmitidas;

                $fechaInicio = new DateTime(date('Y-m-d h:i:s', $alumnosOden[$est->usuario_id]['inicio']));
                $fechaFin = new DateTime(date('Y-m-d h:i:s', $alumnosOden[$est->usuario_id]['fin']));
                $diff = $fechaInicio->diff($fechaFin);
                $tiempo = ($diff->h < 10 ? "0$diff->h" : $diff->h).':'.($diff->i < 10 ? "0$diff->i" : $diff->i);

                $data['alumnos'][] = [
                    'id'                => $est->usuario_id,
                    'nombre'            =>  $alumnosOden[$est->usuario_id]['nombre'],
                    'apellido'          =>  $alumnosOden[$est->usuario_id]['apellido'],
                    'tiempo'            =>  $tiempo,
                    'correctas'         => (int)$est->correctas,
                    'incorrectas'       => (int)$est->incorrectas,
                    'omitidas'          => (int)$omitidas,
                    'porc_correctas'    => $porcCorrectas,
                    'porc_incorrectas'  => $porcIncorrectas,
                    'porc_omitidas'     => $porcOmitidas,
                    'nota'              => $nota,
                    'oas'               => [],
                    'agrupacion'        => [
                        'correctas'     => [],
                        'incorrectas'   => [],
                        'omitidas'      => []
                    ]
                ];
            }
        }

        if ($data['cantAlumnos'] > 0) {
            $data['porc_correctas']     = round($data['porc_correctas'] / $data['cantAlumnos'], 1);
            $data['porc_incorrectas']   = round($data['porc_incorrectas'] / $data['cantAlumnos'], 1);
            $data['porc_omitidas']      = round($data['porc_omitidas'] / $data['cantAlumnos'], 1);
        } else {
            $data['porc_correctas'] = $data['porc_incorrectas'] = $data['porc_omitidas'] = 0;
        }

        $preguntasAlumno = [];
        if ($oasRespIes)
            foreach ($oasRespIes as $val)
                $preguntasAlumno[$val->usuario_id][$val->pregunta_id] = [$val->respuesta_id, $val->correcta];

        $numClave = 0;
        $aux = FALSE;
        $preguntas = [];
        $preguntaClave = [];
        for ($i = 0; $i < count($ordenPreguntas); $i++) {
            if ($ordenPreguntas[$i]->pregunta_id == $aux) {
                $numClave++;
            } else {
                $aux = $ordenPreguntas[$i]->pregunta_id;
                $numClave = 1;
            }

            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['pregunta_id'] = $ordenPreguntas[$i]->pregunta_id;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['orden'] = $ordenPreguntas[$i]->orden;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['correctas'] = 0;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['incorrectas'] = 0;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['omitidas'] = 0;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['alternativas'][$ordenPreguntas[$i]->respuesta_id] = [
                'letra' => $this->getLetraByNumber($numClave),
                'ocurrencia' => 0
            ];

            if ($ordenPreguntas[$i]->correcta == 't') {
                $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['clave'] = $this->getLetraByNumber($numClave);
                $preguntas[] = $preguntaClave[$ordenPreguntas[$i]->pregunta_id];
            }

            if (!in_array($this->getLetraByNumber($numClave), $data['alternativas']))
                $data['alternativas'][] = $this->getLetraByNumber($numClave);

            //Se recorre por alumnos
            foreach ($preguntasAlumno as $index => $pa) {
                if (array_key_exists($ordenPreguntas[$i]->pregunta_id, $preguntasAlumno[$index])) {
                    if ($pa[$ordenPreguntas[$i]->pregunta_id][1] == 't') {
                        for ($x = 0; $x < count($data['alumnos']); $x++) {
                            if ($data['alumnos'][$x]['id'] == $index) {
                                if (!in_array($ordenPreguntas[$i]->orden, $data['alumnos'][$x]['agrupacion']['correctas'])) {
                                    $data['alumnos'][$x]['agrupacion']['correctas'][] = $ordenPreguntas[$i]->orden;
                                }
                            }
                        }
                    } else {
                        for ($x = 0; $x < count($data['alumnos']); $x++) {
                            if ($data['alumnos'][$x]['id'] == $index) {
                                if (!in_array($ordenPreguntas[$i]->orden, $data['alumnos'][$x]['agrupacion']['incorrectas'])) {
                                    $data['alumnos'][$x]['agrupacion']['incorrectas'][] = $ordenPreguntas[$i]->orden;
                                }
                            }
                        }
                    }
                } else {
                    for ($x = 0; $x < count($data['alumnos']); $x++) {
                        if ($data['alumnos'][$x]['id'] == $index) {
                            if (!in_array($ordenPreguntas[$i]->orden, $data['alumnos'][$x]['agrupacion']['omitidas'])) {
                                $data['alumnos'][$x]['agrupacion']['omitidas'][] = $ordenPreguntas[$i]->orden;
                            }
                        }
                    }
                }
            }
        }

        foreach ($preguntasAlumno as $pa)
            foreach ($pa as $key => $val)
                $preguntaClave[$key]['alternativas'][$pa[$key][0]]['ocurrencia']++;

        for ($i = 0; $i < count($preguntas); $i++)
            $data['preguntas'][] = $preguntaClave[$preguntas[$i]['pregunta_id']];

        if ($pregIeOa) {
            foreach ($pregIeOa as $pio) {
                $flag = FALSE;
                for ($x = 0; $x < count($data['oas']); $x++)
                    if ($data['oas'][$x]['oa'] == $pio->codigo)
                        $flag = TRUE;

                if ($flag == FALSE) {
                    $data['oas'][] = [
                        'oa' => $pio->codigo,
                        'logro' => 0,
                        'desc' => $pio->oa,
                        'ies' => [],
                        'prom_ies' => 0,
                        'count_ies' => 0
                    ];
                }
            }

            foreach ($pregIeOa as $pio) {
                for ($x = 0; $x < count($data['oas']); $x++) {
                    if ($data['oas'][$x]['oa'] == $pio->codigo) {
                        if (count($data['oas'][$x]['ies']) > 0) {
                            $flag1 = FALSE;
                            for ($i = 0; $i < count($data['oas'][$x]['ies']); $i++)
                                if ($data['oas'][$x]['ies'][$i]['ie_id'] == $pio->ie_id)
                                    $flag1 = TRUE;

                            if ($flag1 == FALSE) {
                                $data['oas'][$x]['ies'][] = [
                                    'ie_id' => $pio->ie_id,
                                    'ie' => $pio->ie,
                                    'logro' => 0,
                                    'count' => 0
                                ];
                            }
                        } else {
                            $data['oas'][$x]['ies'][] = [
                                'ie_id' => $pio->ie_id,
                                'ie' => $pio->ie,
                                'logro' => 0,
                                'count' => 0
                            ];
                        }
                    }
                }
            }
        }

        for ($i = 0; $i < count($data['oas']); $i++)
            for ($x = 0; $x < count($data['alumnos']); $x++)
                if (!in_array($data['oas'][$i], $data['alumnos'][$x]['oas']))
                    $data['alumnos'][$x]['oas'][] = $data['oas'][$i];

        if($oasRespIes) {
            foreach ($oasRespIes as $ori) {
                for ($i = 0; $i < count($data['alumnos']); $i++) {
                    if ($data['alumnos'][$i]['id'] == $ori->usuario_id) {
                        for ($x = 0; $x < count($data['alumnos'][$i]['oas']); $x++) {
                            if ($data['alumnos'][$i]['oas'][$x]['oa'] == $ori->codigo) {
                                if ($ori->correcta == 't') {
                                    $data['alumnos'][$i]['oas'][$x]['logro']++;
                                }
                            }
                        }
                    }
                }
            }
            foreach ($oasRespIes as $ori) {
                for ($i = 0; $i < count($data['alumnos']); $i++) {
                    if ($data['alumnos'][$i]['id'] == $ori->usuario_id) {
                        for ($x = 0; $x < count($data['alumnos'][$i]['oas']); $x++) {
                            if ($data['alumnos'][$i]['oas'][$x]['oa'] == $ori->codigo) {
                                for ($e = 0; $e < count($data['alumnos'][$i]['oas'][$x]['ies']); $e++) {
                                    if ($data['alumnos'][$i]['oas'][$x]['ies'][$e]['ie_id'] == $ori->ie_id) {
                                        if ($ori->correcta == 't') {
                                            $data['alumnos'][$i]['oas'][$x]['ies'][$e]['logro']++;
                                        }
                                        $data['alumnos'][$i]['oas'][$x]['ies'][$e]['count']++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        for ($i = 0; $i < count($data['alumnos']); $i++) {
            for ($x = 0; $x < count($data['alumnos'][$i]['agrupacion']['correctas']); $x++) {
                for ($z = 0; $z < count($data['preguntas']); $z++) {
                    if ($data['preguntas'][$z]['orden'] == $data['alumnos'][$i]['agrupacion']['correctas'][$x]) {
                        $data['preguntas'][$z]['correctas']++;
                    }
                }
            }
            for ($x = 0; $x < count($data['alumnos'][$i]['agrupacion']['incorrectas']); $x++) {
                for ($z = 0; $z < count($data['preguntas']); $z++) {
                    if ($data['preguntas'][$z]['orden'] == $data['alumnos'][$i]['agrupacion']['incorrectas'][$x]) {
                        $data['preguntas'][$z]['incorrectas']++;
                    }
                }
            }
            for ($x = 0; $x < count($data['alumnos'][$i]['agrupacion']['omitidas']); $x++) {
                for ($z = 0; $z < count($data['preguntas']); $z++) {
                    if ($data['preguntas'][$z]['orden'] == $data['alumnos'][$i]['agrupacion']['omitidas'][$x]) {
                        $data['preguntas'][$z]['omitidas']++;
                    }
                }
            }

            $data['nota'] += $data['alumnos'][$i]['nota'];
            if ($data['alumnos'][$i]['nota'] >= 4)
                $data['porc_aprobados']++;
            else
                $data['porc_reprobados']++;

            for ($x = 0; $x < count($data['alumnos'][$i]['oas']); $x++) {
                $data['alumnos'][$i]['oas'][$x]['logro'] = $oasSum[$data['alumnos'][$i]['oas'][$x]['oa']] > 0 ? round(($data['alumnos'][$i]['oas'][$x]['logro'] * 100) / $oasSum[$data['alumnos'][$i]['oas'][$x]['oa']], 1) : 0;

                for ($z = 0; $z < count($data['oas']); $z++) {
                    if ($data['oas'][$z]['oa'] == $data['alumnos'][$i]['oas'][$x]['oa'])
                        $data['oas'][$z]['logro'] += $data['alumnos'][$i]['oas'][$x]['logro'];
                }

                for ($k = 0; $k < count($data['alumnos'][$i]['oas'][$x]['ies']); $k ++) {
                    $porcIe = $data['alumnos'][$i]['oas'][$x]['ies'][$k]['count'] > 0 ? round(($data['alumnos'][$i]['oas'][$x]['ies'][$k]['logro'] * 100) / $data['alumnos'][$i]['oas'][$x]['ies'][$k]['count'], 1) : 0;
                    $data['alumnos'][$i]['oas'][$x]['ies'][$k]['logro'] = $porcIe;
                }
            }
        }

        for ($i = 0; $i < count($data['alumnos']); $i++) {
            for ($x = 0; $x < count($data['alumnos'][$i]['oas']); $x++) {
                for ($z = 0; $z < count($data['oas']); $z++) {
                    if ($data['oas'][$z]['oa'] == $data['alumnos'][$i]['oas'][$x]['oa']) {
                        for ($k = 0; $k < count($data['alumnos'][$i]['oas'][$x]['ies']); $k++) {
                            for ($l = 0; $l < count($data['oas'][$z]['ies']); $l++) {
                                if ($data['oas'][$z]['ies'][$l]['ie_id'] == $data['alumnos'][$i]['oas'][$x]['ies'][$k]['ie_id']) {
                                    $data['oas'][$z]['ies'][$l]['logro'] +=  $data['alumnos'][$i]['oas'][$x]['ies'][$k]['logro'];
                                }
                            }
                        }
                    }
                }
            }
        }

        $data['nota'] = $data['nota'] > 0 ? round($data['nota'] / $data['cantAlumnos'], 1) : 0;
        $data['porc_aprobados'] = $data['cantAlumnos'] > 0 ? round(($data['porc_aprobados'] * 100) / $data['cantAlumnos'], 1) : 0;
        $data['porc_reprobados'] = $data['cantAlumnos'] > 0 ? round(($data['porc_reprobados'] * 100) / $data['cantAlumnos'], 1) : 0;

        for ($o = 0; $o < count($data['oas']); $o++) {
            $data['oas'][$o]['logro'] = $data['cantAlumnos'] > 0 ? round($data['oas'][$o]['logro'] / $data['cantAlumnos'], 1) : 0;
            for ($i = 0; $i < count($data['oas'][$o]['ies']); $i++) {
                $data['oas'][$o]['ies'][$i]['logro'] = $data['cantAlumnos'] > 0 ? round($data['oas'][$o]['ies'][$i]['logro'] / $data['cantAlumnos'], 1) : 0;
                $data['oas'][$o]['prom_ies'] += $data['oas'][$o]['ies'][$i]['logro'];
                $data['oas'][$o]['count_ies']++;
            }
        }

        for ($o = 0; $o < count($data['oas']); $o++)
            $data['oas'][$o]['prom_ies'] = $data['oas'][$o]['count_ies'] > 0 ? round($data['oas'][$o]['prom_ies'] / $data['oas'][$o]['count_ies'], 1) : 0;

        $ordenOas = [];
        foreach ($data['oas'] as $oa)
            $ordenOas[preg_replace("/[^0-9]/", "", $oa['oa'])] = $oa;
        ksort($ordenOas);
        $newOas = [];
        foreach ($ordenOas as $oa)
            $newOas[] = $oa;
        $data['oas'] = $newOas;

        graphPie([
            'graphName' => 'chart_corr.jpg',
            'color' => ['#3491e0','#a7a7a7'],
            'data' => [$data['porc_correctas'], (100-$data['porc_correctas'])]
        ]);
        graphPie([
            'graphName' => 'chart_incorr.jpg',
            'color' => ['#fb3131','#a7a7a7'],
            'data' => [$data['porc_incorrectas'], (100-$data['porc_incorrectas'])]
        ]);
        graphPie([
            'graphName' => 'chart_omit.jpg',
            'color' => ['#deb41c','#a7a7a7'],
            'data' => [$data['porc_omitidas'], (100-$data['porc_omitidas'])]
        ]);

        $dataPorcOa = ['values' => [], 'labels' => []];
        for ($i = 0; $i < count($data['oas']); $i++) {
            $dataPorcOa['values'][] = $data['oas'][$i]['logro'];
            $dataPorcOa['labels'][] = $data['oas'][$i]['oa'];
        }
        graphBar([
            'graphName' => 'chart_promOaCurso.jpg',
            'label' => count($dataPorcOa['labels']) > 0 ? $dataPorcOa['labels'] : ['OA'],
            'data' => count($dataPorcOa['values']) > 0 ? $dataPorcOa['values'] : [0],
            'color' => [$color],
            'size' => ['width' => 700, 'height' => 200]
        ]);
        graphBar([
            'graphName' => 'chart_aproRepro.jpg',
            'label' => ['APROBADOS', 'REPROBADOS'],
            'data' => [$data['porc_aprobados'], $data['porc_reprobados']],
            'color' => [$color],
            'size' => ['width' => 500, 'height' => 200]
        ]);

        return $data;
    }

    public function pme($idAsignacion, $origen='AEDUC', $rbd=false) {
        $this->load->library('pdfgenerator');

        if ($rbd)
            $this->setRbd($rbd);

        $pmeData = $this->pmeData($idAsignacion, $origen);

        $partial = null;
        if ($origen =='AEDUC')
            $partial = getPartialsByCodigo($pmeData['codigo'], $pmeData['tipo_id']);
        else
            $partial = getPartialsByAsignatura($pmeData['asignatura_id'], $pmeData['tipo_id']);

        $pmeData['pdfName'] = 'REPORTE_PME';
        $pmeData['encabezado'] = $partial['enc'];
        $pmeData['portada'] = $partial['port'];
        $pmeData['pie'] = $partial['pie'];

        $html = $this->load->view('modulos/reportes/pme', $pmeData, TRUE);
        $graphName = 'report_'.time();
        $this->pdfgenerator->generate($html, $graphName, TRUE, 'A4', 'portrait');
    }

    public function pmeData($idAsignacion, $origen) {
        $asignacion         = $this->asignacion->get($idAsignacion);
        $curso              = $this->curso->getCursoByAsignacion($idAsignacion);
        $estadisticas       = $this->estadistica->getRespuestas($idAsignacion, $origen);
        $alumnos            = $this->estadistica->getAlumnosByAsignacion($idAsignacion);
        $habilidadesAlumno  = $this->estadistica->gethabilidadesByAsignacion($idAsignacion, $origen);
        $preguntasRespuesta = $this->estadistica->getPreguntasRespuesta($idAsignacion, $origen);
        $habilidades        = $this->estadistica->getHabilidadesByPrueba($asignacion->prueba_id, $origen);
        $prueba             = $this->prueba->get($asignacion->prueba_id, $origen);
        $ordenPreguntas     = $this->estadistica->getOrdenPreguntas($prueba->id, $origen);

        $color = $color2 = $color3 = '';
        switch ($prueba->asignatura_id) {
            case 1: $color = '#F47921'; $color2 = '#EBCFBB'; $color3 = '#FAC49D'; break; //LENGUAJE
            case 3: $color = '#ED028A'; $color2 = '#F7CFE7'; $color3 = '#FFD5EC'; break; //MATEMATICAS
            case 4: $color = '#00B4E0'; $color2 = '#6BD1EB'; $color3 = '#A6E5F4'; break; //Historia, Geografía y Ciencias Sociales
            case 5: $color = '#56B947'; $color2 = '#9DD597'; $color3 = '#B3DFAC'; break; //Ciencias Naturales
            default: $color = '#56B947'; $color2 = '#9DD597'; $color3 = '#B3DFAC'; break;
        }

        $data = [
            'color'             => $color,
            'color2'            => $color2,
            'color3'            => $color3,
			'codigo'			=> $prueba->codigo,
            'asignatura_id'     => $prueba->asignatura_id,
            'tipo_id'           => $prueba->tipo_id,
            'num_evaluacion'    => $prueba->num,
            'curso'             => $curso->nivel.' "'.$curso->letra.'"',
            'correctas'         => 0,
            'incorrectas'       => 0,
            'omitidas'          => 0,
            'porc_correctas'    => 0,
            'porc_incorrectas'  => 0,
            'porc_omitidas'     => 0,
            'preguntas'         => [],
            'alternativas'      => [],
            'cantAlumnos'       => 0,
            'puntajeSimce'      => 0,
            'nota'              => 0,
            'alumnos'           => [],
            'habilidades'       => [],
            'respAltPreg'       => [
                'correcta'      => [],
                'omitidas'      => []
            ],
            'nivel'             => [
                'inicial'       => 0,
                'intermedio'    => 0,
                'adecuado'      => 0
            ]
        ];

        $alumnosOden = [];
        foreach ($alumnos as $alum) {
            $alumnosOden[$alum->id] = [
                'nombre' => $alum->nombre,
                'apellido' => $alum->apellido,
                'inicio' => $alum->inicio,
                'fin' => $alum->fin
            ];
        }

        if ($habilidades) {
            foreach ($habilidades as $h) {
                $data['habilidades'][] = array(
                    'id'        => $h->id,
                    'habilidad' => $h->habilidadcurricular,
                    'count'     => $h->count,
                    'porc'      => 0
                );
            }
        }

        $preguntasAlumno = array();
        if ($preguntasRespuesta)
            foreach ($preguntasRespuesta as $val) {
                $preguntasAlumno[$val->usuario_id][$val->pregunta_id] = [$val->respuesta_id, $val->correcta];
            }

        if ($estadisticas) {
            foreach ($estadisticas as $estadistica) {
                $omitidas = $prueba->preguntas - $estadistica->correctas - $estadistica->incorrectas;

                $data['correctas']      = $data['correctas'] + $estadistica->correctas;
                $data['incorrectas']    = $data['incorrectas'] + $estadistica->incorrectas;
                $data['omitidas']       = $data['omitidas'] + $omitidas;
                $data['cantAlumnos']++;

                if ($prueba->preguntas > 0) {
                    $puntajeSimce           = round(((($estadistica->correctas - $prueba->preguntas / 2)) / 3 * (450 / $prueba->preguntas) + 265), 0);

                    $porcCorrectas          = round(($estadistica->correctas * 100)/$prueba->preguntas, 1);
                    $porcIncorrectas        = round(($estadistica->incorrectas * 100)/$prueba->preguntas, 1);
                    $porcOmitidas           = round(($omitidas * 100)/$prueba->preguntas, 1);
                } else {
                    $puntajeSimce = $porcCorrectas = $porcIncorrectas = $porcOmitidas = 0;
                }

                $data['porc_correctas']     += $porcCorrectas;
                $data['porc_incorrectas']   += $porcIncorrectas;
                $data['porc_omitidas']      += $porcOmitidas;

                $data['puntajeSimce']  += $puntajeSimce;
                
                $nota = 0;
                if($porcCorrectas < 60)
                    $nota = round(5 * ($porcCorrectas*0.01) + 1, 1);
                else
                    $nota = round(7.5 * (($porcCorrectas*0.01) - 0.6) + 4, 1);

                if($nota < 2)
                    $nota = 2;

                $nivel = '';
                if ($nota <= 4) {
                    $nivel = 'INSUFICIENTE';
                    $data['nivel']['inicial']++;
                } elseif ($nota >= 4.1 && $nota <= 5.4) {
                    $nivel = 'ELEMENTAL';
                    $data['nivel']['intermedio']++;
                } else {
                    $nivel = 'ADECUADO ';
                    $data['nivel']['adecuado']++;
                }

                $data['nota'] += $nota;

                $fechaInicio = new DateTime(date('Y-m-d h:i:s', $alumnosOden[$estadistica->usuario_id]['inicio']));
                $fechaFin = new DateTime(date('Y-m-d h:i:s', $alumnosOden[$estadistica->usuario_id]['fin']));
                $diff = $fechaInicio->diff($fechaFin);
                $tiempo = ($diff->h < 10 ? "0$diff->h" : $diff->h).':'.($diff->i < 10 ? "0$diff->i" : $diff->i);

                $data['alumnos'][] = [
                    'id'                => $estadistica->usuario_id,
                    'nombre'            => $alumnosOden[$estadistica->usuario_id]['nombre'],
                    'apellido'          => $alumnosOden[$estadistica->usuario_id]['apellido'],
                    'tiempo'            => $tiempo,
                    'correctas'         => $estadistica->correctas,
                    'incorrectas'       => $estadistica->incorrectas,
                    'omitidas'          => $omitidas,
                    'porc_correctas'    => $porcCorrectas,
                    'puntajeSimce'      => $puntajeSimce,
                    'nivel'             => $nivel,
                    'nota'              => $nota,
                    'habilidades'       => $data['habilidades'],
                    'agrupacion'        => [
                        'correctas'     => [],
                        'incorrectas'   => [],
                        'omitidas'      => []
                    ]
                ];
            }
        }

        $data['puntajeSimce'] = $data['cantAlumnos'] > 0 ? round($data['puntajeSimce'] / $data['cantAlumnos'], 0) : 0;
        $data['nota'] = $data['cantAlumnos'] > 0 ? round($data['nota'] / $data['cantAlumnos'], 1) : 0;

        if ($data['cantAlumnos'] > 0) {
            $data['porc_correctas']     = round($data['porc_correctas'] / $data['cantAlumnos'], 1);
            $data['porc_incorrectas']   = round($data['porc_incorrectas'] / $data['cantAlumnos'], 1);
            $data['porc_omitidas']      = round($data['porc_omitidas'] / $data['cantAlumnos'], 1);

            $data['nivel']['inicial'] = round(($data['nivel']['inicial'] * 100) / $data['cantAlumnos'], 1);
            $data['nivel']['intermedio'] = round(($data['nivel']['intermedio'] * 100) / $data['cantAlumnos'], 1);
            $data['nivel']['adecuado'] = round(($data['nivel']['adecuado'] * 100) / $data['cantAlumnos'], 1);
        } else {
            $data['porc_correctas'] = $data['porc_incorrectas'] = $data['porc_omitidas'] =
            $data['nivel']['inicial'] = $data['nivel']['intermedio'] = $data['nivel']['adecuado'] = 0;
        }

        if ($habilidadesAlumno) {
            foreach ($habilidadesAlumno as $ha) {
                $porc = 0;
                for ($i = 0; $i < count($data['habilidades']); $i++)
                    if ($data['habilidades'][$i]['id'] == $ha->habilidadcurricular_id) {
                        $porc = $data['habilidades'][$i]['count'] > 0 ? round(($ha->correctas * 100) / $data['habilidades'][$i]['count'], 1) : 0;
                        $data['habilidades'][$i]['porc'] += $porc;
                    }

                for ($x = 0; $x < count($data['alumnos']); $x++) {
                    if ($data['alumnos'][$x]['id'] == $ha->usuario_id) {
                        for ($z = 0; $z < count($data['alumnos'][$x]['habilidades']); $z++) {
                            if ($data['alumnos'][$x]['habilidades'][$z]['id'] == $ha->habilidadcurricular_id) {
                                $data['alumnos'][$x]['habilidades'][$z] = [
                                    'id'        => $ha->habilidadcurricular_id,
                                    'habilidad' => $ha->habilidadcurricular,
                                    'porc'      => $porc
                                ];
                            }
                        }
                    }
                }
            }
        }

        $numClave = 0;
        $aux = FALSE;
        $preguntas = [];

        $preguntaClave = [];
        for ($i = 0; $i < count($ordenPreguntas); $i++) {
            if ($ordenPreguntas[$i]->pregunta_id == $aux) {
                $numClave++;
            } else {
                $aux = $ordenPreguntas[$i]->pregunta_id;
                $numClave = 1;
            }

            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['pregunta_id'] = $ordenPreguntas[$i]->pregunta_id;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['orden'] = $ordenPreguntas[$i]->orden;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['correctas'] = 0;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['incorrectas'] = 0;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['omitidas'] = 0;
            $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['alternativas'][$ordenPreguntas[$i]->respuesta_id] = [
                'letra' => $this->getLetraByNumber($numClave),
                'ocurrencia' => 0
            ];

            if ($ordenPreguntas[$i]->correcta == 't') {
                $preguntaClave[$ordenPreguntas[$i]->pregunta_id]['clave'] = $this->getLetraByNumber($numClave);

                $preguntas[] = $preguntaClave[$ordenPreguntas[$i]->pregunta_id];
            }

            if (!in_array($this->getLetraByNumber($numClave), $data['alternativas']))
                $data['alternativas'][] = $this->getLetraByNumber($numClave);

            foreach ($preguntasAlumno as $index => $pa) {
                if (array_key_exists($ordenPreguntas[$i]->pregunta_id, $preguntasAlumno[$index])) {
                    if ($pa[$ordenPreguntas[$i]->pregunta_id][1] == 't') {
                        for ($x = 0; $x < count($data['alumnos']); $x++)
                            if ($data['alumnos'][$x]['id'] == $index)
                                if (!in_array($ordenPreguntas[$i]->orden, $data['alumnos'][$x]['agrupacion']['correctas']))
                                    $data['alumnos'][$x]['agrupacion']['correctas'][] = $ordenPreguntas[$i]->orden;
                    } else {
                        for ($x = 0; $x < count($data['alumnos']); $x++)
                            if ($data['alumnos'][$x]['id'] == $index)
                                if (!in_array($ordenPreguntas[$i]->orden, $data['alumnos'][$x]['agrupacion']['incorrectas']))
                                    $data['alumnos'][$x]['agrupacion']['incorrectas'][] = $ordenPreguntas[$i]->orden;
                    }
                } else {
                    for ($x = 0; $x < count($data['alumnos']); $x++)
                        if ($data['alumnos'][$x]['id'] == $index)
                            if (!in_array($ordenPreguntas[$i]->orden, $data['alumnos'][$x]['agrupacion']['omitidas']))
                                $data['alumnos'][$x]['agrupacion']['omitidas'][] = $ordenPreguntas[$i]->orden;
                }
            }
        }

        foreach ($preguntasAlumno as $pa) {
            foreach ($pa as $key => $val) {
                $preguntaClave[$key]['alternativas'][$pa[$key][0]]['ocurrencia']++;
            }
        }

        for ($i = 0; $i < count($preguntas); $i++) {
            $data['preguntas'][] = $preguntaClave[$preguntas[$i]['pregunta_id']];
        }

        for ($i = 0; $i < count($data['alumnos']); $i++) {
            for ($x = 0; $x < count($data['alumnos'][$i]['agrupacion']['correctas']); $x++) {
                for ($z = 0; $z < count($data['preguntas']); $z++) {
                    if ($data['preguntas'][$z]['orden'] == $data['alumnos'][$i]['agrupacion']['correctas'][$x]) {
                        $data['preguntas'][$z]['correctas']++;
                    }
                }
            }
            for ($x = 0; $x < count($data['alumnos'][$i]['agrupacion']['incorrectas']); $x++) {
                for ($z = 0; $z < count($data['preguntas']); $z++) {
                    if ($data['preguntas'][$z]['orden'] == $data['alumnos'][$i]['agrupacion']['incorrectas'][$x]) {
                        $data['preguntas'][$z]['incorrectas']++;
                    }
                }
            }
            for ($x = 0; $x < count($data['alumnos'][$i]['agrupacion']['omitidas']); $x++) {
                for ($z = 0; $z < count($data['preguntas']); $z++) {
                    if ($data['preguntas'][$z]['orden'] == $data['alumnos'][$i]['agrupacion']['omitidas'][$x]) {
                        $data['preguntas'][$z]['omitidas']++;
                    }
                }
            }
        }

        for ($i = 0; $i < count($data['habilidades']); $i++) {
            $data['habilidades'][$i]['porc'] = $data['cantAlumnos'] > 0 ? round($data['habilidades'][$i]['porc'] / $data['cantAlumnos'], 1) : 0;
        }

        graphPie([
            'graphName' => 'chart_corr.jpg',
            'color' => ['#3491e0','#a7a7a7'],
            'data' => [$data['porc_correctas'], (100-$data['porc_correctas'])]
        ]);
        graphPie([
            'graphName' => 'chart_incorr.jpg',
            'color' => ['#fb3131','#a7a7a7'],
            'data' => [$data['porc_incorrectas'], (100-$data['porc_incorrectas'])]
        ]);
        graphPie([
            'graphName' => 'chart_omit.jpg',
            'color' => ['#deb41c','#a7a7a7'],
            'data' => [$data['porc_omitidas'], (100-$data['porc_omitidas'])]
        ]);

        graphPie([
            'graphName' => 'chart_ini.jpg',
            'color' => ['#3491e0','#a7a7a7'],
            'data' => [$data['nivel']['inicial'], (100-$data['nivel']['inicial'])]
        ]);
        graphPie([
            'graphName' => 'chart_int.jpg',
            'color' => ['#fb3131','#a7a7a7'],
            'data' => [$data['nivel']['intermedio'], (100-$data['nivel']['intermedio'])]
        ]);
        graphPie([
            'graphName' => 'chart_ade.jpg',
            'color' => ['#deb41c','#a7a7a7'],
            'data' => [$data['nivel']['adecuado'], (100-$data['nivel']['adecuado'])]
        ]);

        $dataPorcH = ['values' => [], 'labels' => []];
        for ($i=0; $i < count($data['habilidades']); $i++) {
            $dataPorcH['values'][] = $data['habilidades'][$i]['porc'];
            $dataPorcH['labels'][] = $data['habilidades'][$i]['habilidad'];
        }
        graphBar([
            'graphName' => 'chart_rendHabilidad.jpg',
            'label' => count($dataPorcH['labels']) > 0 ? $dataPorcH['labels'] : ['Hbilidades'],
            'data' => count($dataPorcH['values']) > 0 ? $dataPorcH['values'] : [0],
            'color' => [$color],
            'size' => ['width' => 700, 'height' => 350],
            'angle' => 20
        ]);
        $puntSimceAlum = [];
        for ($i=0; $i < count($data['alumnos']); $i++)
            $puntSimceAlum[] = $data['alumnos'][$i]['puntajeSimce'];
        graphBar([
            'graphName' => 'chart_puntSimceAlum.jpg',
            'label' => [],
            'data' => count($puntSimceAlum) > 0 ? $puntSimceAlum : [0],
            'color' => [$color],
            'size' => ['width' => 700, 'height' => 200]
        ]);
        $nivelAprendizaje = [
            $data['nivel']['inicial'],
            $data['nivel']['intermedio'],
            $data['nivel']['adecuado']
        ];
        graphBar([
            'graphName' => 'chart_nivel_aprendizaje.jpg',
            'label' => ['INICIAL', 'INTERMEDIO', 'ADECUADO'],
            'data' => $nivelAprendizaje,
            'color' => [$color],
            'size' => ['width' => 500, 'height' => 200]
        ]);

        return $data;
    }

    public function getPartialsByAsignatura($idAsignatura, $tipoEvaluacion) {
        $data = [
            'enc' => 'ENC_',
            'port' => 'PORT_',
            'pie' =>'PIE_'
        ];

        if ($tipoEvaluacion == 1) { #Simce
            $data['enc'] .= 'SIMCE_';
            $data['port'] .= 'SIMCE_';
        } elseif ($tipoEvaluacion == 2) { #Pme
            $data['enc'] .= 'PME_';
            $data['port'] .= 'PME_';
        } elseif ($tipoEvaluacion == 3) { #Unidad
            $data['enc'] .= 'UNIDAD_';
            $data['port'] .= 'UNIDAD_';
        } elseif ($tipoEvaluacion == 4) { #Cobertura
            $data['enc'] .= 'COBERTURA_';
            $data['port'] .= 'COBERTURA_';
        }

        if ($idAsignatura == 1) { #LENGUAJE Y COMUNICACION
            $data['enc'] .= 'LEN.png';
            $data['port'] .= 'LEN.png';
            $data['pie'] .= 'LEN.png';
        } elseif ($idAsignatura == 3) { #Matematica
            $data['enc'] .= 'MATE.png';
            $data['port'] .= 'MATE.png';
            $data['pie'] .= 'MATE.png';
        } elseif ($idAsignatura == 4) { #Historia, Geografía y Ciencias Sociales
            $data['enc'] .= 'HIST.png';
            $data['port'] .= 'HIST.png';
            $data['pie'] .= 'HIST.png';
        } elseif ($idAsignatura == 5) { #Ciencias Naturales
            $data['enc'] .= 'CIEN.png';
            $data['port'] .= 'CIEN.png';
            $data['pie'] .= 'CIEN.png';
        } elseif ($idAsignatura == 12) { #Biologia
            $data['enc'] .= 'BIO.png';
            $data['port'] .= 'BIO.png';
            $data['pie'] .= 'BIO.png';
        } elseif ($idAsignatura == 13) { #Fisica
            $data['enc'] .= 'FIS.png';
            $data['port'] .= 'FIS.png';
            $data['pie'] .= 'FIS.png';
        } elseif ($idAsignatura == 14) { #Quimica
            $data['enc'] .= 'QUI.png';
            $data['port'] .= 'QUI.png';
            $data['pie'] .= 'QUI.png';
        }

        return $data;
    }

    public function getPartialsByCodigo($codigo, $tipoEvaluacion) {
		$asignatura=substr($codigo, 0,3);
        $data = [
            'enc' => 'ENC_',
            'port' => 'PORT_',
            'pie' =>'PIE_'
        ];

        if ($tipoEvaluacion == 1) { #Simce
            $data['enc'] .= 'SIMCE_';
            $data['port'] .= 'SIMCE_';
        } elseif ($tipoEvaluacion == 2) { #Pme
            $data['enc'] .= 'PME_';
            $data['port'] .= 'PME_';
        } elseif ($tipoEvaluacion == 3) { #Unidad
            $data['enc'] .= 'UNIDAD_';
            $data['port'] .= 'UNIDAD_';
        } elseif ($tipoEvaluacion == 4) { #Cobertura
            $data['enc'] .= 'COBERTURA_';
            $data['port'] .= 'COBERTURA_';
        }

        if ($asignatura == 'LEN') { #LENGUAJE Y COMUNICACION
            $data['enc'] .= 'LEN.png';
            $data['port'] .= 'LEN.png';
            $data['pie'] .= 'LEN.png';
        } elseif ($asignatura == 'MAT') { #Matematica
            $data['enc'] .= 'MATE.png';
            $data['port'] .= 'MATE.png';
            $data['pie'] .= 'MATE.png';
        } elseif ($asignatura == 'HIS') { #Historia, Geografía y Ciencias Sociales
            $data['enc'] .= 'HIST.png';
            $data['port'] .= 'HIST.png';
            $data['pie'] .= 'HIST.png';
        } elseif ($asignatura == 'CIE') { #Ciencias Naturales
            $data['enc'] .= 'CIEN.png';
            $data['port'] .= 'CIEN.png';
            $data['pie'] .= 'CIEN.png';
        } elseif ($asignatura == 'BIO') { #Ciencias Naturales
            $data['enc'] .= 'BIO.png';
            $data['port'] .= 'BIO.png';
            $data['pie'] .= 'BIO.png';
        } elseif ($asignatura == 'FIS') { #Ciencias Naturales
            $data['enc'] .= 'FIS.png';
            $data['port'] .= 'FIS.png';
            $data['pie'] .= 'FIS.png';
        } elseif ($asignatura == 'QUI') { #Ciencias Naturales
            $data['enc'] .= 'QUI.png';
            $data['port'] .= 'QUI.png';
            $data['pie'] .= 'QUI.png';
        }

        return $data;
    }

    public function getLetraByNumber($num) {
        $letras = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','Ñ','O','P','Q','R','S','T','U','V','X','Y','Z'];
        return $letras[$num-1];
    }
}
