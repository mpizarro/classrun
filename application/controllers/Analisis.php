<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once("secure_area.php");

class Analisis extends Secure_area {

    public function __construct() {
        parent::__construct();

        $this->load->model('Analisis_model', 'analisis');
        $this->load->model('Usuario_model', 'usuario');
    }

    public function get() {
        $profesor = $this->input->get('profesor');
        $profesor = !isset($profesor) || $profesor == "" ? FALSE : $profesor;

        $inicio = $this->input->get('inicio');
        $inicio = !isset($inicio) || $inicio == "" ? FALSE : $inicio;

        $termino = $this->input->get('termino');
        $termino = !isset($termino) || $termino == "" ? FALSE : $termino;

        $data = [
            'css' => "<link href='".base_url('assets/modulos/analisis/css/analisis.css')."' rel='stylesheet' type='text/css'>",
            'profesores' => $this->usuario->getProfesoresByColegio($this->session->colegio_id),
            'analisis' => $this->analisis->getAnalisis($this->session->colegio_id , $profesor, $inicio, $termino)
        ];

        $this->load->view('layout/header', $data);
        $this->load->view('layout/menu');
        $this->load->view('modulos/analisis/analisis_vista');
        $this->load->view('modulos/analisis/analisis_footer');
    }
}