<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once ("secure_area.php");

class Asignaciones extends Secure_area {
    private $asignacionModel;
    private $evaluacionModel;
    private $preguntaModel;
    private $alumnoModel;
    private $respuestaModel;
    private $usuarioModel;
    private $cursoModel;
    private $pruebaModel;
    private $crearEvaluacionModel;
    private $nivelModel;

    public function __construct() {
        parent::__construct();
        $this->load->model(['Asignacion_model', 'Evaluacion_model', 'Pregunta_model', 'Alumno_model', 'Respuesta_model',
                            'Usuario_model', 'Curso_model', 'Prueba_model', 'CrearEvaluacion_model', 'Nivel_model']);
        $this->asignacionModel = new Asignacion_model();
        $this->evaluacionModel = new Evaluacion_model();
        $this->preguntaModel = new Pregunta_model();
        $this->alumnoModel = new Alumno_model();
        $this->respuestaModel = new Respuesta_model();
        $this->usuarioModel = new Usuario_model();
        $this->cursoModel = new Curso_model();
        $this->pruebaModel = new Prueba_model();
        $this->crearEvaluacionModel = new CrearEvaluacion_model();
        $this->nivelModel = new Nivel_model();
        $this->load->model('Analisis_model', 'analisis');
    }
    public function index() {

        $this->analisis->set();

        $css = '<link href="' . base_url() . 'assets/modulos/asignaciones/css/asignaciones.css" rel="stylesheet" type="text/css" />';

        $this->load->view('layout/header', array('css' => $css));
        $this->load->view('layout/menu');

        $idUsuario = $this->session->perfil_id == '7' ? FALSE : $this->session->idUser;

        $asignacionEjecutada = $this->asignacionModel->getActivaUsuario($idUsuario);
        $asignacionesFinalizadas = $this->asignacionModel->getFinalizadasUsuario($idUsuario);
        $profesores = $this->usuarioModel->getProfesoresByColegio($this->session->colegio_id);

        $this->load->view('modulos/asignaciones/asignaciones_vista');
        $this->load->view('modulos/asignaciones/asignaciones_footer', [
            'perfil' => $this->session->perfil_id,
            'data' => ['asignacionEjecutada' => $asignacionEjecutada,
                'asignacionesFinalizadas' => $asignacionesFinalizadas,
                'profesores' => $profesores
            ]
        ]);
    }

    public function personal()
    {
        $this->analisis->set();

        $idAsignatura = $this->input->get('asignatura');
        $idAsignatura = !isset($idAsignatura) || $idAsignatura == "" ? FALSE : $idAsignatura;

        $idNivel = $this->input->get('nivel');
        $idNivel = !isset($idNivel) || $idNivel == "" ? FALSE : $idNivel;

        $idTipo = $this->input->get('tipo');
        $idTipo = !isset($idTipo) || $idTipo == "" ? FALSE : $idTipo;

        $idUsuario = $this->session->perfil_id == '7' ? FALSE : $this->session->idUser;

        $asignacionEjecutada = $this->asignacionModel->getAsignacionesPersonal(1, $idUsuario, $idAsignatura, $idNivel, $idTipo);
        $asignacionesFinalizadas = $this->asignacionModel->getAsignacionesPersonal(3, $idUsuario, $idAsignatura, $idNivel, $idTipo);
        $arrCursos = [];
        if ($cursos = $this->cursoModel->getCursosConAlumnosByColegio($this->session->colegio_id))
            foreach ($cursos as $curso)
                $arrCursos[] = $curso->curso_id;

        $data = [
            'css' => '<link href="' . base_url() . 'assets/modulos/asignaciones/css/asignaciones_personal.css" rel="stylesheet" type="text/css" />',
            'perfil' => $this->session->perfil_id,
            'asignacionEjecutada' => $asignacionEjecutada ? $asignacionEjecutada : [],
            'asignacionesFinalizadas' => $asignacionesFinalizadas ? $asignacionesFinalizadas : [],
            'tipoPruebas' => $this->crearEvaluacionModel->getTipoPruebas($this->session->colegio_id),
            'asignaturas' => $this->crearEvaluacionModel->getAsignaturas(),
            'nivelesColegio' => $this->nivelModel->getNivelesByCursos($arrCursos)
        ];

        $this->load->view('layout/header', $data);
        $this->load->view('layout/menu');
        $this->load->view('modulos/asignaciones/asignaciones_personal_vista');
        $this->load->view('modulos/asignaciones/asignaciones_personal_footer');
    }
    public function finalizarAsignacion($idAsignacion) {
        $this->db->trans_begin();
        $idUsuario = $this->session->userdata['idUser'];

        $this->asignacionModel->setId($idAsignacion);
        $this->asignacionModel->finalizarAsignacion($idAsignacion);
        $this->asignacionModel->finalizarAsignacionesUsuario($idAsignacion);
        $this->asignacionModel->setTiempoFinAll($idAsignacion, time());

        if($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }

        echo json_encode(array('status' => $this->db->trans_status()));
    }

    public function ejecutarAsignacion($idAsignacion) {
        $idUsuario = $this->session->userdata['idUser'];
        $this->db->trans_begin();

        $this->asignacionModel->setId(FALSE);
        $this->asignacionModel->finalizarAsignacion($idUsuario);
        $this->asignacionModel->setId($idAsignacion);

        $this->asignacionModel->ejecutarAsignacion($idAsignacion);
        $cursoAsignacion = $this->asignacionModel->getAlumnosAsignacion($idAsignacion);
        if($cursoAsignacion){
            foreach($cursoAsignacion as $alumno){
                $this->asignacionModel->renovarAsignacionesUsuario($alumno->id, $idAsignacion);
            }
        }
        if($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
        
        echo json_encode(array('status' => $this->db->trans_status()));
    }
    public function getPlanillaTabulacion($idAsignacion){
        $data = array('status' => false);
        $evaluacion = $this->evaluacionModel->getEvaluacionByAsignacion($idAsignacion);
        $dataEvaluacion = $this->getDataEvaluacion($evaluacion[0]->prueba_id);
        $alumnos = $this->asignacionModel->getAlumnosAsignacion($idAsignacion);

        if($alumnos !== false){
            foreach ($alumnos as $alumno) {
                $alumno->{'respuestas'} = $this->asignacionModel->getRespuestasAlumnoAsignacion($idAsignacion, $alumno->id, 'CLASSRUN');
            }
            $data['alumnos'] = $alumnos;
            $data['dataEvaluacion'] = $dataEvaluacion;
            $data['idAsignacion'] = $idAsignacion;
            $data['status'] = true;
        }

        echo json_encode($data);
    }
    public function getCeldaPlanillaTabulacion($idAsignacion, $idPregunta, $letra, $origen="CLASSRUN") {
        $class = 'incorrecta';
        $letraCorrecta = '';

        $count = 0;
        $indexCorrecta = FALSE;

        if ($respuestas = $this->asignacionModel->getCorrectaIncorrectaByPreguntaByAsignacion($idAsignacion, $idPregunta, $origen))
            foreach ($respuestas as $resp) {
                $count++;
                if ($resp->correcta == 't') {
                    $letraCorrecta = strtoupper($this->numberToLetter($count));
                }
            }

        if ($letraCorrecta == $letra)
            $class = 'correcta';

        return $class;
    }
    public function getLetraByPregunta($idAsignacion, $idPregunta, $idRespuesta, $origen) {
        
        $letra = '';
        $count = 0;
        
        if ($respuestas = $this->asignacionModel->getCorrectaIncorrectaByPreguntaByAsignacion($idAsignacion, $idPregunta, $origen))
            foreach ($respuestas as $resp) {
                $count++;
                if ($resp->respuesta_id == $idRespuesta)
                    $letra = strtoupper($this->numberToLetter($count));
            }

        return $letra;
    }
	private function getDataEvaluacion($idEvaluacion)
	{
		$preguntas = $this->preguntaModel->getPreguntasEvaluacion($idEvaluacion);
		if($preguntas)
		{
			foreach ($preguntas as $pregunta)
			{
                unset($pregunta->pregunta);
				$pregunta->{'alternativas'} = $this->evaluacionModel->getAlternativasPregunta($pregunta->id);

				foreach($pregunta->alternativas as $alternativa)
				{
                    unset($alternativa->respuesta);
				}
			}
		}
		return $preguntas;
    }
    private function getActivasUser($idUsuario)
	{
		return $this->evaluacionModel->getActivasUser($idUsuario) !== FALSE ? TRUE : FALSE;
    }
    public function setAlternativa() {
        $idRespuesta = null;
        $response = FALSE;

        $idAlumno = $this->input->post('id_alumno');
        $idPregunta = $this->input->post('id_pregunta');
        $idAsignacion = $this->input->post('id_asignacion');
        $letra = strtoupper($this->input->post('respuesta'));
        $origen = $this->input->post('origen') == 'PERSONAL' ? 'PERSONAL': 'CLASSRUN';

        
        
        if ($letra != null) 
        {
            if ($respuestas = $this->respuestaModel->getAlternativasPregunta($idPregunta,$origen)) 
            {
                $num = 1;
                foreach ($respuestas as $resp) {
                    if ($letra == $this->numberToLetter($num))
                        $idRespuesta = $resp->id;
                    $num++;
                }
            }

            if ($idRespuesta) {
                if ($usuarioAsignacion = $this->asignacionModel->getUsuarioAsignacionByUsuarioByAsignacion($idAsignacion, $idAlumno)) {
                    if ($this->respuestaModel->getHojaRespuestaByUsuarioAsignacion($idAlumno, $idPregunta, $idAsignacion))
                        $response = $this->respuestaModel->setRespuesta($idAlumno, $idPregunta, $idRespuesta, $idAsignacion);
                    else
                        $response = $this->respuestaModel->saveRespuesta($idAlumno, $idPregunta, $idRespuesta, $idAsignacion);
                }
            }
        }
        else 
            $response = $this->respuestaModel->deleteResouesta($idAlumno, $idPregunta, $idAsignacion);
        
        echo json_encode(['response' => $response, 'class' => $this->getCeldaPlanillaTabulacion($idAsignacion, $idPregunta, $letra, $origen)]);
    }

    public function eliminarHojaRespuesta() {
        $idAsignacion = $this->input->post('idAsignacion');
        $response['response'] = $this->asignacionModel->eliminarHojaRespuesta($idAsignacion);
        echo json_encode($response);
    }
    public function numberToLetter($num) {
        $letters = ['A','B','C','D','E','F','G','H','I','J','K','L'];
        return $letters[$num-1];
    }
    public function renovarAsignacionesUsuario() {
        $idAlumno = $this->input->post('id_alumno');
        $idAsignacion = $this->input->post('id_asignacion');
        $response['response'] = $this->asignacionModel->renovarAsignacionesUsuario($idAlumno, $idAsignacion);
        echo json_encode($response);
    }
    public function deshabilitarAsignacionesUsuario() {
        $idAlumno = $this->input->post('id_alumno');
        $idAsignacion = $this->input->post('id_asignacion');
        $response['response'] = $this->asignacionModel->deshabilitarAsignacionesUsuario($idAlumno, $idAsignacion);
        echo json_encode($response);
    }
    public function setAsignacionProfesor() {
        $idProfesor = $this->input->post('idProfesor');
        $idAsignacion = $this->input->post('idAsignacion');

        $response = $this->asignacionModel->setAsignacionProfesor($idProfesor, $idAsignacion);

        echo json_encode(['response' => $response]);
    }

    public function getDataTabulacion()
    {
        
        $idAsignacion = $this->input->post('idAsignacion');
        $origen = $this->input->post('origen');
        
        $alumnos = null;
        $dataRespuesta = null;
        $respuestasByAlumno = FALSE;
        $data = FALSE;

        if ($prueba = $this->pruebaModel->getPruebaByAsignacion($idAsignacion, $origen))
            if ($evaluacion = $this->evaluacionModel->getEvaluacion($prueba->id, $origen, $idAsignacion)) {
                $data['asignatura'] = $evaluacion->asignatura;
                $data['tipo'] = $evaluacion->tipo;
                $data['origen'] = $origen;
            }

        if ($curso = $this->cursoModel->getCursoByAsignacion($idAsignacion)) {
            $data['curso'] = $curso->curso;
            $data['letra'] = $curso->letra;
            if ($alumnos = $this->cursoModel->getAlumnosCursoById($curso->id))
                foreach ($alumnos as $alumno)
                    $respuestasByAlumno[$alumno->id] = $this->asignacionModel->getRespuestasAlumnoAsignacion($idAsignacion, $alumno->id, $origen);
        }

        if ($respuestasByAlumno)
            foreach ($respuestasByAlumno as $key => $respuestas) {
                if ($respuestas)
                    foreach ($respuestas as $respuesta) {
                        $letra = $this->getLetraByPregunta($idAsignacion, $respuesta->pregunta_id, $respuesta->respuesta_id, $origen);
                        $dataRespuesta[] = [
                            'letra' => $respuesta->letra,
                            'alumno_id' => $key,
                            'pregunta_id' => $respuesta->pregunta_id,
                            'correcta' => $respuesta->correcta,
                            'class' => $respuesta->correcta == 't' ? 'correcta' : 'incorrecta'
                        ];
                    }
            }

        $preguntas = $this->pruebaModel->getPruebaPreguntaByAsignacion($idAsignacion, $origen);
        
        echo json_encode(['alumnos' => $alumnos, 'preguntas' => $preguntas, 'dataRespuesta' => $dataRespuesta, 'data' => $data]);
    }

    public function deleteAsignacion()
    {
        $data['success'] = false;

        $asignacion = $this->input->post('asignacion');
        
        if( $this->asignacionModel->delete($asignacion) )
            $data['success'] = 'success';
    
        echo json_encode($data);
    }
}
