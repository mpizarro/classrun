<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once("secure_area.php");

class Administrador extends Secure_area
{
    private $evaluacionModel;
    private $nivelModel;
    private $asignaturaModel;
    private $preguntaModel;
    private $respuestaModel;
    private $pruebaModel;
    private $UsuarioModel;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('html');

        // @TODO corregir para que solo administrador pueda entrar a esta sección
        // if ($this->session->userdata('perfil_id') != 1) {
        //     redirect('login');
        // }

        $this->load->model('Evaluacion_model');
        $this->load->model('Nivel_model');
        $this->load->model('Asignatura_model');
        $this->load->model('Pregunta_model');
        $this->load->model('Respuesta_model');
        $this->load->model('Prueba_model');
        $this->load->model('Colegio_model');
        $this->load->model('Usuario_model');

        $this->evaluacionModel = new Prueba_model();
        $this->evaluacionModel = new Evaluacion_model();
        $this->nivelModel = new Nivel_model();
        $this->asignaturaModel = new Asignatura_Model();
        $this->preguntaModel = new Pregunta_model();
        $this->respuestaModel = new Respuesta_model();
        $this->usuarioModel = new Usuario_model();
    }

    public function test()
    {
        $docentes = $this->Colegio_model->getCantidadDocentes();
        
        print_r($docentes);
    }

    public function colegios()
    {
        $colegios = $this->Colegio_model->getAll();
        $docentes = $this->Colegio_model->getCantidadDocentes();

        $data['data'] = buildTablaColegios($colegios, site_url(), $docentes);

        $this->load->view('layout/header');
        $this->load->view('layout/admin/menu');
        $this->load->view('layout/admin/body', $data);
        $this->load->view('layout/admin/footer');
    }

    public function cursos($rbd)
    {
        $cursos  = $this->Colegio_model->getCursos($rbd);
        $alumnos = $this->Colegio_model->getCantidadAlumnos($rbd);

        $data['data'] = buildTablaCursos($cursos, site_url(), $alumnos);
        
        $this->load->view('layout/header');
        $this->load->view('layout/admin/menu');
        $this->load->view('layout/admin/body', $data);
        $this->load->view('layout/admin/footer');
    }

    public function alumnos($colegio, $curso)
    {
        $alumnos = $this->Colegio_model->getAlumnos($colegio, $curso);
        $data['data'] = buildTablaAlumnos($alumnos);
        
        $this->load->view('layout/header');
        $this->load->view('layout/admin/menu');
        $this->load->view('layout/admin/body', $data);
        $this->load->view('layout/admin/footer');
    }

    public function nuevoColegio()
    {
    
        $data = array(  'nombre'    => $_POST['colegioNombre'],
                        'rbd'       => $_POST['colegioRbd'],
                        'director'  => $_POST['colegioDirector'],
                        'direccion' => $_POST['colegioDireccion'],
                        'telefono'  => $_POST['colegioTelefono'],
                        'email'     => $_POST['colegioEmail'],
                        'comuna_id' =>196
                        
                    );
        
        if($this->Colegio_model->nuevoColegio($data))
        {
            echo "<script>alert('Colegio Creado!');</script>";
            echo "<script>location.href='".site_url()."/Administrador/colegios'</script>";
        
        }
        else
        {
            echo "<script>alert('Error! Preguntar a Manuel xD');</script>";
            echo "<script>location.href='".site_url()."/Administrador/colegios'</script>";
        }
    
    }

    public function formatNivel($format)
    {
        $nivel_id = 0;

        switch ($format) {
            case "1M":
                $nivel_id = 9; 
                break;
            case "2M":
                $nivel_id = 10; 
                break;
            case "3M":
                $nivel_id = 11; 
                break;
            case "4M":
                $nivel_id = 12; 
                break;
            case "Prekinder":
                $nivel_id = 13; 
                break;
            case "Kinder":
                $nivel_id = 14; 
                break;
            default:
                # code...
                break;
        }

        return $nivel_id;
    }

    public function idCurso($rbd, $tag)
    {
        $cursos = $this->Colegio_model->getCursos($rbd);

        foreach ($cursos as $curso)
        {
            if($curso->nivel_id > 12)
                {
                    if($curso->nivel_id == 13)
                        $arrayCursos['Prekinder'] = $curso->id;
                    if($curso->nivel_id == 14)
                        $arrayCursos['Kinder'] = $curso->id;
                }
            else
                $arrayCursos[$curso->nivel_id." ".$curso->letra] = $curso->id;  
        }
        
        if(isset($arrayCursos[$tag]))
            return $arrayCursos[$tag];
        else
            return false;
        
    }

    public function docentesCarga($colegio, $rbd)
    {
        $csv = $_FILES['file']['tmp_name'];
        $handle = fopen($csv,"r");
        
        $fila = 0;
        $data = false;
        $error = false;

        while (($row = fgetcsv($handle, 10000, ",")) != FALSE) //get row vales
        {
                        
            if($fila > 0)
            {
                                
                if(strlen($row[0]) > 7 && $row[0] != 'Prekinder')
                {
                    $data[] = array(
                        'username' => $row[0], 
                        'password' => substr($row[0],-7,-2),
                        'nombre' => $row[1],
                        'apellido' => $row[2]." ".$row[3],
                        'email' => $row[4],
                        'perfil_id' => 2,
                        'colegio_id' => $colegio
                        );
                }
                else
                {
                    //echo $rbd.",".$row[0];
                    //echo $this->idCurso($rbd, $row[0]);
                    /*
                    $cursos = $this->Colegio_model->getCursos($rbd);

                    foreach ($cursos as $curso) 
                        $arrayCursos[$curso->nivel_id." ".$curso->letra] = $curso->id;  
                    */
                    
                    //$temp = $row[0];

                    /*
                    if($temp[1] == 'M')
                    {
                        if(isset($arrayCursos[$this->formatNivel($temp[0].$temp[1])." ".$temp[3]]))
                            $cursoId = $arrayCursos[$this->formatNivel($temp[0].$temp[1])." ".$temp[3]];
                        else
                            $error = "1. No está creado el curso:".$temp[0].$temp[1]." ".$temp[3];
                    }
                    */
                    if($this->idCurso($rbd, $row[0]))
                        $cursoId = $this->idCurso($rbd, $row[0]);
                    else
                        $error = "No está creado el curso:".$row[0];
                    

                    if(!$error)
                        $data[] = array(
                            'username' => $row[1], 
                            'password' => '',
                            'nombre' => $row[4],
                            'apellido' => $row[2]." ".$row[3],
                            'email' => '',
                            'perfil_id' => 3,
                            'colegio_id' => $colegio,
                            'curso_id' => $cursoId
                            );
                }
                
            }
            
            $fila++;
        }

        if($error)
            echo $error;
        else
        {
            //print_r($data);

            if($this->usuarioModel->insertMasivo($data))
            {
                echo "<script>alert('Importación Exitosa!');</script>";
                echo "<script>location.href='".site_url()."/Administrador/colegios'</script>";
                echo "<script>history.back();</script>";
            }
        
        }
      
    }

    public function evaluaciones()
    {
        $this->load->view('layout/header');
        $this->load->view('layout/admin/menu');

        $evaluaciones = $this->evaluacionModel->getEvaluaciones();
        $data['tablaEvaluaciones'] = buildTablaEvaluaciones($evaluaciones);
        $niveles = $this->nivelModel->getNiveles();
        $data['selectNiveles'] = buildSelectNiveles($niveles);
        $asignaturas = $this->asignaturaModel->getAsignaturas();
        $data['selectAsignaturas'] = buildSelectAsignaturas($asignaturas);
        $tipos = $this->evaluacionModel->getTipo();
        $data['selectTipos'] = buildSelectTiposEvaluacion($tipos);
        $data['titulo'] = "Evaluaciones";

        $this->load->view('modulos/evaluaciones/admin/evaluaciones_vista', $data);
        $this->load->view('modulos/evaluaciones/admin/evaluaciones_footer');
    }
    public function preguntas()
    {
        $css = '<link href="'.base_url().'assets/plugins/bower_components/footable/css/footable.core.css" rel="stylesheet">
                <link href="'.base_url().'assets/modulos/preguntas/css/preguntas.css" rel="stylesheet">';

        $data['idEvaluacion'] = $this->input->get('idEvaluacion');
        $data['codigo'] = $this->input->get('codigo');

        $prueba                 = $this->evaluacionModel->getEvaluacion($data['idEvaluacion']);
        $data['nivel_id']       = $prueba->nivel_id;
        $data['asignatura_id']  = $prueba->asignatura_id;

        $this->load->view('layout/header', array('css' => $css));
        $this->load->view('layout/admin/menu');

        $dataPreguntas = array();
        $preguntas = $this->preguntaModel->getPreguntasEvaluacion($data['idEvaluacion']);

        $dataAlternativas = array();
        if ($alternativas = $this->respuestaModel->getAlternativasEvaluacion($data['idEvaluacion'])) {
            foreach ($alternativas as $key => $alt) {
                $dataAlternativas[$alt->pregunta_id][] = array(
                    'id' => $alt->id,
                    'respuesta' => $alt->respuesta,
                    'pregunta_id' => $alt->pregunta_id,
                    'correcta' => $alt->correcta
                );
            }
        }
        $data['tablaPreguntas'] = buildTablaPreguntas($preguntas, $dataAlternativas, $prueba);

        $script = '<script src="'. base_url() .'assets/modulos/preguntas/js/preguntas.js"></script>';

        $this->load->view('modulos/preguntas/admin/preguntas_vista', $data);
        $this->load->view('modulos/preguntas/admin/preguntas_footer', array('script' => $script));
    }

    public function selectOas()
    {
        $oas = $this->evaluacionModel->getOasByUnidad($this->input->post('unidad'));
        echo json_encode(array('success'=>'success', 'data' => buildSelectOas($oas)));
    }

    public function selectIes()
    {
        $ies = $this->evaluacionModel->getIesByOa($this->input->post('oa'));
        echo json_encode(array('success'=>'success', 'data' => buildSelectIes($ies)));
    }

    public function pregunta()
    {
        $data['pregunta'] = array();
        $data['idEvaluacion'] = $this->input->get('idEvaluacion');
        $data['codigo'] = $this->input->get('codigo');
        $data['idPregunta'] = $this->input->get('idPregunta');
        $data['nivel_id'] = $this->input->get('nivel_id');
        $this->load->view('layout/header');
        $this->load->view('layout/admin/menu');

        $taxonomias = $this->evaluacionModel->getTaxonomias();
        $ejes = $this->evaluacionModel->getEjes();
        $aprendizajes = $this->evaluacionModel->getHabilidadCurricular();

        $unidades = $this->evaluacionModel->getUnidadesByNivelAsignatura($this->input->get('nivel_id'), $this->input->get('asignatura_id'));


        if ($data['idPregunta']) {
            if ($pregunta = $this->preguntaModel->getPreguntaId($data['idPregunta'])) {
                $data['pregunta'] = json_encode(array(
                    'taxonimia'     => $pregunta->taxonomia_id ? $pregunta->taxonomia_id: "",
                    'eje'           => $pregunta->ejetematico_id,
                    'aprendizaje'   => $pregunta->habilidadcurricular_id,
                    'pregunta'      => $pregunta->pregunta
                ));
            }

            $data['selectTaxonomias'] = buildSelectTaxonomias($taxonomias);
            $data['selectEjes'] = buildSelectEjes($ejes);
            $data['selectAprendizajes'] = buildSelectAprendizajes($aprendizajes);
            $data['selectUnidades'] = buildSelectUnidades($unidades, (isset($pregunta->unidad_id))?$pregunta->unidad_id:FALSE);
            if(isset($pregunta->unidad_id))
            {
                $oas = $this->evaluacionModel->getOasByUnidad($pregunta->unidad_id);
                $data['selectOas'] = buildSelectOas($oas, (isset($pregunta->oa_id))?$pregunta->oa_id:FALSE);

                $ies = $this->evaluacionModel->getIesByOa($pregunta->oa_id);
                $data['selectIes'] = buildSelectIes($ies, (isset($pregunta->ie_id))?$pregunta->ie_id:FALSE);
            }
        }
        else
        {
            $data['selectTaxonomias'] = buildSelectTaxonomias($taxonomias);
            $data['selectEjes'] = buildSelectEjes($ejes);
            $data['selectAprendizajes'] = buildSelectAprendizajes($aprendizajes);
            $data['selectUnidades'] = buildSelectUnidades($unidades, (isset($pregunta->unidad_id))?$pregunta->unidad_id:FALSE);
            //$data['selectOas'] = buildSelectOas($oas, (isset($pregunta->oa_id))?$pregunta->oa_id:FALSE);
        }

        $script = '<script src="'. base_url() .'assets/modulos/preguntas/js/nuevaPregunta.js"></script>';

        $this->load->view('modulos/preguntas/admin/nuevaPregunta_vista', $data);
        $this->load->view('modulos/preguntas/admin/preguntas_footer', array('script' => $script));
    }

    public function alternativa()
    {
        $css = '<link href="'.base_url().'assets/plugins/bower_components/switchery/dist/switchery.css" rel="stylesheet" />';
        $data['idPregunta'] = $this->input->get('idPregunta');
        $data['idEvaluacion'] = $this->input->get('idEvaluacion');
        $data['codigo'] = $this->input->get('codigo');
        $data['idAlternativa'] = $this->input->get('idAlternativa');

        $this->load->view('layout/header', array('css' => $css));
        $this->load->view('layout/admin/menu');

        $data['alternativa'] = array();
        if ($data['idAlternativa']) {
            if ($alternativa = $this->respuestaModel->getAlternativaId($data['idAlternativa'])) {
                $data['alternativa'] = array(
                'contenido' => $alternativa->respuesta,
                'correcta' => $alternativa->correcta == "t" ? "true" : "false"
            );
            }
        }

        $this->load->view('modulos/alternativas/admin/nuevaAlternativa_vista', $data);
        $this->load->view('modulos/alternativas/admin/alternativas_footer');
    }

    public function verEvaluacion($idEvaluacion, $origen='classrun', $id_asignacion = null)
    {
        $color = $this->ColorPrueba($idEvaluacion, $origen, $id_asignacion);
        $css = '<link href="' . base_url() . 'assets/modulos/evaluaciones/css/verEvaluacion.css" rel="stylesheet" type="text/css" />' .
        '<link href="' . base_url() . 'assets/modulos/hacerEvaluacion/css/hacerEvaluacion.css" rel="stylesheet" type="text/css" />';

        $style['css'] = $css;
        $style['color'] = $color['color'];
        $style['header'] = $color['header'];
        $style['nombre'] = $color['nombre'];
        $style['tipo'] = $color['tipo'];
        $style['img1'] = '<img src="'.base_url().'/assets/images/modal_estudiante1.png" style="position: absolute;right: 0px;left: -187px;top: -134px;bottom: 0px;" >';
        $style['img2'] = '<img src="'.base_url().'/assets/images/modal_estudiante2.png" style="position: absolute;right: 0px;left: 565px;bottom: -46px;">';
        $style['menu'] = $color['menu'];
        $data['idEvaluacion'] = $idEvaluacion;
        $this->load->view('modulos/evaluaciones/evaluacion_header', $style);

        $data['preguntas'] = $this->getDataEvaluacion($idEvaluacion, $origen);
        $data['respuestas'] = array();
        $data['idEvaluacion'] = $idEvaluacion;

        
        $this->load->view('modulos/evaluaciones/verEvaluacion_vista', $style);
        $this->load->view('modulos/evaluaciones/verEvaluacion_footer', $data);
    }

    public function verEvaluacionModal($idEvaluacion, $origen='AEDUC', $id_asignacion = null)
    {
        $color = $this->ColorPrueba($idEvaluacion, $origen, $id_asignacion);
        $css = '<link href="' . base_url() . 'assets/modulos/evaluaciones/css/verEvaluacion.css" rel="stylesheet" type="text/css" />' .
        '<link href="' . base_url() . 'assets/modulos/hacerEvaluacion/css/hacerEvaluacion.css" rel="stylesheet" type="text/css" />';

        $style['css'] = $css;
        $style['color'] = $color['color'];
        $style['header'] = $color['header'];
        $style['nombre'] = $color['nombre'];
        $style['tipo'] = $color['tipo'];
        $style['img1'] = '<img src="'.base_url().'/assets/images/modal_estudiante1.png" style="position: absolute;right: 0px;left: -187px;top: -134px;bottom: 0px;" >';
        $style['img2'] = '<img src="'.base_url().'/assets/images/modal_estudiante2.png" style="position: absolute;right: 0px;left: 565px;bottom: -46px;">';
        $style['menu'] = $color['menu'];
        $data['idEvaluacion'] = $idEvaluacion;
        $this->load->view('modulos/evaluaciones/evaluacion_header', $style);

        $data['preguntas'] = $this->getDataEvaluacion($idEvaluacion, $origen);
        $data['respuestas'] = array();
        $data['idEvaluacion'] = $idEvaluacion;

        
        $this->load->view('modulos/evaluaciones/verEvaluacion_vista', $style);
        $this->load->view('modulos/evaluaciones/verEvaluacion_footer', $data);
    }

    public function verEvaluacionImprime($idEvaluacion, $origen='classrun', $id_asignacion = null)
    {
        if($origen == 'classrun')
            $origen = 'CLASSRUN';
            
        $color = $this->ColorPrueba($idEvaluacion, $origen, $id_asignacion);
        $css = '<link href="' . base_url() . 'assets/modulos/evaluaciones/css/verEvaluacion.css" rel="stylesheet" type="text/css" />' .
        '<link href="' . base_url() . 'assets/modulos/hacerEvaluacion/css/hacerEvaluacion.css" rel="stylesheet" type="text/css" />';

        $style['css'] = $css;
        $style['color'] = $color['color'];
        $style['header'] = $color['header'];
        $style['nombre'] = $color['nombre'];
        $style['tipo'] = $color['tipo'];
        $style['img1'] = '<img src="'.base_url().'/assets/images/modal_estudiante1.png" style="position: absolute;right: 0px;left: -187px;top: -134px;bottom: 0px;" >';
        $style['img2'] = '<img src="'.base_url().'/assets/images/modal_estudiante2.png" style="position: absolute;right: 0px;left: 565px;bottom: -46px;">';
        $style['menu'] = $color['menu'];
        $data['idEvaluacion'] = $idEvaluacion;
        $style['imprime'] = 'ok';
        $this->load->view('modulos/evaluaciones/evaluacion_header', $style);

        $data['preguntas'] = $this->getDataEvaluacion($idEvaluacion, $origen);
        $data['respuestas'] = array();
        $data['idEvaluacion'] = $idEvaluacion;

        
        $this->load->view('modulos/evaluaciones/verEvaluacion_vista', $style);
        $this->load->view('modulos/evaluaciones/verEvaluacion_footer', $data);
    }

    public function ColorPrueba($idEvaluacion,$origen="classrun",$id_asignacion=null)
    {
        
        $nivel = 1;

        if ($id_asignacion)
            $tipoEvaluacion = $this->evaluacionModel->getEvaluacion($idEvaluacion,$origen,$id_asignacion);
        else 
            $tipoEvaluacion = $this->evaluacionModel->getEvaluacionWithoutAsignacion($idEvaluacion,$origen);
        
        $asignatura = '';

        $alumno = $this->evaluacionModel->getUsuario($this->session->userdata['idUser']);
        
                
        if($tipoEvaluacion)
            switch ($tipoEvaluacion->asignatura_id) 
            {
                case 1: $asignatura = 'leng.png'; break;
                case 3: $asignatura = 'mat.png'; break;
                case 4: $asignatura = 'hist.png'; break;
                case 12: $asignatura = 'bio.png'; break;
                case 13: $asignatura = 'fisi.png'; break;
                case 14: $asignatura = 'qui.png'; break;
                case 5: $asignatura = 'cien.png'; break;
            }

        if($tipoEvaluacion)
            if ($tipoEvaluacion->nivel_id > 8) 
                $nivel = 2;
            else
                $nivel = 1;
        
        if($tipoEvaluacion)
            $data['color']  = $tipoEvaluacion->color;
        else
            $data['color'] = "";

        $data['header']  = '';
        $data['menu'] = '';

        $data['nombre'] = $alumno[0]->nombre;
        $data['tipo'] = $nivel;
        
        return $data;
    }

    private function getDataEvaluacion($idEvaluacion, $origen)
    {
        $preguntas = $this->evaluacionModel->getPreguntasEvaluacion($idEvaluacion, $origen);

        if ($preguntas) {
            foreach ($preguntas as $pregunta) {
                $pregunta->pregunta = trim(preg_replace('/(\r\n)|\n|\r/', '<br/>', $pregunta->pregunta));
                $pregunta->pregunta = str_replace('"', '\\"', $pregunta->pregunta);

                $pregunta->{'alternativas'} = $this->evaluacionModel->getAlternativasPregunta($pregunta->id, $origen);
                
                if ($pregunta->alternativas) 
                {
                    foreach ($pregunta->alternativas as $alternativa) 
                    {
                        $alternativa->respuesta = trim(preg_replace('/(\r\n)|\n|\r/', '<br/>', $alternativa->respuesta));
                        $alternativa->respuesta = str_replace('"', '\\"', $alternativa->respuesta);
                    }
                }
            }
        }
        return $preguntas;
    }
/*
    public function alumnoscarga($curso)
    {
        $file = fopen('1.csv', 'r');

        $datos = fgetcsv($file, 0, ",");

        while (($datos = fgetcsv($file, 0, ",")) !== false) {
            echo "INSERT INTO usuario(username, password, nombre, perfil_id, curso_id) VALUES('".$datos[0]."', '', '".$datos[1]."', 3, $curso);";
            echo "INSERT INTO usuario_has_brokers(usuario_id, brokers_id) VALUES((SELECT id FROM usuario WHERE username='$datos[0]'),2);";
        }
    }
    */
}
