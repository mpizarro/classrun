<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once("secure_area.php");

class Alumno extends Secure_area
{
    private $alumnoModel;
    private $evaluacionModel;
    private $asignacionModel;
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('perfil_id') != 3) {
            redirect(base_url());
            //redirect('login');
        }

        $this->load->model(array('Alumno_model', 'Evaluacion_model'));
        $this->load->model('Curso_model');
        $this->load->model('Asignacion_model');
        $this->alumnoModel = new Alumno_model();
        $this->evaluacionModel = new Evaluacion_model();
        $this->asignacionModel = new Asignacion_model();
    }
    public function index()
    {
        $evaluacionesDisponibles = $this->getEvaluacionesDisponibles();
        $header['left'] = site_url();

        $this->load->view('modulos/alumno/alumno_header', $header);

        $this->load->view('modulos/alumno/alumno_vista', array('evaluaciones' => $evaluacionesDisponibles));
        $this->load->view('modulos/alumno/alumno_footer');
    }
    public function hacerEvaluacion($idEvaluacion, $origen, $asignacion_id)
    {
        if ($this->alumnoModel->checkearSiEsRealizable($idEvaluacion, false, true, $asignacion_id)) {
            $color = $this->ColorPrueba($idEvaluacion, $origen, $asignacion_id);
            $css = '<link href="' . base_url() . 'assets/modulos/evaluaciones/css/verEvaluacion.css" rel="stylesheet" type="text/css" />' .
            '<link href="' . base_url() . 'assets/modulos/hacerEvaluacion/css/hacerEvaluacion.css" rel="stylesheet" type="text/css" />';

            $style['css'] = $css;
            $style['color'] = $color['color'];
            $style['header'] = $color['header'];
            $style['nombre'] = $color['nombre'];
            $style['tipo'] = $color['tipo'];
            $style['img1'] = '';
            $style['img2'] = '';
            $style['menu'] = $color['menu'];
            $data['idEvaluacion'] = $idEvaluacion;

            $usuarioHasAsignacion = $this->asignacionModel->getUsuarioHasAsignacionByEvaluacionByUsuario($idEvaluacion, $this->session->idUser, $origen, $asignacion_id);

            $this->load->view('modulos/hacerEvaluacion/hacerEvaluacion_header', $style);
            $this->load->view('modulos/hacerEvaluacion/hacerEvaluacion_menu');

            $data['preguntas'] = $this->getDataEvaluacion($idEvaluacion, $origen);
            $data['respuestas'] = $this->getRespuestas($asignacion_id);
            $data['idEvaluacion'] = $idEvaluacion;
            $data['asignacion_id'] = $asignacion_id;
            $data['origen'] = $origen;
            $data['usuarioHasAsignacion_id'] = $usuarioHasAsignacion->id;

            $this->load->view('modulos/hacerEvaluacion/hacerEvaluacion_vista');
            $this->load->view('modulos/hacerEvaluacion/hacerEvaluacion_footer', $data);
        } else {
            redirect('alumno');
        }
    }
    private function getDataEvaluacion($idEvaluacion, $origen)
    {
        $preguntas = $this->evaluacionModel->getPreguntasEvaluacion($idEvaluacion, $origen);

        if ($preguntas) {
            foreach ($preguntas as $pregunta) {
                $pregunta->pregunta = trim(preg_replace('/(\r\n)|\n|\r/', '<br/>', $pregunta->pregunta));
                $pregunta->pregunta = str_replace('"', '\\"', $pregunta->pregunta);

                $pregunta->{'alternativas'} = $this->evaluacionModel->getAlternativasPregunta($pregunta->id, $origen);

                foreach ($pregunta->alternativas as $alternativa) {
                    $alternativa->respuesta = trim(preg_replace('/(\r\n)|\n|\r/', '<br/>', $alternativa->respuesta));
                    $alternativa->respuesta = str_replace('"', '\\"', $alternativa->respuesta);
                }
            }
        }
        return $preguntas;
    }
    private function getRespuestas($asignacion_id)
    {
        return $this->alumnoModel->getRespuestas($asignacion_id, $this->session->idUser);
    }
    public function getEvaluacionesDisponibles()
    {
        $evaluaciones_aeduc = $this->alumnoModel->getEvaluacionesAlumno('CLASSRUN');
        $evaluaciones_personal = $this->alumnoModel->getEvaluacionesAlumno('PERSONAL');

        return array_merge($evaluaciones_aeduc, $evaluaciones_personal);
        
    }
    public function ejectuarEvaluacion($idAsignacion, $origen='classrun', $id)
    {
        echo json_encode(array('data' => $this->alumnoModel->ejectuarEvaluacion($idAsignacion, $origen, $id)));
    }
    public function setAlternativa($idAlternativa, $idPregunta, $idEvaluacion, $id_asignacion)
    {
        // verifica si el usuario tiene una asignación en curso
        $row = $this->db->query('SELECT curso_id
                                   FROM "'.$this->session->rbd.'".asignacion
                                  WHERE prueba_id = ?
                                    AND usuario_id = ?
                                    AND id = ?
                                    AND estado != 1', array($idEvaluacion, $this->session->idUser, $id_asignacion))->row();
        if (isset($row)) {
            $this->session->sess_destroy();
            echo json_encode(array('status' => 'closed'));

            return;
        }

        echo json_encode(array('status' => $this->alumnoModel->setAlternativa($idAlternativa, $idPregunta, $idEvaluacion, $id_asignacion)));
    }

    public function eliminarAlternativa($idAlternativa, $idPregunta, $idEvaluacion, $id_asignacion)
    {
        // verifica si el usuario tiene una asignación en curso
        $row = $this->db->query('SELECT curso_id
                                   FROM "'.$this->session->rbd.'".asignacion
                                  WHERE prueba_id = ?
                                    AND usuario_id = ?
                                    AND id = ?
                                    AND estado != 1', array($idEvaluacion, $this->session->idUser, $id_asignacion))->row();
        if (isset($row)) {
            echo json_encode(array('status' => 'closed'));
            return;
        }

        $idAlumno = $this->session->idUser;
        echo json_encode(array('status' => $this->alumnoModel->eliminarAlternativa($idAlternativa, $idPregunta, $idEvaluacion, $idAlumno, $id_asignacion)));
    }

    public function finalizarEvaluacion($idUsuarioHasAsignacion, $idEvaluacion, $idAsignacion, $origen)
    {
        $usuarioHasAsignacion = $this->asignacionModel->getUsuarioHasAsignacionByEvaluacionByUsuario($idEvaluacion, $this->session->idUser, $origen, $idAsignacion);

        $this->asignacionModel->setTime($idUsuarioHasAsignacion, ['fin' => time()]);

        if ($this->alumnoModel->finalizarAsignacion($idAsignacion, $this->session->idUser)) {
            $this->session->sess_destroy();
            echo json_encode(array('status' => true));
        }
    }
    public function ColorPrueba($idEvaluacion, $origen = 'CLASSRUN', $asignacion_id)
    {
        $tipoEvaluacion = $this->evaluacionModel->getEvaluacion($idEvaluacion, $origen, $asignacion_id);
        $asignatura = '';

        $alumno = $this->evaluacionModel->getUsuario($this->session->userdata['idUser']);
        switch ($tipoEvaluacion->asignatura_id) {
            case 3: $asignatura = 'mat.png'; break;
            case 1: $asignatura = 'leng.png'; break;
            case 5: $asignatura = 'cien.png'; break;
            case 4: $asignatura = 'hist.png'; break;
            case 12: $asignatura = 'bio.png'; break;
            case 13: $asignatura = 'fisi.png'; break;
            case 14: $asignatura = 'qui.png'; break;
            case 5: $asignatura = 'cien.png'; break;
        }
        if ($tipoEvaluacion->nivel_id > 8) {
            $nivel = 2;
        } else {
            $nivel = 1;
        }

        $data['color']  = $tipoEvaluacion->color;

        $data['header']  = '';
        $data['menu'] = '';

        $data['nombre'] = $alumno[0]->nombre;
        $data['tipo'] = $nivel;
        return $data;
    }

    public function setTime($usuario_has_asignacion_id)
    {
        $this->asignacionModel->setTime($usuario_has_asignacion_id, ['inicio' => time(), 'fin' => null]);
    }
}
