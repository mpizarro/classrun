<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once("secure_area.php");

class Home extends Secure_area
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('utilidades');
        $this->load->helper('core_helper');
        $this->load->library('ciqrcode');

        $this->load->model('CrearEvaluacion_model', 'model');
        $this->load->model('Nivel_model', 'nivelModel');
        $this->load->model('Curso_model', 'cursoModel');
        $this->load->model('Alumno_model', 'alumnoModel');
        $this->load->model('Prueba_model', 'pruebaModel');
    }

    public function qr()
    {
        
        $asignacion = $this->input->post('asignacion');
        $preguntas  = $this->input->post('preguntas');
        $nombre     = $this->input->post('nombre');

        $data = array(
            'title' => $nombre,
            'userId' => $this->session->idUser,
            'assignmentId' => $asignacion,
            'numberOfQuestions' => $preguntas, 
            'numberOfAlternatives' => 4
        );
         
        $payload = json_encode($data);
         
        $ch = curl_init("https://us-central1-kimunteacher.cloudfunctions.net/classRunQR");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
         
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );
         
        
        $result = curl_exec($ch);
        
        $obj = json_decode($result);
        
        curl_close($ch);

        
        $data['html'] = $obj->{'qr'};
        
        echo json_encode($data);

    }

    public function importNumikApp($asignacion)
    {
        $data = array('assignmentId' => "$asignacion");

        $payload = json_encode($data);
         
        $ch = curl_init("https://us-central1-kimunteacher.cloudfunctions.net/classRunGetAnswers");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
         
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload))
        );

        $result = curl_exec($ch);
        
        $obj = json_decode($result, true);
        
        curl_close($ch);

        $origen = $this->model->getOrigen($asignacion);
        //print_r($origen);
        //print_r($obj);
        
        foreach ($obj as $key => $alumno) 
        {
            if($dataAlumno = $this->alumnoModel->verificaAlumnoRespuestas($key, $asignacion))
            {        
                if($this->alumnoModel->deleteHojarespuestaAlumno($asignacion, $dataAlumno->id))

                //if($dataAlumno->respuestas == 0)
                //{
                                
                    foreach ($alumno as $numPregunta=>$alternativa) 
                    {
                        
                        if($alternativa!='empty')
                        {
                            $respuesta = $this->model->getDataRespuesta($asignacion, $alternativa, $numPregunta, $origen->origen);
                            //echo $key.':$asignacion:'.$asignacion.', $alternativa:'.$alternativa.', $numPregunta:'.$numPregunta.', $origen->origen:'.$origen->origen.'<br>';
                            $this->model->setRespuesta($dataAlumno->id, $respuesta->id, $respuesta->pregunta_id, $asignacion);
                            
                        }
                        
                    }
                //}

                
            }
            else
                echo "alumno".$key." no encontrado";
        }
        
    }

    public function test()
    {
        $html = 'βιβλος <font color="#008000"><sup>G<WG976>:<WTN-NSF></sup></font><font color="#0000ff">Libro</font> γενεσεως <font color="#008000"><sup><WTG<WG1>><WG078>:<WTN-GSF></sup></font><font color="#0000ff">de origen</font> ιησου <font color="#008000"><sup><WTG<WG2>><WG424>:<WTN-GSM></sup></font><font color="#0000ff">de Jesús</font> χριστου <font color="#008000"><sup>G<WG5547>:<WTN-GSM></sup></font><font color="#0000ff">Ungido</font> υιου <font color="#008000"><sup>G<WG5207>:<WTN-GSM></sup></font><font color="#0000ff">hijo</font> δαυιδ <font color="#008000"><sup><WTG<WG113>><WG8>:<WTN-PRI></sup></font><font color="#0000ff">de David</font> υιου <font color="#008000"><sup>G<WG5207>:<WTN-GSM></sup></font><font color="#0000ff">hijo</font> αβρααμ <font color="#008000"><sup><WTG<WG11>>:<WTN-PRI></sup></font><font color="#0000ff">de Abrahán</font>';
        
        echo $html;
        //$this->load->helper('form');
        //$this->load->view('view_work_add');
    }

    public function index()
    {
        $idAsignatura = $this->input->get('asignatura');
        $idAsignatura = !isset($idAsignatura) || $idAsignatura == "" || !is_numeric($idAsignatura) ? FALSE : $idAsignatura;

        $idNivel = $this->input->get('nivel');
        $idNivel = !isset($idNivel) || $idNivel == "" || !is_numeric($idNivel) ? FALSE : $idNivel;

        $idTipo = $this->input->get('tipo');
        $idTipo = !isset($idTipo) || $idTipo == "" || !is_numeric($idTipo) ? FALSE : $idTipo;

        $data['pruebas'] = $this->model->getTipoPruebas($this->session->colegio_id);     
        $data['niveles'] = $this->model->getNiveles();
        $data['asignaturas'] = $this->model->getAsignaturas();
       
        $arrCursos = [];
        
        if ($cursos = $this->cursoModel->getCursosConAlumnosByColegio($this->session->colegio_id))
        {
            foreach ($cursos as $curso)
                $arrCursos[] = $curso->curso_id;
                $data['nivelesColegio'] = $this->nivelModel->getNivelesByCursos($arrCursos);
        }
        else
            $data['nivelesColegio'] = "";

        $data['css']            = '<link href="' . base_url() . 'assets/modulos/crearEvaluacion/css/index.css" rel="stylesheet" type="text/css" />';
        //$data['contenidos']     = $this->model->getPruebasCreadas($idAsignatura, $idNivel, $idTipo);
        if($idAsignatura)
        {
            $data['asignatura'] = $idAsignatura;
            
            $data['path'] = "var pTipo =".$idTipo.";";
            $data['path'].= "var pAsignatura =".$idAsignatura.";";
            $data['path'].= "var pNivel =".$idNivel.";";

        }
        
        $this->load->view('epoch/header');
        $this->load->view('epoch/body', $data);
        $this->load->view('epoch/footer');

    }

    public function seleccionar()
    {
        $idNivel       = $this->input->post('nivel_id');
        $idAsignatura  = $this->input->post('asignatura_id'); 
        $idTipo        = $this->input->post('tipo_id');

        switch ($idTipo) 
        {
            case 1:
                $nombreTipo = "SIMCE";
            break;
            case 4:
                $nombreTipo = "COBERTURA";
            break;
            case 5:
                $nombreTipo = "PSU";
            break;
            
        }
         

        $html           = '';
        $card_footer    = '';
        $row            = 1;
        $pruebas        =   $this->model->getPruebasCreadas($idAsignatura, $idNivel, $idTipo);
        $pruebasAeduc   = $this->model->getPruebasAeduc($idNivel, $idAsignatura, $idTipo);
        

        if($pruebasAeduc)
            foreach ($pruebasAeduc as $prueba) 
            {
                $nombre         = "Numik Eva. ".$prueba->num;
                $aplicar        = '';
                $alumnos        = '';
                $card_footer    = '';
                $btn_plus       = '';
                $botonVisualizar = '';
                $asignaciones = $this->model->getAsignaciones($prueba->id);
                $cantPreguntas = 0;

                $preguntas = $this->model->getPreguntasCreadas($prueba->id);
                
                foreach ($preguntas as $pregunta) 
                    $cantPreguntas++;


                if($asignaciones)
                {
                    $btn_plus = '<button class="btnAsignacionesPrueba'.$prueba->id.'" estado="cerrado" onclick="btnAsignacionesPrueba('.$prueba->id.')" style="background: #40a3f1;color: #fff;font-size: 17px;font-weight: bold;border-color: #40a3f1;" >
                                <i class="mdi mdi-plus"></i>
                            </button>';
                    $card_footer.= $this->htmlAsignaciones($prueba, $asignaciones, $nombre, 'classrun');
                }
                
                if($row == 1)
                    $html.= '<div class="row">';
                
                $html.= $this->htmlPruebas($nombre, $prueba, $btn_plus, $card_footer, 'classrun');
                
                if($row == 4)
                {
                    $html.= '</div>';
                    $row = 0;
                }

            $row++;
        }

        if($pruebas)
            foreach ($pruebas as $prueba) 
            {
                $aplicar        = '';
                $alumnos        = '';
                $card_footer    = '';
                $btn_plus       = '';
                $nombre         = $prueba->nombre;
                
                $asignaciones = $this->model->getAsignaciones($prueba->id);
                $botonVisualizar = '';
                
                if($asignaciones)
                {
                    $btn_plus = '<button class="btnAsignacionesPrueba'.$prueba->id.'" estado="cerrado" onclick="btnAsignacionesPrueba('.$prueba->id.')" style="background: #40a3f1;color: #fff;font-size: 17px;font-weight: bold;border-color: #40a3f1;" >
                                <i class="mdi mdi-plus"></i>
                            </button>';
                    $card_footer.= $this->htmlAsignaciones($prueba, $asignaciones, $nombre);
                }
                
                
                if($row == 1)
                    $html.= '<div class="row">';
                    
                $html.= $this->htmlPruebas($nombre, $prueba, $btn_plus, $card_footer, 'profesor');
                

                if($row == 4)
                {
                    $html.= '</div>';
                    $row = 0;
                }

                $row++;
        
            }


//CREAR NUEVA
        $html.='<div class="col-md-6 col-lg-3 col-xl-3" style="float:left">            
                            <div class="card m-b-30">
                                <div class="card-body" style="padding-top: 0px;padding-right: 0px;padding-left: 0px;padding-bottom:0px;border: 3px solid;border-color: #56b2bf;border-radius: 10px;">
                                    <div style="background-color: #56b2bf;padding-top: 19px;padding-left: 23px;padding-bottom: 2px;color: #FFF;">
                                        <h4 class="card-title font-20 mt-0">Crear Nueva</h4>
                                    </div>
                                <form action="home/crear_prueba" method="post">
                                <input type="hidden" id="nivel_id" name="nivel_id" value="'.$idNivel.'">
                                <input type="hidden" id="asignatura_id" name="asignatura_id" value="'.$idAsignatura.'">
                                <input type="hidden" id="tipo_id" name="tipo_id" value="'.$idTipo.'">

                                    <div style="padding:12px 12px 0px 12px">    
                                        <input type="text" class="form-control" id="inputNuevaEv" name="nombre" placeholder="Nombre Evaluación"
                                        style="margin-bottom:5px">
                                        
                                        <select class="form-control" name="alternativas">
                                            <option value="2">2 Alternativas</option>
                                            <option value="3">3 Alternativas</option>
                                            <option value="4" selected>4 Alternativas</option>
                                            <option value="5">5 Alternativas</option>
                                        </select>
                                    </div>
                                    <div class="card-body" style="padding-top:6px">
                                        
                                        <hr>
                                        <div class="row">
                                                                               
                                            <div class="col-md-12 col-lg-12 col-xl-12" style="padding-left: 0px;padding-right:0px;">
                                            <center>    
                                                <button disabled id="btnNuevaEv" class="btn btn-primary btn-sm">
                                                <i class="mdi mdi-plus"></i> Crear Evaluación
                                                </button> 
                                            </center>
                                            </div>
                                </form>            
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>            
                        </div>';

        $data['html'] = $html;
        
        echo json_encode($data);
        
    }

    public function htmlPruebas($nombre, $prueba, $btn_plus, $card_footer, $origen)
    {
        if($origen == 'classrun')
        {
            $btnEditar = '  <button type="button" title="Asignar" class="tabledit-edit-button btn btn-sm btn-info btn-ev btn-editar" nombre="'.$nombre.'" id-evaluacion="'.$prueba->id.'">
                                <i class="mdi mdi-lead-pencil"></i>
                            </button>';
            $btnEliminar = '';
        }
        else
        {
            $disabled = '';

            if($btn_plus != '')
                $disabled = 'disabled';

            $btnEditar      = '<a  href="'.site_url().'/home/crear/'.$prueba->id.'/'.$prueba->nivel_id.'/'.$prueba->asignatura_id.'/'.$prueba->tipo_id.'" class="btn btn-primary btn-sm"><i class="mdi mdi-lead-pencil"></i></a>';
            $btnEliminar    = '<button '.$disabled.' type="button" id="'.$prueba->id.'" class="delete-button btn btn-sm btn-danger btn-ev">
                                <i class="mdi mdi-delete-forever"></i>
                                </button>';
        }
        
        $html= '<div class="col-md-6 col-lg-3 col-xl-3" id="containerPrueba'.$prueba->id.'" style="float:left">            
                        
                        <div class="card m-b-30">
                            <div class="card-body" style="padding-top: 0px;padding-right: 0px;padding-left: 0px;padding-bottom:0px;border: 3px solid;border-color: #56b2bf;border-radius: 10px;">
                                <div style="background-color: #56b2bf;padding-top: 19px;padding-left: 23px;padding-bottom: 2px;color: #FFF;">
                                    <h4 class="card-title font-20 mt-0">'.$nombre.'</h4>
                                </div>
                
                                <div style="padding-left: 16px;">
                                    <p class="font-14" style="font-size: 20px;font-weight: bold;margin-bottom: 0px;">'.$prueba->asignatura.'</p>
                                    <p class="font-14" style="font-size: 20px;font-weight: bold;margin-bottom: 0px;">'.$prueba->nivel.'</p>
                                </div>
                                <div class="card-body">
                                    <h6 class="font-14 mt-0">'.$prueba->tipo.'</h6>
                                    <hr></hr>
                                    <div class="row">
                                        <div class="col-md-2 col-lg-2 col-xl-2">
                                            <button type="button" title="Asignar" origen="'.strtoupper($origen).'" class="tabledit-edit-button btn btn-sm btn-info btn-ev btn-asignar" btn-asignar="" id-evaluacion="'.$prueba->id.'">
                                                Aplicar
                                            </button>
                                        </div>
                                        <div class="col-md-2 col-lg-2 col-xl-2">
                                            
                                        </div>
                                        <div class="col-md-2 col-lg-2 col-xl-2" style="padding-right: 0px;">    
                                            <a  btn-ver-evaluacion="" id-evaluacion="'.$prueba->id.'" origen="'.$origen.'" class="btn btn-primary btn-sm"><i class="mdi mdi-eye"></i></a>
                                        </div>
                                        <div class="col-md-2 col-lg-2 col-xl-2" style="padding-right:0px;">
                                            '.$btnEditar.'
                                        </div>
                                        
                                        <div class="col-md-2 col-lg-2 col-xl-2" style="padding-right:0px;">
                                            '.$btnEliminar.'
                                        </div>
                                        
                                        <div class="col-md-2 col-lg-2 col-xl-2" style="padding-right: 0px;">
                                            '.$btn_plus.'
                                        </div>
                                    </div>
                                </div>
                                '.$card_footer.'
                            </div>
                        </div>            
                    </div>';
        
        return $html;
    }

    public function htmlAsignaciones($prueba, $asignaciones, $nombre, $origen=false)
    {
        $card_footer = '';
        $origenTabulacion = 'PERSONAL';
        
        if($origen)
            $origenTabulacion = 'CLASSRUN';
        
        if($asignaciones)
            {
                
                foreach ($asignaciones as $asignacion) 
                {
                    $url = site_url() . '/Reporte/home/'.$asignacion->id.'/'.$origenTabulacion;
                    $informe =  '<a class="btn btn-ev" href="'.$url.'" target="_blank">
                                    <i class="fa fa-cloud-download"></i>
                                </a>';
                    
                    $alumnos = '<button style="margin-left: -18px;" class="btn btn-info btn-alumnos" origen="'.$origenTabulacion.'" id-curso="'.$asignacion->curso_id.'" id-evaluacion="'.$asignacion->prueba_id.'" id-asignacion="'.$asignacion->id.'">
                        <i class="mdi mdi-school"></i>
                    </button>';

                    if($asignacion->estado != 1)
                    {
                        $btnPlay = '<button curso_id="'.$asignacion->curso_id.'" estado="0" class="btn btn-finalizar btn-sm" onclick="ejecutarAsignacion('.$asignacion->id.','.$asignacion->curso_id.')" id="btns-'.$asignacion->id.'" style="background: #46da46;color: #fff;">
                                        <i class="mdi mdi-play" style="font-size:25px" ></i>
                                    </button>';
                        $btnStop = '<button type="button" disabled class="btn btn-info btn-finalizar" onclick="finalizarAsignacion('.$asignacion->id.','.$asignacion->curso_id.')" id="btn-'.$asignacion->id.'" style="background: #f15252;color: #fff;">
                                        <i class="mdi mdi-stop" style="font-size:25px"></i>
                                    </button>';
                        $informe =  '<a class="btn btn-primary" class="btn btn-ev" href="'.$url.'" target="_blank">
                                        <i class="fa fa-cloud-download"></i>
                                    </a>';
                    }
                    else
                    {
                        $btnPlay = ' <button curso_id="'.$asignacion->curso_id.'" estado="1" type="button" class="btn btn-finalizar" onclick="finalizarAsignacion('.$asignacion->id.','.$asignacion->curso_id.')" id="btns-'.$asignacion->id.'" style="background: #f1df49;color: #fff;">
                                        <i class="mdi mdi-pause" style="font-size:25px" ></i>
                                    </button>';
                        
                        $btnStop = '<button type="button" class="btn btn-finalizar" onclick="finalizarAsignacion('.$asignacion->id.','.$asignacion->curso_id.')" id="btn-'.$asignacion->id.'" style="background: #f15252;color: #fff;">
                                    <i class="mdi mdi-stop" style="font-size:25px"></i>
                                </button>';
                        $informe = '<a id="btn-informe-'.$asignacion->id.'" style="display:none;height: 39px;width: 35px;" class="btn btn-primary" class="btn btn-ev" href="'.$url.'" target="_blank">
                                        <i class="fa fa-cloud-download" style="margin-left: -3px;margin-top: 4px;"></i>
                                    </a>';
                    }

                    $card_footer.= '<div class="card-footer text-muted asignacionesPrueba'.$prueba->id.'"  style="display:none">
                                        <div class="row asignacionHeader'.$asignacion->id.'">
                                            <div class="col-lg-12">
                                                    '.$asignacion->curso.' '.$asignacion->letra.'
                                            </div>
                                        </div>
                                            <div class="row asignacionBody'.$asignacion->id.'">
                                                <div class="col-md-2 col-lg-2 col-xl-2">
                                                    '.$btnPlay.'                                            
                                                </div>
                                                <div class="col-md-2 col-lg-2 col-xl-2">                                            
                                                    &nbsp;
                                                </div>
                                                <div class="col-md-2 col-lg-2 col-xl-2">
                                                    '.$alumnos.'
                                                </div>
                                                <div class="col-md-2 col-lg-2 col-xl-2">
                                                    <button style="margin-left: -18px;" class="btn btn-info btn-tabulacion" origen="'.$origenTabulacion.'" id-asignacion="'.$asignacion->id.'" cantPreguntas="'.$prueba->count_pregunta.'" nombrePrueba="'.$nombre.'">
                                                        <i class="fa fa-file-text-o"></i>
                                                    </button>
                                                    
                                                </div>
                                                <div class="col-md-2 col-lg-2 col-xl-2">
                                                    <button style="margin-left: -18px;" class="btn btn-danger delete-button-asignacion" origen="'.$origenTabulacion.'" id="'.$asignacion->id.'">
                                                        <i class="fa fa-trash-o"></i>
                                                    </button>
                                                    
                                                </div>
                                                <div style="margin-left: -18px;" class="col-md-2 col-lg-2 col-xl-2">
                                                    '.$informe.'
                                                </div>
                                            </div>
                                        </div>';
                }
            }
    return $card_footer;
    }


    public function crear_prueba($estandarizada=false)
    {

        
        $nivel_id       = $this->input->post('nivel_id');
        $asignatura_id  = $this->input->post('asignatura_id');
        $tipo_id        = $this->input->post('tipo_id');
        $nombre         = $this->input->post('nombre');
        $alternativas   = $this->input->post('alternativas');
        
        $prueba = [
            'codigo' => '',
            'nivel_id' => $nivel_id,
            'asignatura_id' => $asignatura_id,
            'anyo' => date('Y'),
            'num' => 1,
            'tipo_id' => $tipo_id,
            'usuario_id' => $this->session->idUser,
            'nombre' => $nombre,
            'prueba_original' => $pruebaAeduc ? $pruebaAeduc : null,
            'modificado' => date('Y-m-d H:i:s'),
            'version' => $alternativas
        ];

        $prueba_id = $this->model->setPrueba($prueba);

        if($estandarizada)
        {    
            $prueba_aeduc    = $this->input->post('prueba_id');
            $this->model->copyPruebaToCreada($prueba_id, $prueba_aeduc);
        }

               
        redirect(site_url() . '/home/preguntas/' . $nivel_id ."/". $asignatura_id."/".$tipo_id."/".$nombre."/".$prueba_id);
    }

    public function crear($prueba_id,$nivel_id,$asignatura_id,$tipo_id)
    {
        $nombre = "TEST";
        
        $data['css'] = '<link href="' . base_url() . 'assets/modulos/crearEvaluacion/css/seleccionar.css" rel="stylesheet" type="text/css" />
                        <link href="' . base_url() . 'assets/modulos/crearEvaluacion/css/crear.css" rel="stylesheet" type="text/css" />';

        $data['js'] = '<script src="'.base_url().'assets/modulos/crearEvaluacion/js/crear.js"></script>';
        
        $data['prueba_id']      = $prueba_id;
        $data['nivel_id']       = $nivel_id;
        $data['asignatura_id']  = $asignatura_id;
        $data['tipo_id']        = $tipo_id;
        $data['nombre']         = "ndad";
        
        $color = getColorAsignatura($data['asignatura_id']);
        $data['color'] = $color['color'];
        $data['colorClass'] = $color['clase'];

        $data['prueba_original'] = '';

        //if ($params[4]) {
          //  $data['prueba_original'] = $params[4];
        //}

        $data['oas'] = [];

        switch ($data['tipo_id']) {
            case 3:
            case 4:
                $data['oas_default'] = $this->model->getOAsFromPruebaCopiada($prueba_id);
                $data['oas'] = $this->model->getOAs(['nivel_id' => $nivel_id, 'asignatura_id' => $asignatura_id]);
                break;
            case 1:
            case 2:
                $data['ejes'] = $this->model->getEjes($data['asignatura_id'], $data['nivel_id']);
                $data['ejes_default'] = $this->model->getEjesFromPruebaCopiada($prueba_id);
                break;
        }
        //$base_64url = base64_encode("$prueba_id,$nivel_id,$asignatura_id,$tipo_id");

        redirect(site_url() . '/home/preguntas/' .$nivel_id.'/'.$asignatura_id.'/'.$tipo_id.'/'.$nombre.'/'.$prueba_id);        

    }


    public function get_asignatura(){
            $options = "";
            $nivel_id = $this->input->post('nivel_id');
            $data['asignaturashasnivel'] = $this->model->getAsignaturashasnivel($nivel_id);
    

            echo json_encode($data);

 
    }

    public function crear_prueba_set_oa()
    {

        $flagEje = $this->input->post('flag_eje');
        $oas_eje = '';
        if (empty($flagEje))
            $oas_eje = implode('-', $this->input->post('oas'));
        else
            $oas_eje = implode('-', $this->input->post('ejes'));

        $nivel_id = $this->input->post('nivel_id');
        $asignatura_id = $this->input->post('asignatura_id');
        $tipo_id = $this->input->post('tipo_id');
        $prueba_id = $this->input->post('prueba_id');
        $nombre = $this->input->post('nombre');

        $prueba_aeduc = '';
        if ($this->input->post('prueba_original'))
        {
            $prueba_aeduc = $this->input->post('prueba_original');
        }

        $base_64url = base64_encode("$prueba_id,$nivel_id,$asignatura_id,$tipo_id,$oas_eje,$prueba_aeduc,$nombre");

        redirect(site_url() . '/home/preguntas/' . $base_64url);
    }

    public function preguntas($nivel_id, $asignatura_id, $tipo_id, $nombre, $prueba_id, $pregunta=false)
    {
        
        $dataPrueba             = $this->model->datosPrueba($prueba_id);

        $header['css'] = '<link href="' . base_url() . 'assets/modulos/crearEvaluacion/css/seleccionar.css" rel="stylesheet" type="text/css" />
                        <link href="' . base_url() . 'assets/modulos/crearEvaluacion/css/preguntas.css" rel="stylesheet" type="text/css" />
                        <link href="' . base_url() . 'assets2/css/seleccionar.css" rel="stylesheet" type="text/css">
                        <link href="' . base_url() . 'assets2/css/crear.css" rel="stylesheet" type="text/css">
                        ';
        
        
        $data['prueba_id']      = $prueba_id;
        $data['nivel_id']       = $nivel_id;
        $data['asignatura_id']  = $asignatura_id;
        $data['tipo_id']        = $tipo_id;
        $data['prueba_aeduc']   = 117;//$params[5];
        
        $params[4] = '';
        
        $header['left'] = site_url()."/home?asignatura=$asignatura_id&tipo=$tipo_id&nivel=$nivel_id";

        if ($data['tipo_id'] == 3 || $data['tipo_id'] == 4) 
        {
            $arrOas = explode('-', $params[4])[0] != '' ? explode('-', $params[4]) : [0];
            $data['oas'] = $this->model->getGrupoOAs($nivel_id, $asignatura_id);
            //$data['oas_id'] = $dataPrueba->oa_id;
            $data['preguntas'] = $this->model->getPreguntasFromOA($tipo_id, $asignatura_id, $nivel_id);
            $data['ejes'] = $this->model->getEjes($data['asignatura_id'], $data['nivel_id']);
        } 
        

        $data['ejes']       = $this->model->getEjes($asignatura_id, $nivel_id);
        
        foreach ($data['ejes'] as $eje) 
        {
            $indicadores[] = $eje->id;
        }

        if($tipo_id<3)
            $data['preguntas']  = $this->NumeroPreguntasSugeridas($indicadores, $tipo_id, $asignatura_id, $nivel_id);
        
        $data['ejes_id']    = "91,94";
        
     
        
        $data['nombre']         = $dataPrueba->nombre;
        $data['alternativas']   = $dataPrueba->version;


        $color = getColorAsignatura($data['asignatura_id']);
        $data['color'] = $color['color'];
        $data['colorClass'] = $color['clase'];

        $data['encabezadoImg'] = getHeaderEvaluacion($data['asignatura_id'], $data['tipo_id']);

        $data['habilidades'] = $this->model->getHabilidades($data['asignatura_id'], $data['nivel_id']);

        $data['preguntas_creadas'] = $this->model->getPreguntasCreadas($data['prueba_id']);

        if ($pregunta)
        {
            $data['pregunta_editar'] = $this->model->getPregunta($prueba_id, $pregunta);
            $data['respuesta_editar'] = $this->model->getRespuestas($prueba_id, $pregunta);
            
        }

        //print_r($data);
        $this->load->view('modulos/alumno/alumno_header', $header);
        //$this->load->view('epoch/header', $header);
        $this->load->view('home/pregunta', $data);
    }

    public function NumeroPreguntasSugeridas($indicadores, $tipo_id, $asignatura_id, $nivel_id)
    {
        switch ($tipo_id) {
            case 1:
                $preguntas = $this->model->getPreguntasFromEje($indicadores, null, $asignatura_id, $nivel_id);
            break;
            case 4:
                $preguntas = $this->model->getPreguntasFromOA($tipo_id, $asignatura_id, $nivel_id);
            break;
            
        }
    
        return $preguntas;
            //print_r($preguntas);
    }

    public function crear_pregunta_prueba()
    {        
        if ($this->input->post('correcta'))
        {
            $data = [
                'nivel_id' => $this->input->post('nivel_id'),
                'asignatura_id' => $this->input->post('asignatura_id'),
                'tipo_id' => $this->input->post('tipo_id'),
                'prueba_id' => $this->input->post('prueba_id'),
                'ie_id' => $this->input->post('ie_id') ? $this->input->post('ie_id') : null,
                'ejetematico_id' => $this->input->post('ejetematico_id'),
                'habilidadcurricular_id' => $this->input->post('habilidadcurricular_id'),
                'formato_pregunta' => $this->input->post('formato_pregunta'),
                'imagen_link' => $_FILES['imagen_link'],
                'pregunta' => '<p><strong>' . $this->input->post('pregunta') . '</strong></p>',
                'correcta' => $this->input->post('correcta'),
                'respuesta' => $this->input->post('respuesta'),
                'respuesta_imagen' => $_FILES['respuesta_imagen'],
                'pregunta_editar_id' => $this->input->post('pregunta_editar_id'),
                'pregunta_editar_orden' => $this->input->post('pregunta_editar_orden')
            ];

            if ($this->input->post('pregunta_editar_id'))
            {
                $this->model->removePregunta($this->input->post('prueba_id'), $this->input->post('pregunta_editar_id'));
            }

            if ($this->model->setPregunta($data))
            {
                $nivel_id = $this->input->post('nivel_id');
                $asignatura_id = $this->input->post('asignatura_id');
                $tipo_id = $this->input->post('tipo_id');
                $prueba_id = $this->input->post('prueba_id');
                $nombre = $this->input->post('nombre');
                $oa_eje = '';
                if ($data['tipo_id'] == 3 || $data['tipo_id'] == 4)
                    $oa_eje = $this->input->post('oas');
                elseif ($data['tipo_id'] == 1 || $data['tipo_id'] == 2)
                    $oa_eje = $this->input->post('ejes');

                $prueba_aeduc = $this->input->post('prueba_aeduc');

                redirect(site_url() . '/home/preguntas/'.$nivel_id.'/'.$asignatura_id.'/'.$tipo_id.'/'.$nombre.'/'.$prueba_id);
                
            }

        }
    }

    public function preguntas_sugeridas($prueba_id)
    {

        $dataPrueba             = $this->model->datosPrueba($prueba_id);

        $data['css']            = '<link href="' . base_url() . 'assets/modulos/crearEvaluacion/css/seleccionar.css" rel="stylesheet" type="text/css" />
                                   <link href="' . base_url() . 'assets/modulos/crearEvaluacion/css/preguntas_sugeridas.css" rel="stylesheet" type="text/css" />
                                   <link href="' . base_url() . 'assets/modulos/crearEvaluacion/css/preguntas.css" rel="stylesheet" type="text/css" />';

        $data['prueba_id']      = $dataPrueba->id;
        $data['nivel_id']       =  $dataPrueba->nivel_id;
        $data['asignatura_id']  =  $dataPrueba->asignatura_id;
        $data['tipo_id']        =  $dataPrueba->tipo_id;
        $data['nombre']         = $dataPrueba->nombre;
        
        $color                  = getColorAsignatura($dataPrueba->asignatura_id);
        
        $data['color'] = '#61b7c2;';
        $data['colorClass'] = '#61b7c2;';

        $data['encabezadoImg'] = getHeaderEvaluacion($dataPrueba->asignatura_id, $dataPrueba->tipo_id);

        if ($dataPrueba->tipo_id == 3 || $dataPrueba->tipo_id == 4) 
        {
            $data['oas']        = $this->model->getGrupoOAs($dataPrueba->nivel_id, $dataPrueba->asignatura_id);
            $data['preguntas']  = $this->model->getPreguntasFromOA($dataPrueba->tipo_id, $dataPrueba->asignatura_id, $dataPrueba->nivel_id);
        } 
        else 
        {
            $data['ejes']       = $this->model->getEjes($dataPrueba->asignatura_id, $dataPrueba->nivel_id);
        
            foreach ($data['ejes'] as $eje) 
            {
                $indicadores[] = $eje->id;
            }

            $data['ejes_id'] = "10,11"; 
        
            $data['preguntas'] = $this->model->getPreguntasFromEje($indicadores, null, $dataPrueba->asignatura_id, $dataPrueba->nivel_id);
        }
        //print_r($data['preguntas']);
        /*
        if (isset($params[6]))
        {
            $data['pregunta_editar'] = $this->model->getPregunta($data['prueba_id'], $params[6]);
            $data['respuesta_editar'] = $this->model->getRespuestas($data['prueba_id'], $params[6]);
        }
        */
        
        $this->load->view('layout/header', $data);
        
        $this->load->view('home/preguntas_sugeridas', $data);
    }

    public function eliminar_pregunta()
    {
        $pregunta = $this->input->post('pregunta');
        $prueba = $this->input->post('prueba');

        $this->model->destroyPreguntaCreada($pregunta, $prueba);
    }

    public function mover_arriba_pregunta()
    {
        $pregunta = $this->input->post('pregunta');
        $prueba = $this->input->post('prueba');

        $this->model->moveToUpPreguntaCreada($pregunta, $prueba);
    }

    public function mover_abajo_pregunta()
    {
        $pregunta = $this->input->post('pregunta');
        $prueba = $this->input->post('prueba');

        $this->model->moveToDownPreguntaCreada($pregunta, $prueba);
    }

    public function traspasar_preguntas()
    {
        $data = '';
        $asignatura_id = $this->input->post('asignatura_id');
        $nivel_id = $this->input->post('nivel_id');
        $tipo_id = $this->input->post('tipo_id');
        $prueba_id = $this->input->post('prueba_id');
        $oas_ejes = $tipo_id == 3 || $tipo_id == 4 ? $this->input->post('oas') : $this->input->post('ejes');
        $prueba_aeduc = $this->input->post('prueba_aeduc');
        $nombre = $this->input->post('nombre');

        $preguntas = $this->input->post('pregunta_id');

        $this->model->copyPreguntasToCreadas($prueba_id, $preguntas);

        redirect(site_url() . '/home/preguntas/'.$nivel_id.'/'.$asignatura_id.'/'.$tipo_id.'/'.$nombre.'/'.$prueba_id);
    }

    public function cerrar_prueba()
    {
        $this->model->setCerrarPrueba($this->input->post('prueba_id'));
        redirect(site_url() . '/home');
    }

    public function getCursosByNivel()
    {
        $idNivel = $this->input->post('idNivel');        
        $cursos = $this->cursoModel->getCursosByNivel($idNivel);
        echo json_encode(['cursos' => $cursos]);
    }
    public function imprime_ie()
    {
        $oa_id = $this->input->post('oa_id');
        $ie_id = $this->input->post('ie_id');

        $ies = $this->model->getIEsFromOas($oa_id);

        echo ' <option value="" hidden>Seleccione una opción</option>';
        foreach ($ies as $ie) {
            echo "<option value='$ie->id' ".($ie_id == $ie->id ? 'selected' : '').">$ie->ie</option>";
        }
    }

    public function redirecciona_para_editar()
    {
        $pregunta = $this->input->post('pregunta');
        $asignatura_id = $this->input->post('asignatura_id');
        $nivel_id = $this->input->post('nivel_id');
        $tipo_id = $this->input->post('tipo_id');
        $prueba_id = $this->input->post('prueba_id');
        $oas = $this->input->post('oas');
        $ejes = $this->input->post('ejes');
        $prueba_aeduc = $this->input->post('prueba_aeduc');
        $nombre = $this->input->post('nombre');

        $ejes_o_oas = $oas;
        if ($oas == "")
            $ejes_o_oas = $ejes;

        $base_64url = base64_encode("$prueba_id,$nivel_id,$asignatura_id,$tipo_id,$ejes_o_oas,$prueba_aeduc,$nombre,$pregunta");

        echo site_url() . '/home/preguntas/' .$nivel_id.'/'.$asignatura_id.'/'.$tipo_id.'/'.$nombre.'/'.$prueba_id.'/'.$pregunta;
    }

    public function deletePrueba()
    {
        $data['success'] = false;

        $prueba = $this->input->post('prueba');
        
        if($this->model->deletePrueba($prueba))
            $data['success'] = 'success';
    

        echo json_encode($data);
    }

}
