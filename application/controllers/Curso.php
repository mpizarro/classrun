<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once("secure_area.php");

class Curso extends Secure_area
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Curso_model', 'Evaluacion_model', 'Asignacion_model', 'Prueba_model'));
        $this->load->helper('promedios_helper');

        $this->promedios = new PromediosHelper();
        $this->cursoModel = new Curso_model();
        $this->evaluacionModel = new Evaluacion_model();
        $this->asignacionModel = new Asignacion_model();
        $this->pruebaModel = new Prueba_model();
    }
    public function getAlumnosCursoById($idCurso, $idEvaluacion, $checkStatus = 1)
    {
        $return['status'] = 'error';
        $return['alumnos'] = [];

        if ($this->asignacionModel->verificaPruebaAsignadaAnterior($idEvaluacion, $idCurso))
        {
            $return['status'] = 'prueba_asignada_al_curso';
            echo json_encode($return);
            return;
        }
        // if (!$this->asignacionModel->cursoHasAsignacion($idCurso)) {

            $alumnos = $this->cursoModel->getAlumnosCursoById($idCurso);
            $evaluacion = $this->evaluacionModel->getEvaluacionWithoutAsignacion($idEvaluacion);

            $i = 0;

            $hojasDeRespuestasAlumno = [];
            $dataAlumnos = [];

            $return = array('status' => false);

            if ($alumnos && $evaluacion) {
                foreach ($alumnos as $alumno) {
                    if ($usuarioAsig = $this->asignacionModel->getUsuarioHasAsignacionByEvaluacionByUsuarioWithoutAsignacion($idEvaluacion, $alumno->id)) {
                        $fechaInicio = new DateTime(date('Y-m-d h:i:s', $usuarioAsig->inicio));
                        $fechaFin = new DateTime(date('Y-m-d h:i:s', $usuarioAsig->fin));
                        if (!$usuarioAsig->fin) {
                            $fechaFin = new DateTime(date('Y-m-d h:i:s'));
                        }
                        if (!$usuarioAsig->inicio) {
                            $fechaInicio = new DateTime(date('Y-m-d h:i:s'));
                        }

                        $diff = $fechaInicio->diff($fechaFin);
                        $dataAlumnos[$alumno->id]['tiempo'] = ($diff->h < 10 ? "0$diff->h" : $diff->h).':'.($diff->i < 10 ? "0$diff->i" : $diff->i).':'.($diff->s < 10 ? "0$diff->s" : $diff->s);
                        $dataAlumnos[$alumno->id]['estado'] = $usuarioAsig->estado;
                    }

                    $hojasDeRespuestas = $this->evaluacionModel->checkPrevNotasAlumno($alumno->id, $evaluacion, $checkStatus);

                    if (count($hojasDeRespuestas) > 0) {
                        $hojasDeRespuestasAlumno[$alumno->id] = $hojasDeRespuestas;
                        $i++;
                    } else {
                        $hojasDeRespuestasAlumno[$alumno->id] = array();
                    }
                }
                if ($i > 0) {
                    $asignaciones = array();
                    $evaluaciones = array();
                    foreach ($hojasDeRespuestasAlumno as $idAlumno => $hojasRespuestasAsignacion) {
                        foreach ($hojasRespuestasAsignacion as $idAsignacion => $hojaRespuesta) {
                            if (!array_search($idAsignacion, $asignaciones)) {
                                $asignaciones[] = $idAsignacion;
                            }
                        }
                    }
                    $return['evaluaciones'] = $this->evaluacionModel->getEvaluacionByAsignacion($asignaciones);
                    $return['hojasDeRespuestasAlumno'] = $hojasDeRespuestasAlumno;
                    $return['dataAlumnos'] = $dataAlumnos;
                }
                $return['status'] = 'success';
                $return['alumnos'] = $alumnos;
            }
        // } else {
        //     $return['status'] = 'curso_has_asignaciones';
        // }

        echo json_encode($return);
    }

    public function getCursoById($idCurso, $idEvaluacion, $checkStatus = 1, $id_asignacion)
    {
        $return['status'] = 'error';
        $return['alumnos'] = [];

        $alumnos = $this->cursoModel->getAlumnosCursoById($idCurso);
        $evaluacion = $this->evaluacionModel->getEvaluacion($idEvaluacion, 'AEDUC', $id_asignacion);

        $i = 0;

        $hojasDeRespuestasAlumno = [];
        $dataAlumnos = [];

        $return = array('status' => false);

        if ($alumnos && $evaluacion) {
            foreach ($alumnos as $alumno) {
                if ($usuarioAsig = $this->asignacionModel->getUsuarioHasAsignacionAllByEvaluacionByUsuario($idEvaluacion, $alumno->id, 'AEDUC', $id_asignacion)) {
                    $fechaInicio = $usuarioAsig->inicio ? new DateTime(date('Y-m-d h:i:s', $usuarioAsig->inicio)) : new DateTime(date('Y-m-d h:i:s'));
                    $fechaFin = $usuarioAsig->fin ? new DateTime(date('Y-m-d h:i:s', $usuarioAsig->fin)) : new DateTime(date('Y-m-d h:i:s'));

                    $diff = $fechaInicio->diff($fechaFin);
                    $dataAlumnos[$alumno->id]['tiempo'] = ($diff->h < 10 ? "0$diff->h" : $diff->h).':'.($diff->i < 10 ? "0$diff->i" : $diff->i).':'.($diff->s < 10 ? "0$diff->s" : $diff->s);
                    $dataAlumnos[$alumno->id]['estado'] = $usuarioAsig->estado;
                }

                $hojasDeRespuestas = $this->evaluacionModel->checkPrevNotasAlumno($alumno->id, $evaluacion, $checkStatus);

                if (count($hojasDeRespuestas) > 0) {
                    $hojasDeRespuestasAlumno[$alumno->id] = $hojasDeRespuestas;
                    $i++;
                } else {
                    $hojasDeRespuestasAlumno[$alumno->id] = array();
                }
            }
            if ($i > 0) {
                $asignaciones = array();
                $evaluaciones = array();
                foreach ($hojasDeRespuestasAlumno as $idAlumno => $hojasRespuestasAsignacion) {
                    foreach ($hojasRespuestasAsignacion as $idAsignacion => $hojaRespuesta) {
                        if (!array_search($idAsignacion, $asignaciones)) {
                            $asignaciones[] = $idAsignacion;
                        }
                    }
                }
                $return['evaluaciones'] = $this->evaluacionModel->getEvaluacionByAsignacion($asignaciones);
                $return['hojasDeRespuestasAlumno'] = $hojasDeRespuestasAlumno;
            }
            $return['dataAlumnos'] = $dataAlumnos;
            $return['status'] = 'success';
            $return['alumnos'] = $alumnos;
        }

        echo json_encode($return);
    }

    public function cursoHasAsignacion($idCurso) {
        $hasAsignacion = $this->asignacionModel->cursoHasAsignacion($idCurso);
        echo json_encode(['has_asignacion' => $hasAsignacion]);
    }

    public function getDataPlanillaAlumno()
    {
        $idCurso        = $this->input->post('idCurso');
        $idEvaluacion   = $this->input->post('idEvaluacion');
        $idAsignacion   = $this->input->post('idAsignacion');
        $origen         = $this->input->post('origen');

        $data = [
            'alumnos' => [],
            'curso' => FALSE,
            'evaluacion' => FALSE
        ];

        if ($curso = $this->cursoModel->getCursoById($idCurso))
            $data['curso'] = ['curso' => $curso->curso, 'letra' => $curso->letra];

        if ($evaluacion = $this->evaluacionModel->getEvaluacion($idEvaluacion, $origen, $idAsignacion))
            $data['evaluacion'] = ['tipo' => $evaluacion->tipo, 'nivel' => $evaluacion->nivel, 'asignatura' => $evaluacion->asignatura];

        if ($alumnos = $this->cursoModel->getAlumnosCursoById($idCurso)) {
            foreach ($alumnos as $alumno) {
                $avance = 0;
                $tiempo = '00:00:00:00';
                $estado = 0;

                if ($preguntasRespondidas = $this->asignacionModel->getCountRespondidaByAlumnoByAsignacion($idAsignacion, $alumno->id)) {
                    $estado = $preguntasRespondidas->estado;
                    $nPregunta = $this->pruebaModel->getCountPreguntasPruebaByAsignacion($idAsignacion, $origen);

                    $avance = $nPregunta > 0 ? round(($preguntasRespondidas->avance * 100) / $nPregunta, 0) : 0;

                    $inicio = new DateTime(date('Y-m-d h:i:s'));
                    $fin = new DateTime(date('Y-m-d h:i:s'));

                    if ($preguntasRespondidas->inicio)
                        $inicio = new DateTime(date('Y-m-d h:i:s', $preguntasRespondidas->inicio));
                    if ($preguntasRespondidas->fin)
                        $fin = new DateTime(date('Y-m-d h:i:s',  $preguntasRespondidas->fin));

                    $diff = $fin->diff($inicio);
                    $tiempo = ($diff->h < 10 ? "0$diff->h" : $diff->h) . ':' .
                                ($diff->i < 10 ? "0$diff->i" : $diff->i) . ':' .
                                ($diff->s < 10 ? "0$diff->s" : $diff->s);
                }

                $data['alumnos'][] = [
                    'id' => $alumno->id,
                    'nombre' => $alumno->nombre . ' ' . $alumno->apellido,
                    'rut' => $alumno->username,
                    'estado' => $estado,
                    'avance' => $avance,
                    'tiempo' => $tiempo
                ];
            }
        }

        echo json_encode($data);
    }

}
