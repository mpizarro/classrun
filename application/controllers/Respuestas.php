<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once ("secure_area.php");

class Respuestas extends Secure_area
{
	private $respuestaModel;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Respuesta_model');

		$this->respuestaModel = new Respuesta_model();
	}
	public function guardarAlternativa()
	{
		$data = json_decode($this->input->post('data'));
		$response['response'] = "success";
		if($alternativas = $this->respuestaModel->getAlternativasPregunta($data->idPregunta))
		{
			foreach ($alternativas as $alt) 
			{
				if($alt->correcta == "t" && $data->correcta == "t")
					$response['response'] = "exist-correcta";
			}
		}

		if($response['response'] == "success")
		{
			$dataInsert = array(
				'respuesta' => $data->alternativa,
				'pregunta_id' => $data->idPregunta,
				'correcta' => $data->correcta ? "t" : "f",
				'puntaje' => 0
			);
			$response['response'] = $this->respuestaModel->guardarAlternativa($dataInsert);
		}

		echo json_encode($response);
	}
	public function actualizarAlternativa($idAlternativa, $idPregunta)
	{
		$data = json_decode($this->input->post('data'));
		$dataUpdate = array(
			'respuesta' => $data->alternativa,
			'correcta' => $data->correcta ? "t" : "f"
		);

		$response['response'] = "false";
		if($alternativas = $this->respuestaModel->getAlternativasPregunta($idPregunta))
		{
			foreach ($alternativas as $alt) 
			{
				if($alt->correcta == "t" && $data->correcta == "t")
					$response['response'] = "exist-correcta";
			}
		}

		if($response['response'] == "false")
		{
			if($this->respuestaModel->actualizarAlternativa($dataUpdate, $idAlternativa))
				$response['response'] = "update";
				
		}

		echo json_encode($response);
	}
	public function eliminarAlternativa()
	{
		$idAlternativa = $this->input->post('id');
		$response['response'] = $this->respuestaModel->eliminarAlternativa($idAlternativa);
		echo json_encode($response);
	}
	public function getAlternativa()
	{
		$idAlternativa = $this->input->post('id');
		$data = array();
		if($alternativa = $this->respuestaModel->getAlternativaId($idAlternativa))
		{
			$data = array(
				'contenido' => $alternativa->respuesta,
				'is_correcta' => $alternativa->correcta,
				'correcta' => $alternativa->correcta == "t" 
					? '<span class="label label-rouded label-success">CORRECTA</span>'
					: '<span class="label label-rouded label-danger">INCORRECTA</span>'
			);
		}
		echo json_encode($data);
	}
}
