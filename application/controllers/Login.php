<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Jasny\SSO\NotAttachedException;
use Jasny\SSO\Exception as SsoException;

require_once __DIR__ . '/../../vendor/autoload.php';

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('usuario_model', 'usuarioModel');
        $this->load->helper('core_helper');
    }

    public function index()
    {
        
        $perfil = $this->session->userdata('perfil_id');

        if ($perfil) {
            switch ($perfil) {
                case 1:
                    redirect('administrador/evaluaciones');
                    break;
                case 2:
                    redirect('home');
                    break;
                case 3:
                    redirect('alumno');
                    break;
                case 5:
                    redirect('asignaciones_sostenedor');
                    break;
                default:
                    redirect('home');
                    break;
            }
        }

        
        $this->load->view('login/login');
    }

    public function Rbd($userId)
    {
        $user = $this->usuarioModel->getRbd($userId);
        return $user->rbd;
    }

    public function login($user)
    {
        $this->session->set_userdata('rut', $user->username);
        $this->session->set_userdata('logged', true);
        $this->session->set_userdata('idUser', $user->id);
        $this->session->set_userdata('nombre_completo', $user->nombre . ' ' . $user->apellido);
        $this->session->set_userdata('perfil_id', $user->perfil_id);
        $this->session->set_userdata('colegio_id', $user->colegio_id);
        $this->session->set_userdata('curso_id', $user->curso_id);
        
        if($user->perfil_id != 1)
            $this->session->set_userdata('rbd', $this->Rbd($user->id));
    }

    public function validar()
    {
        $username = $this->input->post("username");     
        $password = $this->input->post("password");     

        
        if($user = $this->usuarioModel->login($username,$password))
            {
                $response['status'] = 'success';
                $this->login($user);
            }
        else
        {
            $response['status'] = 'error';
            //$this->login();
        }
        
        echo json_encode($response);
    }
    public function logout()
    {
        
        $this->session->sess_destroy();
        redirect(base_url());
    }
}
