<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once ("secure_area.php");

class User extends Secure_area
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Usuario_model');
    }
    public function ChangePassword()
    {
        $idUser = $this->session->userdata('idUser');
        $contrasena = $this->input->post('a');    
        $contranew  = $this->input->post('c');        
        
        $actualPass = $this->usuario_model->getPassword($idUser);
        $actualPass = $actualPass[0]->contrasena;
        $data['success'] = FALSE;

        if(md5($contrasena) === $actualPass)
        {
            $passnew = md5($contranew); 

           if($this->usuario_model->ChangePassword($passnew,$idUser))
                $data['success'] = 'success';
           else
                $data['success'] = FALSE;   
        }
        echo json_encode($data);

    }

    public function verificaPassword()
    {
        $return = false;

        $password = $this->input->post('password');

        $actualPass = $this->usuario_model->getPassword($this->session->idUser);
        $actualPass = $actualPass[0]->contrasena;

        if($password == $actualPass)
            $data['success'] = 'success';

        echo json_encode($data);
    }
}