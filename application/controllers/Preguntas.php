<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once ("secure_area.php");

class Preguntas extends Secure_area
{
	private $preguntaModel;
	private $evaluacionModel;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pregunta_model');
		$this->load->model('Evaluacion_model');

		$this->preguntaModel = new Pregunta_model();
		$this->evaluacionModel = new Evaluacion_model();
	}
	public function guardarPregunta()
	{
		$data = json_decode($this->input->post('data'));
		$arrPhp = array();
		if($pruebaHasPregunta = $this->evaluacionModel->getPruebaHasPregunta($data->idEvaluacion))
			foreach ($pruebaHasPregunta as $php)
			{
				$arrPhp[] = $php->orden;
			}
		$newOreden = count($arrPhp) > 0 ? max($arrPhp) + 1 : 1;
		$newCodigo = $data->codigo . "P" . ($newOreden > 9 ? $newOreden : "0" . $newOreden);
		$insertPHP = array(
			'prueba_id' => $data->idEvaluacion,
			'orden' => $newOreden
		);
		if ($data->eje)
			$insertPregunta['ejetematico_id'] = $data->eje;
		if ($data->aprendizaje)
			$insertPregunta['habilidadcurricular_id'] = $data->aprendizaje;

		if($data->ie)
			$insertPregunta['ie_id'] = $data->ie;

		if($data->unidad)
			$insertPregunta['unidad_id'] = $data->unidad;

		$insertPregunta['pregunta'] = $data->pregunta;
		$insertPregunta['codigo']  = $newCodigo;
		if($data->oa)
			$insertPregunta['oa_id'] = $data->oa;
		// if($data->taxonomia)
		$insertPregunta['taxonomia_id'] = null; //$data->taxonomia;

		$response = $this->preguntaModel->guardarPregunta($insertPregunta, $insertPHP);
		echo json_encode($response);
	}
	public function actualizarPregunta($idPregunta)
	{
		$data = json_decode($this->input->post('data'));

		$dataUpdate['pregunta'] = $data->pregunta;

		if($data->eje)
			$dataUpdate['ejetematico_id'] = (int)$data->eje;

		if($data->aprendizaje)
			$dataUpdate['habilidadcurricular_id'] = $data->aprendizaje;

		if(isset($data->ie))
			$dataUpdate['ie_id'] = $data->ie;

		if($data->unidad)
			$dataUpdate['unidad_id'] = $data->unidad;

		if(isset($data->oa))
			$dataUpdate['oa_id'] = $data->oa;

		$response['response'] = $this->preguntaModel->actualizarPregunta($dataUpdate, $idPregunta);
		echo json_encode($response);
	}
	public function eliminarPregunta()
	{
		$idPregunta = $this->input->post('id');
		$response['response'] = $this->preguntaModel->eliminarPregunta($idPregunta);
		echo json_encode($response);
	}
	public function getPregunta()
	{
		$idPregunta = $this->input->post('id');
		$data = array();
		if($pregunta = $this->preguntaModel->getPreguntaId($idPregunta))
		{
			$data = array(
				'taxonomia'     => $pregunta->taxonomia_id ? $pregunta->taxonomia_id: "",
				'eje'           => $pregunta->ejetematico_id,
				'aprendizaje'   => $pregunta->habilidadcurricular_id,
				'pregunta'      => $pregunta->pregunta,
				'ie'      		=> $pregunta->ie_id,
				'unidad'		=> $pregunta->unidad_id,
				'oa'			=> $pregunta->oa_id,
			);
		}

		echo json_encode($data);
	}
}
