<?php
class Asignaciones_sostenedor extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model([
            'colegio_model',
            'asignacion_model',
            'crearEvaluacion_model'
        ]);
    }

    public function index()
    {
        $this->load->library("pagination");

        $filter = (object) [
            'asignatura_id' => !empty($this->input->get('asignatura')) ? $this->input->get('asignatura') : FALSE,
            'tipo_id' => !empty($this->input->get('tipo')) ? $this->input->get('tipo') : FALSE,
            'estado_id' => !empty($this->input->get('estado')) ? $this->input->get('estado') : FALSE
        ];

        $data = [
            'js' => '<script src="'.base_url('assets/modulos/asignaciones/js/asignaciones_sostenedor.js').'"></script>',
            'asignaciones' => [],
            'colegios' => [],
            'asignaturas' => $this->crearEvaluacion_model->getAsignaturas()
        ];

        $usuario_id = $this->session->idUser;
        $rbd = !empty($this->input->get('colegio')) ? $this->input->get('colegio') : FALSE;

        if ($colegios = $this->colegio_model->getColegiosBySostenedor($usuario_id, $rbd))
            foreach ($colegios as $col) {
                $data['colegios'][] = (object)[
                    'nombre' => $col->nombre,
                    'rbd' => $col->rbd
                ];
                if ($asignaciones = $this->asignacion_model->getAllByColrgio($col->rbd, $filter))
                    foreach ($asignaciones as $asig) {
                        $asig->{'rbd'} = $col->rbd;
                        $asig->{'colegio'} = $col->nombre;
                        $asig->{'director'} = $col->director;
                        $asig->{'color_estado'} = '';
                        $asig->{'estado_id'} = $asig->estado;
                        $asig->{'estado'} = '';
                        switch ($asig->{'estado_id'}) {
                            case 1:$asig->{'estado'} = 'En ejecución';  $asig->{'color_estado'} = '#FFAA39'; break;
                            case 3:$asig->{'estado'} = 'Finalizada';  $asig->{'color_estado'} = '#52DD48'; break;
                        }
                        $asig->{'url_informe'} = site_url('/Reporte/') . strtolower($asig->tipo).'/'.$asig->id.'/'. ($asig->origen == 'PERSONAL' ? 'PERSONAL' : 'AEDUC') .'/'.$asig->rbd;

                        $data['asignaciones'][] = $asig;
                    }
            }
                        
        $this->load->view('layout/header', $data);
        $this->load->view('layout/menu');
        $this->load->view('modulos/sostenedor/asignaciones');
        $this->load->view('layout/footer');
    }
}
