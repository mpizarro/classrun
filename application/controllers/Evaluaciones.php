<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once("secure_area.php");

class Evaluaciones extends Secure_area
{
    private $evaluacionModel;
    private $preguntaModel;
    private $cursoModel;
    private $alumnoModel;
    private $nivelModel;
    private $asignacionModel;
    private $pruebaModel;

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('utilidades');

        $this->load->model('Evaluacion_model');
        $this->load->model('Curso_model');
        $this->load->model('Pregunta_model');
        $this->load->model('Alumno_model');
        $this->load->model('Nivel_model');
        $this->load->model('Asignacion_model');
        $this->load->model('Prueba_model');

        $this->preguntaModel = new Pregunta_model();
        $this->evaluacionModel = new Evaluacion_model();
        $this->cursoModel = new Curso_model();
        $this->alumnoModel = new Alumno_model();
        $this->nivelModel = new Nivel_model();
        $this->asignacionModel = new Asignacion_model();
        $this->pruebaModel = new Prueba_model();

        $this->load->model('Analisis_model', 'analisis');
    }
    public function index()
    {
        $this->analisis->set();

        $css = '<link href="' . base_url() . 'assets/modulos/evaluaciones/css/evaluaciones.css" rel="stylesheet" type="text/css" />';

        $this->load->view('modulos/evaluaciones/evaluaciones_header', array('css' => $css));
        $this->load->view('layout/menu');

        $cursos = $this->cursoModel->getCursosPosiblesUsuario($this->session->userdata['idUser']);

        $data = array(
            'cursos' => $cursos ? $cursos : [],
            'activa' => $this->getActivasUser($this->session->userdata['idUser'])
        );
        $this->load->view('modulos/evaluaciones/evaluaciones_vista', $data);
        $this->load->view('modulos/evaluaciones/evaluaciones_footer');
    }
    public function getServicios() {
        $data['servicios'] = [];

        if ($servicios = $this->evaluacionModel->getServicios($this->session->colegio_id))
            foreach ($servicios as $servicio)
                $data['servicios'][] = $servicio->id;

        echo json_encode($data);
    }
    public function getNivelesOpciones($idAsignatura, $numeroEvaluacion, $tipoEvaluacion)
    {
        $maxNivel = $minNivel = null;
        if ($nivel = $this->nivelModel->getMaxNivelByColegio($this->session->colegio_id)) {
            if ($nivel->min >= 1 && $nivel->max <= 8) {
                $maxNivel = 8;
                $minNivel = 1;
            } elseif ($nivel->min >= 9 && $nivel->max <= 12) {
                $maxNivel = 12;
                $minNivel = 9;
            } else {
                $maxNivel = 12;
                $minNivel = 1;
            }
        }


        $evaluaciones = $this->evaluacionModel->getNivelesEvaluacion($idAsignatura, $numeroEvaluacion, $tipoEvaluacion, $maxNivel, $minNivel);


        $return = array(
            'status' => $evaluaciones ? true : false,
            'data' => $evaluaciones
        );
        echo json_encode($return);
    }
    public function verEvaluacion($idEvaluacion)
    {
        $css = '<link href="' . base_url() . 'assets/modulos/evaluaciones/css/verEvaluacion.css" rel="stylesheet" type="text/css" />';

        $this->load->view('layout/header', array('css' => $css));
        $this->load->view('layout/menu');

        $data['preguntas'] = $this->getDataEvaluacion($idEvaluacion);

        $this->load->view('modulos/verEvaluacion/verEvaluacion_vista');
        $this->load->view('modulos/verEvaluacion/verEvaluacion_footer', $data);
    }
    public function ejectuarEvaluacion($idEvaluacion, $idCurso, $origen='CLASSRUN')
    {
        $status = 'error';
        if ($origen == 'PROFESOR') 
        {
            $origen = 'PERSONAL';
            
            $asignacion = $this->evaluacionModel->getAsignacionByPruebaCursoUsuario($idEvaluacion, $idCurso, $this->session->userdata['idUser'], $origen);
                if ($asignacion) {
                    if ($this->evaluacionModel->ejecutarAsignacion($asignacion->id)) {
                        if ($this->alumnoModel->asignarEvaluacion($asignacion->id))
                            $status = 'success';
                    }
                } else {
                    if ($asignacion = $this->evaluacionModel->crearAsignacion($idEvaluacion, $idCurso, $this->session->userdata['idUser'], $origen)) {
                        $insert = [];
                        if ($alumnos = $this->alumnoModel->getAlumnoByCurso($idCurso)) {
                            foreach ($alumnos as $alm) {
                                $insert[] = [
                                    'usuario_id' => $alm->id,
                                    'asignacion_id' => $asignacion,
                                    'estado' => 1
                                ];
                            }
                            if ($this->evaluacionModel->asignarUsuarioAsignacion($insert))
                                $status = 'success';
                        }
                    }
                }
            // } else {
            //     $status = 'has';
            // }
        } 
        else {
            $asignacion = $this->evaluacionModel->getAsignacionByPruebaCursoUsuario($idEvaluacion, $idCurso, $this->session->userdata['idUser']);
            if ($asignacion) {
                if ($this->evaluacionModel->ejecutarAsignacion($asignacion->id)) {
                    if ($this->alumnoModel->asignarEvaluacion($asignacion->id))
                        $status = 'success';
                }
            } else {
                if ($asignacion = $this->evaluacionModel->crearAsignacion($idEvaluacion, $idCurso, $this->session->userdata['idUser'], $origen)) {
                    $insert = [];
                    if ($alumnos = $this->alumnoModel->getAlumnoByCurso($idCurso)) {
                        foreach ($alumnos as $alm) {
                            $insert[] = [
                                'usuario_id' => $alm->id,
                                'asignacion_id' => $asignacion,
                                'estado' => 1
                            ];
                        }
                        if ($this->evaluacionModel->asignarUsuarioAsignacion($insert))
                            $status = 'success';
                    }
                }
            }
        }                
        echo json_encode(['status' => $status]);
    }
    public function finalizarEvaluacion($idEvaluacion)
    {
        $idUsuario = $this->session->userdata['idUser'];

        $this->evaluacionModel->setId($idEvaluacion);

        echo json_encode(array('status' => $this->evaluacionModel->finalizarEvaluacion($idUsuario)));
    }
    private function getDataEvaluacion($idEvaluacion, $origen="AEDUC")
    {
        $preguntas = $this->evaluacionModel->getPreguntasEvaluacion($idEvaluacion, $origen);

        if ($preguntas) {
            foreach ($preguntas as $pregunta) {
                $pregunta->pregunta = trim(preg_replace('/(\r\n)|\n|\r/', '<br/>', $pregunta->pregunta));
                $pregunta->pregunta = str_replace('"', '\\"', $pregunta->pregunta);

                $pregunta->{'alternativas'} = $this->evaluacionModel->getAlternativasPregunta($pregunta->id, $origen);
                if ($pregunta->alternativas) {
                    foreach ($pregunta->alternativas as $alternativa) {
                        $alternativa->respuesta = trim(preg_replace('/(\r\n)|\n|\r/', '<br/>', $alternativa->respuesta));
                        $alternativa->respuesta = str_replace('"', '\\"', $alternativa->respuesta);
                    }
                }
            }
        }
        return $preguntas;
    }
    private function getActivasUser($idUsuario)
    {
        return $this->evaluacionModel->getActivasUser($idUsuario) !== false ? true : false;
    }
    public function guardarEvaluacion()
    {
        $dataInsert = array(
            'codigo' 			=> $this->input->post('codigo'),
            'num' 				=> $this->input->post('numero'),
            'nivel_id' 			=> $this->input->post('nivel'),
            'asignatura_id' 	=> $this->input->post('asignatura'),
            'tipo_id' 			=> $this->input->post('tipo'),
            'anyo'				=> date('Y')
        );

        $response['response'] = $this->evaluacionModel->guardarEvaluacion($dataInsert);
        echo json_encode($response);
    }
    public function getEvaluacion()
    {
        $idEvaluacion = $this->input->post('id');
        $data = array();
        if ($evaluacion = $this->evaluacionModel->getEvaluacion($idEvaluacion)) {
            $data = array(

                'codigo' => $evaluacion->codigo,
                'numero' => $evaluacion->num,
                'nivel' => $evaluacion->nivel_id,
                'asignatura' => $evaluacion->asignatura_id,
                'tipo' => $evaluacion->tipo_id,
                'unidad' => $evaluacion->unidad_id
            );
        }
        echo json_encode($data);
    }
    public function editarEvaluacion($id)
    {
        $dataInsert = array(

            'codigo' 			=> $this->input->post('codigo'),
            'num' 				=> $this->input->post('numero'),
            'nivel_id' 			=> $this->input->post('nivel'),
            'asignatura_id' 	=> $this->input->post('asignatura'),
            'tipo_id' 			=> $this->input->post('tipo')
        );
        $response['response'] = $this->evaluacionModel->editarEvaluacion($dataInsert, $id);
        echo json_encode($response);
    }
    public function getImagenes()
    {
        $codigo = $this->input->post('codigo');
        $html = "<div class='row p-10' style='position: relative; white-space: normal; overflow: auto; height: 350px; border-left: 1px solid #c4c4c4;'>";
        $url = base_url("/ckfinder/userfiles/images/$codigo");
        $path = "ckfinder/userfiles/images/$codigo";
        $cont = 1;
        $flag = true;
        if (file_exists($path)) {
            $dirint = dir($path);
            while (($archivo = $dirint->read()) !== false) {
                if ($archivo != ".." && $archivo != ".") {
                    if ($cont == 1) {
                        $flag = true;
                        $html .= "<div style='width: 100%; height: auto; display: flex;'>";
                    }

                    $html .= "<div class='col-md-2' style='position: relative; white-space: normal; width: 18%; float: left; margin-left: 1.8%; margin-top: 15px;'>
						<img add-img src='$url/$archivo' class='image-add' alt='IMAGEN' style='width:100%!important; border: 1px solid #1d1c1c;'>
					</div>";

                    if ($cont == 5) {
                        $flag = false;
                        $html .= "</div>";
                        $cont = 0;
                    }
                    $cont ++;
                } else {
                    $html .= "<center><strong>No se encontraron imagenes para esta evaluación.<strong></center>";
                }
            }
            $dirint->close();
        } else {
            $html .= "<center><strong>No existe el directorio '$codigo'.<strong></center>";
        }

        if ($flag) {
            $html .= "</div>";
        }

        $html .= "</div>";
        echo json_encode(array('data'=>$html));
    }

    public function getSelectUnidad()
    {
        $post = $this->input->post();
        $options = "<option value='null' hidden>Seleccione...</option>";
        if ($unidades = $this->evaluacionModel->getUnidadesByNivelAsignatura($post['nivel'], $post['asignatura'])) {
            foreach ($unidades as $unidad) {
                $selected = null;
                if (isset($post['selected'])) {
                    $selected = $post['selected'];
                }

                $options .= "<option value='$unidad->id' ".($selected == $unidad->id ? "selected" : "").">Unidad $unidad->unidad</option>";
            }
        }
        echo json_encode(array('options'=>$options));
    }

    public function downloadEvaluacion($codigo)
    {
        $this->load->helper('download');
        $this->load->helper('file');
        $path = '/var/www/PRUEBAS/' . $codigo . '.pdf';
        $documents_file = file_get_contents($path);
        $name = $codigo . '.pdf';
        force_download($name, $documents_file);
    }
    
    public function descargarEvaluacionPDF($id_evaluacion)
    {
        $preguntas = $this->evaluacionModel->getPreguntasEvaluacion($idEvaluacion, $origen);

        if ($preguntas) {
            foreach ($preguntas as $pregunta) {
                $pregunta->pregunta = trim(preg_replace('/(\r\n)|\n|\r/', '<br/>', $pregunta->pregunta));
                $pregunta->pregunta = str_replace('"', '\\"', $pregunta->pregunta);

                $pregunta->{'alternativas'} = $this->evaluacionModel->getAlternativasPregunta($pregunta->id, $origen);
                
                if ($pregunta->alternativas) 
                {
                    foreach ($pregunta->alternativas as $alternativa) 
                    {
                        $alternativa->respuesta = trim(preg_replace('/(\r\n)|\n|\r/', '<br/>', $alternativa->respuesta));
                        $alternativa->respuesta = str_replace('"', '\\"', $alternativa->respuesta);
                    }
                }
            }
        }
        
        //return $preguntas;
        /*
        $evaluacion = $this->pruebaModel->get($id_evaluacion, 'PERSONAL');
        $data['img_head'] = getHeaderEvaluacion($evaluacion->asignatura_id, $evaluacion->tipo_id);
        $data['color'] = getColorAsignatura($evaluacion->asignatura_id);
        $data['preguntas'] = $this->getDataEvaluacion($id_evaluacion, 'PERSONAL');
        */
        $this->load->view('modulos/asignaciones/descargarEvaluacion', $data);
    }
}
