<!-- Page Content -->
<div id="page-wrapper">
   <div class="container-fluid">

   
       <div class="row tab">
            <div class="item-tab ok col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <label><div><i class="ti-check"></i></div >Crear evaluación</label>
            </div>
            <div class="item-tab active col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <label><div>2</div> Seleccionar <?= $tipo_id == 3 || $tipo_id == 4 ? 'OAs' : 'Ejes' ?></label>
            </div>
            <div class="item-tab col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <label><div>3</div> Gestionar preguntas</label>
            </div>
        </div>


        <div class="container-card m-t-20">
            <span class="flap-card" style="background: <?= $color ?>;"></span>
            <div class="content-card row" style="border: 2px solid <?= $color ?>;">
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <p>Nombre evaluación</p>
                    <h5><?=$nombre;?></h5>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <p>Tipo de evaluación</p>
                    <h5><?=$this->model->getTipoPrueba($tipo_id)->tipo;?></h5>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <p>Asignatura</p>
                    <h5 style="color: <?= $color ?>;"><?=$this->model->getAsignatura($asignatura_id)->asignatura;?></h5>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <p>Nivel</p>
                    <h5><?=$this->model->getNivel($nivel_id)->nivel;?></h5>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <p><?= $tipo_id == 3 || $tipo_id == 4 ? 'OAs' : 'Ejes' ?> Seleccionados</p>
                    <div class="row" oas-selected>
                    
                    </div>
                </div>
                <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1" style="display: flex; align-items: center;">
                    <button type="button" class="btn btn-ev btn-block" btn-continuar> Continuar </button>
                </div>
            </div>
        </div>

        <?php if ($tipo_id == 3 || $tipo_id == 4): ?>
            <div class="row m-t-10 m-b-20 title-oas">
                <h5> Seleccionar los OAs que desea evaluar :</h5>
            </div>

            <form id="form-oas" action="<?=site_url();?>/home/crear_prueba_set_oa" method="POST">
                <input type="hidden" name="asignatura_id" value="<?=$asignatura_id;?>">
                <input type="hidden" name="nivel_id" value="<?=$nivel_id;?>">
                <input type="hidden" name="tipo_id" value="<?=$tipo_id;?>">
                <input type="hidden" name="prueba_original" value="<?=$prueba_original;?>">
                <input type="hidden" name="prueba_id" value="<?=$prueba_id;?>">
                <input type="hidden" name="nombre" value="<?=$nombre;?>">

                <?php $count = 0; foreach ($oas as $oa): ?>
                    <?php if ($count == 0): ?>
                        <div class="row row-oas">
                    <?php endif ?>

                    <div class="container-oas col-xs-12 col-sm-3 col-md-3 col-lg-3 m-b-20">
                        <div class="container-panel">
                            <div class="head-panel" style="background: <?= in_array($oa->id, $oas_default) ? $color : '' ?>;">
                                <h5><?= $oa->codigo ?></h5>
                                <div class="checkbox <?= $colorClass ?>">
                                    <input value="<?= $oa->id ?>" oa="<?= $oa->codigo ?>" name="oas[]" id="check-<?= $oa->codigo ?>" check color="<?= $color ?>" type="checkbox" <?= in_array($oa->id, $oas_default) ? 'checked onclick="javascript: return false;"' : '' ?>>
                                    <label for="check-<?= $oa->codigo ?>"></label>
                                </div>
                            </div>
                            <div class="body-panel">
                                <div><?=$oa->oa;?></div>
                            </div>
                            <div class="row">
                                <small see-more="more" style="display: none">Ver más ...</small>
                            </div>
                        </div>
                    </div>

                    <?php $count++; if ($count == 4): $count = 0; ?>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>
            </form>
        <?php elseif ($tipo_id == 1 || $tipo_id == 2): ?>
            <div class="row m-t-10 m-b-20 title-oas">
                <h5> Seleccionar los Ejes que desea evaluar :</h5>
            </div>

            <form id="form-oas" action="<?=site_url();?>/home/crear_prueba_set_oa" method="POST">
                <input type="hidden" name="asignatura_id" value="<?=$asignatura_id;?>">
                <input type="hidden" name="nivel_id" value="<?=$nivel_id;?>">
                <input type="hidden" name="tipo_id" value="<?=$tipo_id;?>">
                <input type="hidden" name="prueba_original" value="<?=$prueba_original;?>">
                <input type="hidden" name="prueba_id" value="<?=$prueba_id;?>">
                <input type="hidden" name="nombre" value="<?=$nombre;?>">
                <input type="hidden" name="flag_eje" value="true">

                <?php $count = 0; foreach ($ejes as $eje): ?>
                    <?php if ($count == 0): ?>
                        <div class="row row-oas">
                    <?php endif ?>

                    <div class="container-oas col-xs-12 col-sm-3 col-md-3 col-lg-3 m-b-20">
                        <div class="container-panel" style="height: auto">
                            <div class="head-panel" style="background: <?= in_array($eje->id, $ejes_default) ? $color : '' ?>;">
                                <h5><?=$eje->ejetematico;?></h5>
                                <div class="checkbox <?= $colorClass ?>">
                                    <input value="<?= $eje->id ?>" name="ejes[]" id="check-<?= $eje->id ?>" eje="<?=$eje->ejetematico;?>" check-eje color="<?= $color ?>" type="checkbox" <?= in_array($eje->id, $ejes_default) ? 'checked onclick="javascript: return false;"' : '' ?>>
                                    <label for="check-<?= $eje->id ?>"></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $count++; if ($count == 4): $count = 0; ?>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>
            </form>
        <?php endif ?>
        
        <input type="hidden" id="color" value="<?=$color;?>">
    </div>
</div>

<script>
const tipo_id = '<?= $tipo_id ?>';
</script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>