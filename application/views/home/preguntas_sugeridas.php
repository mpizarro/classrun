<script src="<?php echo base_url() ?>assets/plugins/bower_components/jquery/dist/jquery.js"></script>
<script type="text/javascript">
    siteUrl = () => {
        return '<?= site_url(); ?>';
    }
</script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/form.js"></script>
<script type="text/javascript">
    function selecciona_oa(oa_id) {
        $.ajax({
            url: siteUrl + '/home/imprime_ie',
            type: 'POST',
            data: {
                oa_id: oa_id.value
            },
            success(e) {
                $('#ie_id').html(e);
            }
        });
    }

    function check_form() {
        var radio_vacio = 0;
        // bloquea submit con tecla enter
        $("form").bind("keypress", function(e) {
            if (e.keyCode == 13) {
                return false;
            }
        });
        var status = true;
        // revisión general formulario
        if (!Form.check())
            return false;
        $('input[name="correcta[]"]').each(function(i, e) {
            if ($('input[name="correcta[]"]').get(i).checked) {
                radio_vacio = 1;
            }
        });

        if (radio_vacio === 0) {
            alert('Debes seleccionar al menos una alternativa correcta');
            status = false;
            return false;
        }

        if (!status)
            return false;
        return Form.checkSend('¿Estás seguro que deseas crear la pregunta?');
    }

    function item_nuevo(tr) {
        n_items = $('textarea[name="respuesta[]"]').length;
        if (n_items > 10) {
            alert('La pregunta no puede tener más de 10 respuestas');
            Form.delJS(tr.childNodes[0].childNodes[0]);
            return false;
        }
    }
</script>

<?php

$letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S'];

?>
<!-- Page Content -->
<div id="page-wrapper ">
    <div class="container-fluid">


        <div class="row tab" style="display:none;">
            <div class="item-tab ok col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <label>
                    <div><i class="ti-check"></i></div>Crear evaluación
                </label>
            </div>
            <div class="item-tab ok col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <label>
                    <div><i class="ti-check"></i></div> Seleccionar <?= $tipo_id == 3 || $tipo_id == 4 ? 'OAs' : 'Ejes' ?>
                </label>
            </div>
            <div class="item-tab active col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <label>
                    <div>3</div> Gestionar preguntas
                </label>
            </div>
        </div>

        <div class="row m-t-20">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 container-breadcrumbs">                
                <h2 class="title" style="color: #61b7c1;font-size: 60px;font-weight: bold;">Preguntas sugeridas</h2>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                        <?php $data = '';
                        /*
                        if ($tipo_id == 3 || $tipo_id == 4) {
                            $data = $oas_id;
                        } else {
                            $data = $ejes_id;
                        } 
                        */
                        ?>

                        <a href="<?= site_url('/home/preguntas/'.$nivel_id.'/'.$asignatura_id.'/'.$tipo_id.'/TEST/'.$prueba_id) ?>" class="btn btn-ev btn-block btn-outline" style="color: #fff;background: #61b7c1;border-color: #61b7c1;font-size: 30px;font-weight: bold;">Volver</a>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <button type="submit" form="form-sugeridas" class="btn btn-ev btn-block" disabled style="color: #fff;background: #61b7c1;border-color: #61b7c1;font-size: 30px;font-weight: bold;">Agregar preguntas</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">

                <div class="panel-evaluacion">

                    <div class="p-20">

                        <?php if ($tipo_id == 3 || $tipo_id == 4) : ?>
                            <?php if (!empty($oas_id)) : ?>
                                <div class="row p-20 p-b-0">
                                    <div class="col-xs-12 label-preguntas-sug">
                                        Hay <span><?= count($preguntas) ?></span> preguntas sugeridas para tu evaluación
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php elseif ($tipo_id == 1 || $tipo_id == 2) : ?>
                            <?php if (!empty($ejes_id)) : ?>
                                <div class="row p-20 p-b-0">
                                    <div class="col-xs-12 label-preguntas-sug">
                                        Hay <span><?= count($preguntas) ?></span> preguntas sugeridas para tu evaluación
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>


                        <div class="row m-t-10" id="container_preguntas">
                            <form method="POST" id="form-sugeridas" action="<?= site_url(); ?>/home/traspasar_preguntas">

                                <input type="hidden" name="asignatura_id" value="<?= $asignatura_id; ?>">
                                <input type="hidden" name="nivel_id" value="<?= $nivel_id; ?>">
                                <input type="hidden" name="tipo_id" value="<?= $tipo_id; ?>">
                                <input type="hidden" name="prueba_id" value="<?= $prueba_id; ?>">
                                <?php 
                                if ($tipo_id == 3 || $tipo_id == 4)
                                {
                                    if(!isset($oas_id))
                                        $oas_id = '';
                                    echo ' <input type="hidden" id="oas" name="oas" value="'.$oas_id.'">';
                                }
                                else
                                    echo '<input type="hidden" id="ejes" name="ejes" value="'.$ejes_id.'">';
                                
                                if(isset($prueba_aeduc))
                                    echo '<input type="hidden" name="prueba_aeduc" value="'.$prueba_aeduc.'">';
                                
                                if(isset($nombre))
                                    echo '<input type="hidden" name="nombre" value="'.$nombre.'">';
                                
                                ?>
                                <?php foreach ($preguntas as $pregunta) : ?>

                                    <?php
                                    $pregunta_limpia = str_replace(["<div>", "</div>"], "", $pregunta->pregunta);
                                    
                                    ?>

                                    

                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 m-b-30 m-t-20">
                                        <div class="enc-pregunta" style="border-color: #b5b5b5">

                                            <div class="row row-badges m-l-10">
                                                <div style="width:98%">
                                                <?php
                                                
                                                if ($tipo_id == 3 || $tipo_id == 4)
                                                    echo '<span class="badges-pregunta" style="float:left;color: #b5b5b5; border-color: #b5b5b5">'.$pregunta->codigo.'</span>';
                                                else
                                                    echo '<span class="badges-pregunta" style="color: #b5b5b5; border-color: #b5b5b5">'.$pregunta->ejetematico.'</span>';
                                                
                                                ?>
                                                </div>    

                                                <span class="checkbox <?= $colorClass ?>" style="width:2%">

                                                    <?php $data = '';
                                                    /*
                                                    if ($tipo_id == 3 || $tipo_id == 4) 
                                                    {
                                                        $data = 'data-oa="' . $pregunta->oa . '"';
                                                    } else {
                                                        $data = 'data-eje="' . $pregunta->ejetematico_id . '"';
                                                    }
                                                    */
                                                    ?>

                                                    <input id="check-<?= $pregunta->id; ?>" color="#61b7c2" type="checkbox" <?= $data ?> name="pregunta_id[]" value="<?php echo $pregunta->id; ?>">
                                                    <label class="<?= $colorClass ?>" for="check-<?= $pregunta->id; ?>"></label>
                                                </span>
                                            </div>
                                            <div class="row row-pregunta m-l-10 m-t-5">
                                                <div name="pregunta" rows="2"><?= $pregunta_limpia; ?></div>
                                            </div>

                                        </div>

                                        <?php foreach ($this->model->getRespuestasFromPregunta($pregunta->id) as $key => $respuesta) : ?>

                                            <div class="container-resp row">
                                                <label class="leter-resp" style="border-color: '#61b7c2'; <?= $respuesta->correcta == 't' ? 'background:#61b7c2' : '' ?>">
                                                    <input type="radio" value="0" color="#61b7c2">
                                                    <span style="color: <?= $respuesta->correcta == 't' ? '#fff' : '#61b7c2' ?>"><?= $letters[$key]; ?></span>
                                                </label>
                                                <div class="respuesta">
                                                    <div rows="1"><?= $respuesta->respuesta ?></div>
                                                </div>
                                            </div>

                                        <?php endforeach; ?>

                                    </div>

                                <?php endforeach; ?>

                            </form>

                        </div>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

                <div class="panel-resumen">
                    <h4 class="panel-resumen-title <?= $colorClass ?>">Resumen</h4>
                    <ul class="panel-resumen-body">
                        <li>
                            <p>Nombre evaluación</p>
                            <h5><?= $nombre; ?></h5>
                        </li>
                        <li>
                            <p>Tipo de evaluación</p>
                            <h5><?= $this->model->getTipoPrueba($tipo_id)->tipo; ?></h5>
                        </li>
                        <li>
                            <p>Asignatura</p>
                            <h5 style="color: <?= $color ?>;"><?= $this->model->getAsignatura($asignatura_id)->asignatura; ?></h5>
                        </li>
                        <li>
                            <p>Nivel</p>
                            <h5><?= $this->model->getNivel($nivel_id)->nivel; ?></h5>
                        </li>
                        <li>
                            <p>OAs Seleccionados</p>
                            <div id="oas-selected">
                                <table>
                                    <?php if ($tipo_id == 3 || $tipo_id == 4) : ?>
                                        <?php foreach ($oas as $oa) : ?>
                                            <tr>
                                                <td style="width: 30%"><label class="name-oa m-r-20" oa="<?= $oa->codigo ?>"><?= $oa->codigo ?></label></td>
                                                <td style="width: 30%"><label class="count-oas" count="0" id="oa-<?= $oa->codigo ?>">0 preguntas</label></td>
                                                <td style="color: #61b7c2;" <label id="add-oa-<?= $oa->codigo ?>" count="0"></label></td>
                                            </tr>
                                        <?php endforeach ?>
                                    <?php elseif ($tipo_id == 1 || $tipo_id == 2) : ?>
                                        <?php foreach ($ejes as $eje) : ?>
                                            <tr>
                                                <td style="width: 50%"><label class="name-eje m-r-20" eje="<?= $eje->id ?>"><?= $eje->ejetematico ?></label></td>
                                                <td style="width: 25%"><label class="count-ejes" count="0" id="eje-<?= $eje->id ?>">0 preguntas</label></td>
                                                <td style="width: 25%; color: #61b7c2;"><label id="add-eje-<?= $eje->id ?>" count="0"></label></td>
                                            </tr>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </table>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>




<script src="<?php echo base_url() ?>assets/inverse/bootstrap/dist/js/bootstrap.js"></script>
<!-- Sidebar menu plugin JavaScript -->
<script src="<?php echo base_url() ?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.js"></script>
<!--Slimscroll JavaScript For custom scroll-->
<script src="<?php echo base_url() ?>assets/inverse/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url() ?>assets/inverse/js/waves.js"></script>
<!-- Sweet-Alert  -->
<script src="<?php echo base_url() ?>assets/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url()?>assets/inverse/js/custom.js"></script>
<script src="<?php echo base_url()?>assets/inverse/js/jasny-bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/plugins/bower_components/switchery/dist/switchery.js"></script>
<script src="<?php echo base_url()?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<script src="<?php echo base_url()?>assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="<?php echo base_url()?>assets/modulos/analisis/js/analisis.js"></script>

<script>
    const tipo_id = '<?= $tipo_id ?>';
    $(() => {
        url = window.location.href;
        let params = url.split('/');
        let countDataB64 = params[params.length - 1];
        let countData = JSON.parse(atob(countDataB64));

        if (tipo_id == '3' || tipo_id == '4') {
            for (const oa of countData) {
                textCount = oa.count == 1 ? `${ oa.count } pregunta` : `${ oa.count } preguntas`;
                $('#oas-selected').find(`#oa-${ oa.oa }`).html(textCount);
            }
        } else {
            for (const eje of countData) {
                textCount = eje.count == 1 ? `${ eje.count } pregunta` : `${ eje.count } preguntas`;
                $('#oas-selected').find(`#eje-${ eje.eje }`).html(textCount);
            }
        }

        $(document).on('click', 'input[type="checkbox"]', function() {
            let color = '#61b7c2';
            let textCount = '';

            if (tipo_id == 3 || tipo_id == 4) {
                let oa = $(this).attr('data-oa');
                let count = Number($(`#add-oa-${ oa }`).attr('count'));

                if ($(this).prop('checked')) {
                    count++;
                    $(this).siblings('label').attr('check', 'true');
                    $(this).parents('.enc-pregunta').css({
                        'border-color': '#61b7c2'
                    });
                    $(this).parents('.enc-pregunta').find('.badges-pregunta').css({
                        'border-color': '#61b7c2',
                        'color': '#61b7c2'
                    });
                } else {
                    count--;
                    $(this).siblings('label').attr('check', 'false');
                    $(this).parents('.enc-pregunta').css({
                        'border-color': '#b5b5b5'
                    });
                    $(this).parents('.enc-pregunta').find('.badges-pregunta').css({
                        'border-color': '#b5b5b5',
                        'color': '#b5b5b5'
                    });
                }

                if (count == 1) textCount = `+ ${ count } pregunta`;
                if (count > 1) textCount = `+ ${ count } preguntas`;

                if (count > 0) $(`label.name-oa[oa='${ oa }']`).css({
                    'color': '#61b7c2'
                });
                else $(`label.name-oa[oa='${ oa }']`).css({
                    'color': '#797979'
                });

                $(`#add-oa-${ oa }`).attr('count', count);
                $(`#add-oa-${ oa }`).html(textCount);
            } else {
                let eje = $(this).attr('data-eje');
                let count = Number($(`#add-eje-${ eje }`).attr('count'));

                if ($(this).prop('checked')) {
                    count++;
                    $(this).siblings('label').attr('check', 'true');
                    $(this).parents('.enc-pregunta').css({
                        'border-color': '#61b7c2'
                    });
                    $(this).parents('.enc-pregunta').find('.badges-pregunta').css({
                        'border-color': '#61b7c2',
                        'color':'#61b7c2'
                    });
                } else {
                    count--;
                    $(this).siblings('label').attr('check', 'false');
                    $(this).parents('.enc-pregunta').css({
                        'border-color': '#b5b5b5'
                    });
                    $(this).parents('.enc-pregunta').find('.badges-pregunta').css({
                        'border-color': '#b5b5b5',
                        'color': '#b5b5b5'
                    });
                }

                if (count == 1) textCount = `+ ${ count } pregunta`;
                if (count > 1) textCount = `+ ${ count } preguntas`;

                if (count > 0) $(`label.name-eje[eje='${ eje }']`).css({
                    'color': color
                });
                else $(`label.name-eje[eje='${ eje }']`).css({
                    'color': '#797979'
                });

                $(`#add-eje-${ eje }`).attr('count', count);
                $(`#add-eje-${ eje }`).html(textCount);
            }

            if ($(document).find('input[type="checkbox"]:checked').length > 0) $('[form="form-sugeridas"]').removeAttr('disabled');
            else $('[form="form-sugeridas"]').attr('disabled', 'disabled');
        });
    })
</script>

</body>

</html>
