<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">

        <div class="row m-t-20">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <h4 class="m-0 title">Mis evaluaciones</h4>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                <button class="btn btn-ev" btn-open-modal-crear><i class="ti-plus"></i> Crear evaluación</button>
            </div>
        </div>

        <div class="white-box p-0 p-b-20" style="box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);">
            <div class="row border-radius-5 m-t-20 p-10 p-l-20 p-r-20 p-t-20 p-b-0">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                    <form action="<?= site_url('/home/index') ?>" method="get">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nombreEvaluacion" style="font-weight: 400; margin-bottom: 10px">Filtrar por:</label>
                                    <select name="asignatura" id="" class="form-control">
                                        <option value='' hidden>Asignatura</option>
                                        <option value=''>Todo</option>
                                        <?php foreach ($asignaturas as $asignatura) : ?>
                                            <option value='<?= $asignatura->id ?>' <?= $this->input->get('asignatura') == $asignatura->id ? 'selected' : '' ?>><?= $asignatura->asignatura ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group" style="margin-top: 30px;">
                                    <select name="nivel" id="" class="form-control">
                                        <option value='' hidden>Nivel</option>
                                        <option value=''>Todo</option>
                                        <?php foreach ($niveles as $nivel) : ?>
                                            <option value='<?= $nivel->id ?>' <?= $this->input->get('nivel') == $nivel->id ? 'selected' : '' ?>><?= $nivel->nivel ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group" style="margin-top: 30px;">
                                    <select name="tipo" id="" class="form-control">
                                        <option value='' hidden>Tipo</option>
                                        <option value=''>Todo</option>
                                        <?php foreach ($pruebas as $prueba) : ?>
                                            <option value='<?= $prueba->id ?>' <?= $this->input->get('tipo') == $prueba->id ? 'selected' : '' ?>><?= $prueba->tipo ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-ev btn-outline p-l-20 p-r-20" style="padding: 4px; margin-top: 30px;">Filtrar</button>
                        </div>
                    </form>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="col-md-offset-3 col-md-9">
                        <div class="form-group">
                            <label for="nombreEvaluacion" style="font-weight: 400; margin-bottom: 10px">Buscar</label>
                            <div class="container-search">
                                <div class="flap-search"><i class="ti-search"></i></div>
                                <input type="text" class="form-control search" id="search" placeholder="Buscar">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive" style="border: 0">
                <div class="shadow-xs hidden-sm hidden-md hidden-lg"></div>
                <div class="shadow-sm hidden-xs hidden-md hidden-lg"></div>
                <div class="shadow-md hidden-xs hidden-sm hidden-lg"></div>
                <div class="shadow-lg hidden-xs hidden-sm hidden-md"></div>
                <table id="table-evaluaciones">
                    <thead>
                        <tr>
                            <th style="width: 150px">Fecha</th>
                            <th style="width: 350px">Nombre</th>
                            <th style="width: 200px">Asignatura</th>
                            <th style="width: 130px">Nivel</th>
                            <th style="width: 130px">Tipo</th>
                            <th style="width: 180px">Estado</th>
                            <th style="width: 130px">N° preguntas</th>
                            <th style="width: 150px">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($contenidos as $key => $contenido) : ?>
                            <tr>
                                <td><?= fecha_mis_evaluaciones ($contenido->fecha); ?></td>
                                <td><?= $contenido->nombre; ?></td>
                                <td style="color: <?= getColorAsignatura($contenido->asignatura_id)['color'] ?>"><?= $contenido->asignatura; ?></td>
                                <td><?= $contenido->nivel; ?></td>
                                <td><?= $contenido->tipo; ?></td>
                                <td>
                                    <?php if ($contenido->creada == 't') : ?>
                                        <p class="text-evalua"><i class="fa fa-circle icon-estado"></i> Lista para aplicar</p>
                                    <?php else : ?>
                                        <p class="text-clear"><i class="fa fa-circle icon-estado"></i> Borrador</p>
                                    <?php endif ?>
                                </td>
                                <td><?= $contenido->count_pregunta ?></td>
                                <td class="text-center">
                                <?php if ($contenido->creada == 't'): ?>
                                    <button title="Asignar" class='m-l-10 btn btn-ev' btn-asignar id-evaluacion='<?= $contenido->id ?>'><i class="ti-file"></i></button>
                                    <button class='m-l-10 btn btn-default btn-outline' btn-ver-evaluacion id-evaluacion="<?= $contenido->id ?>"> <i class="ti-eye"></i></button>
                                <?php else: ?>
                                    <?php $prueba_original = '';
                                    if ($contenido->prueba_original)
                                        $prueba_original = $contenido->prueba_original;

                                    $base_64url = base64_encode("$contenido->id,$contenido->nivel_id,$contenido->asignatura_id,$contenido->tipo_id,$prueba_original,$contenido->nombre"); ?>
                                    <a title="Continuar" href="<?= site_url('/home/crear/'.$base_64url) ?>" class="m-l-10 btn btn-ev"><i class="ti-pencil"></i></a>
                                <?php endif ?>
                                    <!-- <button class='m-l-10 btn btn-default btn-outline'> <i class="ti-pencil"></i></button> -->
                                    <!-- <button class='m-l-10 btn btn-danger btn-outline'> <i class="fa fa-trash-o"></i></button> -->
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div id="paginador">

        </div>
    </div>
</div>

<!-- Modals -->
<!-- Modal asignar curso -->
<div id="modal-asignar-curso" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Asignar evaluación</h4>
            </div>
            <div class="modal-body">
                <form id="form-asignar-curso" action="javascript:void(0)" method="post">
                    <div class="form-group">
                        <label for="select-niveles">Niveles</label>
                        <select class="form-control" id="select-niveles" name="nivel">
                            <?php if ($nivelesColegio) : ?>
                                <option value="null" hidden>Selecciona un nivel</option>
                                <?php foreach ($nivelesColegio as $nivel) : ?>
                                    <option value="<?= $nivel->id ?>"><?= $nivel->nivel ?></option>
                                <?php endforeach ?>
                            <?php else : ?>
                                <option value="null" hidden>No existen niveles</option>
                            <?php endif ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="select-cursos">Selecciona un curso</label>
                        <select class="form-control" id="select-cursos" name="cursos">

                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect" btn-asignar-curso disabled>Asignar</button>
                <button type="button" class="btn btn-info waves-effect" btn-cerrar-modal-asignar-curso data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Modal crear -->
<div id="modal-crear" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-evalua p-10">
                <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true"><i class="ti-close"></i></button>
                <h4 class="modal-title text-white"><i class="ti-plus"></i> Crear evaluación</h4>
            </div>
            <div class="modal-body p-30 m-l-20 m-r-20">
                <form action="<?= site_url(); ?>/home/crear_prueba" method="POST" id="form-crear">
                    <div class="form-group">
                        <label for="nombreEvaluacion">Nombre de la evaluación</label>
                        <input type="text" class="form-control" name="nombre" required placeholder="Ingrese el nombre de la prueba" value="">
                    </div>
                    <div class="form-group">
                        <label for="tipoEvaluacion">Tipo de evaluación</label>
                        <select name="tipo_id" class="form-control" required>
                            <option value='' hidden>Seleccione un opción</option>
                            <?php foreach ($pruebas as $prueba) : ?>
                                <option value='<?= $prueba->id ?>'><?= $prueba->tipo ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="NivelEvaluacion">Nivel</label>
                        <select name="nivel_id" id="nivel_id" class="form-control" required>
                            <option value='' hidden>Seleccione un opción</option>
                            <?php foreach ($niveles as $nivel) : 
                                if($nivel->nivel != "Kinder" && $nivel->nivel != "Prekinder"){
                                ?>
                                <option value='<?= $nivel->id ?>'><?= $nivel->nivel ?></option>
                                <?php } endforeach ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="asignaturaEvaluacion">Asignatura</label>
                        <select name="asignatura_id" id= "asignatura_id" class="form-control" required>
                            <option value='' hidden>Seleccione un opción</option>
                            <?php foreach ($asignaturashasnivel as $asignaturahasnivel) : ?>
                            <option value='<?= $asignaturahasnivel->id ?>'><?= $asignaturahasnivel->asignatura ?></option>
                            <?php endforeach ?>;

                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer" style="border: 0">
                <button type="button" class="btn btn-default btn-outline waves-effect" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-ev waves-effect" form="form-crear">Crear</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>