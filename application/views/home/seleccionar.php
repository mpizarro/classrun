<!-- Page Content -->
<div id="page-wrapper">
   <div class="container-fluid">


   <div class="row tab">
            <div class="item-tab active col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <label><div>1</div >Crear evaluación</label>
            </div>
            <div class="item-tab col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <label><div>2</div> Seleccionar OAs</label>
            </div>
            <div class="item-tab col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <label><div>3</div> Gestionar preguntas</label>
            </div>
        </div>


        <div class="container-card m-t-20">
            <span class="flap-card" style="background: <?= $color ?>;"></span>
            <div class="content-card row" style="border: 2px solid <?= $color ?>;">
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <p>Nombre evaluación</p>
                    <h5><?=$nombre;?></h5>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <p>Tipo de evaluación</p>
                    <h5><?=$this->model->getTipoPrueba($tipo_id)->tipo;?></h5>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <p>Asignatura</p>
                    <h5 style="color: <?= $color ?>;"><?=$this->model->getAsignatura($asignatura_id)->asignatura;?></h5>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <p>Nivel</p>
                    <h5><?=$this->model->getNivel($nivel_id)->nivel;?></h5>
                </div>
            </div>
        </div>


        <div class="row m-t-30">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 p-r-20">
                <div class="container-panel">
                    <div class="head-panel">
                        <h5>Crear prueba a partir de una evaluación</h5>
                    </div>
                    <div class="body-panel">
                        <h5>Seleccione evaluación</h5>
                        <div class="table-responsive">
                            <table>
                                <thead>
                                    <tr>
                                        <th colspan="2">Nombre evaluación</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($pruebas_aeduc as $key => $prueba): ?>
                                    <tr tr-prueba id-prueba="<?=$prueba->id;?>">
                                        <td><img src="<?= base_url('assets/images/pdf.png') ?>" alt="PDF"> <?=$prueba->asignatura;?> <?=$prueba->nivel;?> <?=$prueba->tipo;?> <?=$prueba->num;?> (<?=$prueba->codigo;?>)</td>
                                        <td><i class="icon-eye"></i> Ver evaluación</td>
                                    </tr>
                                <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="row m-t-40 m-b-20">
                            <div class="col-xs-12 col-sm-offset-4 col-sm-4 col-md-offset-4 col-md-4 col-lg-offset-4 col-lg-4">
                                <button btn-usar-evaluacion class="btn btn-ev btn-block">Usar evaluación</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 p-0 p-l-20">
                <div class="container-panel">
                    <div class="head-panel" style="background: <?= $color ?>;">
                        <h5>Crear evaluación desde cero</h5>
                    </div>
                    <div class="body-panel">
                        <p class="text-center">Con el gestor de preguntas puedes crear tu evaluación personalizada desde cero</p>

                        <div class="row m-t-40 m-b-20" style="margin-top: 234px!important;">
                            <div class="col-xs-12 col-sm-offset-4 col-sm-4 col-md-offset-4 col-md-4 col-lg-offset-4 col-lg-4">
                                <button btn-crear-evaluacion class="btn btn-ev btn-block" style="border-color: <?= $color ?>;display:flex;justify-content:space-between;background:<?= $color ?>;">
                                    <i class="ti-plus" style="font-size:20px;"></i> Crear evaluación
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <input type="hidden" id="asignatura_id" value="<?=$asignatura_id;?>">
        <input type="hidden" id="nivel_id" value="<?=$nivel_id;?>">
        <input type="hidden" id="tipo_id" value="<?=$tipo_id;?>">
        <input type="hidden" id="nombre" value="<?=$nombre;?>">

    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
