<script src="<?php echo base_url()?>assets/plugins/bower_components/jquery/dist/jquery.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script type="text/javascript">
    var siteUrl = () => {
        return '<?= site_url();?>';
    }
    var asignatura  = "<?= $asignatura_id ?>";
    var nivel       = "<?= $nivel_id ?>";
    var tipo        = "<?= $tipo_id ?>";
    var nombre      = "<?= $nombre ?>";

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000
    });
</script>
<script type="text/javascript" src="<?=base_url();?>assets/js/form.js"></script>
<script type="text/javascript">

    $(() => {
        switch ('<?= $tipo_id ?>') {
            case '3':
                $('select[name="oa_id"], select[name="ie_id"]').prop('required', true);
                $('[row-ejes], [row-habilidad]').css({'display': 'none'});
                break;
            case '4':
                $('select[name="oa_id"], select[name="ie_id"]').prop('required', true);
                $('[row-ejes], [row-habilidad]').css({'display': 'none'});
                break;
            case '2':
            case '1':
                $('select[name="ejetematico_id"], select[name="habilidadcurricular_id"]').prop('required', true);
                $('[row-oas], [row-ies]').css({'display': 'none'});
                break;
        }
    });

    function selecciona_oa(oa_id)
    {
        $.ajax({
            url: siteUrl() + '/home/imprime_ie',
            type: 'POST',
            data : {
                oa_id : oa_id.value
            },
            success(e) {
                $('#ie_id').html(e);
            }
        });
    }

    function check_form()
    {
        var radio_vacio = 0;
        // bloquea submit con tecla enter
        $("form").bind("keypress", function(e) {
            if (e.keyCode == 13) {
                return false;
            }
        });
        var status = true;
        // revisión general formulario
        if (!Form.check())
            return false;
        $('input[name="correcta[]"]').each(function (i, e) {
            if ($('input[name="correcta[]"]').get(i).checked) {
                radio_vacio = 1 ;
            }
        });

        if (radio_vacio===0)
        {
            Toast.fire({
                type: 'warning',
                title: 'Debes seleccionar al menos una alternativa correcta'
            });
            status = false;
            return false;
        }

        if (!status)
            return false;
        return Form.checkSend('¿Estás seguro que deseas crear la pregunta?');
    }

    function item_nuevo(item) {
        n_items = $('textarea[name="respuesta[]"]').length;
        if (n_items > 5) {
            Toast.fire({
                type: 'warning',
                title: 'La pregunta no puede tener más de 5 respuestas'
            });
            item.remove();
            return false;
        }
    }
</script>

<?php

$letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S'];

?>

<!-- Page Content -->
<div id="page-wrapper" style="margin-left: 0px;">
   <div class="container-fluid">
        <div class="row m-t-20">
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                <h3 class="title">Gestor de preguntas</h3>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <form method="POST" action="<?=site_url();?>/home/cerrar_prueba" onSubmit="return confirm('¿Estás seguro que deseas cerrar la prueba?')">
                    <input type="hidden" name="prueba_id" value="<?=$prueba_id;?>">
                    <button class="btn btn-ev btn-block">Terminar edición de la prueba</button>
                </form>
            </div>
        </div>
        <div class="row" style="margin-top: 70px;">                
                    <h4 class="panel-resumen-title " style="color:#61b7c3;font-weight: bold !important;font-size: 36px;">Resumen</h4>

                    <div class="col-lg-12">
                        <div class="col-lg-3" style="color: #61b7c3;font-size: 20px;font-weight: bold;">Nombre</div>
                        <div class="col-lg-2" style="color: #61b7c3;font-size: 20px;font-weight: bold;">Tipo</div>
                        <div class="col-lg-2" style="color: #61b7c3;font-size: 20px;font-weight: bold;">Asignatura</div>
                        <div class="col-lg-3" style="color: #61b7c3;font-size: 20px;font-weight: bold;">Nivel</div>
                        <div class="col-lg-2" style="color: #61b7c3;font-size: 20px;font-weight: bold;">Alternativas</div>

                    </div>
                    <div class="col-lg-12">
                        <div class="col-lg-3"><?=$nombre;?></div>
                        <div class="col-lg-2"><?=$this->model->getTipoPrueba($tipo_id)->tipo;?></div>
                        <div class="col-lg-2"><?=$this->model->getAsignatura($asignatura_id)->asignatura;?></div>
                        <div class="col-lg-3"><?=$this->model->getNivel($nivel_id)->nivel;?></div>
                        <div class="col-lg-2"><?=$alternativas?></div>
                    </div>            
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div class="panel-evaluacion">
                    
                    <div class="p-20">
                        <?php if ($tipo_id == 3 || $tipo_id == 4): ?>
                            <?php if (count($preguntas) > 0) : ?>
                                <div class="row p-20 p-b-0">
                                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 label-preguntas-sug">
                                        Hay <span><?= count($preguntas) ?></span> preguntas sugeridas para tu evaluación
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

                                        <?php $link = site_url('/home/preguntas_sugeridas/') . $prueba_id; ?>
                                        <button link="<?= $link ?>" btn-pregunta-sugeridas class="btn btn-ev btn-block btn-outline">Ver preguntas sugeridas</button>

                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php elseif ($tipo_id == 1 || $tipo_id == 2): ?>
                            <?php 
                            if($preguntas)
                                if (!empty($ejes_id) && count($preguntas) > 0) : 
                            ?>
                                <div class="row p-20 p-b-0">
                                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 label-preguntas-sug">
                                        Hay <span><?= count($preguntas) ?></span> preguntas sugeridas para tu evaluación
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">

                                        <?php $link = site_url('/home/preguntas_sugeridas/') . $prueba_id; ?>
                                        <button link="<?= $link ?>" btn-pregunta-sugeridas class="btn btn-ev btn-block btn-outline">Ver preguntas sugeridas</button>

                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                        <div class="row p-10 p-t-0">
                            <h4>Crear pregunta</h4>
                        </div>

                        <form id="form1" method="POST" onSubmit="return check_form()" action="<?=site_url();?>/home/crear_pregunta_prueba" enctype="multipart/form-data">
                            <input type="hidden" id="asignatura_id" name="asignatura_id" value="<?=$asignatura_id;?>">
                            <input type="hidden" id="nivel_id" name="nivel_id" value="<?=$nivel_id;?>">
                            <input type="hidden" id="tipo_id" name="tipo_id" value="<?=$tipo_id;?>">
                            <input type="hidden" id="prueba_id" name="prueba_id" value="<?=$prueba_id;?>">

                            
                            
                            <input type="hidden" id="prueba_aeduc" name="prueba_aeduc" value="<?=$prueba_aeduc?>">
                            <input type="hidden" id="nombre" name="nombre" value="<?=$nombre?>">

                            <?php if (isset($pregunta_editar)) : ?>
                                <input type="hidden" name="pregunta_editar_id" value="<?=$pregunta_editar->id;?>">
                                <input type="hidden" name="pregunta_editar_orden" value="<?=$pregunta_editar->orden;?>">
                            <?php else : ?>
                                <input type="hidden" name="pregunta_editar_id" value="">
                                <input type="hidden" name="pregunta_editar_orden" value="">
                            <?php endif; ?>

                            <div class="panel-evaluacion-crear" style="border-color: #61b7c2;">
                                <div class="row">

                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                                        <div class="numero-preg" style="background: #61b7c2; border-color:#61b7c2;"></div>
                                        <div class="enc-pregunta p-20" style="border-color: #61b7c2;">
                                        
                                        <div id="preguntaDiv" contentEditable="true" style="width:91%">
                                            <?php
                                                if(isset($pregunta_editar->pregunta))
                                                    echo $pregunta_editar->pregunta
                                                ?>    
                                        </div>
                                            <textarea id="pregunta" name="pregunta" rows="2" style="display:none" ></textarea>

                                            <a href="javascript:void(0)" class="file"><i class="fa fa-image">
                                                <input type="file" name="imagen_link" accept="image/x-png,image/gif,image/jpeg" value="" onchange="readURL(this);"></i>
                                            </a>
                                            <a href="javascript:void(0)" clean-enc-pregunta><i class="fa fa-trash-o"></i></a>
                                            <div>
                                                <img id="blah" src="" alt="" style="display:block"/>
                                            </div>
                                        </div>
                                        
                                        <p class="subtitle">Seleccionar respuesta correcta</p>

                                        <script type="text/javascript">
                                            window["inputsJS_container_alternativas"] = `<div class="container-resp row">
                                                <label class="leter-resp" style="border-color: '#56b2bf';width:4%">
                                                    <input input-leter type="radio" name="correcta[]" color="#56b2bf">
                                                    <span style="color: '#56b2bf';">A</span>
                                                </label>
                                                <div class="respuesta" style="width:95%">
                                                    <div id="respuestaDiv'.$i.'"  contentEditable="true" style="width:91%">
                                                    </div>
                                                    <textarea name="respuesta[]" rows="1"></textarea>
                                                    <a href="javascript:void(0)" class="file"><i class="fa fa-image">
                                                        <input type="file" name="respuesta_imagen[]" accept="image/x-png,image/gif,image/jpeg" value=""></i></a>
                                                    <a href="javascript:void(0)" onclick="Form.delJS(this); return false"><i class="fa fa-trash-o"></i></a>
                                                </div>
                                            </div>`;
                                        </script>

                                        <div id="container_alternativas">
                                            
                                        <?php 
                                            
                                            if (isset($respuesta_editar)) :
                                                $i = 0; 
                                                foreach($respuesta_editar as $key => $respuesta)
                                                { 
                                                    
                                                    $checked = '';
                                                    
                                                    if ($respuesta->correcta == 't') 
                                                        $checked = 'checked'; 
                                                                                                        
                                                    echo '<div class="container-resp row">
                                                                <label class="leter-resp" style="width:4%;border-color:#61b7c2;';
                                                    
                                                    
                                                    if($respuesta->correcta == 't')
                                                        echo 'background:#56b2bf';
                                                    
                                                    echo '"> <input input-leter type="radio" name="correcta[]" value="'.$key.'" '.$checked.' color="#56b2bf">
                                                                    <span style="color:';
                                                    
                                                    echo '#56b2bf';
                            
                                                    if($respuesta->correcta == 't')
                                                        echo '#fff';
                                                    else
                                                        echo '#56b2bf';
                                                    
                                                    
                                                    echo '">'.getLetraByIndice($i).' </span>
                                                                </label>
                                                                <div class="respuesta" style="width:95%">
                                                                
                                                                <div id="respuestaDiv'.$i.'"  contentEditable="true" style="width:91%">
                                                                    '.$respuesta->respuesta.'    
                                                                </div>

                                                                    <textarea name="respuesta[]" rows="1" style="display:none"></textarea>';

                                                    echo        '<a href="javascript:void(0)" class="file"><i class="fa fa-image">
                                                                        <input type="file" name="respuesta_imagen[]" accept="image/x-png,image/gif,image/jpeg" value="" onchange="readURLX(this, '.$i.');"></i>
                                                                    </a>
                                                                    <a href="javascript:void(0)" onclick="Form.delJS(this); return false"><i class="fa fa-trash-o"></i></a>
                                                                </div>
                                                            </div>';

                                                    $i++; 
                                                }
                                            
                                            ?>
                                        <?php else: ?>

                                            <div class="container-resp row">
                                                <label class="leter-resp" style="border-color: #56b2bf;">
                                                    <input input-leter type="radio" name="correcta[]" value="0" color="#56b2bf">
                                                    <span style="color: #56b2bf;">A</span>
                                                </label>
                                                <div class="respuesta" style="width:95%">
                                                    <textarea name="respuesta[]" rows="1"></textarea>
                                                    <a href="javascript:void(0)" class="file"><i class="fa fa-image"><input type="file" name="respuesta_imagen[]" accept="image/x-png,image/gif,image/jpeg" value=""></i></a>
                                                    <a href="javascript:void(0)" onclick="Form.delJS(this); return false"><i class="fa fa-trash-o"></i></a>
                                                </div>
                                            </div>

                                        <?php endif; ?>
                                        </div>

                                            <div class="container-resp new-resp row">
                                                <a href="javascript:Form.addJS('container_alternativas', <?=$alternativas?>, item_nuevo)" title="Agregar [D]" accesskey="D" class="leter-resp" style="border-color: #56b2bf;">
                                                    <span style="color: #56b2bf;"><i class="ti-plus"></i></span>
                                                </a>
                                                Crear alternativa
                                            </div>


                                    </div>

                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <div class="container-conf m-t-20 form-body" style="border-color: #56b2bf;">

                                            <div class="row" row-oas>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="control-label">OA</label>
                                                        <select class="form-control" name="oa_id" onChange="selecciona_oa(this);">
                                                            <?php

                                                            if ($oas) 
                                                                echo '<option value="" hidden>Seleccione una opción</option>';
                                                            else
                                                                echo '<option value="" hidden>No existen datos</option>';
                                                            
                                                            foreach ($oas as $oa) 
                                                            {
                                                                $selected = '';
                                                                
                                                                if (isset($pregunta_editar))
                                                                    if ($oa->id == $pregunta_editar->oa_id || $oa->codigo == $pregunta_editar->oa_codigo) 
                                                                        $selected = 'selected';
                                                                    
                                                                echo '<option value="'.$oa->id.'" '.$selected.'>'.$oa->codigo.'</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" row-ies>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Ie</label>
                                                        <select id="ie_id" class="form-control" name="ie_id">
                                                            <option value="" hidden>Seleccione una opción</option>
                                                        </select>
                                                        <?php if (isset($pregunta_editar->ie_id)):?>
                                                            <script>
                                                                $(() => {
                                                                    const oa_id = $(`[name="oa_id"]`).val();
                                                                    $.post(`<?= site_url('home/imprime_ie') ?>`, {oa_id: oa_id, ie_id: <?= $pregunta_editar->ie_id ?>})
                                                                    .done(json => {
                                                                        $("#ie_id").html(json);
                                                                    });
                                                                });
                                                            </script>
                                                            <!-- <option selected value="<?=$pregunta_editar->ie_id;?>"><?=$pregunta_editar->ie_ie;?></option> -->
                                                        <?php endif ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" row-ejes>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Eje</label>
                                                        <select class="form-control" name="ejetematico_id">
                                                            <option value="" hidden>Seleccione una opción</option>
                                                        <?php foreach ($ejes as $eje): $selected = ''; ?>
                                                            <?php if (isset($pregunta_editar)) {
                                                                if ($eje->id == $pregunta_editar->ejetematico_id) {
                                                                    $selected = 'selected';
                                                                }
                                                             } ?>
                                                            <option value="<?= $eje->id ?>" <?= $selected ?>><?= $eje->ejetematico ?></option>
                                                        <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" row-habilidad>
                                                <div class="col-xs-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Habilidad</label>
                                                        <select class="form-control" name="habilidadcurricular_id">
                                                            <option value="" hidden>Seleccione una opción</option>
                                                        <?php foreach ($habilidades as $habilidad): $selected = ''; ?>
                                                            <?php if (isset($pregunta_editar)) {
                                                                if ($habilidad->id == $pregunta_editar->habilidadcurricular_id) {
                                                                    $selected = 'selected';
                                                                    }
                                                                }?>
                                                            <option value="<?= $habilidad->id ?>" <?= $selected ?>><?= $habilidad->habilidadcurricular ?></option>
                                                        <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row m-t-10 m-b-10" style="padding-left: 7.5px; padding-right: 7.5px;">
                                            <button type="submit" class="btn <?= $colorClass ? $colorClass : 'btn-ev' ?> col-xs-6 col-sm-offset-4 col-sm-4 col-md-offset-4 col-md-4 col-lg-offset-4 col-lg-4" style="background:#56b2bf !important;color:#FFF">Guardar</button>
                                            <button type="button" clean-all class="btn btn-outline <?= $colorClass ? $colorClass : 'btn-ev' ?> col-xs-6 col-sm-offset-2 col-sm-2 col-md-offset-2 col-md-2 col-lg-offset-2 col-lg-2" style="background:#56b2bf !important;color:#fff;border-color:#56b2bf;"><i class="ti-reload"></i></button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="p-20">
                        <div class="row p-10">
                            <h4>Listado de preguntas</h4>
                        </div>
                    
                    <!-- Contenedor de preguntas-->
                        <div class="row" id="container_preguntas">
<div style="background-image: url('<?=base_url()."/assets/images/hojaprepicado1.png"?>');background-size: 83% 5.7%;background-repeat: repeat-y;">
                            <?php if ($preguntas_creadas): ?>
                                <?php foreach ($preguntas_creadas as $pregunta_creada): ?>

                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 m-b-20" style="margin-left:36px;width:97%">
                                        <div class="numero-preg" style="background: #56b2bf; border-color: #56b2bf;"><?= $pregunta_creada->orden ?></div>
                                        <div class="enc-pregunta" data-oa="<?=$pregunta_creada->oa;?>" data-eje="<?=$pregunta_creada->ejetematico_id;?>" style="border-color: #56b2bf;">
                                            <div class="row row-badges m-l-10">
                                                <?php if ($tipo_id == 3 || $tipo_id == 4): ?>
                                                    <span class="badges-pregunta" style="color: #56b2bf; border-color: #56b2bf;"><?=$pregunta_creada->oa?></span>
                                                <?php else: ?>
                                                    <span class="badges-pregunta" style="color: #56b2bf; border-color: #56b2bf;"><?=$pregunta_creada->ejetematico?></span>
                                                <?php endif ?>

                                                <a href="javascript:void(0)" class="p-0 m-r-10" onclick="Form.moveToUpPreguntaJS(this); return false" data-pregunta="<?=$pregunta_creada->id;?>" data-prueba="<?=$prueba_id;?>" style="color: #56b2bf; margin-top: -3px;"><i class="ti-arrow-up"></i></a>
                                                <a href="javascript:void(0)" class="p-0 m-r-10" onclick="Form.moveToDownPreguntaJS(this); return false" data-pregunta="<?=$pregunta_creada->id;?>" data-prueba="<?=$prueba_id;?>" style="color: #56b2bf; margin-top: -3px;"><i class="ti-arrow-down"></i></a>
                                                <a href="javascript:void(0)" class="p-0 m-r-10" onclick="Form.editPreguntaJS(this); return false" data-pregunta="<?=$pregunta_creada->id;?>" data-prueba="<?=$prueba_id;?>" style="margin-top: -3px"><i class="ti-pencil"></i></a>
                                                <a href="javascript:void(0)" class="p-0 m-r-10" onclick="Form.delPreguntaJS(this); return false" data-pregunta="<?=$pregunta_creada->id;?>" data-prueba="<?=$prueba_id;?>" style="margin-top: -3px"><i class="fa fa-trash-o"></i></a>
                                            </div>
                                            <div class="row row-pregunta m-l-10 m-t-5">
                                                <div name="pregunta" rows="2"><?=$pregunta_creada->pregunta;?></div>
                                            </div>
                                        </div>

                                        <?php foreach ($this->model->getRespuestasFromPreguntaCreada($pregunta_creada->id) as $key => $respuesta_creada): ?>

                                            <div class="container-resp row">
                                                <label class="leter-resp" style="border-color:#56b2bf; <?= $respuesta_creada->correcta == 't' ? 'background:#56b2bf;' : '' ?>">
                                                    <input type="radio" value="0" color="#56b2bf">
                                                    <span style="color: <?= $respuesta_creada->correcta == 't' ? '#fff' : '#56b2bf' ?>"><?= $letters[$key]; ?></span>
                                                </label>
                                                <div class="respuesta" style="width:50%">
                                                    <div rows="1"><?= $respuesta_creada->respuesta ?></div>
                                                </div>
                                            </div>

                                        <?php endforeach; ?>

                                    </div>

                                <?php endforeach; ?>
                            <?php else: ?>
                                <p class="subtitle">La evaluación aún no tiene preguntas.</p>
                            <?php endif ?>
</div>
                        </div><!-- Cierre contenedor de preguntas-->
                    

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<script>
    
    $('#form1').submit(function() 
    {
        $('#pregunta').val($('#preguntaDiv').html());
        
        $( "textarea[name='respuesta[]']" ).each(function(index, value)
        {
            $(this).val( $('#respuestaDiv'+index).html() )
        });

    });
    
    function readURL(input) 
    {
        
        if (input.files && input.files[0]) 
        {
                        
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $( "#preguntaDiv" ).append( '<img src="'+e.target.result+'">' );

            };

            reader.readAsDataURL(input.files[0]);
        }
        
    }

    function readURLX(input,index) 
    {
        
        if (input.files && input.files[0]) 
        {
                        
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $( "#respuestaDiv"+index ).append( '<img src="'+e.target.result+'">' );

            };

            reader.readAsDataURL(input.files[0]);
        }
        
    }


    const tipo_id = '<?= $tipo_id ?>';
    <?php 
        if (isset($respuesta_editar))
            echo "respuestas=".$i.";";
    ?>

</script>

<script src="<?php echo base_url()?>assets/inverse/bootstrap/dist/js/bootstrap.js"></script>
<!-- Sidebar menu plugin JavaScript -->
<script src="<?php echo base_url()?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.js"></script>
<!--Slimscroll JavaScript For custom scroll-->
<script src="<?php echo base_url()?>assets/inverse/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url()?>assets/inverse/js/waves.js"></script>
<!-- Sweet-Alert  -->
<script src="<?php echo base_url()?>assets/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url()?>assets/inverse/js/custom.js"></script>
<script src="<?php echo base_url()?>assets/inverse/js/jasny-bootstrap.js"></script>
<script src="<?php echo base_url()?>assets/plugins/bower_components/switchery/dist/switchery.js"></script>
<script src="<?php echo base_url()?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
<script src="<?php echo base_url()?>assets/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!--<script src="<?php echo base_url()?>assets/modulos/analisis/js/analisis.js"></script>-->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="<?= base_url() ?>assets/modulos/crearEvaluacion/js/preguntas.js"></script>

</body>

</html>
