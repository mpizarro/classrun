 <!-- Page Content -->
 <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4 class="page-title">Material Pedagógico</h4>
                <ol class="breadcrumb">
                    <li>Repositorio</li>
                    <li class="active">Material Pedagógico</li>
                </ol>
            </div>
            <!-- /.page title -->
        </div>
        <div class="mainRow row">
            <div class="col-md-6 row-eq-height">
                <div class="row" id="rowContainerAsignaturas">
                </div>
                <div class="row" white>
                    <div class="col-md-12" margin-vertical>
                        <h4 class="page-title bigger">Seleccionar Tipo de Documento</h4>
                    </div>
                    <div class="row" id="nivelesContainer">
                        <div class="col-md-12">
                            <select id="nivelesContainerSelect" class="form-control" style="margin-left : 5%; width : 90%">
                                <option value="" hidden>Seleccionar Tipo de Documento</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12" margin-vertical hidden id="titleNiveles">
                        <h4 class="page-title bigger">Seleccionar Nivel</h4>
                    </div>
                    <div class="row" id="nivelesContainer-1">
                    </div>
                    <div class="row" margin-top id="nivelesContainer-2">
                    </div>
                    <div class="row" margin-top id="nivelesContainer-3">
                    </div>
                    <div class="col-md-12" margin-vertical>
                        <h4 class="page-title bigger">Lista de Archivos</h4>
                    </div>
                    <div class="col-md-12" padding-horizontal>
                        <div class="tableContainer">
                            <div class="head">
                                <div class="th first">Nombre</div>
                                <div class="th two">Peso</div>
                                <div class="th three">Descarga</div>
                            </div>
                            <hr>
                            <div class="body" id="bodyTable">
                                <div class="rowTable">
                                    <div class="th first">
                                        <p>Aún no se ha cargado material pedagógico</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 row-eq-height">
                <div class="row" white id="vistaPreviaContainer">
                    <h5 padding class="vistaPrevia">Vista Previa</h5>
                    <iframe id="iframe" src="<?= site_url('/repositorio') ?>" style="width : 100%;"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
    <a href="javascript:void(0)" download="" id="dowloadAux" hidden></a>