<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <title>Vista previa</title>
    <style>
    p {
        text-align: center;
        margin: auto;
        margin-top: 6rem;
        font-family: 'Raleway', sans-serif;
        font-size: 2rem;
    }
    </style>
</head>
<body>
    <p>Vista previa no diponible.</p>
</body>
</html>