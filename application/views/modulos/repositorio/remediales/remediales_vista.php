 <!-- Page Content -->
 <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h4 class="page-title">REMEDIALES</h4>
                <ol class="breadcrumb">
                    <li>Repositorio</li>
                    <li class="active">Remediales</li>
                </ol>
            </div>
            <!-- /.page title -->
        </div>
        <div class="mainRow row">
            <div class="col-md-6 row-eq-height">
                <div class="row" id="rowContainerAsignaturas">
                </div>
                <div class="row" white>
                    <div class="col-md-12" margin-vertical>
                        <h4 class="page-title bigger">Seleccionar Curso</h4>
                    </div>
                    <div class="row" id="nivelesContainer-1">
                        <p class="text-center">No hay cursos disponibles.</p>
                    </div>
                    <div class="row" margin-top id="nivelesContainer-2">
                    </div>
                    <div class="col-md-12" margin-vertical hidden id="titleEvaluaciones">
                        <h4 class="page-title bigger">Seleccionar Nº Evaluación</h4>
                    </div>
                    <div class="col-md-12" padding-horizontal id="containerEvaluaciones" hidden>
                    </div>
                    <div class="col-md-12" margin-vertical>
                        <h4 class="page-title bigger">Lista de Archivos</h4>
                    </div>
                    <div class="col-md-12" padding-horizontal>
                        <div class="tableContainer">
                            <div class="head">
                                <div class="th first">Nombre</div>
                                <div class="th two">Peso</div>
                                <div class="th three">Descarga</div>
                            </div>
                            <hr>
                            <div class="body" id="bodyTable">
                                <div class="rowTable">
                                    <div class="th first">
                                        <p>Aún no se ha cargado remediales</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 row-eq-height">
                <div class="row" white id="vistaPreviaContainer">
                    <h5 padding class="vistaPrevia">Vista Previa</h5>
                    <iframe id="iframe" src="<?= site_url('/repositorio') ?>" style="width : 100%;"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
    <a href="javascript:void(0)" download="" id="dowloadAux" hidden></a>