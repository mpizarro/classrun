<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid p-30">

        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    
                    <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

                </div>
            </div>
        </div>

   </div>
   <!-- /.container-fluid -->

   <script>
   const data = JSON.parse('<?= json_encode($data) ?>');
   </script>