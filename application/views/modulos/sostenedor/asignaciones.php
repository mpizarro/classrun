<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid p-30">

        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <form action="<?= site_url('asignaciones_sostenedor/index') ?>" method="get">
                        <div class="row">
                            <div class="col-md-2">
                                <select class="form-control" name="colegio">
                                    <option value="" hidden>Selecciona un colegio</option>
                                    <option value="">Todo</option>
                                <?php foreach ($colegios as $col): ?>
                                    <option value="<?= $col->rbd ?>" <?= $this->input->get('colegio') == $col->rbd ? 'selected' : '' ?>><?= $col->nombre ?></option>
                                <?php endforeach ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control" name="asignatura">
                                    <option value="" hidden>Selecciona una asignatura</option>
                                    <option value="">Todo</option>
                                <?php foreach ($asignaturas as $asig): ?>
                                    <option value="<?= $asig->id ?>"  <?= $this->input->get('asignatura') == $col->rbd ? 'selected' : '' ?>><?= $asig->asignatura ?></option>
                                <?php endforeach ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control" name="tipo">
                                    <option value="" hidden>Selecciona un tipo</option>
                                    <option value="">Todo</option>
                                    <option value="1" <?= $this->input->get('tipo') == 1 ? 'selected' : '' ?>>SIMCE</option>
                                    <option value="2" <?= $this->input->get('tipo') == 2 ? 'selected' : '' ?>>PME</option>
                                    <option value="3" <?= $this->input->get('tipo') == 3 ? 'selected' : '' ?>>UNIDAD</option>
                                    <option value="4" <?= $this->input->get('tipo') == 4 ? 'selected' : '' ?>>COBERTURA</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <select class="form-control" name="estado" <?= $this->input->get('estado') == $col->rbd ? 'selected' : '' ?>>
                                    <option value="" hidden>Selecciona un estado</option>
                                    <option value="">Todo</option>
                                    <option value="1" <?= $this->input->get('estado') == 2 ? 'selected' : '' ?>>En ejecución</option>
                                    <option value="3" <?= $this->input->get('estado') == 3 ? 'selected' : '' ?>>Finalizadas</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-info"><i class="fa fa-search"></i> Buscar</button>
                                <a role="button" href="<?= site_url('asignaciones_sostenedor/index') ?>" class="btn btn-info">Limpiar</a>
                            </div>
                            <div class="col-md-2">
                                <input type="text" id="search" class="form-control" placeholder="Buscar">
                            </div>
                        </div>
                    </form>

                    <div class="table_responsive m-t-20">
                        <table id="table_asignaciones" class="table">
                            <thead>
                                <tr>
                                    <th>Colegio</th>
                                    <th>Asignatura</th>
                                    <th>Curso</th>
                                    <th>N°</th>
                                    <th>Tipo</th>
                                    <th>Origen</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($asignaciones as $asig): ?>
                                <tr>
                                    <td style="font-weight: 500"><?= $asig->colegio ?></td>
                                    <td style="color: <?= $asig->color ?>; font-weight: 500"><?= $asig->asignatura ?></td>
                                    <td><?= $asig->curso . ' ' . $asig->letra ?></td>
                                    <td><?= $asig->num ?></td>
                                    <td><?= $asig->tipo ?></td>
                                    <td><?= $asig->origen ?></td>
                                    <td style="color: <?= $asig->color_estado ?>"><i class="fa fa-circle m-r-5" style="color: <?= $asig->color_estado ?>"></i> <?= $asig->estado ?></td>
                                    <td>
                                    <?php if ($asig->estado_id == 3): ?>
                                        <a role="button" target="_blank" href="<?= $asig->url_informe ?>" class="btn btn-info btn-outline btn-xs"><i class="fa fa-cloud-download"></i> Informe</a>
                                    <?php endif ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

   </div>
   <!-- /.container-fluid -->