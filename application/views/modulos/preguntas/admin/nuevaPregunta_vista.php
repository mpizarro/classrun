 <!-- Page Content -->
 <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title" id="title-page">Nueva Pregunta</h4>
            </div>
            <!-- /.page title -->
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <form id="frmNuevaPregunta" id-evaluacion="<?= $idEvaluacion ?>" codigo="<?= $codigo ?>" nivel_id="<?= $nivel_id ?>"  action="javascript:void(0)" method="post">
                        <div class="form-body">
                            <div class="row">                          
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Eje</label>
                                        <select name="eje" class="form-control"><?= $selectEjes ?></select>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Aprendizaje</label>
                                        <select name="aprendizaje" class="form-control"><?= $selectAprendizajes ?></select>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                            <div class="row">
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Unidad</label>
                                        <select name="unidad" id="unidad"class="form-control"><?= $selectUnidades ?></select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">OA</label>
                                        <select name="oa" id="oa" class="form-control" <?= $selectOas ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">IE</label>
                                        <select name="ie" id="ie" class="form-control" <?php echo (isset($selectIes))?$selectIes:""; ?>
                                        <option>Elige Ie</option>
                                        </select>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Pregunta</label>
                                        <textarea class="editor" name="pregunta"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /row -->
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="button" id="btnAtrasPreguntas" class="btn btn-default" id-evaluacion="<?= $idEvaluacion ?>" codigo="<?= $codigo ?>">Cancelar</button>
                                    <button type="submit" id="btnGuardarPregunta" class="btn btn-success"><i class="fa fa-check"></i> Guardar pregunta y agregar alternativas</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->


    <script>
    let idPregunta = <?= $idPregunta ? $idPregunta : "false" ?>;
    </script>