 <!-- Page Content -->
 <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Preguntas</h4>
            </div>
            <!-- /.page title -->
        </div>
        <!-- acciones -->
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <input type="search" id="search" class="form-control" placeholder="Buscar">
                </div>
            </div>
            <div class="col-md-2">
                <button id="btnModalCrearPregunta" class="btn btn-info waves-effect waves-light" id-evaluacion="<?= $idEvaluacion ?>" nivel_id="<?= $nivel_id ?>" asignatura_id="<?= $asignatura_id ?>" codigo="<?= $codigo ?>"><span>Crear pregunta</span> <i class="fa fa-plus m-l-5"></i></button>
            </div>
        </div>
        <!-- /acciones -->
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="table-preguntas" class="table color-table evalua-table toggle-circle">
                        <thead>
                            <tr>
                                <th data-toggle='true' style="width:100px;">#</th>
                                <th>Codigo</th>
                                <th class="text-center" style="width:100px;">Clave</th>
                                <th class="text-center" style="width:340px;">Acciones</th>
                                <th data-hide='all'></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?= $tablaPreguntas ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">
                                    <div class="text-right">
                                        <ul class="pagination pagination-split m-t-30">
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <!-- Ver pregunta -->
    <div class="modal fade" id="mdlVerPregunta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title colorText" id="exampleModalLabel2">Pregunta</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div id="contenido-pregunta"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Ver alternativa -->
    <div class="modal fade" id="mdlVerAlternativa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title colorText" id="exampleModalLabel1">Alternativa</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div id="contenido-alternativa"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div id="contenido-correcta"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
