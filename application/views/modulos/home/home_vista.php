 <!-- Page Content -->
 <div id="page-wrapper">
    <div class="container-fluid">

        <div class="white-box">
            <div class="row m-b-20 m-t-20">
                <div class="col-lg-offset-4 col-md-offset-4 col-sm-offset-4 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <img src="<?= base_url('assets/images/log_full.svg') ?>" alt="LOGO" style="width: 100%;">
                </div>
            </div>

            <p class="title-bienvenida">Te damos la bienvenida, <span class="user-name"><?= $nombreCompleto ?></span></p>

            <div class="row">
                <p class="text-bienvenida col-lg-offset-3 col-md-offset-3 col-sm-offset-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                AEDUC Evalúa es un software de instrumentos evaluativos <br>
                que permiten obtener resultados de manera instantánea</p>
            </div>

            <div class="row m-t-10">
                <div class="col-lg-offset-5 col-md-offset-5 col-sm-offset-5 col-xs-offset-2 col-lg-2 col-md-2 col-sm-2 col-xs-8">
                    <a class="btn btn-block btn-ev" href="<?= site_url('/Evaluaciones') ?>">Empezar</a>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-offset-1 col-md-offset-1 col-lg-10 col-md-10 col-sm-12 col-xs-12">
                    <img src="<?= base_url('assets/images/banner_home.svg') ?>" alt="LOGO" style="width: 100%;">
                </div>
            </div>
        </div>
        
    </div>
    <!-- /.container-fluid -->