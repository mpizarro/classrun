 <!-- Page Content -->

    <div class="container-fluid">
        <div class="row">
            <!-- .page title -->
            <div class="col-lg-12 col-md-12 col-sm-4 col-xs-12">
                &nbsp;
            </div>
            <!-- /.page title -->
        </div>
        <div class="row" id="rowContainer" style="margin-top: 125px;">
            <?php foreach ($evaluaciones as $key => $evaluacion): ?>
                <div class="col-lg-3 col-sm-3">
                    <div class="panel panel-alumno">
                        <div class="panel-heading" style="display:flex;align-items:center;justify-content: space-between;background-color: <?=$evaluacion->color;?>">
                            <div>
                                <div class="panel-heading-alumno-asignatura"><?=$evaluacion->asignatura;?></div>
                                <p class="panel-heading-alumno-evaluacion"><?=$evaluacion->tipo." ";?>
                                <?php 
                                    switch ($evaluacion->nivel_id) {
                                        case 1:
                                            echo "Primero Básico";
                                            break;
                                        case 2:
                                            echo "Segundo Básico";
                                            break;
                                        case 3:
                                            echo "Tercero Básico";
                                            break;
                                        case 4:
                                            echo "Cuarto Básico";
                                            break;
                                        case 5:
                                            echo "Quinto Básico";
                                            break;
                                        case 6:
                                            echo "Sexto Básico";
                                            break;
                                        
                                        default:
                                            # code...
                                            break;
                                    }
                                  

                                ?>
                                </p>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="text-center">
                            <p class="panel-footer-profesor-titulo">
                                <?php 
                                    if(isset($evaluacion->nombrePrueba))
                                        echo $evaluacion->nombrePrueba;
                                    else
                                        echo "Numik Eva. ".$evaluacion->num;
                                ?>
                            </p>
                                
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="panel-footer-profesor-titulo">Profesor/a</div>
                            <div class="panel-footer-profesor-nombre"><?=$evaluacion->nombre;?> <?=$evaluacion->apellido;?></div>
                            <center>
                                <button class="btn-alumno btn-<?=$evaluacion->alias;?>" style="border-color:<?=$evaluacion->color;?>;" onclick="ejectuarEvaluacion('<?=$evaluacion->usuario_has_asignacion_id;?>', '<?=$evaluacion->origen;?>', '<?=$evaluacion->id?>')">
                                    Realizar Evaluacion
                                </button>
                            </center>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <!-- /.container-fluid -->
