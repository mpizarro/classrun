<!DOCTYPE html>
<!--
   This is a starter template page. Use this page to start your new project from
   scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="#">
        <title>ClassRun</title>

        <link href="https://fonts.googleapis.com/css?family=Nunito:300" rel="stylesheet">
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url()?>assets/inverse/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>assets/plugins/bower_components/datatables/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" type="text/css" />
        <!-- This is Sidebar menu CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.css" rel="stylesheet">
        <!-- TPermite usar el multi select de cursos, entre otros. -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <!--alerts CSS -->
        <!-- Calendar CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/calendar/dist/fullcalendar.css" rel="stylesheet" />
        <!--alerts CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
        <!-- This is a Animation CSS -->
        <link href="<?php echo base_url()?>assets/inverse/css/animate.css" rel="stylesheet">
        <?php
            if (isset($css)) {
                echo $css;
            }
        ?>
        <!-- toast CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">

        
        <!-- color CSS you can use different color css from css/colors folder -->
        <!-- We have chosen the skin-blue (default.css) for this starter
            page. However, you can choose any other skin from folder css / colors .
        -->
        <link href="<?php echo base_url()?>assets/inverse/css/colors/default.css" id="theme" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/inverse/css/headStyle.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/inverse/css/menuStyle.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/inverse/css/modalStyle.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/plugins/bower_components/switchery/dist/switchery.css" rel="stylesheet" />
        <link href="<?php echo base_url()?>assets/css/alumno.css" rel="stylesheet">

        <link href="<?php echo base_url()?>assets2/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url()?>assets2/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url()?>assets2/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url()?>assets2/css/seleccionar.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url()?>assets2/css/crear.css" rel="stylesheet" type="text/css">
        
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Implementación del chat para profesores -->


    </head>
    <body class="body-alumno">
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
        <header id="topnav">
            

            <div class="topbar-main">
            <div class="container-fluid">

                    <!-- Logo container-->
                    <div class="logo">
                        <!-- Text Logo -->
                        <!--<a href="index.html" class="logo">-->
                        <!--Epoch-->
                        <!--</a>-->
                        <!-- Image Logo -->
                        <a href="index.html" class="logo">
                            <img src="<?php echo base_url()?>assets2/images/logo_classrun_blanco.png" alt="" height="50" width="100" class="logo-small">
                            <img src="<?php echo base_url()?>assets2/images/logo_classrun_blanco.png" alt="" class="logo-large">
                        </a>

                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras topbar-custom">

                        <ul class="list-inline float-right mb-0">
                            <li class="list-inline-item hide-phone app-search">
                                <form role="search" class="">
                                    <input type="text" placeholder="Buscar" class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>
                            
                            <!-- notification-->
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <i class="mdi mdi-bell-outline noti-icon"></i>
                                    <span class="badge badge-success noti-icon-badge">21</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5>Notification (3)</h5>
                                    </div>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                        <p class="notify-details"><b>Your order is placed</b><small class="text-muted">Dummy text of the printing and typesetting industry.</small></p>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-warning"><i class="mdi mdi-message"></i></div>
                                        <p class="notify-details"><b>New Message received</b><small class="text-muted">You have 87 unread messages</small></p>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-info"><i class="mdi mdi-martini"></i></div>
                                        <p class="notify-details"><b>Your item is shipped</b><small class="text-muted">It is a long established fact that a reader will</small></p>
                                    </a>

                                    <!-- All-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        View All
                                    </a>

                                </div>
                            </li>
                            <!-- User-->
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <img src="<?php echo base_url()?>assets2/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5>Bienvenido</h5>
                                    </div>
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5 text-muted"></i> Perfil</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="<?=site_url()."/login/logout"?>"><i class="mdi mdi-logout m-r-5 text-muted"></i> Salir</a>
                                </div>
                            </li>
                            <li class="menu-item list-inline-item">
                                <!-- Mobile menu toggle-->
                                <a class="navbar-toggle nav-link">
                                    <div class="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                                <!-- End mobile menu toggle-->
                            </li>

                        </ul>
                        
                    </div>
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>

                </div> <!-- end container -->
            </div>
            <!-- end topbar-main -->




                        <div class="navbar-custom">
                <div class="container-fluid">
                    <div id="navigation">
                      <!-- Navigation Menu-->
                        <ul class="navigation-menu text-center">
                            <a id="left" href="<?=$left?>">
                                <i style="font-size: 36px;color: #FFF;float: left;margin-left: 26%;margin-top: 13px;" class="mdi mdi-arrow-left"></i>
                            </a>
                            
                            <li class="has-submenu ">
                                <a id="ruta" href="index.html"><i class="mdi mdi-airplay"></i>Evaluaciones</a>
                            </li>

                        </ul>
                        
                        
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>
