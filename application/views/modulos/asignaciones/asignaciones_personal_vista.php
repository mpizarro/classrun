<!-- Page Content -->
<div id="page-wrapper">
   <div class="container-fluid">
       <div class="row bg-title">
            <h2>Asignaciones del profesor</h2>
            <p>Filtrar por:</p>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group m-b-0">
                        <select id="filtro-asignatura" class="form-control">
                            <option value="null" hidden>Asignatura</option>
                            <?php foreach ($asignaturas as $asignatura): ?>
                                <option value="<?= $asignatura->id ?>" <?= $this->input->get('asignatura') == $asignatura->id ? 'selected' : '' ?>><?= $asignatura->asignatura ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group m-b-0">
                        <select id="filtro-nivel" class="form-control">
                            <option value="null" hidden>Nivel</option>
                            <?php foreach ($nivelesColegio as $nivel): ?>
                                <option value="<?= $nivel->id ?>" <?= $this->input->get('nivel') == $nivel->id ? 'selected' : '' ?>><?= $nivel->nivel ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group m-b-0">
                        <select id="filtro-tipo" class="form-control">
                            <option value="null" hidden>Tipo</option>
                            <?php foreach ($tipoPruebas as $tipo): ?>
                                <option value="<?= $tipo->id ?>" <?= $this->input->get('tipo') == $tipo->id ? 'selected' : '' ?>><?= $tipo->tipo ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group m-b-0">
                        <button id="filter" class="btn btn-info btn-block">Filtrar</button>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group m-b-0">
                        <button id="clean-filter" class="btn btn-info btn-block">Limpiar</button>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-3">
                    <div class="form-group m-b-0">
                        <input type="text" id="search" class="form-control" placeholder="Buscar">
                    </div>
                </div>
            </div>
        </div>
        <br>
        <!-- Evaluaciones e ejecución -->
        <div class="white-box">
            <h3 class="box-title">En Ejecución</h3>
            <div class="table-responsive">
                <table class="table" id="table-ejecutadas">
                    <thead>
                        <tr>
                            <th># ID</th>
                            <th>Nombre</th>
                            <th>Creador</th>
                            <th>Tipo</th>
                            <th>Asignatura</th>
                            <th>Curso</th>
                            <th style="width: 280px;">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($asignacionEjecutada as $asignacion): ?>
                        <tr>
                            <td><?= $asignacion->id ?></td>
                            <td><?= $asignacion->nombre ?></td>
                            <td><?= $asignacion->nombre_creador . ' ' . $asignacion->apellido_creador ?></td>
                            <td><?= $asignacion->tipo ?></td>
                            <td><?= $asignacion->asignatura ?></td>
                            <td><?= $asignacion->curso . ' ' . $asignacion->letra ?></td>
                            <td>
                                <button class="btn btn-ev" btn-alumnos id-curso="<?= $asignacion->curso_id ?>" id-evaluacion="<?= $asignacion->prueba_id ?>" id-asignacion="<?= $asignacion->id ?>">
                                    <i class="fa fa-users"></i>
                                </button>
                                <button class="btn btn-ev" btn-tabulacion id-asignacion="<?= $asignacion->id ?>">
                                    <i class="fa fa-file-text-o"></i>
                                </button>
                                <button class="btn btn-ev" btn-ver-evaluacion id-asignacion="<?= $asignacion->id ?>" id-evaluacion="<?= $asignacion->prueba_id ?>">
                                    <i class="ti-eye"></i>
                                </button>
                                <button class="btn btn-success" btn-finalizar id-asignacion="<?= $asignacion->id ?>">Finalizar</button>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Evaluaciones finalizadas -->
        <div class="white-box">
            <h3 class="box-title">Finalizadas</h3>
            <div class="table-responsive">
            <table class="table" id="table-finalizadas">
                    <thead>
                        <tr>
                            <th># ID</th>
                            <th>Nombre</th>
                            <th>Creador</th>
                            <th>Tipo</th>
                            <th>Asignatura</th>
                            <th>Curso</th>
                            <th style="width: 280px;">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($asignacionesFinalizadas as $asignacion):
                        $url = site_url() . '/Reporte/';
                        switch ($asignacion->tipo_id) {
                            case 1: $url .= "simce/$asignacion->id/PERSONAL"; break;
                            case 2: $url .= "pme/$asignacion->id/PERSONAL"; break;
                            case 3: $url .= "unidad/$asignacion->id/PERSONAL"; break;
                            case 4: $url .= "cobertura/$asignacion->id/PERSONAL"; break;
                            default: $url = 'javascript:void(0)'; break;
                        } ?>
                        <tr>
                            <td><?= $asignacion->id ?></td>
                            <td><?= $asignacion->nombre ?></td>
                            <td><?= $asignacion->nombre_creador . ' ' . $asignacion->apellido_creador ?></td>
                            <td><?= $asignacion->tipo ?></td>
                            <td><?= $asignacion->asignatura ?></td>
                            <td><?= $asignacion->curso . ' ' . $asignacion->letra ?></td>
                            <td>
                                <a class="btn btn-ev" href="<?= $url ?>" target="_blank">
                                    <i class="fa fa-cloud-download"></i>
                                </a>
                                <button class="btn btn-ev" btn-alumnos id-curso="<?= $asignacion->curso_id ?>" id-evaluacion="<?= $asignacion->prueba_id ?>" id-asignacion="<?= $asignacion->id ?>">
                                    <i class="fa fa-users"></i>
                                </button>
                                <button class="btn btn-ev" btn-ver-evaluacion id-asignacion="<?= $asignacion->id ?>" id-evaluacion="<?= $asignacion->prueba_id ?>">
                                    <i class="ti-eye"></i>
                                </button>
                                <button class="btn btn-primary" btn-ejecutar id-asignacion="<?= $asignacion->id ?>" id-curso="<?= $asignacion->curso_id ?>">Ejecutar</button>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>

<!-- Modals -->
<!-- Modal curso -->
<div id="modal-curso" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="modal-curso-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="modal-curso-label"><i class="fa fa-users"></i> ALUMNOS </h4>
                <p>SIMCE CUARTO BÁSICO CIENCIAS NATURALES</p>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table" id="table-curso">
                        <thead>
                            <tr>
                                <th>Habilitar</th>
                                <th>Rut</th>
                                <th>Nombre Alumno</th>
                                <th>Progreso</th>
                                <th>Tiempo</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal ver -->
<div id="modal-evaluacion" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="modal-evaluacion-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="modal-evaluacion-label"> <i class="fa fa-file-text-o"></i> VER EVALUACIÓN</h4>
            </div>
            <div class="modal-body">
                <object id="data-prueba-url" data="" type=""></object>
            </div>
        </div>
    </div>
</div>

<!-- Modal tabulación -->
<div id="modal-tabulacion" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="modal-tabulacion-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 10px;border: 3px solid #56b2bf;">
            <div class="modal-header" style="background: #56b2bf;color: #fff;font-size: 25px;font-weight: bold;">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>                        
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6t">
                        <p id="detalle-evaluacion"></p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-right" style="padding-left: 0px;padding-right: 100px;">
                        <button class="btn btn-ev btn-outline" btn-actualizar style="background: #fff;font-size: 20px;font-weight: bold;color: #56b2bf;border-color: #56b2bf;">Actualizar</button>                        
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="table-responsive" id="tabla-tabulacion">

                </div>
            </div>
        </div>
    </div>
</div>
