<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="<?php echo base_url()?>assets2/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    
    <title>Evaluación</title>

    <style>
    .encabezado {
        border: 1px solid <?= $color['color'] ?>;
        padding: 13px 25px;
        border-radius: 10px;
    }
    .numero {
        border: 1px solid <?= $color['color'] ?>;
        position: relative;
        border-radius: 7px;
        top: 16px;
        width: 30px;
        height: 30px;
        left: -13px;
        padding: 2px;
        font-weight: 600;
        color: <?= $color['color'] ?>
    }
    .letra {
        width: 30px;
        height: 30px;
        border: 1px solid <?= $color['color'] ?>;
        border-radius: 7px;
        padding-top: 2px;
        color: <?= $color['color'] ?>;
        font-weight: 600
    }

    @media print {
        @page { 
            margin: 0;
        }
        body { 
            margin: 10px;
        }
    }
    
    </style>
</head>
<body>
    
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <img style="width: 100%;" class="" src="<?= base_url() . 'assets/images/header/' . $img_head ?>" alt="">
            </div>
        </div>
    <?php foreach($preguntas as $pregunta): ?>
        <div class="mb-5">
            <div class="row mb-4">
                <div class="col">
                    <div class="numero bg-white text-center"><?= $pregunta->orden ?></div>
                    <div class="encabezado bg-white">
                        <?= str_replace(['<div>', '</div>'], '', $pregunta->pregunta) ?>
                    </div>
                </div>
            </div>

            <?php foreach($pregunta->alternativas as $alternativa):  
                //echo $alternativa->respuesta;
            ?>
                <div class="row mb-3">
                    <div class="col d-flex">
                        <div class="letra rounded-lg text-center mr-4"><?= $alternativa->letra ?></div>
                        <div>
                        <?php
                            //$alternativa->respuesta = trim(preg_replace('/(\r\n)|\n|\r/', '<br/>', $alternativa->respuesta));
                            //$alternativa->respuesta = str_replace('"', '\\"', $alternativa->respuesta);

                            echo $alternativa->respuesta;
                            ?>
                        </div>
                    
                    </div>
                </div>
            <?php endforeach ?>
            
        </div>
    <?php endforeach ?>
    </div>


    <script src="<?php echo base_url()?>assets2/js/jquery.min.js"></script>
    <script>
/*
    $(() => {
        window.print();
        window.onafterprint = function() {
            window.close(); 
            window.location.href = `<?= site_url() ?>/home`;
        }
    });
    */
    </script>

</body>
</html>