            <footer class="footer text-center hidden-sm hidden-md hidden-lg"> Aeduc <?=date('Y');?> </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/jquery/dist/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()?>assets/inverse/bootstrap/dist/js/bootstrap.js"></script>
    <!-- Sidebar menu plugin JavaScript -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.js"></script>
    <!--Slimscroll JavaScript For custom scroll-->
    <script src="<?php echo base_url()?>assets/inverse/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url()?>assets/inverse/js/waves.js"></script>
    <!-- Sweet-Alert  -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url()?>assets/inverse/js/custom.js"></script>
    <script src="<?php echo base_url()?>assets/inverse/js/jasny-bootstrap.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/switchery/dist/switchery.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <script type="text/javascript">
        let data = JSON.parse(`<?php echo json_encode($data); ?>`);
        let perfil = JSON.parse('<?php echo json_encode($perfil); ?>');
    </script>
    <script src="<?php echo base_url()?>assets/modulos/asignaciones/js/asignaciones.js"></script>
</body>

</html>
