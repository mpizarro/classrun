 <!-- Page Content -->
 <div id="page-wrapper">
    <div class="container-fluid">
        <h5 class="title-data" id="title-ejecucion">
            En Ejecución
        </h3>
        <div class="row" id="rowData">
            <div class="col-md-9 col-same-height">
                <div class="en-ejecucion">
                </div>

                <h5 class="title-data" margin-top-title>
                    Finalizadas
                </h5>
                <div id="asignacionContainer">
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="asignacionBackground">
                            <ul class="pagination pagination-split pull-right" id="paginador">
                                <li>
                                    <a href="javascript:void(0)">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                </li>

                                <li>
                                    <a href="javascript:void(0)">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-3 col-same-height" id="filtrosContainer">
                <div class="row">
                    <div class="col-md-12">
                        <h5 id="title-filtros"><strong>FILTROS</strong></h5>
                        <button class="btn btn-ejecutar" id="btn-limpiarFiltros">Limpiar Filtros</button>
                    </div>
                </div>
                <div class="row" padding-left-1>
                    <div class="col-md-12">
                        <h5 id="title-data"><strong>Tipo de Prueba</strong></h5>
                    </div>
                </div>
                <div class="row" padding-left-1>
                    <div class="col-md-12">
                        <div class="checkbox-inline-custom tipoFilter" margin-bottom>
                            <h6>SIMCE</h6>
                            <input type="checkbox" value="">
                        </div>
                        <div class="checkbox-inline-custom tipoFilter" margin-bottom>
                            <h6>COBERTURA</h6>
                            <input type="checkbox" value="">
                        </div>
                        <div class="checkbox-inline-custom tipoFilter" margin-bottom>
                            <h6>PME</h6>
                            <input type="checkbox" value="">
                        </div>
                        <div class="checkbox-inline-custom tipoFilter" margin-bottom>
                            <h6>UNIDAD</h6>
                            <input type="checkbox" value="">
                        </div>
                    </div>
                </div>
                <hr black>
                <div class="row" padding-left-1>
                    <div class="col-md-12">
                        <h5 id="title-data"><strong>Número de evaluación</strong></h5>
                    </div>
                </div>
                <div class="row" padding-left-1>
                    <div class="col-md-12 col-lg-6">
                        <div class="checkbox-inline-custom numeroFilter" num="1" margin-bottom>
                            <h6>Evaluación 1</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-6">
                        <div class="checkbox-inline-custom numeroFilter" num="2" margin-bottom>
                            <h6>Evaluación 2</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="checkbox-inline-custom numeroFilter" num="3" margin-bottom>
                            <h6>Evaluación 3</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="checkbox-inline-custom numeroFilter" num="4" margin-bottom>
                            <h6>Evaluación 4</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="checkbox-inline-custom numeroFilter" num="5" margin-bottom>
                            <h6>Evaluación 5</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="checkbox-inline-custom numeroFilter" num="6" margin-bottom>
                            <h6>Evaluación 6</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                </div>
                <hr black>
                <div class="row" padding-left-1>
                    <div class="col-md-12">
                        <h5 id="title-data"><strong>Asignatura</strong></h5>
                    </div>
                </div>
                <div class="row" padding-left-1>
                    <div class="col-md-12">
                        <div class="checkbox-inline-custom asignaturaFilter lenguaje" data-asignatura="LENG" margin-bottom>
                            <h6>Lenguaje</h6>
                            <input type="checkbox" value="">
                        </div>
                        <div class="checkbox-inline-custom asignaturaFilter matematicas" data-asignatura="MATE" margin-bottom>
                            <h6>Matemática</h6>
                            <input type="checkbox" value="">
                        </div>
                        <div class="checkbox-inline-custom asignaturaFilter ciencias" margin-bottom data-asignatura="CIEN">
                            <h6>Ciencias Naturales</h6>
                            <input type="checkbox" value="">
                        </div>
                        <div class="checkbox-inline-custom asignaturaFilter historia" margin-bottom data-asignatura="HISTO">
                            <h6>Historia</h6>
                            <input type="checkbox" value="">
                        </div>
                    </div>
                </div>
                <hr black>
                <div class="row" padding-left-1>
                    <div class="col-md-12">
                        <h5 id="title-data"><strong>Nivel Curso</strong></h5>
                    </div>
                </div>
                <div class="row" padding-left-1>
                    <div class="col-md-6">
                        <div class="checkbox-inline-custom nivelFilter" data-nivel="1" margin-bottom>
                            <h6>1º Básico</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox-inline-custom nivelFilter" data-nivel="2" margin-bottom>
                            <h6>2º Básico</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox-inline-custom nivelFilter" data-nivel="3" margin-bottom>
                            <h6>3º Básico</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox-inline-custom nivelFilter" data-nivel="4" margin-bottom>
                            <h6>4º Básico</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox-inline-custom nivelFilter" data-nivel="5" margin-bottom>
                            <h6>5º Básico</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox-inline-custom nivelFilter" data-nivel="6" margin-bottom>
                            <h6>6º Básico</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox-inline-custom nivelFilter" data-nivel="7" margin-bottom>
                            <h6>7º Básico</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox-inline-custom nivelFilter" data-nivel="8" margin-bottom>
                            <h6>8º Básico</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                </div>
                <div class="row" padding-left-1 margin-top-1>
                    <div class="col-md-6">
                        <div class="checkbox-inline-custom nivelFilter" data-nivel="1M" margin-bottom>
                            <h6>1º Medio</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox-inline-custom nivelFilter" data-nivel="2M" margin-bottom>
                            <h6>2º Medio</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox-inline-custom nivelFilter" data-nivel="3M" margin-bottom>
                            <h6>3º Medio</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox-inline-custom nivelFilter" data-nivel="4M" margin-bottom>
                            <h6>4º Medio</h6>
                            <input type="checkbox" value=""/>
                        </div>
                    </div>
                </div>
                <hr black>
                <div id="letrasFilter"></div>
            </div>
        </div>


    </div>
    <!-- /.container-fluid -->
    <!-- Modal -->
    <div id="ver-prueba" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-contenido">
                        <div class="modal-header">
                            <div class="first">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title ver-prueba"><i class="fa fa-file-text-o"></i> VER PRUEBA</h4>
                            </div>
                            <div class="last">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 class="modal-title info-prueba">Simce 2º Matemática</h6>
                                        <a href="javascript:void(0)" id="btn-descargar" class="btn btn-primary btn-descargar">Descargar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row container-evaluacion">
                                <object id="data-prueba-url" data="" type=""></object>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="planilla-tabulacion" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-contenido">
                        <div class="modal-header">
                            <div class="first">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title ver-prueba"><i class="fa fa-file-text-o"></i> PLANILLA DE TABULACIÓN</h4>
                            </div>
                            <div class="last">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h6 class="modal-title info-prueba"></h6>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <button id="btn-actualizar-planilla" id-asignacion="false" class="btn btn-outline btn-info m-r-5">Actualizar</button>
                                        <button id="btn-reiniciar" id-asignacion="false" class="btn btn-reiniciar m-r-40"><i class="fa fa-refresh"></i> Reiniciar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row container-evaluacion">
                                <div class="table-responsive" style="overflow: scroll; height: -webkit-fill-available; padding-right: 15px;">
                                    <table class="table">
                                        <thead id="head-planilla-tabulacion">
                                        </thead>
                                        <tbody id="body-planilla-tabulacion">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="planilla-usuarios" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="modal-contenido">
                        <div class="modal-header">
                            <div class="first">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title planilla-usuarios"><i class="fa fa-file-text-o"></i> ALUMNOS - </h4>
                            </div>
                            <div class="last">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h6 class="modal-title info-prueba"></h6>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <!--<button id="btn-actualizar-alumnos" id-asignacion="false" class="btn btn-outline btn-info m-r-40">Actualizar</button>-->
                                    </div>
                                </div>
                            </div>
                            <div class="row container">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead id="head-usuarios">
                                        </thead>
                                        <tbody id="body-usuarios">
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
