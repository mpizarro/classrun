<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">

        <div class="container">
            <div class="panel panel-default m-t-20">
                <div class="panel-heading">Análisis</div>
                <div class="panel-body">
                    <form class="form-horizontal" action="javascript:void(0)">
                        <div class="form-group" style="display: flex;">
                            <div class="col-sm-3">
                                <label for="profesor" class="control-label">Profesor</label>
                                <select id="profesor" class="form-control">
                                <?php if ($profesores): ?>
                                    <option value="" hidden>Seleccione un profesor</option>
                                    <?php foreach ($profesores as $profesor): ?>
                                        <option value="<?= $profesor->id ?>" <?= $this->input->get('profesor') == $profesor->id ? 'selected' : '' ?>><?= $profesor->nombre . " " . $profesor->apellido ?></option>
                                    <?php endforeach ?>
                                <?php else: ?>
                                    <option value="" hidden>Sin usuarios</option>
                                <?php endif; ?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label for="fechaInicio" class="control-label">Fecha inicio</label>
                                <input type="date" class="form-control" id="fechaInicio" value="<?= $this->input->get('inicio') ?>">
                            </div>
                            <div class="col-sm-3">
                                <label for="fechaFin" class="control-label">Fecha fin</label>
                                <input type="date" class="form-control" id="fechaFin" value="<?= $this->input->get('termino') ?>">
                            </div>
                            <div class="col-sm-1" style="align-self: flex-end;">
                                <button type="button" id="buscar" class="btn btn-ev form-control">Buscar</button>
                            </div>
                            <div class="col-sm-1" style="align-self: flex-end;">
                                <button type="button" id="clear" class="btn btn-ev form-control">Limpiar</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="search" class="control-label">Buscar</label>
                                <input type="search" class="form-control" id="search" placeholder="Buscar...">
                            </div>
                        </div>
                    </form>

                    <table class="table table-hover" id="analisis">
                        <thead>
                            <tr>
                                <th>Profesor</th>
                                <th>Interacciones</th>
                                <th>Fecha</th>
                            </tr>
                        </thead>
                        <tbody id="body-analisis">
                        <?php if ($analisis): ?>
                            <?php foreach ($analisis as $a): ?>
                                <tr>
                                    <td><?= $a->nombre . ' ' . $a->apellido?></td>
                                    <td><?= $a->cantidad ?></td>
                                    <td><?= date('d/m/Y', strtotime($a->fecha.'000000')) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>