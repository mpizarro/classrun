            <footer class="footer text-center hidden-sm hidden-md hidden-lg"></footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/jquery/dist/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()?>assets/inverse/bootstrap/dist/js/bootstrap.js"></script>
    <!-- Sidebar menu plugin JavaScript -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.js"></script>
    <!--Slimscroll JavaScript For custom scroll-->
    <script src="<?php echo base_url()?>assets/inverse/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url()?>assets/inverse/js/waves.js"></script>
    <!-- Sweet-Alert  -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/moment/moment.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url()?>assets/inverse/js/custom.js"></script>
    <script src="<?php echo base_url()?>assets/inverse/js/jasny-bootstrap.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
    <script>
        let data = JSON.parse(`<?php echo json_encode($preguntas); ?>`);
        let newTime = '01:30:02';
        let idEvaluacion = <?php echo $idEvaluacion; ?>;
        var tipoEvaluacion = <?php echo $tipo; ?>;
        let respuestas = JSON.parse('<?php echo json_encode($respuestas);?>');
        let site_url = '<?php echo site_url();?>';
        let asignacion_id = '<?php echo $asignacion_id; ?>';
        let origen = '<?php echo $origen; ?>';
        let idUsuarioHasAsignacion = '<?php echo $usuarioHasAsignacion_id; ?>';
        console.log(respuestas);

        $('#myModal').modal('show');


    </script>
    <script src="<?php echo base_url()?>assets/modulos/hacerEvaluacion/js/hacerEvaluacion.js?v=<?php echo(rand()); ?>"></script>
</body>

</html>
