<!DOCTYPE html>
<!--
   This is a starter template page. Use this page to start your new project from
   scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/plugins/images/favicon.png">
        <title>ClassRun</title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url()?>assets/inverse/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>assets/plugins/bower_components/datatables/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" type="text/css" />
        <!-- This is Sidebar menu CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.css" rel="stylesheet">
        <!-- TPermite usar el multi select de cursos, entre otros. -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <!--alerts CSS -->
        <!-- Calendar CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/calendar/dist/fullcalendar.css" rel="stylesheet" />
        <!--alerts CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
        <!-- This is a Animation CSS -->
        <link href="<?php echo base_url()?>assets/inverse/css/animate.css" rel="stylesheet">
        <?php
            if(isset($css))
            {
                echo $css;
            }
        ?>
        <!-- toast CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">

        <link href="<?php echo base_url()?>assets/inverse/css/style.css" rel="stylesheet">
        <!-- color CSS you can use different color css from css/colors folder -->
        <!-- We have chosen the skin-blue (default.css) for this starter
            page. However, you can choose any other skin from folder css / colors .
        -->
        <link href="<?php echo base_url()?>assets/inverse/css/colors/default.css" id="theme" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/inverse/css/headStyle.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/inverse/css/menuStyle.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/inverse/css/modalStyle.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
        .preguntas-style{
            border-top: 3px solid #56b2bf;
            border-radius: 15px;
        }
        .preguntas-style-2{
            display: flex;
            align-items: center;
        }
        .respuestas-style{
            height: 45px;
            width: 45px;
            display: flex;
            justify-content: center;
            vertical-align: middle;
            align-items: center;
            border: 1px solid #56b2bf;
            position: absolute;
            background: #fff;
            color: #56b2bf;
            border-radius: 30px;
            left: -18px;
        }
        .text-respuesta{
            padding-left: 29px;
            font-size: 20px;
        }
        .text-h1{
            margin-bottom: 0px;
            margin-top: 0px;
            text-align: center;
            border-radius: 10px;
            color:#56b2bf;
            font-weight: 300;
        }
        .num {
            background-color:#56b2bf;
            color:#fff;
        }
        .text-alternativas{
            height: 35px;
            width: 35px;
            border: 1px solid #56b2bf;
            position: absolute;
            background: #fff;
            color: #F47921;
            border-radius: 30px;

            left: -7px;
        }
        .text-h3{
            margin-bottom: 0px;
            margin-top: 2px;
            border-radius: 10px;
            color:#56b2bf;
            font-weight: 300;
            text-align: center;
        }
        .selected , .selected > h3
        {
            color: #fff !important;
            background-color:#56b2bf !important;
            font-weight: 500;
            text-transform: uppercase;
        }
        .selected , .selected > h2
        {
            color: #fff !important;
            background-color:#56b2bf  !important;
            font-weight: 500;
            text-transform: uppercase;
        }
        #numeros-preguntas{
            width: 100%;
            background: #fff;
            border-radius:7px;
            padding-bottom: 20px;
            padding-top: 10px;
        }
        #numeros-preguntas > label{
            padding-top: 27px;
            padding-bottom: 0px;
            margin-bottom: 17px;
            color:#56b2bf;
        }
        #numeros-preguntas-22{
            width: 100%;
            background: #fff;
            border-radius:7px;
            padding-bottom: 20px;
            padding-top: 10px;
        }
        #numeros-preguntas-22 > label{
            padding-top: 27px;
            padding-bottom: 0px;
            margin-bottom: 17px;
            color:#56b2bf;
        }
        .numeros{
            border: 1px solid #56b2bf;
            border-radius: 5px;
            margin-left: 3px;
            margin-bottom: 2px;
            margin-top: 2px;
            margin-right: 2px;
            color: #56b2bf;
            font-weight: 300;
            cursor: pointer;
        }
        .text-numeros{
            color: #56b2bf;
        }
        .finalizar{
            background: red;
            border: red;
            border-radius: 7px;
        }
        .numeros-2{
            border: 2px solid #56b2bf;
            border-radius: 25px;
            margin-left: 3px;
            margin-bottom: 2px;
            margin-top: 2px;
            margin-right: 2px;
            color: #56b2bf ;
            background-color: #fff;
            font-weight: 300;
            cursor: pointer;
        }

        #menu-lateral{
            
            width: auto;
            height: 100%;
        }
        #menu-lateral::-webkit-scrollbar {
            width: 1em;
        }

        #menu-lateral::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        }

        #menu-lateral::-webkit-scrollbar-thumb {
          background-color: #40464e;
          outline: 1px solid slategrey;
        }
        .div-numeros {
            background: none repeat scroll 0 0 #97C02F;
            color: #FFFFFF;
            margin: auto;
            overflow: hidden;
            padding: 0 10px;
            position: relative;
            width: 90%;
        }
        .div-numeros:before {
            background: none repeat scroll 0 0 #252e3a;
            border-color: transparent #efb94e;
            border-style: solid;
            border-width: 0 44px 44px 0px;
            content: "";
            display: block;
            position: absolute;
            left: 0;
            bottom: 0;
            width: 0;
        }
        .div-numeros-1{
            background-color: #fff;
            border-radius: 10px;
            width: 90%;
        }
        </style>
    </head>
    <body class="fix-sidebar">
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            <!-- Top Navigation -->
            <nav class="navbar navbar-default navbar-static-top m-b-0">
                <div class="navbar-header " style="background: #fff;">
                    <!-- Toggle icon for mobile view -->
                    <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                    <div class="top-left-part colorTopLeftPart">
                        
                    </div>
                    <!-- This is the message dropdown -->
                    <ul class="nav navbar-top-links navbar-right pull-right">
                        <br>
                        <br>
                        <br>
                        <!-- /.dropdown -->
                    </ul>
                </div>
                <!-- /.navbar-header -->
                <!-- /.navbar-top-links -->
                <!-- /.navbar-static-side -->
            </nav>
            <!-- End Top Navigation -->
