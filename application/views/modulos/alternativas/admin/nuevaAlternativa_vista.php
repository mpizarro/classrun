 <!-- Page Content -->
 <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title" id="title-page">Nueva alternativa</h4>
            </div>
            <!-- /.page title -->
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <form id="frmNuevaAlternativa" id-pregunta="<?= $idPregunta ?>" id-evaluacion="<?= $idEvaluacion ?>" codigo="<?= $codigo ?>" action="javascript:void(0)" method="post">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Alternativa</label>
                                        <textarea class="editor" name="alternativa"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /row -->
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-proup">
                                        <label class="control-label">Incorrecta </label>
                                        &nbsp;<input type="checkbox" id="correcta" class="js-switch" data-color="#99d683" data-secondary-color="#f96262" />&nbsp;
                                        <label class="control-label"> Correcta</label>
                                    </div>
                                </div>
                                <div class="col-md-6 text-right">
                                    <button type="button" id="btnAtrasPreguntas" class="btn btn-default" id-evaluacion="<?= $idEvaluacion ?>" codigo="<?= $codigo ?>">Atras</button>
                                    <button type="submit" id="btnGuardarAlternativa" class="btn btn-success"><i class="fa fa-check"></i> Guardar alternativa</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->

    <script>
        let idAlternativa = <?= $idAlternativa ? $idAlternativa : "false" ?>;
        let idPregunta = <?= $idPregunta ? $idPregunta : "false" ?>;
    </script>