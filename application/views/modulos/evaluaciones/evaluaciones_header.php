<!DOCTYPE html>
<!--
   This is a starter template page. Use this page to start your new project from
   scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="#">
        <title>ClassRun</title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url()?>assets/inverse/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>assets/plugins/bower_components/datatables/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" type="text/css" />
        <!-- This is Sidebar menu CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.css" rel="stylesheet">
        <!-- TPermite usar el multi select de cursos, entre otros. -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <!--alerts CSS -->
        <!-- Calendar CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/calendar/dist/fullcalendar.css" rel="stylesheet" />
        <!--alerts CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
        <!-- This is a Animation CSS -->
        <link href="<?php echo base_url()?>assets/inverse/css/animate.css" rel="stylesheet">
        <?php
            if (isset($css)) {
                echo $css;
            }
        ?>
        <!-- toast CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">

        <link href="<?php echo base_url()?>assets/inverse/css/style.css" rel="stylesheet">
        <!-- color CSS you can use different color css from css/colors folder -->
        <!-- We have chosen the skin-blue (default.css) for this starter
            page. However, you can choose any other skin from folder css / colors .
        -->
        <link href="<?php echo base_url()?>assets/inverse/css/colors/default.css" id="theme" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/inverse/css/headStyle.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/inverse/css/menuStyle.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/inverse/css/modalStyle.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Implementación del chat para profesores -->
        <?php if ($this->session->perfil_id != 3 && $this->session->perfil_id != 1 && $this->session->perfil_id != 8): ?>
        <script>
        document.addEventListener("DOMContentLoaded", function(event) { 
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function()
            {
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/5a03014fbb0c3f433d4c7ede/default';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
            })();
        });
        </script>
        <?php endif; ?>
    </head>
    <body class="fix-sidebar">
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            <!-- Top Navigation -->
            <nav class="navbar navbar-default navbar-static-top m-b-0">
                <div class="navbar-header colorNavbarHeader">
                    <!-- Toggle icon for mobile view -->
                    <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                    <div class="top-left-part colorTopLeftPart">
                        <!-- Logo -->
                        <a class="logo" href="<?php site_url()?>">
                            <!-- Logo icon image, you can use font-icon also -->
                            <b>
                                <!--This is dark logo icon-->
                                <img src="<?php echo base_url()?>assets/images/logo_evalua.png" alt="home" class="dark-logo" />
                                <!--This is light logo icon-->
                                <img src="<?php echo base_url()?>assets/images/logo_evalua.png" alt="home" class="light-logo" />
                            </b>
                            <!-- Logo text image you can use text also -->
                            <span class="hidden-md">
                                <!--This is dark logo text-->
                                <img src="<?php echo base_url()?>assets/images/aeduc_evalua.png" alt="home" class="dark-logo" />
                                <!--This is light logo text-->
                                <img src="<?php echo base_url()?>assets/images/aeduc_evalua.png" alt="home" class="light-logo" />
                            </span>
                        </a>
                    </div>
                    <!-- Search input and Toggle icon -->
                    <ul class="nav navbar-top-links navbar-left hidden-xs">
                        <li>
                            <a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light contentCenter">
                                <i class="icon-arrow-left-circle ti-menu fa-2x"></i>
                            </a>
                        </li>
                        <li>
                            <a class="waves-effect waves-light contentCenter" href="<?php echo site_url()?>">
                                <h5 class="page-title colorText"><strong>Inicio</strong></h5>
                            </a>
                        </li>
                    </ul>
                    <!-- This is the message dropdown -->
                    <ul class="nav navbar-top-links navbar-right pull-right">
                        <li class="hidden-xs">
                            <a class="waves-effect waves-light contentCenter" href="javascript:void(0)">
                                <h5 class="page-title colorText"><strong></strong></h5>
                            </a>
                        </li>
                        <li class="right-side-toggle">
                            <a class="waves-effect waves-light contentCenter borderDropdownToggle" href="javascript:cerrarSession()">
                                <i class="fa fa-times fa-2x"></i>
                            </a>
                        </li>
                        <!-- /.dropdown -->
                    </ul>
                </div>
                <!-- /.navbar-header -->
                <!-- /.navbar-top-links -->
                <!-- /.navbar-static-side -->
            </nav>
            <!-- End Top Navigation -->
            <!-- /.modal -->
            <div id="responsive-modal" class="modal fade bs-example-modal-lg positionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content contentModal">
                        <div class="modal-body">
                            <div class="row rowModal">
                                <div class="col-md-6 col-lg-6 modalPanelLeft">
                                    <center>
                                        <br><div><img src="<?php echo base_url()?>assets/plugins/images/usuario.png" alt="user-img" class="img-circle"></div><br>
                                        <h2 style="font-weight: bold;color:#fff;"><?php echo $this->session->userdata('nombre_completo');?></h2><br>
                                        <br/><br>

                                    </center>
                                </div>
                                <div class="col-md-6 col-lg-6 modalPanelRigth">
                                    <form class="form-horizontal" id="formPerfil" action="">
                                        <div class="form-group">
                                            <label for="contraseña" class="col-sm-4 control-label">Contraseña actual</label>
                                            <div class="col-sm-7">
                                                <input type="password" class="form-control cleanInput" id="pass_actual" name="pass_actual" placeholder="Ingrese..." required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="confirmar" class="col-sm-4 control-label">Nueva contraseña</label>
                                            <div class="col-sm-7">
                                                <input type="password" class="form-control cleanInput" id="pass_nueva" name="pass_nueva" placeholder="Ingrese.." required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="confirmar" class="col-sm-4 control-label">Confirmar nueva contraseña</label>
                                            <div class="col-sm-7">
                                                <input type="password" class="form-control cleanInput" id="pass_nueva_confirmar" name="pass_nueva_confirmar" placeholder="Confirmar..." onkeyup="enabledSave()" required>
                                                <small id="small_new_pass" style="color:red;display:none" class="form-text text-muted">Las contraseñas no coinciden.</small>
                                            </div>
                                            <label id="alerta" style=""></label>
                                        </div>
                                        <div class="col-sm-offset-5">
                                            <button type="button" class="btn waves-effect btnCancel" data-dismiss="modal" onclick="limpiarFormularioPerfil();">Cancelar</button>
                                            <button  id="SavePass" type="button" class="btn waves-effect waves-light btnSave" onclick="ChangePass();" disabled="true">Guardar cambios</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
