            
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/jquery/dist/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()?>assets/inverse/bootstrap/dist/js/bootstrap.js"></script>
    <!-- Sidebar menu plugin JavaScript -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.js"></script>
    <!--Slimscroll JavaScript For custom scroll-->
    <script src="<?php echo base_url()?>assets/inverse/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url()?>assets/inverse/js/waves.js"></script>
    <!-- Sweet-Alert  -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/moment/moment.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url()?>assets/inverse/js/custom.js"></script>
    <script src="<?php echo base_url()?>assets/inverse/js/jasny-bootstrap.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
    <script>
        let data = JSON.parse(`<?php echo json_encode($preguntas); ?>`);
        let idEvaluacion = <?php echo isset($idEvaluacion) ? $idEvaluacion : ""; ?>;
        var tipoEvaluacion = <?php echo isset($tipo) ? $tipo : ""; ?>;
        let respuestas = JSON.parse('<?php echo isset($respuestas) ? json_encode($respuestas) : json_encode(array());?>');
        <?php
            
            if(isset($imprime))
            {
                echo '
                window.print();
                window.onafterprint = function() 
                {
                    window.close();
                    window.location.href = "'.site_url().'/home"
                }';
            }
            
        ?>
    </script>
    <script src="<?php echo base_url()?>assets/modulos/evaluaciones/js/verEvaluacion.js"></script>
</body>

</html>
