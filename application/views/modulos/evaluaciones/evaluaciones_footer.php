            
            <!-- Modal -->
            <div id="ver-prueba" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="modal-contenido">
                                <div class="modal-header">
                                    <div class="first">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title ver-prueba"><i class="fa fa-file-text-o"></i> VER PRUEBA</h4>
                                        <div class="row">
                                            <div class="col-xs-12 text-right">
                                                <a href="javascript:void(0)" id="btn-descargar" class="btn btn-primary btn-descargar m-0">Descargar</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row container-evaluacion">
                                        <object id="data-prueba-url" data="" type=""></object>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/jquery/dist/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url()?>assets/inverse/bootstrap/dist/js/bootstrap.js"></script>
    <!-- Sidebar menu plugin JavaScript -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.js"></script>
    <!--Slimscroll JavaScript For custom scroll-->
    <script src="<?php echo base_url()?>assets/inverse/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url()?>assets/inverse/js/waves.js"></script>
    <!-- Sweet-Alert  -->
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url()?>assets/inverse/js/custom.js"></script>
    <script src="<?php echo base_url()?>assets/inverse/js/jasny-bootstrap.js"></script>
    <script src="<?php echo base_url()?>assets/inverse/js/jasny-bootstrap.js"></script>
    <script>
        let data = JSON.parse('<?php echo json_encode($cursos); ?>');
    </script>
    <script src="<?php echo base_url();?>assets/modulos/evaluaciones/js/evaluaciones.js"></script>
</body>

</html>
