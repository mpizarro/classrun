 <!-- Page Content -->
 <div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Evaluaciones</h4>
            </div>
            <!-- /.page title -->
        </div>
        <!-- acciones -->
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <input type="search" id="search" class="form-control" placeholder="Buscar">
                </div>
            </div>
            <div class="col-md-2">
                <button id="btnModalCrearEvaluacion" class="btn btn-info waves-effect waves-light"><span>Crear evaluación</span> <i class="fa fa-plus m-l-5"></i></button>
            </div>
        </div>
        <!-- /acciones -->
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table color-table evalua-table" id="table-evaluaciones">
                        <thead>
                            <tr>
                                <th># ID</th>
                                <th>Codigo</th>
                                <th>Titulo</th>
                                <th>Curso</th>
                                <th>Subsector</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="table-evaluaciones">
                            <?= $tablaEvaluaciones ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <!-- Crear -->
    <div class="modal fade" id="mdlCrearEvaluacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title colorText" id="exampleModalLabel1">Crear evaluación</h4>
                </div>
                <div class="modal-body">
                    <form id="frmCrearEvaluacion" class="form-horizontal" action="javascript:void(0)" method="post">
                        
                        <div class="form-group">
                            <label for="exampleInputuname" class="col-sm-3 control-label">Codigo</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="codigo" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputuname" class="col-sm-3 control-label">Numero</label>
                            <div class="col-sm-9">
                                <input type="number" min="1" class="form-control" name="numero" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputuname" class="col-sm-3 control-label">Tipo</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="tipo" required><?= $selectTipos ?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputuname" class="col-sm-3 control-label">Nivel</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="nivel" required><?= $selectNiveles ?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputuname" class="col-sm-3 control-label">Asignatura</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="asignatura" required><?= $selectAsignaturas ?></select>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary" form="frmCrearEvaluacion">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Editar -->
    <div class="modal fade" id="mdlEditarEvaluacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title colorText" id="exampleModalLabel2">Editar evaluación</h4>
                </div>
                <div class="modal-body">
                    <form id="frmEditarEvaluacion" class="form-horizontal" action="javascript:void(0)" method="post">
                       
                        <div class="form-group">
                            <label for="exampleInputuname" class="col-sm-3 control-label">Codigo</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="codigo" id="codigo" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputuname" class="col-sm-3 control-label">Numero</label>
                            <div class="col-sm-9">
                                <input type="number" min="1" class="form-control" name="numero" id="numero" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputuname" class="col-sm-3 control-label">Tipo</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="tipo" id="tipo" required><?= $selectTipos ?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputuname" class="col-sm-3 control-label">Nivel</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="nivel" id="nivel" required><?= $selectNiveles ?></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputuname" class="col-sm-3 control-label">Asignatura</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="asignatura" id="asignatura" required><?= $selectAsignaturas ?></select>
                            </div>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary" form="frmEditarEvaluacion">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Ver -->
    <div class="modal fade" id="modalVerEvaluacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2">
        <div class="modal-dialog" style="width: 80%;" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <object type="text/html" id="objectVistaEvaluacion" data="" width="100%"></object>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>