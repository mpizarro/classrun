<!DOCTYPE html>
<!--
   This is a starter template page. Use this page to start your new project from
   scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="#">
        <title>ClassRun</title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url()?>assets/inverse/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>assets/plugins/bower_components/datatables/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" type="text/css" />
        <!-- This is Sidebar menu CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.css" rel="stylesheet">
        <!-- TPermite usar el multi select de cursos, entre otros. -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <!--alerts CSS -->
        <!-- Calendar CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/calendar/dist/fullcalendar.css" rel="stylesheet" />
        <!--alerts CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
        <!-- This is a Animation CSS -->
        <link href="<?php echo base_url()?>assets/inverse/css/animate.css" rel="stylesheet">
        <?php
            if(isset($css))
            {
                echo $css;
            }
        ?>
        <!-- toast CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">

        <link href="<?php echo base_url()?>assets/inverse/css/style.css" rel="stylesheet">
        <!-- color CSS you can use different color css from css/colors folder -->
        <!-- We have chosen the skin-blue (default.css) for this starter
            page. However, you can choose any other skin from folder css / colors .
        -->
        <link href="<?php echo base_url()?>assets/inverse/css/colors/default.css" id="theme" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/inverse/css/headStyle.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/inverse/css/menuStyle.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/inverse/css/modalStyle.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .preguntas-style{
            border: 1px solid <?php echo $color; ?>;
            border-radius: 15px;
        }
        .preguntas-style-2{
            display: flex;
            align-items: center;
        }
        .respuestas-style{
            height: 45px;
            width: 45px;
            display: flex;
            justify-content: center;
            vertical-align: middle;
            align-items: center;
            border: 1px solid <?php echo $color; ?>;
            position: absolute;
            background:#FFFFFF;
            color:<?php echo $color; ?>;
            border-radius: 10px;
            left: -18px;
            top:-15px;
        }
        .text-respuesta{
            padding-left: 29px;
        }
        .text-h1{
            margin-bottom: 0px;
            margin-top: 0px;
            text-align: center;
            border-radius: 10px;
            color:<?php echo $color; ?>;
            font-weight: 300;
        }
        .num {
            background-color:<?php echo $color; ?>;
            color:#fff;
        }
        .text-alternativas{
            height: 35px;
            width: 35px;
            border: 1px solid <?php echo $color; ?>;
            position: absolute;
            background: #fff;
            color: #F47921;
            border-radius: 10px;

            left: -7px;
        }
        .text-h3{
            margin-bottom: 0px;
            margin-top: 2px;
            border-radius: 10px;
            color:<?php echo $color; ?>;
            font-weight: 300;
            text-align: center;
        }
        .selected , .selected > h3
        {
            color: #fff !important;
            background-color:<?php echo $color; ?>;
            font-weight: 500;
            text-transform: uppercase;
        }
        .selected , .selected > h2
        {
            color: #fff !important;
            background-color:<?php echo $color; ?> !important;
            font-weight: 500;
            text-transform: uppercase;
        }
        #numeros-preguntas{
            width: 100%;
            background: #fff;
            border-radius:7px;
            padding-bottom: 20px;
            padding-top: 10px;
        }
        #numeros-preguntas > label{
            padding-top: 27px;
            padding-bottom: 0px;
            margin-bottom: 17px;
            color:<?php echo $color; ?>;
        }
        #numeros-preguntas-22{
            width: 100%;
            background: #fff;
            border-radius:7px;
            padding-bottom: 20px;
            padding-top: 10px;
        }
        #numeros-preguntas-22 > label{
            padding-top: 27px;
            padding-bottom: 0px;
            margin-bottom: 17px;
            color:<?php echo $color; ?>;
        }
        .numeros{
            border: 1px solid <?php echo $color; ?>;
            border-radius: 5px;
            margin-left: 3px;
            margin-bottom: 2px;
            margin-top: 2px;
            margin-right: 2px;
            color: <?php echo $color; ?>;
            font-weight: 300;
        }
        .text-numeros{
            color: <?php echo $color; ?>;
        }
        .finalizar{
            background: red;
            border: red;
            border-radius: 7px;
        }
        .numeros-2{
            border: 1px solid <?php echo $color; ?>;
            border-radius: 5px;
            margin-left: 3px;
            margin-bottom: 2px;
            margin-top: 2px;
            margin-right: 2px;
            color: <?php echo $color; ?>;
            background-color: #fdfdfd;
            font-weight: 300;
        }

        #menu-lateral{
            overflow: scroll !important;
            width: auto;
            height: 100%;
        }
        #menu-lateral::-webkit-scrollbar {
            width: 1em;
        }

        #menu-lateral::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        }

        #menu-lateral::-webkit-scrollbar-thumb {
          background-color: #40464e;
          outline: 1px solid slategrey;
        }
        .div-numeros {
            background: none repeat scroll 0 0 #97C02F;
            color: #FFFFFF;
            margin: auto;
            overflow: hidden;
            padding: 0 10px;
            position: relative;
            width: 90%;
        }
        .div-numeros:before {
            background: none repeat scroll 0 0 #252e3a;
            border-color: transparent #efb94e;
            border-style: solid;
            border-width: 0 44px 44px 0px;
            content: "";
            display: block;
            position: absolute;
            left: 0;
            bottom: 0;
            width: 0;
        }
        .div-numeros-1{
            background-color: #fff;
            border-radius: 10px;
            width: 90%;
        }
        
        <?php
        if(isset($imprime))
        echo '
        body
        {
			margin: 0px;
			-webkit-print-color-adjust: exact !important;	
		}
        @media print 
        {
            input[type="checkbox"] 
            {
                display: none;
            }
		}
        ';
        ?>

        </style>
    </head>
    <body class="fix-sidebar">
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            <!-- Top Navigation -->
