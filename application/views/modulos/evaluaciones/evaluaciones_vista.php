 <!-- Page Content -->
 <div id="page-wrapper">
    <div class="row steps">
        <div class="col-md-3 step" data-step-nav="1" passed="false" active="true">
            <div class="numberStep">
                <span>1</span>
            </div>
            <h6>Seleccionar Prueba</h6>
        </div>
        <div class="col-md-3 step" data-step-nav="2" passed="false">
            <div class="numberStep">
                <span>2</span>
            </div>
            <h6>Seleccionar Nivel Prueba</h6>
        </div>
        <div class="col-md-3 step" data-step-nav="3" passed="false">
            <div class="numberStep">
                <span>3</span>
            </div>
            <h6>Seleccionar Curso</h6>
        </div>
        <div class="col-md-3 step" data-step-nav="4" passed="false">
            <div class="numberStep">
                <span>4</span>
            </div>
            <h6>Ejecutar Prueba</h6>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row" data-step="1" id="rowContainer">
            <div class="col-md-3 shadow">
                <div class="title-tipo">
                    <h4>Simce</h4>
                </div>
                <div class="row">
                    <center><p><strong>Nº evaluación :</strong> Diagnóstico</p></center>
                </div>
                <div class="row numbersContent">
                    <div class="col-md-12">
                        <div class="number">
                            1
                        </div>
                        <div class="divider first"></div>
                        <div class="ellipsis dropDownEllipsis">
                            <div class="btn-group" style="width:100%">
                              <button type="button" style="border: none; padding:0px;width:100%; background: none; box-shadow: none;" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">...
                              </button>
                              <ul class="dropdown-menu">
                                <li><a href="javascript:void(0)">2</a></li>
                                <li><a href="javascript:void(0)">3</a></li>
                                <li><a href="javascript:void(0)">4</a></li>
                                <li><a href="javascript:void(0)">5</a></li>
                              </ul>
                            </div>
                        </div>
                        <div class="divider last"></div>
                        <div class="number">
                            6
                        </div>
                    </div>
                </div>
                <div class="row">
                    <hr margin-horizontal no-margin-bottom>
                </div>
                <div class="row text-center" no-margin-top>
                    <h5>Asignatura</h5>
                </div>
                <div class="row asignaturaContainer" no-margin-top>
                </div>
                <div class="row">
                    <hr margin-horizontal no-margin-bottom>
                </div>
                <div class="row text-center btn-continuar" margin-bottom>
                    <button class="btn btn-default"> Continuar </button>
                </div>
            </div>
            <div class="col-md-3 shadow">
                <div class="title-tipo">
                    <h4>Cobertura</h4>
                </div>
                <div class="row">
                    <center><p><strong>Nº evaluación :</strong> Diagnóstico</p></center>
                </div>
                <div class="row numbersContent">
                    <div class="col-md-12">
                        <div class="number">
                            1
                        </div>
                        <div class="divider first"></div>
                        <div class="ellipsis numberEllipsis">
                            2
                        </div>
                        <div class="divider last"></div>
                        <div class="number">
                            3
                        </div>
                    </div>
                </div>
                <div class="row">
                    <hr margin-horizontal no-margin-bottom>
                </div>
                <div class="row text-center" no-margin-top>
                    <h5>Asignatura</h5>
                </div>
                <div class="row asignaturaContainer" no-margin-top>
                </div>
                <div class="row">
                    <hr margin-horizontal no-margin-bottom>
                </div>
                <div class="row text-center btn-continuar" margin-bottom>
                    <button class="btn btn-default"> Continuar </button>
                </div>
            </div>
            <div class="col-md-3 shadow">
                <div class="title-tipo">
                    <h4>Unidad</h4>
                </div>
                <div class="row">
                    <center><p><strong>Nº evaluación :</strong> Diagnóstico</p></center>
                </div>
                <div class="row numbersContent">
                    <div class="col-md-12">
                        <div class="number">
                            1
                        </div>
                        <div class="divider first"></div>
                        <div class="ellipsis">
                            <div class="btn-group" style="width:100%">
                              <button type="button" style="border: none; padding:0px;width:100%; background: none; box-shadow: none;" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">...
                              </button>
                              <ul class="dropdown-menu">
                                <li><a href="javascript:void(0)">2</a></li>
                                <li><a href="javascript:void(0)">3</a></li>
                                <li><a href="javascript:void(0)">4</a></li>
                                <li><a href="javascript:void(0)">5</a></li>
                              </ul>
                            </div>
                        </div>
                        <div class="divider last"></div>
                        <div class="number">
                            6
                        </div>
                    </div>
                </div>
                <div class="row">
                    <hr margin-horizontal no-margin-bottom>
                </div>
                <div class="row text-center" no-margin-top>
                    <h5>Asignatura</h5>
                </div>
                <div class="row asignaturaContainer" no-margin-top>
                </div>
                <div class="row">
                    <hr margin-horizontal no-margin-bottom>
                </div>
                <div class="row text-center btn-continuar" margin-bottom>
                    <button class="btn btn-default"> Continuar </button>
                </div>
            </div>
            <div class="col-md-3 shadow">
                <div class="title-tipo">
                    <h4>PME</h4>
                </div>
                <div class="row">
                    <center><p><strong>Nº evaluación :</strong> Diagnóstico</p></center>
                </div>
                <div class="row numbersContent">
                    <div class="col-md-12">
                        <div class="number">
                            1
                        </div>
                        <div class="divider first"></div>
                        <div class="ellipsis numberEllipsis">
                            2
                        </div>
                        <div class="divider last"></div>
                        <div class="number">
                            3
                        </div>
                    </div>
                </div>

                <div class="row">
                    <hr margin-horizontal no-margin-bottom>
                </div>
                <div class="row text-center" no-margin-top>
                    <h5>Asignatura</h5>
                </div>
                <div class="row asignaturaContainer" no-margin-top>
                </div>
                <div class="row">
                    <hr margin-horizontal no-margin-bottom>
                </div>
                <div class="row text-center btn-continuar" margin-bottom>
                    <button class="btn btn-default"> Continuar </button>
                </div>
            </div>
        </div>
        <div class="row" data-step="2" hidden>
            <div class="col-md-3 shadow" id="dataSelected">
                <div class="title-tipo" orange>
                    <h4 id="infoTipo">Simce</h4>
                </div>
                <div class="row numbersContent">
                    <div class="col-md-12 padding" no-padding-bottom>
                        <div style="width : 100%; text-align: center;" id="infoEvaluacion">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <hr margin-horizontal no-margin-bottom>
                </div>
                <div class="row text-center" no-margin-top>
                    <h5>Asignatura</h5>
                </div>
                <div class="col-md-12 text-center" style="pointer-events: none;">
                    <button class="btn btn-primary" margin-vertical style="width:50%; white-space: initial" id="infoAsignatura"> </button>
                </div>
                <div class="row">
                    <hr margin-horizontal no-margin-bottom>
                </div>
            </div>
            <div class="col-md-3 shadow" id="selectNivel">
                <div class="title-tipo" orange>
                    <h4>Nivel</h4>
                </div>
                <div class="text-center row" id="rowNiveles">
                    <div margin-vertical>
                        <strong margin-vertical>Seleccione el nivel de la prueba</strong>
                    </div>
                </div>
                <div class="row" margin-bottom>
                    <hr margin-horizontal no-margin-bottom>
                </div>
                <div class="row text-center" margin-bottom>
                    <button class="btn btn-info" id="continuarNivel" disabled onclick="goSetCurso()">Continuar</button>
                </div>
            </div>
            <div class="col-md-7 shadow" id="selectCurso" hidden>
                <div class="title-tipo" orange>
                    <h4>Curso</h4>
                </div>
                <div class="text-center row">
                    <div margin-vertical class="text-left padding-left">
                        <strong margin-vertical>Seleccione el nivel que evaluará</strong>
                    </div>
                    <div class="col-md-12 text-center padding"  style="padding-bottom : 0px">
                        <div class="row padding-horizontal" id="basica">

                        </div>
                    </div>
                    <div class="col-md-12 text-center padding" style="padding-top : 0px">
                        <div class="row padding-horizontal" id="media">

                        </div>
                    </div>
                </div>
                <div class="row" margin-bottom>
                    <hr margin-horizontal no-margin-bottom>
                </div>
                <div class="row padding-horizontal">
                    <div class="col-md-12" id="colLetter">

                    </div>
                </div>
                <div class="row padding padding-horizontal">
                    <div class="col-md-12">
                        <button class="btn btn-info" disabled id="continuarEjecutar">Continuar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" data-step="3" hidden>
            <div class="col-md-3 shadow" id="dataSelected">
                <div class="title-tipo" orange>
                    <h4 id="infoTipo">Simce</h4>
                </div>
                <div class="row numbersContent">
                    <div class="col-md-12 padding" no-padding-bottom>
                        <div style="width : 100%; text-align: center;" id="infoEvaluacion">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <hr margin-horizontal no-margin-bottom>
                </div>
                <div class="row text-center" no-margin-top>
                    <h5>Asignatura</h5>
                </div>
                <div class="col-md-12 text-center" style="pointer-events: none;">
                    <button class="btn btn-primary" margin-vertical style="width:50%; white-space: initial" id="infoAsignatura"> </button>
                </div>
                <div class="row">
                    <hr margin-horizontal no-margin-bottom>
                </div>
            </div>
            <div class="col-md-3 shadow" id="selectNivel">
                <div class="title-tipo" orange>
                    <h4>Nivel</h4>
                </div>
                <div class="text-center row" id="rowNiveles">
                    <div margin-vertical>
                        <strong margin-vertical>Seleccione el nivel de la prueba</strong>
                    </div>
                    <div class="col-md-12 text-center" margin-vertical>
                        <button class="btn btn-default btn-nivel"> 2º Básico</button>
                    </div>
                </div>
                <div class="row" margin-bottom>
                    <hr margin-horizontal no-margin-bottom>
                </div>
                <div class="row text-center" margin-bottom>
                    <button class="btn btn-info" id="continuarNivel" disabled onclick="goSetCurso()">Continuar</button>
                </div>
            </div>
            <div class="col-md-7 shadow" id="selectCurso" hidden>
                <div class="title-tipo" orange>
                    <h4>Curso</h4>
                </div>
                <div class="text-center row">
                    <div margin-vertical class="text-left padding-left">
                        <strong margin-vertical>Seleccione el curso que evaluará</strong>
                    </div>
                    <div class="col-md-12 text-center padding"  style="padding-bottom : 0px">
                        <div class="row padding-horizontal" id="basica">

                        </div>
                    </div>
                    <div class="col-md-12 text-center padding" style="padding-top : 0px">
                        <div class="row padding-horizontal" id="media">

                        </div>
                    </div>
                </div>
                <div class="row" margin-bottom>
                    <hr margin-horizontal no-margin-bottom>
                </div>
                <div class="row padding-horizontal">
                    <div class="col-md-12" id="colLetter">

                    </div>
                </div>
                <div class="row padding padding-horizontal">
                    <div class="col-md-12">
                        <button class="btn btn-info" disabled id="continuarEjecutar">Continuar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" data-step="4" hidden>
            <div class="col-md-3 shadow" id="dataSelected">
                <div class="title-tipo" orange>
                    <h4 id="infoTipo">Simce</h4>
                </div>
                <div class="row numbersContent">
                    <div class="col-md-12 padding" no-padding-bottom>
                        <div style="width : 100%; text-align: center;" id="infoEvaluacion">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <hr margin-horizontal no-margin-bottom>
                </div>
                <div class="row text-center" no-margin-top>
                    <h5>Asignatura</h5>
                </div>
                <div class="col-md-12 text-center" style="pointer-events: none;">
                    <button class="btn btn-primary" margin-vertical style="width:50%; white-space: initial" id="infoAsignatura"> </button>
                </div>
                <div class="row">
                    <hr margin-horizontal no-margin-bottom>
                </div>
            </div>
            <div class="col-md-8 shadow">
                <div class="title-tipo" orange>
                    <h4 id="title-alumnos">LISTA DE ALUMNOS - 4° BÁSICO B</h4>
                </div>
                <div class="text-center row" id="alumnosList">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

    <div id="modal-evaluacion-deshabilitada" class="modal fade" role="dialog" style="">
        <div class="modal-dialog" role="document" style="width: 32%; height: auto!important;">
            <div class="modal-content" style="background: white; height: auto;">
                <div class="modal-header bg-evalua" style="height: auto; padding: 15px; color: #fff;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" class="text-white">&times;</span></button>
                    <h4 class="modal-title text-white"><i class="fa fa-file-text-o"></i> Evaluación deshabilitada</h4>
                </div>
                <div class="modal-body" style="height: auto; background: white; border-radius: 0; padding: 15px; text-align: center;">
                    <p style="color: #707070; margin: 20px 0;">Este tipo de evaluación no está habilitada para tu establecimiento.</p>
                    <a href="https://www.aeduc.cl/contacto/" target="_blank"><div class="btn btn-info" style="margin: 20px 0;">Solicitar Información</div></a>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default btn-outline" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
