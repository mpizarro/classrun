<!DOCTYPE html>
<html lang="en">
<head>
    <link href="<?php echo base_url()?>assets/inverse/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="<?php echo base_url()?>assets2/css/highcharts.css" rel="stylesheet" type="text/css">


    <title><?= $pdfName ?></title>
    <style>
    {
        font-family: 'Open Sans', sans-serif;
    }

    .title1 {
        height: 46px;
        padding: -10px 0 15px 0;
        margin-top: 15px;
        color: #fff;
        text-align: center;
        background: <?= $color ?>;
        font-size: 34px;
    }
    .title2, .title3 {
        border: 1px solid #61b7c3;
        font-size: 20px;
        text-align: center;
        padding: -5px 0 3px 0;
        color: #61b7c3;
    }
    .title2 {
        margin-top: 30px;
    }
    .title3 {
        margin-top: 35px;
    }


#container {
    height: 400px;
    max-width: 800px;
    margin: 0 auto;
}

/* Link the series colors to axis colors */
.highcharts-color-0 {
    fill: #7cb5ec;
    stroke: #7cb5ec;
}
.highcharts-axis.highcharts-color-0 .highcharts-axis-line {
    stroke: #7cb5ec;
}
.highcharts-axis.highcharts-color-0 text {
    fill: #7cb5ec;
}
.highcharts-color-1 {
    fill: #90ed7d;
    stroke: #90ed7d;
}
.highcharts-axis.highcharts-color-1 .highcharts-axis-line {
    stroke: #90ed7d;
}
.highcharts-axis.highcharts-color-1 text {
    fill: #90ed7d;
}


.highcharts-yaxis .highcharts-axis-line {
    stroke-width: 2px;
}

    </style>
</head>
<body style="padding: 25px -45px 30px -45px;">