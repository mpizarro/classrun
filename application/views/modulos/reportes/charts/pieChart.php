<?php
$pie3dChart = new gchart\gPieChart();
$pie3dChart->addDataSet($data);
$pie3dChart->setColors($color);

//Save the image
print $pie3dChart->renderImage();
imagepng($pie3dChart->renderImage());

$data = ob_get_clean();
file_put_contents("assets/images/reportes/$fileName", $data);

?>