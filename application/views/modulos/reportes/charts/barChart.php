<?php
$barChart = new gchart\gBarChart($width, 200, 'g');
$barChart->addDataSet($datos);
$barChart->setColors($colors);

$barChart->setAutoBarWidth();
//Save the image
imagepng($barChart->renderImage());

$data = ob_get_clean();
file_put_contents("assets/images/reportes/$fileName", $data);
?>