    </div>

<script src="<?php echo base_url()?>assets2/js/highcharts.js"></script>
<script src="<?php echo base_url()?>assets2/js/exporting.js"></script>
<script src="<?php echo base_url()?>assets2/js/data.js"></script>
<script src="<?php echo base_url()?>assets2/js/drilldown.js"></script>
<script src="<?php echo base_url()?>assets2/js/export-data.js"></script>


<script type="text/javascript">

    // Create the chart
Highcharts.chart('grafico1', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:f}%'
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}%</b> total<br/>'
    },
    series: [
        {
            name: "estándares",
            colorByPoint: true,
            data: [
                {
                    name: "Inicial",
                    y:<?= $nivel['inicial'] ?>                    
                },
                {
                    name: "Intermedio",
                    y:<?= $nivel['intermedio'] ?>                    
                },
                {
                    name: "Adecuado",
                    y: <?= $nivel['adecuado'] ?>                    
                }
            ]
        }
    ]
});


Highcharts.chart('graficoHabilidades', {
    chart: {
        type: 'bar'
    },
    title: {
        text: ' '
    },
    xAxis: {
        categories: [<?= $habilidadesGrafico ?>]
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: [{
        name: 'habilidad',
        data: [<?= $habilidadesValor ?>]
    }]
});
</script>

</body>
</html>