

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Epoch - Responsive Bootstrap 4 Admin Dashboard</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        
        <link rel="shortcut icon" href="<?php echo base_url()?>assets2/images/favicon.ico">

        <link href="<?php echo base_url()?>assets2/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url()?>assets2/css/icons.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url()?>assets2/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url()?>assets2/css/seleccionar.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url()?>assets2/css/crear.css" rel="stylesheet" type="text/css">
        <?= $this->load->view('modulos/reportes/header', [], TRUE) ?>

    </head>


    <body>
        
        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
                <div class="container-fluid">

                    <!-- Logo container-->
                    <div class="logo">
                        <!-- Text Logo -->
                        <!--<a href="index.html" class="logo">-->
                        <!--Epoch-->
                        <!--</a>-->
                        <!-- Image Logo -->
                        <a href="#" class="logo">
                            <img src="<?php echo base_url()?>assets2/images/logo_classrun_blanco.png" alt="" height="50" width="100" class="logo-small">
                            <img src="<?php echo base_url()?>assets2/images/logo_classrun_blanco.png" alt="" class="logo-large">
                        </a>

                    </div>
                    <!-- End Logo container-->


                    <div class="menu-extras topbar-custom">

                        <ul class="list-inline float-right mb-0">
                            <li class="list-inline-item hide-phone app-search">
                                <form role="search" class="">
                                    <input type="text" placeholder="Buscar" class="form-control">
                                    <a href=""><i class="fa fa-search"></i></a>
                                </form>
                            </li>
                            
                            <!-- notification-->
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <i class="mdi mdi-bell-outline noti-icon"></i>
                                    <span class="badge badge-success noti-icon-badge">21</span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-menu-lg">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5>Notification (3)</h5>
                                    </div>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-success"><i class="mdi mdi-cart-outline"></i></div>
                                        <p class="notify-details"><b>Your order is placed</b><small class="text-muted">Dummy text of the printing and typesetting industry.</small></p>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-warning"><i class="mdi mdi-message"></i></div>
                                        <p class="notify-details"><b>New Message received</b><small class="text-muted">You have 87 unread messages</small></p>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <div class="notify-icon bg-info"><i class="mdi mdi-martini"></i></div>
                                        <p class="notify-details"><b>Your item is shipped</b><small class="text-muted">It is a long established fact that a reader will</small></p>
                                    </a>

                                    <!-- All-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        View All
                                    </a>

                                </div>
                            </li>
                            <!-- User-->
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <img src="<?php echo base_url()?>assets2/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                                </a>
                                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h5>Welcome</h5>
                                    </div>
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-account-circle m-r-5 text-muted"></i> Profile</a>
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-wallet m-r-5 text-muted"></i> My Wallet</a>
                                    <a class="dropdown-item" href="#"><span class="badge badge-success float-right">5</span><i class="mdi mdi-settings m-r-5 text-muted"></i> Settings</a>
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-lock-open-outline m-r-5 text-muted"></i> Lock screen</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#"><i class="mdi mdi-logout m-r-5 text-muted"></i> Logout</a>
                                </div>
                            </li>
                            <li class="menu-item list-inline-item">
                                <!-- Mobile menu toggle-->
                                <a class="navbar-toggle nav-link">
                                    <div class="lines">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </a>
                                <!-- End mobile menu toggle-->
                            </li>

                        </ul>
                        
                    </div>
                    <!-- end menu-extras -->

                    <div class="clearfix"></div>

                </div> <!-- end container -->
            </div>
            <!-- end topbar-main -->

            <!-- MENU Start -->
            <div class="navbar-custom">
                <div class="container-fluid">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        <ul class="navigation-menu text-center">

                            <li class="has-submenu ">
                                <a href="#"><i class="mdi mdi-airplay"></i>Evaluaciones</a>
                            </li>

                            <li class="has-submenu ">
                                <a href="#"><i class="mdi mdi-layers"></i>Mis Asignaciones</a>
                            </li>
                        </ul>
                        
                        
                        <!-- End navigation menu -->
                    </div> <!-- end #navigation -->
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->

        <?=$html?>

        <!-- jQuery  -->
        <script src="<?php echo base_url()?>assets2/js/jquery.min.js"></script>
        <script src="<?php echo base_url()?>assets2/js/popper.min.js"></script>
        <script src="<?php echo base_url()?>assets2/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url()?>assets2/js/modernizr.min.js"></script>
        <script src="<?php echo base_url()?>assets2/js/waves.js"></script>
        <script src="<?php echo base_url()?>assets2/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url()?>assets2/js/jquery.nicescroll.js"></script>
        <script src="<?php echo base_url()?>assets2/js/jquery.scrollTo.min.js"></script>
        

        <script src="<?php echo base_url()?>assets2/plugins/tiny-editable/mindmup-editabletable.js"></script>
        <script src="<?php echo base_url()?>assets2/plugins/tiny-editable/numeric-input-example.js"></script>
        <script src="<?php echo base_url()?>assets2/plugins/tabledit/jquery.tabledit.js"></script>
        <script src="<?php echo base_url()?>assets2/js/highcharts.js"></script>
        <script src="<?php echo base_url()?>assets2/js/exporting.js"></script>
        <script src="<?php echo base_url()?>assets2/js/data.js"></script>
        <script src="<?php echo base_url()?>assets2/js/drilldown.js"></script>
        <script src="<?php echo base_url()?>assets2/js/export-data.js"></script>

        <?=$js?>
        
        <!-- App js -->
        <script src="<?php echo base_url()?>assets2/js/app.js"></script>
        <script src="<?php echo base_url()?>assets2/js/crear.js"></script>
        <script type="text/javascript">
            function exportTableToExcel(tableID, filename = ''){
                var downloadLink;
                var dataType = 'application/vnd.ms-excel';
                var tableSelect = document.getElementById(tableID);
                var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');
                
                // nombre de archivo
                filename = filename?filename+'.xls':'excel_data.xls';
                
                // referencia agregada
                downloadLink = document.createElement("a");
                
                document.body.appendChild(downloadLink);
                
                if(navigator.msSaveOrOpenBlob){
                    var blob = new Blob(['\ufeff', tableHTML], {
                        type: dataType
                    });
                    navigator.msSaveOrOpenBlob( blob, filename);
                }else{
                    // link de archivo
                    downloadLink.href = 'data:' + dataType + ', ' + tableHTML;
                
                    //el nombre archivo a link
                    downloadLink.download = filename;
                    
                    //ejecutando la descarga
                    downloadLink.click();
                }
            }
        </script>

    </body>
</html>



    

