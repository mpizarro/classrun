
<?= $this->load->view('modulos/reportes/header', [], TRUE) ?>

    <div class="title1">
        RENDIMIENTO GLOBAL
    </div>
    <table style="border-collapse: collapse; border: 1px solid <?= $color ?>; width: 100%; margin-top: 24px;">
        <tr>
            <td style="background: <?= $color ?>; padding: 5px; font-size: 15px; color: #fff; font-weight: bold; text-align: center;">APROBADOS</td>
            <td style="background: <?= $color ?>; padding: 5px; font-size: 15px; color: #fff; font-weight: bold; text-align: center;">REPROBADOS</td>
        </tr>
        <tr >
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $porc_aprobados ?>%</td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $porc_reprobados ?>%</td>
       </tr>
    </table>
    <div style="margin-top: 30px; height: 225px;">
        <div style="text-align: center;">
            <img src="<?= FCPATH ?>assets/images/reportes/chart_aproRepro.jpg" alt="CHART">
        </div>
    </div>

    <div class="title2">
    RENDIMIENTO DEL CURSO
    </div>
    <table style="margin-top: 24px; width: 100%; border-collapse: collapse; border: 1px solid <?= $color ?>;">
        <tr>
            <td style="background: <?= $color ?>; padding: 6px; font-size: 15px; text-align: center; color: #fff; font-weight: bold;">% CORRECTAS</td>
            <td style="background: <?= $color ?>; padding: 6px; font-size: 15px; text-align: center; color: #fff; font-weight: bold;">% INCORRECTAS</td>
            <td style="background: <?= $color ?>; padding: 6px; font-size: 15px; text-align: center; color: #fff; font-weight: bold;">% OMITIDAS</td>
        </tr>
        <tr>
            <td style="background: #F4F6F8; padding: 2px; font-size: 15px; text-align: center;"><?= $porc_correctas ?>%</td>
            <td style="background: #F4F6F8; padding: 2px; font-size: 15px; text-align: center;"><?= $porc_incorrectas ?>%</td>
            <td style="background: #F4F6F8; padding: 2px; font-size: 15px; text-align: center;"><?= $porc_omitidas ?>%</td>
        </tr>
        <tr>
            <td style="padding: 5px; text-align: center;"><img src="<?= FCPATH ?>assets/images/reportes/chart_corr.jpg" style="width: 220px;" alt="CHART"></td>
            <td style="padding: 5px; text-align: center;"><img src="<?= FCPATH ?>assets/images/reportes/chart_incorr.jpg" style="width: 220px;" alt="CHART"></td>
            <td style="padding: 5px; text-align: center;"><img src="<?= FCPATH ?>assets/images/reportes/chart_omit.jpg" style="width: 220px;" alt="CHART"></td>
        </tr>
    </table>

    <!-- salto de pagina -->
    <div style="page-break-after:always;"></div>

    <div class="title3">
        PROMEDIO RENDIMIENTO OBJETIVO DE APRENDIZAJE POR CURSO
    </div>
    <div style="margin-top: 24px; height: 225px;">
        <div style="text-align: center;">
            <img src="<?= FCPATH ?>assets/images/reportes/chart_promOaCurso.jpg" alt="CHART">
        </div>
    </div>

    <table style="border-collapse: collapse; border: 1px solid <?= $color ?>; margin: 10px auto; width: 180px;">
        <tr>
            <td style="background: <?= $color ?>; padding: 5px; font-size: 15px; color: #fff; font-weight: 600; text-align: left;">OA</td>
            <td style="background: <?= $color ?>; padding: 5px; font-size: 15px; color: #fff; font-weight: 600; text-align: right;">% LOGRO</td>
        </tr>
        <?php
        $num = 2;
        foreach ($oas as $oa):
            $bg = $num % 2 == 0 ? '#F4F6F8' : '#FFF';
            $num++;
            ?>
            <tr style="background: <?= $bg ?>;">
                <td style="padding: 3px; font-size: 12px; text-align: left;"><?= $oa['oa'] ?></td>
                <td style="padding: 3px; font-size: 12px; text-align: right;"><?= $oa['logro'] ?></td>
            </tr>
        <?php endforeach ?>
    </table>

    <!-- salto de pagina -->
    <div style="page-break-after:always;"></div>

    <div class="title3">
        TABLAS ESPECIFICACIÓN % LOGRO DE INDICADORES DE EVALUACIÓN SUGERIDOS (IES)
    </div>
    <?php for ($i = 0; $i < count($oas); $i++): ?>
    <table style="border-collapse: collapse; border: 1px solid <?= $color ?>; margin: 10px auto; width: 100%;">
        <tr>
            <td style="background: <?= $color ?>; padding: 5px; font-size: 15px; color: #fff; font-weight: 600; text-align: left; width:80%;"><?= $oas[$i]['oa'] ?></td>
            <td style="background: <?= $color ?>; padding: 5px; font-size: 15px; color: #fff; font-weight: 600; text-align: center; width:20%;">% LOGRO</td>
        </tr>
        <?php
        $num = 2;
        for ($x = 0; $x < count($oas[$i]['ies']); $x++):
            $bg = $num % 2 == 0 ? '#F4F6F8' : '#FFF';
            $num++;
        ?>
        <tr style="background: <?= $bg ?>;">
            <td style="padding: 3px; font-size: 12px; text-align: left;"><?= $oas[$i]['ies'][$x]['ie'] ?></td>
            <td style="padding: 3px; font-size: 12px; text-align: center;"><?= $oas[$i]['ies'][$x]['logro'] ?></td>
        </tr>
        <?php endfor ?>
        <tr>
            <td style="border: 1px solid <?= $color ?>; color: <?= $color ?>; padding: 6px; font-size: 12px; font-weight: 600; text-align: right;">% PROMEDIO</td>
            <td style="background: <?= $color3 ?>; font-weight: 600; border: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $oas[$i]['prom_ies'] ?></td>
        </tr>
    </table>
    <?php endfor; ?>

    <!-- salto de pagina -->
    <div style="page-break-after:always;"></div>

    <div class="title3">
        ESPECIFICACIÓN DESEMPEÑO EN OBJETIVO DE APRENDIZAJE POR ALUMNO
    </div>
    <?php
    $ordenOasSplt = array_chunk($oas, 6, TRUE);
    for ($e = 0; $e < count($ordenOasSplt); $e++):
    ?>
    <table style="border-collapse: collapse; border: 1px solid <?= $color ?>; width: 100%; margin-top: 24px;">
        <tr>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: left;">ALUMNO</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">PC</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">PI</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">PO</td>
        <?php foreach ($ordenOasSplt[$e] as $oa): ?>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;"><?= $oa['oa'] ?></td>
        <?php endforeach; ?>
        </tr>
        <?php $num = 2;
        for ($i = 0; $i < count($alumnos); $i++):
            $bg = $num % 2 == 0 ? '#F4F6F8' : '#FFF';
            $num++; ?>
        <tr style="background: <?= $bg ?>;">
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: left;"><?= ucwords($alumnos[$i]['nombre']." ".$alumnos[$i]['apellido']) ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;">
            <?php $n = 0; for ($x = 0; $x < count($alumnos[$i]['agrupacion']['correctas']); $x++) {
                $n++;
            }
            echo $n; ?>
            </td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;">
            <?php $n = 0; for ($x = 0; $x < count($alumnos[$i]['agrupacion']['incorrectas']); $x++) {
                $n++;
            }
            echo $n; ?>
            </td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;">
            <?php $n = 0; for ($x = 0; $x < count($alumnos[$i]['agrupacion']['omitidas']); $x++) {
                $n++;
            }
            echo $n; ?>
            </td>
        <?php
        $ordenOasAlumno = [];
        foreach ($alumnos[$i]['oas'] as $oa) {
            $ordenOasAlumno[preg_replace("/[^0-9]/", "", $oa['oa'])] = $oa;
        }

        ksort($ordenOasAlumno);

        $ordenOasAlumnosAux = array_chunk($ordenOasAlumno, 6, TRUE);

        foreach ($ordenOasAlumnosAux[$e] as $oa): ?>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $oa['logro'] ?></td>
        <?php endforeach; ?>
        </tr>
        <?php endfor; ?>
        <tr>
            <td colspan="4" style="border: 1px solid <?= $color ?>; color: <?= $color ?>; padding: 6px; font-size: 12px; font-weight: 600; text-align: right;">% PROMEDIO</td>
            <?php foreach ($ordenOasSplt[$e] as $oa): ?>
                <td style="background: <?= $color3 ?>; font-weight: 600; border: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $oa['logro'] ?></td>
            <?php endforeach; ?>
        </tr>
    </table>
    <?php endfor; ?>
    <label style="color: <?= $color ?>; font-size: 12px; margin-right: 20px;">PC: PREGUNTAS CORRECTAS</label>
    <label style="color: <?= $color ?>; font-size: 12px; margin-right: 20px;">PI: PREGUNTAS INCORRECTAS</label>
    <label style="color: <?= $color ?>; font-size: 12px; margin-right: 20px;">PO: PREGUNTAS OMITIDAS</label>

    <!-- salto de pagina -->
    <div style="page-break-after:always;"></div>

    <div class="title3">
        RENDIMIENTO POR ALUMNO - NOTA
    </div>
    <table style="border-collapse: collapse; border: 1px solid <?= $color ?>; width: 100%; margin-top: 24px;">
        <tr>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">N°</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: left;">ALUMNO</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">N° CORRECTAS</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">% LOGRO</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">TIEMPO</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">NOTA</td>
        </tr>
        <?php $num = 2;
        for ($i = 0; $i < count($alumnos); $i++):
            $bg = $num % 2 == 0 ? '#F4F6F8' : '#FFF';
            $num++; ?>
            <tr style="background: <?= $bg ?>;">
                <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= ($i + 1) ?></td>
                <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: left;"><?= ucwords($alumnos[$i]['nombre']." ".$alumnos[$i]['apellido']) ?></td>
                <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;">
                <?php $n = 0; for ($x = 0; $x < count($alumnos[$i]['agrupacion']['correctas']); $x++) {
                    $n++;
                }
                echo $n; ?>
                </td>
                <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $alumnos[$i]['porc_correctas'] ?></td>
                <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $alumnos[$i]['tiempo'] ?> MIN</td>
                <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $alumnos[$i]['nota'] ?></td>
            </tr>
        <?php endfor; ?>
        <tr>
            <td colspan="5" style="border: 1px solid <?= $color ?>; color: <?= $color ?>; padding: 3px; font-size: 12px; font-weight: 600; text-align: right;">PROMEDIO GENERAL</td>
            <td style="background: <?= $color3 ?>; font-weight: 600; border: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $nota ?></td>
        </tr>
    </table>

    <!-- salto de pagina -->
    <div style="page-break-after:always;"></div>

    <div class="title3">
        DETALLE DE REPETICIONES A LAS ALTERNATIVAS POR PREGUNTA
    </div>
    <table style="border-collapse: collapse; border: 1px solid <?= $color ?>; width: 640px; margin-top: 24px;">
        <?php $width = (640 - 240) / count($alternativas); ?>
        <tr>
            <td style="width: 40px; background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">N°</td>
            <td style="width: 100px;background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">CORRECTA</td>
            <?php for ($i = 0; $i < count($alternativas); $i++): ?>
            <td style="width: <?= $width ?>px;background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;"><?= $alternativas[$i] ?></td>
            <?php endfor; ?>
            <td style="width: 100px;background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">OMITIDAS</td>
        </tr>
        <?php $num = 2;
        for ($i = 0; $i < count($preguntas); $i++):
            $bg = $num % 2 == 0 ? '#F4F6F8' : '#FFF';
            $bg2 = $num % 2 == 0 ? $color2 : $color4; 
            $num++; ?>
            <tr style="background: <?= $bg ?>;">
                <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntas[$i]['orden'] ?></td>
                <td style="background: <?= $bg2 ?>; font-weight: 600; border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntas[$i]['clave'] ?></td>
                <?php $count = 0;
                foreach ($preguntas[$i]['alternativas'] as $key => $alt): $count++; ?>
                <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $alt['ocurrencia'] ?></td>
                <?php endforeach; ?>

                <?php if ($count < count($alternativas)) {
                    for ($k = $count; $k < count($alternativas); $k++) { ?>
                        <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;">0</td>
                    <?php } ?>
                <?php } ?>
                <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntas[$i]['omitidas'] ?></td>
            </tr>
        <?php endfor; ?>
    </table>

    <!-- salto de pagina -->
    <div style="page-break-after:always;"></div>

    <div class="title3">
    DETALLE DE PREGUNTAS POR ORDEN DE REPETICIÓN
    </div>
    <table style="border-collapse: collapse; border: 1px solid <?= $color ?>; width: 100%; margin-top: 24px;">
        <tr>
            <td colspan="2" style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center; border-right: 1px solid #fff; border-bottom: 1px solid #fff;">CORRECTA</td>
            <td colspan="2" style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center; border-bottom: 1px solid #fff;">INCORRECTA</td>
            <td colspan="2" style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center; border-left: 1px solid #fff; border-bottom: 1px solid #fff;">OMITIDAS</td>
        </tr>
        <tr>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">N° PREGUNTA</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">REPETICIONES</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">N° PREGUNTA</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">REPETICIONES</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">N° PREGUNTA</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">REPETICIONES</td>
        </tr>

        <?php
        $preguntasAux = [];
        $pCorr = [];
        $pInc = [];
        $pOmit = [];
        for ($x = 0; $x < count($preguntas); $x++) {
            $pCorr[$preguntas[$x]['orden']] = $preguntas[$x]['correctas'];
            $pInc[$preguntas[$x]['orden']] = $preguntas[$x]['incorrectas'];
            $pOmit[$preguntas[$x]['orden']] = $preguntas[$x]['omitidas'];
        }

        arsort($pCorr);
        arsort($pInc);
        arsort($pOmit);

        $pCorrAux = [];
        foreach ($pCorr as $key => $val)
            $pCorrAux[] = ['orden' => $key, 'ocurrencia' => $val];
        $pIncAux = [];
        foreach ($pInc as $key => $val)
            $pIncAux[] = ['orden' => $key, 'ocurrencia' => $val];
        $pOmitAux = [];
        foreach ($pOmit as $key => $val)
            $pOmitAux[] = ['orden' => $key, 'ocurrencia' => $val];

        $preguntasAux[0] = $pCorrAux;
        $preguntasAux[1] = $pIncAux;
        $preguntasAux[2] = $pOmitAux;

        $num = 2;
        for ($i = 0; $i < count($preguntasAux[0]); $i++):
            $bg = $num % 2 == 0 ? '#F4F6F8' : '#FFF';
            $num++;?>

        <tr style="background: <?= $bg ?>;">
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntasAux[0][$i]['orden'] ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntasAux[0][$i]['ocurrencia'] ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntasAux[1][$i]['orden'] ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntasAux[1][$i]['ocurrencia'] ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntasAux[2][$i]['orden'] ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntasAux[2][$i]['ocurrencia'] ?></td>
        </tr>
        <?php endfor; ?>
    </table>

<?= $this->load->view('modulos/reportes/footer', [], TRUE) ?>
