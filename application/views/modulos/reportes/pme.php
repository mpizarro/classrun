
<?= $this->load->view('modulos/reportes/header', [], TRUE) ?>

    <div class="title1">
    RENDIMIENTO DEL CURSO
    </div>
    <table style="margin-top: 24px; width: 100%; border-collapse: collapse; border: 1px solid <?= $color ?>;">
        <tr>
            <td style="background: <?= $color ?>; padding: 6px; font-size: 15px; text-align: center; color: #fff; font-weight: bold;">% CORRECTAS</td>
            <td style="background: <?= $color ?>; padding: 6px; font-size: 15px; text-align: center; color: #fff; font-weight: bold;">% INCORRECTAS</td>
            <td style="background: <?= $color ?>; padding: 6px; font-size: 15px; text-align: center; color: #fff; font-weight: bold;">% OMITIDAS</td>
        </tr>
        <tr>
            <td style="background: #F4F6F8; padding: 2px; font-size: 15px; text-align: center;"><?= $porc_correctas ?>%</td>
            <td style="background: #F4F6F8; padding: 2px; font-size: 15px; text-align: center;"><?= $porc_incorrectas ?>%</td>
            <td style="background: #F4F6F8; padding: 2px; font-size: 15px; text-align: center;"><?= $porc_omitidas ?>%</td>
        </tr>
        <tr>
            <td style="padding: 5px; text-align: center;"><img src="<?= FCPATH ?>assets/images/reportes/chart_corr.jpg" style="width: 220px;" alt="CHART"></td>
            <td style="padding: 5px; text-align: center;"><img src="<?= FCPATH ?>assets/images/reportes/chart_incorr.jpg" style="width: 220px;" alt="CHART"></td>
            <td style="padding: 5px; text-align: center;"><img src="<?= FCPATH ?>assets/images/reportes/chart_omit.jpg" style="width: 220px;" alt="CHART"></td>
        </tr>
    </table>

    <div class="title2">
        % ALUMNOS POR NIVEL PME
    </div>
    <table style="margin-top: 24px; width: 100%; border-collapse: collapse; border: 1px solid <?= $color ?>;">
        <tr>
            <td style="background: <?= $color ?>; padding: 6px; font-size: 15px; text-align: center; color: #fff; font-weight: bold;">% INSUFICIENTE</td>
            <td style="background: <?= $color ?>; padding: 6px; font-size: 15px; text-align: center; color: #fff; font-weight: bold;">% ELEMENTAL</td>
            <td style="background: <?= $color ?>; padding: 6px; font-size: 15px; text-align: center; color: #fff; font-weight: bold;">% ADECUADO</td>
        </tr>
        <tr>
            <td style="background: #F4F6F8; padding: 2px; font-size: 15px; text-align: center;"><?= $nivel['inicial'] ?>%</td>
            <td style="background: #F4F6F8; padding: 2px; font-size: 15px; text-align: center;"><?= $nivel['intermedio'] ?>%</td>
            <td style="background: #F4F6F8; padding: 2px; font-size: 15px; text-align: center;"><?= $nivel['adecuado'] ?>%</td>
        </tr>
        <tr>
            <td style="padding: 5px; text-align: center;"><img src="<?= FCPATH ?>assets/images/reportes/chart_ini.jpg" style="width: 220px;" alt="CHART"></td>
            <td style="padding: 5px; text-align: center;"><img src="<?= FCPATH ?>assets/images/reportes/chart_int.jpg" style="width: 220px;" alt="CHART"></td>
            <td style="padding: 5px; text-align: center;"><img src="<?= FCPATH ?>assets/images/reportes/chart_ade.jpg" style="width: 220px;" alt="CHART"></td>
        </tr>
    </table>

    <!-- salto de pagina -->
    <div style="page-break-after:always;"></div>

    <div class="title3">
        ESTÁNDARES POR ALUMNO
    </div>
    <table style="border-collapse: collapse; border: 1px solid <?= $color ?>; width: 100%; margin-top: 24px;">
        <tr>
            <td style="background: <?= $color ?>; padding: 5px; font-size: 15px; color: #fff; font-weight: bold;">INSUFICIENTE</td>
            <td style="background: <?= $color ?>; padding: 5px; font-size: 15px; color: #fff; font-weight: bold; text-align: center;">ELEMENTAL</td>
            <td style="background: <?= $color ?>; padding: 5px; font-size: 15px; color: #fff; font-weight: bold; text-align: center;">ADECUADO</td>
        </tr>
        <tr >
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $nivel['inicial'] ?>%</td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $nivel['intermedio'] ?>%</td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $nivel['adecuado'] ?>%</td>
        </tr>
    </table>

    <div style="margin-top: 24px; height: 225px;">
        <div style="text-align: center;">
            <div style="margin-bottom: 5px; padding: 6px; font-size: 18px; text-align: center;">GRÁFICO NIVEL DE APRENDIZAJE POR CURSO</div>
            <img src="<?= FCPATH ?>assets/images/reportes/chart_nivel_aprendizaje.jpg" alt="CHART">
        </div>
    </div>

    <!-- salto de pagina -->
    <div style="page-break-after:always;"></div>

    <div class="title3">
        RENDIMIENTO POR HABILIDAD
    </div>
    <table style="border-collapse: collapse; border: 1px solid <?= $color ?>; width: 100%; margin-top: 24px;">
        <tr>
        <?php for ($i = 0; $i < count($habilidades); $i++): ?>
            <td style="background: <?= $color ?>; padding: 5px; font-size: 15px; color: #fff; font-weight: bold; text-align: center;"><?= $habilidades[$i]['habilidad'] ?></td>
        <?php endfor; ?>
        </tr>
        <tr>
        <?php for ($i = 0; $i < count($habilidades); $i++): ?>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px 20px; font-size: 12px; text-align: center;"><?= $habilidades[$i]['porc'] ?>%</td>
        <?php endfor; ?>
        </tr>
    </table>

    <div style="margin-top: 24px; height: 350px;">
        <div style="text-align: center;">
            <div style="margin-bottom: 5px; padding: 6px; font-size: 18px; text-align: center;">PROMEDIO RENDIMIENTO POR HABILIDAD</div>
            <img src="<?= FCPATH ?>assets/images/reportes/chart_rendHabilidad.jpg" alt="CHART">
        </div>
    </div>

    <div class="title3">
        TABLA RESUMEN DESEMPEÑO DE HABILIDAD POR ALUMNO
    </div>
    <table style="border-collapse: collapse; border: 1px solid <?= $color ?>; width: 100%; margin-top: 24px;">
        <tr>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: left; width: 180px;">ALUMNO</td>
            <?php for ($i = 0; $i < count($habilidades); $i++): ?>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">% LOGRO: <?= $habilidades[$i]['habilidad'] ?></td>
            <?php endfor; ?>
        </tr>
        <?php $num = 2;
        for ($i = 0; $i < count($alumnos); $i++):
            $bg = $num % 2 == 0 ? '#F4F6F8' : '#FFF';
            $num++; ?>
            <tr style="background: <?= $bg ?>;">
                <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: left;"><?= ucwords($alumnos[$i]['nombre']." ".$alumnos[$i]['apellido']) ?></td>
                <?php for ($x = 0; $x < count($alumnos[$i]['habilidades']); $x++): ?>
                <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $alumnos[$i]['habilidades'][$x]['porc'] ?></td>
                <?php endfor; ?>
            </tr>
        <?php endfor; ?>
    </table>

    <!-- salto de pagina -->
    <div style="page-break-after:always;"></div>


    <div class="title3">
        NIVELES DE APRENDIZAJE POR ALUMNO - PRUEBA PME
    </div>
    <table style="border-collapse: collapse; border: 1px solid <?= $color ?>; width: 100%; margin-top: 24px;">
        <tr>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">N°</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: left;">ALUMNO</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">N° CORRECTAS</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">TIEMPO</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">NIVEL</td>
        </tr>
        <?php $num = 2;
        for ($i = 0; $i < count($alumnos); $i++):
            $bg = $num % 2 == 0 ? '#F4F6F8' : '#FFF';
            $num++; ?>
        <tr style="background: <?= $bg ?>;">
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $i + 1 ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: left;"><?= ucwords($alumnos[$i]['nombre']." ".$alumnos[$i]['apellido']) ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $alumnos[$i]['correctas'] ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $alumnos[$i]['tiempo'] ?> MIN</td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $alumnos[$i]['nivel'] ?></td>
        </tr>
        <?php endfor; ?>
    </table>

    <div class="title3">
        RENDIMIENTO POR ALUMNO - NOTA
    </div>
    <table style="border-collapse: collapse; border: 1px solid <?= $color ?>; width: 100%; margin-top: 24px;">
        <tr>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">N°</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: left;">ALUMNO</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">N° CORRECTAS</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">N° INCORRECTAS</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">N° OMITIDAS</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">% LOGRO</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">NOTA</td>
        </tr>
        <?php $num = 2;
        for ($i = 0; $i < count($alumnos); $i++):
            $bg = $num % 2 == 0 ? '#F4F6F8' : '#FFF';
            $num++; ?>
        <tr style="background: <?= $bg ?>;">
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $i + 1 ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: left;"><?= ucwords($alumnos[$i]['nombre']." ".$alumnos[$i]['apellido']) ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $alumnos[$i]['correctas'] ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $alumnos[$i]['incorrectas'] ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $alumnos[$i]['omitidas'] ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $alumnos[$i]['porc_correctas'] ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;;"><?= $alumnos[$i]['nota'] ?></td>
        </tr>
        <?php endfor; ?>
        <tr>
            <td colspan="6" style="border: 1px solid <?= $color ?>; color: <?= $color ?>; padding: 3px; font-size: 12px; font-weight: 600; text-align: right;">PROMEDIO GENERAL</td>
            <td style="background: <?= $color3 ?>; font-weight: 600; border: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $nota ?></td>
        </tr>
    </table>

    <!-- salto de pagina -->
    <div style="page-break-after:always;"></div>

    <div class="title3">
        DETALLE DE RESPUESTAS A PREGUNTAS POR ALUMNO
    </div>
    <table style="border-collapse: collapse; border: 1px solid <?= $color ?>; width: 100%; margin-top: 24px;">
        <tr>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">N°</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: left;">ALUMNO</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">CORRECTAS</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">INCORRECTAS</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">OMITIDAS</td>
        </tr>
        <?php $num = 2;
        for ($i = 0; $i < count($alumnos); $i++):
            $bg = $num % 2 == 0 ? '#F4F6F8' : '#FFF';
            $num++; ?>
        <tr style="background: <?= $bg ?>;">
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $i + 1 ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: left;"><?= ucwords($alumnos[$i]['nombre']." ".$alumnos[$i]['apellido']) ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: left;">
            <?php for ($x = 0; $x < count($alumnos[$i]['agrupacion']['correctas']); $x++) {
                echo $alumnos[$i]['agrupacion']['correctas'][$x] . ($x < count($alumnos[$i]['agrupacion']['correctas']) - 1 ? ', ': ' ');
            } ?>
            </td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: left;">
            <?php for ($x = 0; $x < count($alumnos[$i]['agrupacion']['incorrectas']); $x++) {
                echo $alumnos[$i]['agrupacion']['incorrectas'][$x] . ($x < count($alumnos[$i]['agrupacion']['incorrectas']) - 1 ? ', ': ' ');
            } ?>
            </td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: left;">
            <?php for ($x = 0; $x < count($alumnos[$i]['agrupacion']['omitidas']); $x++) {
                echo $alumnos[$i]['agrupacion']['omitidas'][$x] . ($x < count($alumnos[$i]['agrupacion']['omitidas']) - 1 ? ', ': ' ');
            } ?>
            </td>
        </tr>
        <?php endfor; ?>
    </table>

    <!-- salto de pagina -->
    <div style="page-break-after:always;"></div>

    <div class="title3">
        DETALLE DE REPETICIONES A LAS ALTERNATIVAS POR PREGUNTA
    </div>
    <table style="border-collapse: collapse; border: 1px solid <?= $color ?>; width: 640px; margin-top: 24px;">
        <?php $width = (640 - 240) / count($alternativas); ?>
        <tr>
            <td style="width: 40px; background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">N°</td>
            <td style="width: 100px;background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">CORRECTA</td>
            <?php for ($i = 0; $i < count($alternativas); $i++): ?>
            <td style="width: <?= $width ?>px;background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;"><?= $alternativas[$i] ?></td>
            <?php endfor; ?>
            <td style="width: 100px;background: <?= $color ?>; padding: 3px; font-size: 15px; color: #fff; font-weight: 600; text-align: center;">OMITIDAS</td>
        </tr>
        <?php $num = 2;
        for ($i = 0; $i < count($preguntas); $i++):
            $bg = $num % 2 == 0 ? '#F4F6F8' : '#FFF';
            $bg2 = $num % 2 == 0 ? $color2 : $color3;
            $num++; ?>
        <tr style="background: <?= $bg ?>;">
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntas[$i]['orden'] ?></td>
            <td style="background: <?= $bg2 ?>; font-weight: 600; border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntas[$i]['clave'] ?></td>
            <?php foreach ($preguntas[$i]['alternativas'] as $key => $alt): ?>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $alt['ocurrencia'] ?></td>
            <?php endforeach; ?>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntas[$i]['omitidas'] ?></td>
        </tr>
        <?php endfor; ?>
    </table>

    <!-- salto de pagina -->
    <div style="page-break-after:always;"></div>

    <div class="title3">
    DETALLE DE PREGUNTAS POR ORDEN DE REPETICIÓN
    </div>
    <table style="border-collapse: collapse; border: 1px solid <?= $color ?>; width: 100%; margin-top: 24px;">
        <tr>
            <td colspan="2" style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center; border-right: 1px solid #fff; border-bottom: 1px solid #fff;">CORRECTA</td>
            <td colspan="2" style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center; border-bottom: 1px solid #fff;">INCORRECTA</td>
            <td colspan="2" style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center; border-left: 1px solid #fff; border-bottom: 1px solid #fff;">OMITIDAS</td>
        </tr>
        <tr>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">N° PREGUNTA</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">REPETICIONES</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">N° PREGUNTA</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">REPETICIONES</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">N° PREGUNTA</td>
            <td style="background: <?= $color ?>; padding: 3px; font-size: 12px; color: #fff; font-weight: 600; text-align: center;">REPETICIONES</td>
        </tr>

        <?php
        $preguntasAux = [];
        $pCorr = [];
        $pInc = [];
        $pOmit = [];
        for ($x = 0; $x < count($preguntas); $x++) {
            $pCorr[$preguntas[$x]['orden']] = $preguntas[$x]['correctas'];
            $pInc[$preguntas[$x]['orden']] = $preguntas[$x]['incorrectas'];
            $pOmit[$preguntas[$x]['orden']] = $preguntas[$x]['omitidas'];
        }

        arsort($pCorr);
        arsort($pInc);
        arsort($pOmit);

        $pCorrAux = [];
        foreach ($pCorr as $key => $val)
            $pCorrAux[] = ['orden' => $key, 'ocurrencia' => $val];
        $pIncAux = [];
        foreach ($pInc as $key => $val)
            $pIncAux[] = ['orden' => $key, 'ocurrencia' => $val];
        $pOmitAux = [];
        foreach ($pOmit as $key => $val)
            $pOmitAux[] = ['orden' => $key, 'ocurrencia' => $val];

        $preguntasAux[0] = $pCorrAux;
        $preguntasAux[1] = $pIncAux;
        $preguntasAux[2] = $pOmitAux;

        $num = 2;
        for ($i = 0; $i < count($preguntasAux[0]); $i++):
            $bg = $num % 2 == 0 ? '#F4F6F8' : '#FFF';
            $num++;?>

        <tr style="background: <?= $bg ?>;">
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntasAux[0][$i]['orden'] ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntasAux[0][$i]['ocurrencia'] ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntasAux[1][$i]['orden'] ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntasAux[1][$i]['ocurrencia'] ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntasAux[2][$i]['orden'] ?></td>
            <td style="border-left: 1px solid <?= $color ?>; padding: 3px; font-size: 12px; text-align: center;"><?= $preguntasAux[2][$i]['ocurrencia'] ?></td>
        </tr>
        <?php endfor; ?>
    </table>

<?= $this->load->view('modulos/reportes/footer', [], TRUE) ?>
