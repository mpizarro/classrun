<!DOCTYPE html>
<!--
   This is a starter template page. Use this page to start your new project from
   scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="#">
        <title>ClassRun</title>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url()?>assets/inverse/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url()?>assets/plugins/bower_components/datatables/buttons/1.2.2/css/buttons.dataTables.css" rel="stylesheet" type="text/css" />
        <!-- This is Sidebar menu CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.css" rel="stylesheet">
        <!-- TPermite usar el multi select de cursos, entre otros. -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <!--alerts CSS -->
        <!-- Calendar CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/calendar/dist/fullcalendar.css" rel="stylesheet" />
        <!--alerts CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
        <!-- This is a Animation CSS -->
        <link href="<?php echo base_url()?>assets/inverse/css/animate.css" rel="stylesheet">
        <?php
            if (isset($css)) {
                echo $css;
            }
        ?>
        <!-- toast CSS -->
        <link href="<?php echo base_url()?>assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">

        <link href="<?php echo base_url()?>assets/inverse/css/style.css" rel="stylesheet">
        <!-- color CSS you can use different color css from css/colors folder -->
        <!-- We have chosen the skin-blue (default.css) for this starter
            page. However, you can choose any other skin from folder css / colors .
        -->
        <link href="<?php echo base_url()?>assets/inverse/css/colors/default.css" id="theme" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/plugins/bower_components/tablesaw-master/dist/tablesaw.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/inverse/css/headStyle.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/inverse/css/menuStyle.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/inverse/css/modalStyle.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/plugins/bower_components/switchery/dist/switchery.css" rel="stylesheet" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        


    </head>
    <body class="fix-sidebar">
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">

