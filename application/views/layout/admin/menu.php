<!-- Left navbar-header -->
<div class="navbar-default sidebar colorNavbarDefault" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <div><img src="<?php echo base_url()?>assets/plugins/images/usuario.png" alt="user-img" class="img-circle"></div>
                <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><p class="userText"><strong><?php if ($this->session->userdata('username')!=null) {
    echo "Usuario: ". $this->session->userdata('username');
}?></strong></p> <button class="btn btn-primary btn-sm btnEdit">Editar perfil</button></a>
                <ul class="dropdown-menu animated flipInY">
                    <li><a href="#" data-toggle="modal" data-target="#responsive-modal"><i class="ti-user"></i> Mi Perfil</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="javascript:cerrarSession()"><i class="fa fa-power-off"></i> Salir</a></li>
                </ul>
            </div>
        </div>
        <ul class="nav" id="side-menu">
            <li class="menuLi">
                <a href="<?php echo site_url();?>/administrador/evaluaciones" class="waves-effect borderBottomNav borderTopNav"><i class="fa fa-book sizeFa fa-2x"></i> <span class="hide-menu colorTextNav marginNavOne">Evaluaciones</span></a>
            </li>
            <li class="menuLi">
                <a href="<?php echo site_url();?>/administrador/colegios" class="waves-effect borderBottomNav borderTopNav"><i class="fa fa-book sizeFa fa-2x"></i> <span class="hide-menu colorTextNav marginNavOne">Colegios</span></a>
            </li>
            <div class="nav footer-menu hidden-xs">
                <center><strong>Classrun <?=date('Y');?></strong></center>
            </div>
        </ul>
    </div>
</div>
<!-- Left navbar-header end -->
