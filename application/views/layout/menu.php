<!-- Left navbar-header -->
<div class="navbar-default sidebar colorNavbarDefault" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <div class="user-profile">
            <div class="dropdown user-pro-body">
                <div><img src="<?php echo base_url()?>assets/plugins/images/usuario.png" alt="user-img" class="img-circle"></div>
                <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><p class="userText"><strong><?php if ($this->session->userdata('username')!=null) {
    echo "Usuario: ". $this->session->userdata('username');
}?></strong></p></a>
                <ul class="dropdown-menu animated flipInY">
                    <li><a href="#" data-toggle="modal" data-target="#responsive-modal"><i class="ti-user"></i> Mi Perfil</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="javascript:cerrarSession()"><i class="fa fa-power-off"></i> Salir</a></li>
                </ul>
            </div>
        </div>
        <ul class="nav" id="side-menu">
            
           
            <?php if (in_array($this->session->userdata['perfil_id'], [5])): ?>
                <li class="menuLi">
                    <a href="javascript:void(0)" class="waves-effect borderBottomNav borderTopNav">
                        <i class="fa fa-file-o sizeFa fa-2x"></i>
                        <span class="hide-menu colorTextNav marginNavOne">Asignaciones</span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="<?= site_url() ?>/asignaciones_sostenedor" class="waves-effect borderBottomNav borderTopNav">
                                <i class="fa  fa-file-text-o sizeFa fa-2x"></i>
                                <span class="hide-menu colorTextNav marginNavOne">Listado por colegio</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= site_url() ?>/grafico_sostenedor" class="waves-effect borderBottomNav borderTopNav">
                                <i class="fa fa-bar-chart-o sizeFa fa-2x"></i>
                                <span class="hide-menu colorTextNav marginNavOne">Grafico por colegio</span>
                            </a>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>
           
            
                <li class="menuLi">
                    <a href="<?= site_url() ?>/home/" class="waves-effect borderBottomNav borderTopNav">
                        <i class="ti-folder sizeFa fa-2x"></i>
                        <span class="hide-menu colorTextNav marginNavOne">Mis pruebas</span>
                    </a>
                </li>
                <li class="menuLi">
                    <a href="<?= site_url() ?>/asignaciones/personal" class="waves-effect borderBottomNav borderTopNav">
                        <i class="ti-folder sizeFa fa-2x"></i>
                        <span class="hide-menu colorTextNav marginNavOne">Mis asignaciones</span>
                    </a>
                </li>
                    
             
            <?php if (in_array($this->session->userdata['perfil_id'], [7])): ?>
                <li class="menuLi">
                    <a href="<?= site_url() ?>/analisis/get" class="waves-effect borderBottomNav borderTopNav"><i class="fa fa-bar-chart-o sizeFa fa-2x"></i> <span class="hide-menu colorTextNav marginNavOne">Análisis</span></a>
                </li>
            <?php endif; ?>

            <div class="nav footer-menu hidden-xs">
                <center><strong>ClassRun <?=date('Y');?></strong></center>
            </div>
        </ul>
    </div>
</div>
<!-- Left navbar-header end -->