<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:500" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Nunito:300" rel="stylesheet">
        <link rel="icon" type="image/png" sizes="16x16" href="#">
        <title>ClassRun - Login</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <meta charset="utf-8">
        <title></title>
    </head>
    <body class="d-flex justify-content-center align-items-center">
            <div class="container">


                <!-- columna izquierda -->
                <div class="row d-flex justify-content-center mx-auto" id="row-contenido">
                    <div class="col-md-6 columna-login">
                        <img src="<?=base_url();?>assets/img/grupo-18229@2x.png" class="img-login" alt="">
                    </div>

                    <!-- columna derecha -->
                    <div class="col-sm-12 col-md-6">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-12 col-sm-12 columna-login mx-auto mt-3">
                                <img src="<?=base_url();?>assets/img/grupo-3621@2x.png" class="iso-plataforma" width="71px" alt="">
                            </div>

                            <div class="col-md-12 col-sm-12 columna-login mx-auto">
                                <img src="<?=base_url();?>assets/img/grupo-3620@2x.png" class="logo-plataforma" alt="">
                            </div>

                            <div class="col-md-12 col-sm-12 mt-3 columna-login descripcion mx-auto">
                                <p>Software de instrumentos evaluativos que permiten obtener resultados de manera instantánea.</p>
                            </div>

                            <div class="col-md-10 form-group align-center formulario">
                                <h4 class="titulo-portal">Portal del profesor</h4>
                                <form id="formLogin" action="javascript:void(0)" method="post">
                                    <div class="form-group">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                              <div class="input-group-text"><i class="far fa-user" style="color:#0095ff"></i></div>
                                            </div>
                                            <input type="text" class="form-control user-input" id="rut" placeholder="Ingrese su RUT" oninput="checkRut(this)" maxlength="11" name="username">
                                        </div>


                                        <!-- pass -->
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                              <div class="input-group-text"><i class="fa fa-lock" style="color:#0095ff"></i></div>
                                            </div>
                                            <input type="password" class="form-control user-input" id="pass" placeholder="Ingrese contraseña"  maxlength="10" name="password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-input btn-block"  id="btn-login"> Ingresar</button>
                                    </div>
                                    <div class="form-group">
                                        <?php
                                        if (!empty($message)) {
                                            echo $message;
                                        }
                                        ?>
                                    </div>
                                </form>

                                <div class="text-center">
                                    <a href="<?=base_url();?>index.php/alumnologin">Soy Estudiante</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <!-- jQuery -->
            <script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.js"></script>
            <script type="text/javascript">
                var siteUrl = '<?= site_url();?>';
            </script>
            <script src="<?php echo base_url();?>assets/modulos/login/js/login.js"></script>
            <script>
              onInit();
            </script>

    </body>
</html>
