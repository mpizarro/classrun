<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="icon" type="image/png" sizes="16x16" href="#">
        <title>ClassRun - Login</title>
        <link rel="stylesheet" href="<?=base_url();?>assets/css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:500" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Nunito:300" rel="stylesheet">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <meta charset="utf-8">
        <title></title>
    </head>
    <body class="d-flex justify-content-center align-items-center">
            <div class="container">
                <!-- titulo -->

                <!-- columna izquierda -->
                <div class="row d-flex justify-content-center mx-auto" id="row-contenido">
                    

                    <!-- columna derecha -->
                    <div class="col-sm-12 col-md-6">
                        <div class="row d-flex justify-content-center">
                        <img class="mb-4" src="<?php echo base_url()?>assets/images/logo_classrun.png" alt="" width="280">

                            <div class="col-md-10 form-group align-center formulario">
                                <h4 class="titulo-portal">Portal del estudiante</h4>
                                <form method="POST" action="<?=site_url();?>/alumnologin/login">
                                    <div class="form-group">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                              <div class="input-group-text"><i class="far fa-user"></i></div>
                                            </div>
                                            <input type="text" class="form-control user-input" id="username" placeholder="Ingrese Usuario" oninput="checkRut(this)" maxlength="11" name="username">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-input btn-block" value="Ingresar">
                                    </div>
                                    <div class="form-group">

                                                <?php
                                                if (!empty($message)) {
                                                    echo $message;
                                                }
                                                ?>

                                    </div>
                                </form>

                                <div class="text-center">
                                    <a href="<?=base_url();?>" style="color: #00b355;">Soy profesor</a>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <script
              src="https://code.jquery.com/jquery-3.4.0.slim.min.js"
              integrity="sha256-ZaXnYkHGqIhqTbJ6MB4l9Frs/r7U4jlx7ir8PJYBqbI="
              crossorigin="anonymous"></script>
            <script type="text/javascript">

                function checkRut(rut)
                {
                  // Despejar Puntos
                  var valor = rut.value.replace('.','');
                  // Despejar GuiÃ³n
                  valor = valor.replace('-','');

                  // Aislar Cuerpo y DÃ­gito Verificador
                  cuerpo = valor.slice(0,-1);
                  dv = valor.slice(-1).toUpperCase();

                  // Formatear RUN
                  rut.value = cuerpo + '-'+ dv
                  if(rut.value == '-')
                  {
                      rut.value = '';
                  }

                  // Si no cumple con el mÃ­nimo ej. (n.nnn.nnn)
                  if(cuerpo.length < 7) { rut.setCustomValidity("Rut Incorrecto"); return false;}

                  // Calcular DÃ­gito Verificador
                  suma = 0;
                  multiplo = 2;

                  // Para cada dÃ­gito del Cuerpo
                  for(i=1;i<=cuerpo.length;i++) {

                      // Obtener su Producto con el MÃºltiplo Correspondiente
                      index = multiplo * valor.charAt(cuerpo.length - i);

                      // Sumar al Contador General
                      suma = suma + index;

                      // Consolidar MÃºltiplo dentro del rango [2,7]
                      if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }

                  }

                  // Calcular DÃ­gito Verificador en base al MÃ³dulo 11
                  dvEsperado = 11 - (suma % 11);

                  // Casos Especiales (0 y K)
                  dv = (dv == 'K')?10:dv;
                  dv = (dv == 0)?11:dv;

                  // Validar que el Cuerpo coincide con su DÃ­gito Verificador
                  if(dvEsperado != dv) { rut.setCustomValidity("Rut Incorrecto"); return false; }

                  // Si todo sale bien, eliminar errores (decretar que es vÃ¡lido)
                  rut.setCustomValidity('');
                }
                $( document ).ready(function() {
                    $('#username').focus();
                });
            </script>
    </body>
</html>
