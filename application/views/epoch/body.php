        <div class="wrapper">
            <div class="container-fluid">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <!--
                            <div class="btn-group pull-right">
                                <ol class="breadcrumb hide-phone p-0 m-0">
                                    <li class="breadcrumb-item"><a href="#">Principal</a></li>
                                    
                                    <li class="breadcrumb-item active">Mantenedor</li>
                                </ol>
                            </div>
    -->
                            <h4 class="page-title">Editor de evaluaciones</h4>
                        </div>
                    </div>
                </div>
                
                <!-- end page title end breadcrumb -->
                <button type="button" style="background-color:#61b7c3;float:right; display:none" class="btn btn-twitter m-b-10 m-l-10 waves-effect waves-light" data-animation="zoomIn" data-toggle="modal" data-target="#exampleModalLong-2">
                    <i class="mdi mdi-library-plus" style="color:#FFF;font-size:65px;"></i>
                </button>
                                
                <div class="row">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <div id="folder-1">    
                                    <div class="col-sm-6 col-md-4 col-lg-4 folder-1" style="float:left" tipo="1" >
                                        <div>
                                            <i class="mdi mdi-folder" style="font-size:196px;width: 100%;color:#61b7c3;text-align: center;"></i>
                                            <div style="text-align: center;font-size: 15px;">
                                                Simce
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-4 folder-1" style="float:left" tipo="4">
                                        <div>
                                            <i class="mdi mdi-folder" style="font-size:196px;width: 100%;color:#61b7c3;text-align: center;"></i>
                                            <div style="text-align: center;font-size: 15px;">
                                                Cobertura
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-4 folder-1" style="float:left" tipo="3">
                                        <div>
                                            <i class="mdi mdi-folder" style="font-size:196px;width: 100%;color:#61b7c3;text-align: center;"></i>
                                            <div style="text-align: center;font-size: 15px;">
                                                Test Proceso
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="display:none" id="folder-2">    
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-2" style="float:left;text-align:center;" asignatura="3">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        <div style="text-align: center;font-size: 15px;">
                                            Matemática    
                                        </div> 
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-2" style="float:left;text-align:center;" asignatura="1">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        <div style="text-align: center;font-size: 15px;">
                                            Lenguaje
                                        </div> 
                                        <center><span style="font-size: 22px;margin-left:-110px"></span></center>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-2" style="float:left;text-align:center;" asignatura="4">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        <div style="text-align: center;font-size: 15px;">
                                            Historia
                                        </div>
                                        
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-2" style="float:left;text-align:center;" asignatura="5">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        <div style="text-align: center;font-size: 15px;">
                                            Ciencias
                                        </div>
                                    </div>
                                </div>

                                <div style="display:none" id="folder-3">    
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-3" style="float:left;text-align:center;" nivel="1">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        <div>
                                            <span>Primero básico</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-3" style="float:left;text-align:center;" nivel="2">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        <div>
                                            <span>Segundo básico</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-3" style="float:left;text-align:center;" nivel="3">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        <div>
                                            <span>Tercero básico</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-3" style="float:left;text-align:center;" nivel="4">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        <div>
                                            <span>Cuarto básico</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-3" style="float:left;text-align:center;" nivel="5">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        <div>
                                            <span>Quinto básico</span>
                                        </div>
                                        
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-3" style="float:left;text-align:center;" nivel="6">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        <div>
                                            <span>Sexto básico</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-3" style="float:left;text-align:center;" nivel="7">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        <div>
                                            <span>Séptimo básico</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-3" style="float:left;text-align:center;" nivel="8">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        <div>
                                            <span>Octavo básico</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-3" style="float:left;text-align:center;" nivel="9">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        
                                        <div>
                                            <span>Primero medio</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-3" style="float:left;text-align:center;" nivel="10">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        <div>
                                            <span>Segundo medio</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-3" style="float:left;text-align:center;" nivel="11">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        <div>
                                            <span>Tercero medio</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-3" style="float:left;text-align:center;" nivel="12">
                                        <i class="mdi mdi-folder" style="font-size:196px;color:#61b7c3"></i>
                                        <div>
                                            <span>Cuarto medio</span>
                                        </div>
                                    </div>
                                </div>

                                <div style="display:none" id="folder-4">     
                                    <div class="col-sm-6 col-md-4 col-lg-3 folder-1" style="float:left" >
                                        <i class="mdi mdi-folder" style="font-size:100px;color:#61b7c3"></i>Simce
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="display:none">
                    <div class="col-12">
                        <div class="card m-b-30">
                            <div class="card-body">

                                <h4 class="mt-0 header-title">Pruebas creadas</h4>
                                <p class="text-muted m-b-30 font-14">En esta sección se agrupan las pruebas creadas por el profesor.</p>
                                <div style="float: right;margin-top: -77px;">
                               
                                <button type="button" class="btn btn-primary waves-effect waves-light btn-ev" data-animation="zoomIn" data-toggle="modal" data-target="#exampleModalLong-2">
                                    <span class="ti-plus"></span> Crear evaluación
                                </button>
                                </div>
                                <table class="table" id="my-table">
                                    <thead>
                                    <tr>
                                        <th style="width: 150px">Fecha</th>
                                        <th style="width: 350px">Nombre</th>
                                        <th style="width: 200px">Asignatura</th>
                                        <th style="width: 130px">Nivel</th>
                                        <th style="width: 130px">Tipo</th>
                                        <th style="width: 180px">Estado</th>
                                        <th style="width: 130px">N° preguntas</th>
                                        <th style="width: 150px">Acciones</th>
                                    </tr>
                                        
                                    </thead>
                                    <tbody>
                                    <?php foreach ($contenidos as $key => $contenido) : ?>
                            <tr>
                                <td>
                                <?php $newDate = date("d-m-Y", strtotime($contenido->fecha));
                                      echo $newDate; 
                                 
                                
                                ?></td>
                                <td><?= $contenido->nombre; ?></td>
                                <td style="color: <?= getColorAsignatura($contenido->asignatura_id)['color'] ?>"><?= $contenido->asignatura; ?></td>
                                <td><?= $contenido->nivel; ?></td>
                                <td><?= $contenido->tipo; ?></td>
                                <td>
                                    <?php if ($contenido->creada == 't') : ?>
                                        <p class="text-evalua"><i class="fa fa-circle icon-estado"></i> Lista para aplicar</p>
                                    <?php else : ?>
                                        <p class="text-clear"><i class="fa fa-circle icon-estado"></i> Borrador</p>
                                    <?php endif ?>
                                </td>
                                <td><?= $contenido->count_pregunta ?></td>

                                <td class="text-center">
                                <?php if ($contenido->creada == 't'): ?>
                                    <button style="float: none; margin: 5px;" type="button" title="Asignar" class='tabledit-edit-button btn btn-sm btn-info btn-ev btn-asignar' btn-asignar id-evaluacion='<?= $contenido->id ?>'>
                                        <span class="ti-file"></span>
                                    </button>
                                    <button style="float: none; margin: 5px;" type="button" class='m-l-10 btn btn-default btn-outline' btn-ver-evaluacion id-evaluacion="<?= $contenido->id ?>"> 
                                        <span class="ti-eye"></span>
                                    </button>
                                <?php else: ?>
                                    <?php $prueba_original = '';
                                    if ($contenido->prueba_original)
                                        $prueba_original = $contenido->prueba_original;

                                        $base_64url = base64_encode("$contenido->id,$contenido->nivel_id,$contenido->asignatura_id,$contenido->tipo_id,$prueba_original,$contenido->nombre"); ?>
                                    <a title="Continuar" href="<?= site_url('/home/crear/'.$base_64url) ?>" class="m-l-10 btn btn-ev">
                                        <span class="ti-pencil"></span>
                                    </a>
                                <?php endif ?>
                                    <!-- <button class='m-l-10 btn btn-default btn-outline'> <i class="ti-pencil"></i></button> -->
                                    <!-- <button class='m-l-10 btn btn-danger btn-outline'> <i class="fa fa-trash-o"></i></button> -->
                                </td>
                            </tr>
                        <?php endforeach; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div> <!-- end row -->
            </div> <!-- end container -->
        </div>
        <!-- end wrapper -->
</div>

<div id="modal-curso" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="modal-curso-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border: 3px solid #56b2bf;border-radius: 10px;">
            <div class="modal-header" style="background: #56b2bf;color: #fff;font-size: 15px;font-weight: bold;">
                <h4 class="modal-title" id="modal-curso-label"><i class="fa fa-users"></i>ALUMNOS</h4>                
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table" id="table-curso">
                        <thead style="background: #56b2bf;color: #fff;font-size: 15px;font-weight: bold;">
                            <tr>
                                <th style="font-weight: bold;">Habilitar</th>
                                <th style="font-weight: bold;">Rut</th>
                                <th style="font-weight: bold;">Nombre Alumno</th>
                                <th style="font-weight: bold;">Progreso</th>
                                <th style="font-weight: bold;">Tiempo</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modal-asignar-curso" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-left: 0;">×</button>
                <h4 class="modal-title">Asignar evaluación</h4>
            </div>
            <div class="modal-body">
                <form id="form-asignar-curso" action="javascript:void(0)" method="post">
                    <div class="form-group">
                        <label for="select-niveles">Niveles</label>
                        <select class="form-control" id="select-niveles" name="nivel">
                            <?php if ($nivelesColegio) : ?>
                                <option value="null" hidden>Selecciona un nivel</option>
                                <?php foreach ($nivelesColegio as $nivel) : ?>
                                    <option value="<?= $nivel->id ?>"><?= $nivel->nivel ?></option>
                                <?php endforeach ?>
                            <?php else : ?>
                                <option value="null" hidden>No existen niveles</option>
                            <?php endif ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="select-cursos">Selecciona un curso</label>
                        <select class="form-control" id="select-cursos" name="cursos">

                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn-asignar-modal" class="btn btn-success waves-effect" btn-asignar-curso disabled>Asignar</button>
                <button type="button" class="btn btn-info waves-effect" btn-cerrar-modal-asignar-curso data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="exampleModalLong-2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog  zoomIn  animated" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle-2">Categorías</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            
            <div id="nombrePrueba" style="display:none">
                <input type="text" class="form-control" maxlength="25" name="alloptions" id="alloptions">
            </div>


            </div>
            <div class="modal-footer" style="display:none">
                <button type="button" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
            
        </div>
    </div>
</div>

<div id="modal-evaluacion" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="modal-evaluacion-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-left: 0;">×</button>
                <div id="para-descarga" style="cursor:pointer;" btn-download-evaluacion id-evaluacion="">
                    <h4 class="modal-title" id="modal-evaluacion-label"> 
                        <i class="fa fa-file-text-o"></i> VER EVALUACIÓN
                    </h4>
                </div>

            </div>
            <div class="modal-body" style="height: 600px;">
                <div id="data-prueba" style="height:100%"></div>
                

            </div>
        </div>
    </div>
</div>

<div id="modal-mensaje-editar" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="modal-evaluacion-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-left: 0;">×</button>
                <h4 class="modal-title" id="modal-evaluacion-label"> <i class="fa fa-file-text-o"></i> Editar Evaluación </h4>
            </div>
            <div class="modal-body">
            <form action="home/crear_prueba/estandarizada" method="post">
                <input type="hidden" id="Eprueba_id" name="prueba_id" value="">
                <input type="hidden" id="Enivel_id" name="nivel_id" value="">
                <input type="hidden" id="Easignatura_id" name="asignatura_id" value="">
                <input type="hidden" id="Etipo_id" name="tipo_id" value="">

            <div id="nombrePrueba">
                <input type="text" class="form-control" maxlength="25" name="nombre" id="editarEstandarizada">
            </div>
            
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal tabulación -->
<div id="modal-tabulacion" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="modal-tabulacion-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 10px;border: 3px solid #56b2bf;">
            <div class="modal-header" style="background: #56b2bf;color: #fff;font-size: 25px;font-weight: bold;">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <button style="margin-left: -17px;margin-right: -3px;" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>                        
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6t">
                        <p id="detalle-evaluacion"></p>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 text-right">
                        <button id="btn-actualizar" class="btn btn-ev btn-outline" style="background: #fff;font-size: 20px;font-weight: bold;color: #56b2bf;border-color: #56b2bf;" btn-actualizar>Actualizar</button>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 text-right">
                        <div id="imgQr"></div>
                    </div>

                </div>

            </div>
            <div class="modal-body">
                <div class="table-responsive" id="tabla-tabulacion">

                </div>
            </div>
        </div>
    </div>
</div>


