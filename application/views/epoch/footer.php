

        <!-- Footer -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        © ClassRun 2019
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->


        <!-- jQuery  -->
        <script src="<?php echo base_url()?>assets2/js/jquery.min.js"></script>
        <script src="<?php echo base_url()?>assets2/js/popper.min.js"></script>
        <script src="<?php echo base_url()?>assets2/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url()?>assets2/js/modernizr.min.js"></script>
        <script src="<?php echo base_url()?>assets2/js/waves.js"></script>
        <script src="<?php echo base_url()?>assets2/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url()?>assets2/js/jquery.nicescroll.js"></script>
        <script src="<?php echo base_url()?>assets2/js/jquery.scrollTo.min.js"></script>
        

        <script src="<?php echo base_url()?>assets2/plugins/tiny-editable/mindmup-editabletable.js"></script>
        <script src="<?php echo base_url()?>assets2/plugins/tiny-editable/numeric-input-example.js"></script>
        <script src="<?php echo base_url()?>assets2/plugins/tabledit/jquery.tabledit.js"></script>
        <script src="<?php echo base_url()?>assets2/js/form-advanced.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/bower_components/switchery/dist/switchery.js"></script>
        <script src="<?php echo base_url()?>assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        
        <!-- App js -->
        <script src="<?php echo base_url()?>assets2/js/app.js"></script>
        <script type="text/javascript">
            <?php
                if(isset($asignatura))
                {
                    echo "var pathPrueba = 1;";
                    echo $path;
                }

                else
                    echo "var pathPrueba = 0;";
            ?>
            
            var siteUrl = "<?= site_url();?>";
        </script>
        <script src="<?php echo base_url()?>assets2/js/crearEvaluacion.js"></script>
        

    </body>
</html>