
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Signin ClassRun</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    
<link href="https://getbootstrap.com/docs/4.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
  </head>
  <body class="text-center">
    <form class="form-signin" id="formLogin" action="javascript:void(0)" method="post">
        <img class="mb-4" src="<?php echo base_url()?>assets/images/logo_classrun.png" alt="" width="280">
        <div id="inputUser">
          <label for="inputEmail" class="sr-only">Email address</label>
          <input type="text" class="form-control user-input" id="rut" placeholder="Nombre Usuario" oninput="validaRut(this)" maxlength="11" name="username">
        </div>
        <div id="inputPass" style="display:none">
          <label for="inputPassword" class="sr-only">Password</label>
          <input type="password" class="form-control user-input" id="pass" placeholder="Ingrese contraseña"  maxlength="10" name="password">
        </div>
        <div class="checkbox mb-3">
        <br>
  </div>
  
  <button class="btn btn-lg btn-primary btn-block" type="submit" id="btn-login">Continuar</button>
  
</form>
</body>
<script src="<?php echo base_url();?>assets/plugins/bower_components/jquery/dist/jquery.js"></script>
<script type="text/javascript">
    var url = '<?= site_url();?>';
</script>
<script src="<?php echo base_url();?>assets/js/login.js"></script>

</html>
