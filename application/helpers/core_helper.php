<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (! function_exists('set_alert')) {

    /**
     * Función que retorna una alerta según el tipo de mensaje
     *
     * @author Miguel López <miguel.lopez[at]aeduc.cl>
     * @version 2019-04-10
     */
    function set_alert($message)
    {
        $icons = [
            'success' => 'ok',
            'info' => 'info-sign',
            'warning' => 'warning-sign',
            'danger' => 'exclamation-sign',
        ];

        return  '<div class="alert alert-' . $message['type'] . '" role="alert">' . '' .
                '    <span class="glyphicon glyphicon-' . $icons[$message['type']] . '" aria-hidden="true"></span>' . '' .
                '    <span class="sr-only">' . $message['type'] . ': </span>' . $message['message'] . '' .
                '    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="Cerrar"></a>' .
                '</div>';
    }
}

if (! function_exists('arrayToObject')) {

    function arrayToObject($d) {
       if (is_array($d)) {
           /*
           * Return array converted to object
           * Using __FUNCTION__ (Magic constant)
           * for recursive call
           */
           return (object) array_map(__FUNCTION__, $d);
       }
       else {
           // Return object
           return $d;
       }
   }
}
if (! function_exists('in_multiarray')) {
    function in_multiarray($elem, $array)
    {
        while (current($array) !== false) {
            if (current($array) == $elem) {
                return true;
            } elseif (is_array(current($array))) {
                if (in_multiarray($elem, current($array))) {
                    return true;
                }
            }
            next($array);
        }
        return false;
    }
}

if (!function_exists('fecha_mis_evaluaciones')) {
    function fecha_mis_evaluaciones($fecha)
    {
        $meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

        $day = date('d', strtotime($fecha));
        $month = date('m', strtotime($fecha)) - 1;
        $year = date('Y', strtotime($fecha));

        return $day . ' de ' . $meses[(int)$month] . ', ' . $year;
    }
}
