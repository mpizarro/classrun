<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PromediosHelper {
    public function __construct(){
        $this->ci =& get_instance();
        $this->ci->load->database();
    }
    public function promedioHojaDeRespuestas($hojasDeRespuestas, $idEvaluacion) {
        $maxCorrectas = $this->getMaxCorrectasEvaluacion($hojasDeRespuestas[0]->usuario_has_asignacion_id);
        $correctas = 0;
        $nota = 2;
        if($hojasDeRespuestas){
            foreach ($hojasDeRespuestas as $hoja) {
                $idColegio = $this->ci->session->userdata['colegio_id'];
                $sql = '
                    SELECT r.correcta FROM respuestas."' . $idColegio . '.hojarespuestas" hr
                    JOIN respuesta r ON r.id = hr.respuesta_id
                    WHERE hr.id = ' . $hoja->id;
                $query = $this->ci->db->query($sql);
                if($query->num_rows() > 0){
                    if($query->row()->correcta == 'f'){
                        $correctas++;
                    }
                }
            }
            $punt = $correctas * 100 / $maxCorrectas;
            $nota = 7.5 * (($punt*0.01) - 0.6) + 4;
            if($punt < 60) {
                $nota = 5 * ($punt*0.01) + 1;
            }
            if($nota < 2){
                $nota = 2;
            }
        }
        echo $nota . '<br>';
        return $nota;
    }
    private function getMaxCorrectasEvaluacion($idUsuarioAsignacion){
        $sql = 'SELECT * FROM prueba_has_pregunta php
        WHERE php.prueba_id = (
        SELECT prueba_id FROM usuario_has_asignacion uha
        JOIN asignacion a ON a.id = uha.asignacion_id
        WHERE uha.id = ' . $idUsuarioAsignacion
        . ')';

        $query = $this->ci->db->query($sql);

        return $query->num_rows();
    }
}