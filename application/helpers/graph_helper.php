<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'/libraries/jpgraph/src/jpgraph.php');

function graphBar($conf) {
    require_once(APPPATH.'/libraries/jpgraph/src/jpgraph_bar.php');

    $path = 'assets/images/reportes/' . $conf['graphName'];
    
    $graph = new Graph($conf['size']['width'], $conf['size']['height']);
    
    $graph->SetScale('textlin');
    $graph->SetMargin( 30, 30, 20, 20 );
    $graph->SetBox(false);
    $graph->ygrid->SetFill(false);
    $graph->yaxis->HideLine(false);
    $graph->xaxis->SetTickLabels($conf['label']);

    if (isset($conf['angle'])) {
        $graph->xaxis->SetLabelAngle($conf['angle']);
        $graph->SetMargin(120, 70, 10, 150);
    }
    
    $barplot = new BarPlot($conf['data']);
    $barplot->SetFillColor($conf['color'][0]);

    $graph->Add( $barplot );
    $graph->Stroke(_IMG_HANDLER);
    $graph->img->Stream($path);

    return $path;
}

function graphPie($conf) {
    require_once(APPPATH.'/libraries/jpgraph/src/jpgraph_pie.php');
    
    $path = 'assets/images/reportes/' . $conf['graphName'];

    $graph = new PieGraph(350,250);

    $theme_class="DefaultTheme";

    $p = new PiePlot($conf['data']);
    $graph->Add($p);

    $p->ShowBorder();
    $p->SetColor('black');
    $p->SetSliceColors($conf['color']);
    $p->value->Show(false);

    $graph->Stroke(_IMG_HANDLER);
    $graph->img->Stream($path);

    return $path;
}