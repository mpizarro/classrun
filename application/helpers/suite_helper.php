<?php
function getMenuPlataformas(){
    $htmlReturn = '';
    $ci = get_instance();
    $i = 0;
    if($ci->session->userdata('servicios')){
        foreach ($ci->session->userdata('servicios') as $servicio) {
            $htmlReturn .='
            <li class="bloque
            ' . $servicio->nombre . '
            ' . (($i == 0)  ? 'bloqueOne' : '') . '
            '. ($i == count($ci->session->userdata('servicios')) ? 'bloqueFour' : '') .'
            " onclick="redirectPlataforma(\'' . $servicio->nombre . '\')">
                <div class="media sizeMedia">
                    <div class="media-left media-middle">
                        <img class="media-object light-logo" style="width: 4rem;" src="'. base_url() . 'assets/plugins/images/logo-' . $servicio->nombre . '.png" alt="...">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">AEDUC '. $servicio->nombre . '</h4>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </div>
                </div>
            </li>';
            $i++;
        }
    }
    return $htmlReturn;
}