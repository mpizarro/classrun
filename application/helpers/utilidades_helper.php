<?php

function getPartialsByAsignatura($idAsignatura, $tipoEvaluacion) {
    $data = [
        'enc' => 'ENC_',
        'port' => 'PORT_',
        'pie' =>'PIE_'
    ];

    if ($tipoEvaluacion == 1) { #Simce
        $data['enc'] .= 'SIMCE_';
        $data['port'] .= 'SIMCE_';
    } elseif ($tipoEvaluacion == 2) { #Pme
        $data['enc'] .= 'PME_';
        $data['port'] .= 'PME_';
    } elseif ($tipoEvaluacion == 3) { #Unidad
        $data['enc'] .= 'UNIDAD_';
        $data['port'] .= 'UNIDAD_';
    } elseif ($tipoEvaluacion == 4) { #Cobertura
        $data['enc'] .= 'COBERTURA_';
        $data['port'] .= 'COBERTURA_';
    }

    if ($idAsignatura == 1) { #LENGUAJE Y COMUNICACION
        $data['enc'] .= 'LEN.png';
        $data['port'] .= 'LEN.png';
        $data['pie'] .= 'LEN.png';
    } elseif ($idAsignatura == 3) { #Matematica
        $data['enc'] .= 'MATE.png';
        $data['port'] .= 'MATE.png';
        $data['pie'] .= 'MATE.png';
    } elseif ($idAsignatura == 4) { #Historia, Geografía y Ciencias Sociales
        $data['enc'] .= 'HIST.png';
        $data['port'] .= 'HIST.png';
        $data['pie'] .= 'HIST.png';
    } elseif ($idAsignatura == 5) { #Ciencias Naturales
        $data['enc'] .= 'CIEN.png';
        $data['port'] .= 'CIEN.png';
        $data['pie'] .= 'CIEN.png';
    } elseif ($idAsignatura == 12) { #Biologia
        $data['enc'] .= 'BIO.png';
        $data['port'] .= 'BIO.png';
        $data['pie'] .= 'BIO.png';
    } elseif ($idAsignatura == 13) { #Fisica
        $data['enc'] .= 'FIS.png';
        $data['port'] .= 'FIS.png';
        $data['pie'] .= 'FIS.png';
    } elseif ($idAsignatura == 14) { #Quimica
        $data['enc'] .= 'QUI.png';
        $data['port'] .= 'QUI.png';
        $data['pie'] .= 'QUI.png';
    }

    return $data;
}

function getPartialsByCodigo($codigo, $tipoEvaluacion) {
    $asignatura=substr($codigo, 0,3);
    $data = [
        'enc' => 'ENC_',
        'port' => 'PORT_',
        'pie' =>'PIE_'
    ];

    if ($tipoEvaluacion == 1) { #Simce
        $data['enc'] .= 'SIMCE_';
        $data['port'] .= 'SIMCE_';
    } elseif ($tipoEvaluacion == 2) { #Pme
        $data['enc'] .= 'PME_';
        $data['port'] .= 'PME_';
    } elseif ($tipoEvaluacion == 3) { #Unidad
        $data['enc'] .= 'UNIDAD_';
        $data['port'] .= 'UNIDAD_';
    } elseif ($tipoEvaluacion == 4) { #Cobertura
        $data['enc'] .= 'COBERTURA_';
        $data['port'] .= 'COBERTURA_';
    }

    if ($asignatura == 'LEN') { #LENGUAJE Y COMUNICACION
        $data['enc'] .= 'LEN.png';
        $data['port'] .= 'LEN.png';
        $data['pie'] .= 'LEN.png';
    } elseif ($asignatura == 'MAT') { #Matematica
        $data['enc'] .= 'MATE.png';
        $data['port'] .= 'MATE.png';
        $data['pie'] .= 'MATE.png';
    } elseif ($asignatura == 'HIS') { #Historia, Geografía y Ciencias Sociales
        $data['enc'] .= 'HIST.png';
        $data['port'] .= 'HIST.png';
        $data['pie'] .= 'HIST.png';
    } elseif ($asignatura == 'CIE') { #Ciencias Naturales
        $data['enc'] .= 'CIEN.png';
        $data['port'] .= 'CIEN.png';
        $data['pie'] .= 'CIEN.png';
    } elseif ($asignatura == 'BIO') { #Ciencias Naturales
        $data['enc'] .= 'BIO.png';
        $data['port'] .= 'BIO.png';
        $data['pie'] .= 'BIO.png';
    } elseif ($asignatura == 'FIS') { #Ciencias Naturales
        $data['enc'] .= 'FIS.png';
        $data['port'] .= 'FIS.png';
        $data['pie'] .= 'FIS.png';
    } elseif ($asignatura == 'QUI') { #Ciencias Naturales
        $data['enc'] .= 'QUI.png';
        $data['port'] .= 'QUI.png';
        $data['pie'] .= 'QUI.png';
    }

    return $data;
}


function getColorAsignatura($idAsignatura)
{
    $response['color'] = '';
    $response['clase'] = '';

    switch ($idAsignatura) {
        case 1: $response['color'] = '#F87E38'; $response['clase'] = 'len'; break;
        case 3: $response['color'] = '#ed028a'; $response['clase'] = 'mat'; break;
        case 4: $response['color'] = '#00b4e0'; $response['clase'] = 'his'; break;
        case 5: $response['color'] = '#56b947'; $response['clase'] = 'cie'; break;
        case 12: $response['color'] = '#52d942'; $response['clase'] = 'bio'; break;
        case 13: $response['color'] = '#009d89'; $response['clase'] = 'fis'; break;
        case 14: $response['color'] = '#8d171b'; $response['clase'] = 'qui'; break;
        default: $response['color'] = '#0095ff'; $response['clase'] = ''; break;
    }
    return $response;
}

function getHeaderEvaluacion($idAsignatura, $tipoEvaluacion) {
    $head = $tipoEvaluacion.'_';

    if ($idAsignatura == 1) #LENGUAJE Y COMUNICACION
        $head .= 'leng';
    elseif ($idAsignatura == 3) #Matematica
        $head .= 'mat';
    elseif ($idAsignatura == 4) #Historia, Geografía y Ciencias Sociales
        $head .= 'hist';
    elseif ($idAsignatura == 5) #Ciencias Naturales
        $head .= 'cien';
    elseif ($idAsignatura == 12) #Biologia
        $head .= 'cien';
    elseif ($idAsignatura == 13) #Fisica
        $head .= 'cien';
    elseif ($idAsignatura == 14) #Quimica
        $head .= 'cien';

    return $head.'.png';
}

function getLetraByIndice($indice) {
    $abc = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","Ñ","O","P","Q","R","S","U","V","W","X","Y","Z"];

    return $abc[$indice];
}
