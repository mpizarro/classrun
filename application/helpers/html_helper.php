<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function buildTablaColegios($colegios, $siteUrl, $docentes)
{   
    $html = "<thead>
                <tr>
                    <th># ID</th>
                    <th>Colegio</th>
                    <th>Rbd</th>
                    <th>Comuna</th>
                    <th>Docentes</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody id='table-evaluaciones'>";

    if($colegios)
        foreach ($colegios as $colegio) 
        {
            $html .= "<tr>
                <td>$colegio->id</td>
                <td>$colegio->nombre</td>
                
                <td>$colegio->rbd</td>
                <td>$colegio->comuna</td>
                <td>".( (isset($docentes[$colegio->id]))?$docentes[$colegio->id]:"" )."</td>

                <td>
                    <div class='button-box'>
                        <button class='btnCursos btn btn-info' id-colegio='$colegio->rbd'>cursos</button>
                        <button class='btnVerEvaluacion btn btn-success'>Ver</button>
                        <button class='btnEditarEvaluacion btn btn-warning' id-colegio='colegio->id'>Editar</button>
                        <button type='submit' form='cargarCsv".$colegio->rbd."' style='float:right' class='btnCargaAlumnos btn btn-warning' id-curso='curso->id'>Cargar Lista</button>
                        <form id='cargarCsv".$colegio->rbd."' style='float:right' action='".$siteUrl."/Administrador/docentesCarga/$colegio->id/$colegio->rbd' method='post' enctype='multipart/form-data'>
                            <div class='form-group'>
                                <input type='file' class='form-control-file' id='exampleFormControlFile1' name='file'>
                            </div>
                        </form>
                    </div>
                </td>
            </tr>";
        }

        $html .= "</tbody>";
    
    return $html;
}

function buildTablaCursos($cursos, $siteUrl, $alumnos)
{   
    
    $html = "<thead>
                <tr>
                    <th># ID</th>
                    <th>Curso</th>
                    <th>Letra</th>
                    <th>Alumnos</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody id='table-evaluaciones'>";

    if($cursos)
        foreach ($cursos as $curso) 
        {
            $html .= "<tr>
                <td>$curso->id</td>
                <td>$curso->curso</td>
                <td>$curso->letra</td>
                <td>".( (isset($alumnos[$curso->id]))?$alumnos[$curso->id]:"" )."</td>                                
                <td>
                    <div class='button-box'>
                        <button class='btnAlumnos btn btn-info' id-curso = '$curso->id' id-colegio='$curso->colegio_id'>Alumnos</button>
                        <button class='btnEditarEvaluacion btn btn-warning' id-curso='curso->id'>Editar</button>
                        <button type='submit' form='cargarCsv' style='float:right' class='btnCargaAlumnos btn btn-warning' id-curso='curso->id'>Cargar Lista</button>
                        <form id='cargarCsv' style='float:right' action='".$siteUrl."/Administrador/alumnoscarga' method='post' enctype='multipart/form-data'>
                            <div class='form-group'>
                                <input type='file' class='form-control-file' id='exampleFormControlFile1' name='file'>
                            </div>
                        </form>                    
                    </div>
                </td>
            </tr>";
        }

        $html .= "</tbody>";
    
    return $html;
}

//<input type="submit" value="Submit">


function buildTablaAlumnos($alumnos)
{   
    $html = "<thead>
                <tr>
                    <th># ID</th>
                    <th>Rut</th>
                    <th>Nombre Completo</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody id='table-evaluaciones'>";

    if($alumnos)
        foreach ($alumnos as $alumno) 
        {
            $html .= "<tr>
                <td>$alumno->id</td>
                <td>$alumno->username</td>
                <td>$alumno->nombre $alumno->apellido</td>
                
                <td>
                    <div class='button-box'>
                        <button class='btnAlumnos btn btn-info' id-curso = '$alumno->id' id-colegio='$alumno->id'>Alumnos</button>
                        <button class='btnEditarEvaluacion btn btn-warning' id-curso='alumno->id'>Editar</button>
                    </div>
                </td>
            </tr>";
        }

        $html .= "</tbody>";
    
    return $html;
}

function buildTablaEvaluaciones($evaluaciones)
{
    $html = "";
    if($evaluaciones)
        foreach ($evaluaciones as $evaluacion) 
        {
            $html .= "<tr>
                <td>$evaluacion->id</td>
                <td>$evaluacion->codigo</td>
                <td></td>
                <td>$evaluacion->nivel</td>
                <td>$evaluacion->asignatura</td>
                <td>
                    <div class='button-box'>
                        <button class='btnPreguntas btn btn-info btn-xs' id-evaluacion='$evaluacion->id' codigo='$evaluacion->codigo'>Preguntas</button>
                        <button class='btnVerEvaluacion btn btn-success btn-xs' id-evaluacion='$evaluacion->id'>Ver</button>
                        <button class='btnEditarEvaluacion btn btn-warning btn-xs' id-evaluacion='$evaluacion->id'>Editar</button>
                    </div>
                </td>
            </tr>";
        }
    return $html;
}
function buildTablaPreguntas($preguntas, $alternativas, $prueba)
{
    $html = "";
    $num = 1;
    if($preguntas)
        foreach($preguntas as $pregunta) 
        {
            $clave = "Sin clave";
            if(array_key_exists($pregunta->id, $alternativas))
                for ($i = 0; $i < count($alternativas[$pregunta->id]); $i++)
                    if($alternativas[$pregunta->id][$i]['correcta'] == "t")
                        $clave = numberToLetter(($i+1));
                    
            $html .= "<tr title='Click aquí para ver las alternativas' class='row-hover'>
                <td>".$num."</td>
                <td>".$pregunta->codigo."</td>
                <td class='text-center'><strong>".$clave."</strong></td>
                <td><div class='button-box text-center'>
                    <button class='btnNuevaAlternativa btn btn-success btn-xs' id-evaluacion='".$prueba->id."' codigo='".$prueba->codigo."' id-pregunta='".$pregunta->id."'>Agregar alternativa</button>
                    <button class='btnVerPregunta btn btn-info btn-xs' id-pregunta='".$pregunta->id."'>Ver</button>
                    <button class='btnEditarPregunta btn btn-warning btn-xs' id-evaluacion='".$prueba->id."' codigo='".$prueba->codigo."' id-pregunta='".$pregunta->id."' nivel_id='".$prueba->nivel_id."' asignatura_id='".$prueba->asignatura_id."'>Editar</button>
                    <button class='btnEliminarPregunta btn btn-danger btn-xs' id-pregunta='".$pregunta->id."'>Eliminar</button>
                </div></td>
                <td>
                    <table class='table'> <thead>
                        <tr><th style='width:130px;'>Alternativas</th>
                            <th class='text-center'>Letra</th>
                            <th class='text-center'>Correcta</th>
                            <th style='width:210px;text-align:center;'>Acciones</th>
                        </tr><thead>";
            if(array_key_exists($pregunta->id, $alternativas)) {
                for ($i = 0; $i < count($alternativas[$pregunta->id]); $i++) {
                    $correcta = $alternativas[$pregunta->id][$i]['correcta'] == "t" 
                    ? "<span class='label label-rouded label-success'>Correcta</span>" 
                    : "<span class='label label-rouded label-danger'>Incorrecta</span>";
                    $html .= "<tr>
                        <td style=''></td>
                        <td class='text-center'><strong>".numberToLetter(($i + 1))."</strong></td>
                        <td class='text-center'>".$correcta."</td>
                        <td class='text-center'>
                            <button class='btnVerAlternativa btn btn-outline btn-info btn-xs' id-alternativa='".$alternativas[$pregunta->id][$i]['id']."'>Ver</button>
                            <button class='btnEditarAlternativa btn btn-outline btn-warning btn-xs' id-pregunta='".$pregunta->id."' id-evaluacion='".$prueba->id."' codigo='".$prueba->codigo."' id-alternativa='".$alternativas[$pregunta->id][$i]['id']."'>Editar</button>
                            <button class='btnEliminarAlternativa btn btn-outline btn-danger btn-xs' id-alternativa='".$alternativas[$pregunta->id][$i]['id']."'>Eliminar</button>
                        </td>
                    </tr>";
                }
            }
            $html .= "</table>
                </td>
            </tr>";
            $num++;
        }
    return $html;
}
function buildSelectNiveles($niveles)
{
    $html = "<option value='' hidden>Seleccione...</option>";
    if($niveles)
        foreach ($niveles as $nivel) 
        {
            $html .= "<option value='$nivel->id'>$nivel->nivel</option>";
        }
    return $html;
}

function buildSelectAsignaturas($asignaturas)
{
    $html = "<option value='' hidden>Seleccione...</option>";
    if($asignaturas)
        foreach ($asignaturas as $asignatura) 
        {
            $html .= "<option value='$asignatura->id'>$asignatura->asignatura</option>";
        }
    return $html;
}

function buildSelectTiposEvaluacion($tipos)
{
    $html = "<option value='' hidden>Seleccione...</option>";
    if($tipos)
        foreach ($tipos as $tipo) 
        {
            $html .= "<option value='$tipo->id'>$tipo->tipo</option>";
        }
    return $html;
}

function buildSelectTaxonomias($taxonomias)
{
    $html = "<option value='' hidden>Seleccione...</option>";
    if($taxonomias)
        foreach ($taxonomias as $taxonomia) 
        {
            $html .= "<option value='$taxonomia->id'>$taxonomia->taxonomia</option>";
        }
    return $html;
}
function buildSelectEjes($ejes)
{
    $html = "<option value='' hidden>Seleccione...</option>";
    if($ejes)
        foreach ($ejes as $eje) 
        {
            $html .= "<option value='$eje->id'>$eje->ejetematico</option>";
        }
    return $html;
}
function buildSelectAprendizajes($aprendizajes)
{
    $html = "<option value='' hidden>Seleccione...</option>";
    if($aprendizajes)
        foreach ($aprendizajes as $aprendizaje) 
        {
            $html .= "<option value='$aprendizaje->id'>$aprendizaje->habilidadcurricular</option>";
        }
    return $html;
}

function buildSelectOas($oas, $oaSeleccionada=FALSE)
{
    
        $html = "<option value='' hidden>Seleccione...</option>";
        $selected = "";
        if($oas)
            foreach ($oas as $oa) 
            {
                if($oaSeleccionada)
                    $selected = ($oaSeleccionada==$oa->id)?"selected":"";

                $html .= "<option value='$oa->id' $selected>$oa->codigo - $oa->oa</option>";
            }
    
    return $html;
}

function buildSelectIes($ies, $ieSeleccionada=FALSE)
{
    
        $html = "<option value='' hidden>Seleccione...</option>";
        $selected = "";
        if($ies)
            foreach ($ies as $ie) 
            {
                if($ieSeleccionada)
                    $selected = ($ieSeleccionada==$ie->id)?"selected":"";

                $html .= "<option value='$ie->id' $selected>$ie->ie</option>";
            }

    
    return $html;
}

function buildSelectUnidades($unidades, $unidadSeleccionada=FALSE)
{
    $html       = "<option value='' hidden>Seleccione...</option>";
    $selected   = "";

    if($unidades)
        foreach ($unidades as $unidad) 
        {
            if($unidadSeleccionada)
                $selected = ($unidadSeleccionada==$unidad->id)?"selected":"";

            $html .= "<option value='$unidad->id' $selected>Unidad $unidad->numero</option>";
        }
    return $html;
}

function numberToLetter($number)
{
    $letra = '';
    switch ($number) 
    {
        case 1: $letra = "A"; break;
        case 2: $letra = "B"; break;
        case 3: $letra = "C"; break;
        case 4: $letra = "D"; break;
        case 5: $letra = "E"; break;
        case 6: $letra = "F"; break;
        case 7: $letra = "G"; break;
        case 8: $letra = "H"; break;
        case 9: $letra = "I"; break;
        case 0: $letra = "J"; break;
        case 10: $letra = "K"; break;
        case 11: $letra = "L"; break;
        case 12: $letra = "M"; break;
        case 13: $letra = "N"; break;
        case 14: $letra = "Ñ"; break;
        case 15: $letra = "O"; break;
        case 16: $letra = "P"; break;
        case 17: $letra = "Q"; break;
        case 18: $letra = "S"; break;
        case 19: $letra = "T"; break;
        case 20: $letra = "U"; break;
        case 21: $letra = "V"; break;
        case 22: $letra = "W"; break;
        case 23: $letra = "X"; break;
        case 24: $letra = "Y"; break;
        case 25: $letra = "Z"; break;
    }
    return $letra;
}

?>